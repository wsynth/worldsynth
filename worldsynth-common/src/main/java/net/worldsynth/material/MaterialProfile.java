/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.material;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.InjectableValues;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

public class MaterialProfile<M extends Material<M, S>, S extends MaterialState<M, S>> {
	private static final Logger logger = LogManager.getLogger(MaterialProfile.class);
	protected final LinkedHashMap<String, M> materials = new LinkedHashMap<>();
	protected final LinkedHashMap<String, S> materialStates = new LinkedHashMap<>();
	private final String profileName;

	public MaterialProfile(String profileName) {
		this.profileName = profileName;
	}

	public final String getProfileName() {
		return profileName;
	}

	public final void parseMaterialsFromJsonNode(ObjectNode node, File materialsFile) {
		node.fields().forEachRemaining(e -> {
			addMaterial(parseMaterialFromJsonNode(e.getKey(), e.getValue(), materialsFile));
		});
	}

	protected final M parseMaterialFromJsonNode(String idName, JsonNode node, File materialsFile) {
		ObjectMapper objectMapper = new ObjectMapper();
		InjectableValues.Std injectableValues = new InjectableValues.Std();
		injectableValues.addValue("idName", idName);
		injectableValues.addValue("materialsFile", materialsFile);
		objectMapper.setInjectableValues(injectableValues);

		logger.info("Parsing material with idName: " + idName);
		return objectMapper.convertValue(node, getMaterialTypeReference());
	}

	protected TypeReference<M> getMaterialTypeReference() {
		return new TypeReference<>() {
		};
	}

	protected final void addMaterial(M material) {
		materials.put(material.getIdName(), material);
		addMaterialStatesFromMaterial(material);
	}

	protected final void addMaterialStatesFromMaterial(M material) {
		for (S state : material.getStates()) {
			materialStates.put(state.getIdName(), state);
		}
	}

	public List<M> getMaterials() {
		return new ArrayList<>(materials.values());
	}

	public M getMaterial(String materialIdName) {
		return materials.get(materialIdName);
	}

	public M getDefaultMaterial() {
		return getMaterials().get((materials.size() > 1) ? 1 : 0);
	}

	public List<S> getMaterialStates() {
		return new ArrayList<>(materialStates.values());
	}

	public S getMaterialState(String stateIdName) {
		return materialStates.get(stateIdName);
	}

	public S getMaterialState(String materialIdName, StateProperties propertiesKey) {
		if (!materials.containsKey(materialIdName)) return null;
		return materials.get(materialIdName).getState(propertiesKey);
	}

	@Override
	public String toString() {
		return profileName;
	}
}
