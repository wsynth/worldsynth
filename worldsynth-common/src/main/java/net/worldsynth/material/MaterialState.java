/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.material;

import javafx.scene.image.Image;
import javafx.scene.paint.Color;
import net.worldsynth.color.WsColor;

import java.util.Objects;

public class MaterialState<M extends Material<M, S>, S extends MaterialState<M, S>> {
	protected StateProperties properties;
	protected String idName;
	protected boolean isDefault;

	// Any of the following that are non-null overrides the corresponding value in the parent material.
	// Otherwise, the value from the parent material is returned by the getter function.
	protected String displayName;
	protected WsColor color;
	protected Texture texture;

	/**
	 * Parent material that this is a state of
	 */
	private M material;

	protected MaterialState(
			StateProperties properties,
			boolean isDefault,
			String displayName,
			WsColor color,
			Texture texture) {

		if (properties == null) throw new NullPointerException("State properties cannot be null");
		this.properties = properties;

		this.isDefault = isDefault;

		this.displayName = displayName;
		this.color = color;
		this.texture = texture;
	}

	/**
	 * Gets the parent material for this state
	 *
	 * @return the state parent material
	 */
	public M getMaterial() {
		return material;
	}

	/**
	 * Sets the parent material of this state.
	 * This is only used during the material building process.<br>
	 * <b>Do not use otherwise, you will break stuff!</b>
	 *
	 * @param material the parent material
	 */
	public void setMaterial(M material) {
		if (this.material != null) {
			throw new IllegalStateException("Material can only be set once");
		}
		if (material == null) {
			throw new IllegalArgumentException("Material cannot be null");
		}

		this.material = material;
		idName = material.getIdName();
		if (properties != null && !properties.isEmpty()) {
			idName += "[";
			idName += properties;
			idName += "]";
		}
	}

	/**
	 * Gets the unique material state ID name.
	 * This is a unique ID name for this material state, and is different from the parent material's ID name.
	 *
	 * @return the state ID name
	 */
	public String getIdName() {
		return idName;
	}

	/**
	 * Gets the state properties with value that identify the state.
	 *
	 * @return state properties with value
	 */
	public StateProperties getProperties() {
		return properties;
	}

	/**
	 * Whether this state is default state for the parent material.
	 *
	 * @return {@code true} if the state is default; {@code false} otherwise
	 */
	public boolean isDefault() {
		return isDefault;
	}

	/**
	 * Gets the display name of this state.
	 * This is the name displayed in a UI.
	 *
	 * @return the state display name
	 */
	public String getDisplayName() {
		if (displayName != null) return displayName;
		return material.getDisplayName() + ((properties != null && !properties.isEmpty()) ? ("[" + properties.toString() + "]") : "");
	}

	/**
	 * Gets the color for the state in the WourldSynth color format.
	 *
	 * @return the state color in the WorldSynth color format
	 */
	public WsColor getWsColor() {
		if (color != null) return color;
		return material.getWsColor();
	}

	/**
	 * Gets the color for the state in the JavaFX color format.
	 *
	 * @return the state color in the JavaFX color format
	 */
	public Color getFxColor() {
		return getWsColor().getFxColor();
	}

	/**
	 * Gets the texture for the state as a texture object.
	 *
	 * @return the state textrue
	 */
	public Texture getTexture() {
		if (texture != null) return texture;
		return material.getTexture();
	}

	/**
	 * Gets the texture for the state as a JavaFX image.
	 *
	 * @return the state textrue image
	 */
	public Image getTextureImage() {
		if (texture != null) {
			return texture.getTextureImage();
		}
		return material.getTextureImage();
	}

	public boolean isAir() {
		return material.isAir();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		else if (obj instanceof MaterialState<?, ?> anotherState) {
			if (!(Objects.equals(material, anotherState.material)))
				return false;
			else if (!(Objects.equals(idName, anotherState.idName))) return false;
			else if (isDefault != anotherState.isDefault) return false;
			else if (!(Objects.equals(displayName, anotherState.displayName)))
				return false;
			else if (!(Objects.equals(color, anotherState.color))) return false;
			else if (!(Objects.equals(texture, anotherState.texture)))
				return false;
			else return Objects.equals(properties, anotherState.properties);
		}
		return false;
	}

	@Override
	public String toString() {
		return getDisplayName();
	}
}
