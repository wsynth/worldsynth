/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.material;

import javafx.scene.image.Image;

import java.io.File;

public class Texture {
	protected String textureRelativePath;
	protected File textureFile;
	protected Image textureImage;

	public Texture(String parent, String textureRelativePath) {
		this.textureRelativePath = textureRelativePath;
		this.textureFile = new File(parent, textureRelativePath);

		if (textureFile.exists()) {
			textureImage = new Image(textureFile.toURI().toString(), 128, 128, false, false);
		}
	}

	public Image getTextureImage() {
		return textureImage;
	}

	public String getTextureRelativePath() {
		return textureRelativePath;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		else if (obj instanceof Texture anotherTexture) {
			return anotherTexture.textureFile.equals(textureFile);
		}
		return false;
	}
}
