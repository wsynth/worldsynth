/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.material;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;
import net.worldsynth.color.WsColor;

import java.util.*;

@JsonDeserialize(using = MaterialJsonDeserializer.class)
public class Material<M extends Material<M, S>, S extends MaterialState<M, S>> implements Comparable<Material<?, ?>> {
	/**
	 * Default no-material constant
	 */
	@SuppressWarnings("rawtypes")
	public static final Material NULL = new MaterialBuilder("", "").isAir(true).createMaterial();

	protected String idName;
	protected String displayName;
	protected WsColor color;

	// Optional texture
	protected Texture texture;

	// Optional tag list for extra filtering
	protected Set<String> tags;

	protected boolean isAir;

	protected Map<String, List<String>> properties;
	protected Map<StateProperties, S> states;
	protected S defaultState;

	protected Material(
			String idName,
			String displayName,
			WsColor color,
			Texture texture,
			Set<String> tags,
			boolean isAir,
			Map<String, List<String>> properties,
			Set<S> states) {
		this.idName = idName;
		this.displayName = displayName;
		this.color = color;
		this.texture = texture;
		this.tags = tags;
		this.isAir = isAir;
		this.properties = properties;

		if (states == null || states.isEmpty()) {
			throw new IllegalArgumentException("States cannot be null or empty");
		}
		if ((properties == null || properties.isEmpty()) && states.size() != 1) {
			throw new IllegalArgumentException("Material must have exactly one state when it has no properties");
		}

		this.states = new LinkedHashMap<>();
		for (S state : states) {
			if (properties == null ? !state.getProperties().isEmpty() : !state.getProperties().isPossibilityIn(properties)) {
				throw new IllegalArgumentException("State properties must be made up of a complete set of possible material properties");
			}
			if (this.states.containsKey(state.properties)) {
				throw new IllegalArgumentException("All states of a material must have unique state properties");
			}

			state.setMaterial((M) this);
			this.states.put(state.getProperties(), state);
			if (state.isDefault()) {
				defaultState = state;
			}
		}
		if (defaultState == null) {
			throw new IllegalArgumentException("Material must have a default state");
		}
	}

	public String getIdName() {
		return idName;
	}

	public String getDisplayName() {
		return displayName;
	}

	public WsColor getWsColor() {
		return color;
	}

	public Color getFxColor() {
		return getWsColor().getFxColor();
	}

	public Texture getTexture() {
		return texture;
	}

	public Image getTextureImage() {
		if (texture != null) {
			return texture.getTextureImage();
		}
		return null;
	}

	public Set<String> getTags() {
		return tags;
	}

	public boolean hasTag(String tag) {
		return tags.contains(tag);
	}

	public boolean isAir() {
		return isAir;
	}

	public Map<String, List<String>> getProperties() {
		return properties;
	}

	public ArrayList<S> getStates() {
		return new ArrayList<>(states.values());
	}

	public S getState(StateProperties propertiesKey) {
		return states.get(propertiesKey);
	}

	public S getDefaultState() {
		return defaultState;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		else if (obj instanceof Material<?, ?> anotherMaterial) {
			if (!(Objects.equals(idName, anotherMaterial.idName)))
				return false;
			else if (!(Objects.equals(displayName, anotherMaterial.displayName)))
				return false;
			else if (!(Objects.equals(color, anotherMaterial.color)))
				return false;
			else if (!(Objects.equals(texture, anotherMaterial.texture)))
				return false;
			else if (!(Objects.equals(tags, anotherMaterial.tags))) return false;
			else if (isAir != anotherMaterial.isAir) return false;
			else if (!(Objects.equals(properties, anotherMaterial.properties)))
				return false;
			else return Objects.equals(states, anotherMaterial.states);
		}
		return false;
	}

	@Override
	public String toString() {
		return getDisplayName();
	}

	@Override
	public int compareTo(Material<?, ?> comp) {
		return getDisplayName().compareTo(comp.getDisplayName());
	}
}
