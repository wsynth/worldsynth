/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.event;

import java.util.ArrayList;
import java.util.Comparator;

public class EventDispatcher<T extends Event> {
	private final SortedEventHandlerList eventHandlers = new SortedEventHandlerList();

	public void addEventHandler(EventType<? extends T> eventType, EventHandler<T> handler) {
		eventHandlers.add(new EventHandlerEntry<>(eventType, handler, 0));
	}

	public void addEventHandler(EventType<? extends T> eventType, EventHandler<T> handler, int priority) {
		eventHandlers.add(new EventHandlerEntry<>(eventType, handler, priority));
	}

	private final ArrayList<EventHandlerEntry<T>> removableHandlers = new ArrayList<>();
	public void removeEventHandler(EventType<? extends T> eventType, EventHandler<T> handler) {
		removableHandlers.add(new EventHandlerEntry<>(eventType, handler, 0));
		if (!isDispatching) realizeEventHandlerRemovals();
	}

	public void removeEventHandler(EventType<? extends T> eventType, EventHandler<T> handler, int priority) {
		removableHandlers.add(new EventHandlerEntry<>(eventType, handler, priority));
		if (!isDispatching) realizeEventHandlerRemovals();
	}

	private void realizeEventHandlerRemovals() {
		for (EventHandlerEntry<T> entry: removableHandlers) {
			eventHandlers.remove(entry);
		}
		removableHandlers.clear();
	}

	private boolean isDispatching = false;
	public void dispatchEvent(T event) {
		isDispatching = true;
		eventHandlers.forEach(eh -> {
			if (event.getEventType() == eh.eventType || event.getEventType().isSubtypeOf(eh.eventType)) {
				eh.handler.handle(event);
			}
		});
		isDispatching = false;
		realizeEventHandlerRemovals();
	}

	private record EventHandlerEntry<T extends Event>(EventType<? extends T> eventType, EventHandler<T> handler, int priority) implements Comparable<EventHandlerEntry<T>> {
		@Override
		public int compareTo(EventHandlerEntry o) {
			return priority - o.priority;
		}
	}

	private class SortedEventHandlerList extends ArrayList<EventHandlerEntry<T>> {

		@Override
		public boolean add(EventHandlerEntry<T> tEntry) {
			boolean result = super.add(tEntry);
			sort(Comparator.comparingInt((EventHandlerEntry<T> o) -> o.priority));
			return result;
		}
	}
}
