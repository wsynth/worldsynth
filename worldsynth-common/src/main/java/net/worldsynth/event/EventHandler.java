/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.event;

import java.util.EventListener;

public interface EventHandler<T extends Event> extends EventListener {

	void handle(T event);
}
