/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.featurepoint;

public class Featurepoint3D {
	
	private double x, y, z;
	private long seed;
	
	public Featurepoint3D(double x, double y, double z, long seed) {
		this.x = x;
		this.y = y;
		this.z = z;
		this.seed = seed;
	}
	
	public double getX() {
		return x;
	}
	
	public double getY() {
		return y;
	}
	
	public double getZ() {
		return z;
	}
	
	public long getSeed() {
		return seed;
	}
}
