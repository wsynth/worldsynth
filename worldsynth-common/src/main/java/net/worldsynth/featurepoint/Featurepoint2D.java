/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.featurepoint;

import java.io.Serializable;

public class Featurepoint2D implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private double x, z;
	private long seed;
	
	public Featurepoint2D(double x, double z, long seed) {
		this.x = x;
		this.z = z;
		this.seed = seed;
	}
	
	public double getX() {
		return x;
	}
	
	public double getZ() {
		return z;
	}
	
	public long getSeed() {
		return seed;
	}
}
