/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.biome;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import net.worldsynth.biome.addon.IBiomeAddon;
import net.worldsynth.color.WsColor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;

public class BiomeRegistry {
	private static final Logger logger = LogManager.getLogger(BiomeRegistry.class);

	private static final HashMap<String, BiomeProfile<?>> profiles = new HashMap<>();

	public BiomeRegistry(File biomesDirectory, IBiomeAddon addon) {
		logger.info("Initializing biome registry");
		logger.info("Registering default biome profile");
		registerProfile(new BiomeProfile<>("default"));

		logger.info("Registering biome profiles from addons");
		for (BiomeProfile<?> profile : addon.getAddonBiomeProfiles()) {
			registerProfile(profile);
		}

		loadBiomes(biomesDirectory);
	}

	private static void registerProfile(BiomeProfile<?> profile) {
		logger.info("Registering biome profile of type \"" + profile.getClass().getName() + "\" and name \"" + profile.getProfileName() + "\"");
		profiles.put(profile.getProfileName(), profile);
	}

	public static BiomeProfile<?> getProfile(String profileName) {
		if (!profiles.containsKey(profileName)) {
			logger.info("Attemted to acces profile with name: " + profileName + ", but it did not exist.");
			registerProfile(new BiomeProfile<>(profileName));
			logger.info("Created profile with name: " + profileName);
		}
		return profiles.get(profileName);
	}

	public static Biome getBiome(String idName) {
		for (BiomeProfile<?> profile : profiles.values()) {
			Biome m = profile.getBiome(idName);
			if (m != null) {
				return m;
			}
		}

		logger.warn("Attempted to get biome with idName: " + idName + ", but it did not exist. Creating placeholder biome.");
		return new BiomeBuilder<>(idName, idName).color(WsColor.MAGENTA).createBiome();
	}

	public static Biome getBiome(String idName, String profileName) {
		if (profiles.containsKey(profileName)) {
			BiomeProfile<?> profile = getProfile(profileName);
			Biome m = profile.getBiome(idName);
			if (m != null) {
				return m;
			}
		}

		logger.warn("Attempted to get biome with idName: " + idName + " from profile: " + profileName + ", but it did not exist.");
		return null;
	}

	public static Biome getDefaultBiome() {
		return profiles.containsKey("minecraft") ? getDefaultBiome("minecraft") : getDefaultBiome("default");
	}

	public static Biome getDefaultBiome(String profileName) {
		BiomeProfile<?> profile = profiles.get(profileName);
		if (profile != null) {
			return profile.getDefaultBiome();
		}
		return null;
	}

	public static ArrayList<Biome> getBiomesAlphabetically() {
		ArrayList<Biome> biomes = new ArrayList<>();
		for (BiomeProfile<?> profile : profiles.values()) {
			biomes.addAll(profile.getBiomes());
		}
		Collections.sort(biomes);
		return biomes;
	}

	public static ArrayList<Biome> getBiomesAlphabetically(String profileName) {
		if (!profiles.containsKey(profileName)) return null;
		ArrayList<Biome> biomes = new ArrayList<>(profiles.get(profileName).getBiomes());
		Collections.sort(biomes);
		return biomes;
	}

	private void loadBiomes(File biomesDirectory) {
		logger.info("Loading biomes from directory: " + biomesDirectory);
		if (!biomesDirectory.exists()) {
			logger.error("Directory at " + biomesDirectory + " did not exist");
			return;
		}

		for (File sub : biomesDirectory.listFiles()) {
			if (sub.isDirectory()) {
				loadBiomes(sub);
			}
			else if (sub.getName().endsWith(".json")) {
				try {
					loadBiomesFromFile(sub);
				} catch (IOException e) {
					logger.error("Problem occoured while trying to read biome file: " + sub.getName(), e);
				}
			}
		}
	}

	private void loadBiomesFromFile(File biomesFile) throws IOException {
		logger.info("Parsing biomes from file: " + biomesFile);
		ObjectMapper objectMapper = new ObjectMapper();
		JsonNode biomesFileNode = objectMapper.readTree(biomesFile);

		Iterator<Entry<String, JsonNode>> iterator = biomesFileNode.fields();
		while (iterator.hasNext()) {
			Entry<String, JsonNode> entry = iterator.next();
			String profileName = entry.getKey();
			logger.info("Parsing biomes into profile with name: " + profileName);
			if (entry.getValue().isObject()) {
				getProfile(profileName).parseBiomesFromJsonNode((ObjectNode) entry.getValue(), biomesFile);
			}
		}
	}
}
