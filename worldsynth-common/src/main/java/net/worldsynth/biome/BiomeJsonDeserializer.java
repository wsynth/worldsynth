/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.biome;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;

import java.io.File;
import java.io.IOException;

public class BiomeJsonDeserializer<B extends Biome> extends StdDeserializer<B> {

	protected BiomeJsonDeserializer() {
		this(null);
	}

	protected BiomeJsonDeserializer(Class<?> vc) {
		super(vc);
	}

	@Override
	public B deserialize(JsonParser jp, DeserializationContext ctxt) throws IOException {
		JsonNode node = jp.getCodec().readTree(jp);
		String idName = (String) ctxt.findInjectableValue("idName", null, null);
		File biomesFile = (File) ctxt.findInjectableValue("biomesFile", null, null);
		return new BiomeBuilder<B>(idName, node, biomesFile).createBiome();
	}
}
