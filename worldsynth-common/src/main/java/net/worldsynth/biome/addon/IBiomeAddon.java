/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.biome.addon;

import net.worldsynth.biome.BiomeProfile;

import java.util.List;

public interface IBiomeAddon {

	List<BiomeProfile<?>> getAddonBiomeProfiles();
}
