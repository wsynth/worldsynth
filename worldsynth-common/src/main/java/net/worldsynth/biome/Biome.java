/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.biome;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import javafx.scene.paint.Color;
import net.worldsynth.color.WsColor;

import java.util.Set;

@JsonDeserialize(using = BiomeJsonDeserializer.class)
public class Biome implements Comparable<Biome> {
	/**
	 * Default no-biome constant
	 */
	public static Biome NULL = new Biome("", "", new WsColor(0, 0, 0), null);

	protected String idName;
	protected String displayName;
	protected WsColor color;

	// Optional tag list for extra filtering
	protected Set<String> tags;

	public Biome(String idName, String displayName, WsColor color, Set<String> tags) {
		this.idName = idName;
		this.displayName = displayName;
		this.color = color;
	}

	public String getIdName() {
		return idName;
	}

	public String getDisplayName() {
		return displayName;
	}

	public WsColor getWsColor() {
		return color;
	}

	public Color getFxColor() {
		return color.getFxColor();
	}

	public Set<String> getTags() {
		return tags;
	}

	public boolean hasTag(String tag) {
		return tags.contains(tag);
	}

	@Override
	public String toString() {
		return getDisplayName();
	}

	@Override
	public int compareTo(Biome comp) {
		return getDisplayName().compareTo(comp.getDisplayName());
	}
}
