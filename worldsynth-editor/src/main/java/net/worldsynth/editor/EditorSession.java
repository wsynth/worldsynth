/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.editor;

import javafx.scene.control.Tab;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import net.worldsynth.WorldSynth;
import net.worldsynth.composer.ComposerSession;
import net.worldsynth.composer.ui.CompositionEditor;
import net.worldsynth.composition.Composition;
import net.worldsynth.composition.layer.Layer;
import net.worldsynth.editor.data.DataManager;
import net.worldsynth.editor.ui.WorldSynthEditor;
import net.worldsynth.editor.ui.explorer.SynthItem;
import net.worldsynth.event.HistoricalEvent;
import net.worldsynth.module.ModuleCompositionLayer;
import net.worldsynth.patch.ModuleConnector;
import net.worldsynth.patch.ModuleWrapper;
import net.worldsynth.patch.ModuleWrapperIO;
import net.worldsynth.patch.Patch;
import net.worldsynth.patch.event.*;
import net.worldsynth.patcher.PatcherSession;
import net.worldsynth.patcher.ui.patcheditor.PatchEditorPane;
import net.worldsynth.synth.Synth;
import net.worldsynth.synth.SynthManager;
import net.worldsynth.synth.event.SynthEvent;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.util.*;

public class EditorSession implements PatcherSession, ComposerSession {
	public static final Logger logger = LogManager.getLogger(EditorSession.class);
	
	private final Synth synth;
	private final WorldSynthEditor editor;
	private final SynthItem synthTreeItem;
	private final EditorHistory history = new EditorHistory(20);
	
	private final HashMap<Patch, Tab> patchTabs = new HashMap<>();
	private final HashMap<Patch, PatchEditorPane> patchEditors = new HashMap<>();
	private final HashMap<Composition, Tab> compositionTabs = new HashMap<>();
	private final HashMap<Composition, CompositionEditor> compositionEditors = new HashMap<>();

	private boolean unsavedChanges = false;
	
	public EditorSession(Synth synth, WorldSynthEditor editor) {
		this.synth = synth;
		this.editor = editor;

		synth.addEventHandler(SynthEvent.SYNTH_PATCH, e -> {
			PatchEvent patchEvent = e.getPatchEvent();
			if (patchEvent instanceof PatchModuleModificationEvent moduleModificationEvent) {
				WrapperEvent wrapperEvent = moduleModificationEvent.getWrapperEvent();
				if (wrapperEvent.getEventType() == WrapperEvent.WRAPPER_MODULE_MODIFIED || wrapperEvent.getEventType() == WrapperEvent.WRAPPER_BYPASS_CHANGED) {
					invalidateCache(moduleModificationEvent.getWrapper());
				}
			}
			else if (patchEvent instanceof PatchConnectorAdditionEvent connectorAdditionEvent) {
				invalidateCache((connectorAdditionEvent.getConnector().module2));
			}
			else if (patchEvent instanceof PatchConnectorRemovalEvent connectorRemovalEvent) {
				invalidateCache((connectorRemovalEvent.getConnector().module2));
			}

			addHistoryEvent(e);
			registerUnsavedChangePerformed();
		});

		synth.addEventHandler(SynthEvent.SYNTH_COMPOSITION, e -> {
			addHistoryEvent(e);
			registerUnsavedChangePerformed();
		});

		synth.addEventHandler(SynthEvent.SYNTH_EXTENT, e -> {
			addHistoryEvent(e);
			registerUnsavedChangePerformed();
		});

		synthTreeItem = new SynthItem(synth, this);

		openPatch(synth.getPatch());
	}
	
	public Synth getSynth() {
		return synth;
	}

	public SynthItem getSynthTreeItem() {
		return synthTreeItem;
	}

	public void openPatch(Patch patch) {
		if (patchTabs.containsKey(patch)) {
			editor.editorTabPane.getSelectionModel().select(patchTabs.get(patch));
			return;
		}

		Tab patchEditorTab = new Tab(synth.getName() + " " + patch.getName());
		if (hasUnsavedChanges()) {
			patchEditorTab.setText("*" + synth.getName() + " " + patch.getName());
		}
		
		PatchEditorPane patchEditor = patchEditors.get(patch);
		if (patchEditor == null) {
			patchEditor = new PatchEditorPane(patch, this) {
				/**
				 * Extended implementation for opening a module editor tha handles composition modules differently from
				 * normal modules.<br>
				 * Composition modules with assigned layers open the layer in composer, otherwise fallback to open the
				 * normal parameter editor.
				 */
				@Override
				public void openModuleEditor(ModuleWrapper wrapper) {
					if (wrapper.module instanceof ModuleCompositionLayer<?> compositionModule) {
						if (compositionModule.getLayer() != null) {
							openComposition(compositionModule.getComposition(), compositionModule.getLayer());
							return;
						}
					}
					super.openModuleEditor(wrapper);
				}
			};
		}
		patchEditorTab.setContent(patchEditor);
		
		patchEditorTab.setOnClosed(e -> {
			patchTabs.remove(patch);
			
			if (patch == synth.getPatch()) {
				editor.closeSynth(synth);
			}
		});
		
		patchTabs.put(patch, patchEditorTab);
		patchEditors.put(patch, patchEditor);
		
		editor.editorTabPane.getTabs().add(patchEditorTab);
		editor.editorTabPane.getSelectionModel().select(patchEditorTab);
	}
	
	public void repaintPatch(Patch patch) {
		if (patchEditors.containsKey(patch)) {
			patchEditors.get(patch).repaint();
		}
	}
	
	public void openComposition(Composition composition, Layer layer) {
		// Select existing tab if it exists
		if (compositionTabs.containsKey(composition)) {
			editor.editorTabPane.getSelectionModel().select(compositionTabs.get(composition));
			compositionEditors.get(composition).selectLayer(layer);
			return;
		}

		Tab compositionEditorTab = new Tab(synth.getName() + " " + composition.getName());
		if (hasUnsavedChanges()) {
			compositionEditorTab.setText("*" + synth.getName() + " " + composition.getName());
		}

		CompositionEditor compositionEditor = compositionEditors.get(composition);
		if (compositionEditor == null) {
			compositionEditor = new CompositionEditor(composition, this);
			compositionEditor.selectLayer(layer);
		}
		compositionEditorTab.setContent(compositionEditor);

		compositionEditorTab.setOnClosed(e -> {
			compositionTabs.remove(composition);
		});

		compositionTabs.put(composition, compositionEditorTab);
		compositionEditors.put(composition, compositionEditor);

		editor.editorTabPane.getTabs().add(compositionEditorTab);
		editor.editorTabPane.getSelectionModel().select(compositionEditorTab);
	}

	public void save() {
		File file = synth.getFile();
		if (file == null) {
			FileChooser fileChooser = new FileChooser();
			fileChooser.setTitle("Open WorldSynth project");
			fileChooser.getExtensionFilters().add(new ExtensionFilter("WorldSynth", "*.wsynth"));

			file = fileChooser.showSaveDialog(WorldSynthEditorApp.primaryStage);
			if (!file.getName().endsWith(".wsynth")) {
				file = new File(file.getAbsolutePath() + ".wsynth");
			}
		}
		if (file == null) {
			return;
		}
		
		String synthName = file.getName();
		synthName = synthName.substring(0, synthName.lastIndexOf(".wsynth"));
		renameSynthInEditor(synthName);
		synth.setFile(file);
		
		SynthManager.saveSynth(synth);
		DataManager.getWorkspaceData().setMostRecentSynth(file);
		
		unsavedChanges = false;
		for (Patch p: patchTabs.keySet()) {
			patchTabs.get(p).setText(synth.getName() + " " + p.getName());
		}

		for (Composition c: compositionTabs.keySet()) {
			compositionTabs.get(c).setText(synth.getName() + " " + c.getName());
		}
	}
	
	public void saveAs() {
		FileChooser fileChooser = new FileChooser();
		fileChooser.setTitle("Open WorldSynth project");
		fileChooser.getExtensionFilters().add(new ExtensionFilter("WorldSynth", "*.wsynth"));

		File file = fileChooser.showSaveDialog(WorldSynthEditorApp.primaryStage);
		if (file == null) {
			return;
		}
		else if (!file.getName().endsWith(".wsynth")) {
			file = new File(file.getAbsolutePath() + ".wsynth");
		}

		synth.setFile(file);
		
		save();
	}
	
	private void renameSynthInEditor(String name) {
		synth.setName(name);
		Tab mainPatchTab = patchTabs.get(synth.getPatch());
		mainPatchTab.setText(name);
	}
	
	public boolean hasUnsavedChanges() {
		return unsavedChanges;
	}
	
	public void registerUnsavedChangePerformed() {
		editor.makePreviewRefreshable();

		if (unsavedChanges) {
			// Skip re-marking all editor tabs if there already are unsaved changes.
			return;
		}

		unsavedChanges = true;
		for (Patch p: patchTabs.keySet()) {
			patchTabs.get(p).setText("*" + synth.getName() + " " + p.getName());
		}

		for (Composition c: compositionTabs.keySet()) {
			compositionTabs.get(c).setText("*" + synth.getName() + " " + c.getName());
		}
	}
	
	public void closeAllPatches() {
		Iterator<Patch> it = patchTabs.keySet().iterator();
		while (it.hasNext()) {
			Patch p = it.next();
			editor.editorTabPane.getTabs().remove(patchTabs.get(p));
			patchEditors.remove(p);
			it.remove();
		}
	}

	public void closeAllCompositions() {
		Iterator<Composition> it = compositionTabs.keySet().iterator();
		while (it.hasNext()) {
			Composition c = it.next();
			editor.editorTabPane.getTabs().remove(compositionTabs.get(c));
			compositionEditors.remove(c);
			it.remove();
		}
	}

	public EditorHistory getHistory() {
		return history;
	}
	
	public void addHistoryEvent(HistoricalEvent event) {
		history.addHistoryEvent(event);
	}

	public void commitHistory() {
		history.commit();
	}
	
	public void undo() {
		ArrayList<HistoricalEvent> historyEvents = history.getLastHistoryEvents();
		if (historyEvents != null) {
			// Undo events in reverse order
			for (int i = historyEvents.size()-1; i >= 0; i--) {
				historyEvents.get(i).undo();
			}
			// Discard events caused by the undo operation
			history.clear();
		}
	}
	
	public void redo() {
		ArrayList<HistoricalEvent> historyEvents = history.getNextHistoryEvents();
		if (historyEvents != null) {
			for (HistoricalEvent e: historyEvents) {
				e.redo();
			}
			// Discard events caused by the redo operation
			history.clear();
		}
	}

	@Override
	public void updatePreview() {
		WorldSynthEditor.updatePreview();
	}

	public void updatePreview(boolean clearExtentsEditorPreview) {
		WorldSynthEditor.updatePreview(clearExtentsEditorPreview);
	}

	@Override
	public void updatePreview(ModuleWrapper moduleWrapper) {
		WorldSynthEditor.updatePreview(moduleWrapper, WorldSynth.getBuildCache());
	}

	public Set<ModuleWrapper> invalidateCache(ModuleWrapper wrapper) {
		ArrayList<ModuleWrapper> wrappersList = new ArrayList<>();
		wrappersList.add(wrapper);
		return invalidateCache(wrappersList);
	}

	public Set<ModuleWrapper> invalidateCache(List<ModuleWrapper> wrappers) {
		Set<ModuleWrapper> invalidatedSet = traverseForward(wrappers);
		for (ModuleWrapper wrapper: invalidatedSet) {
			WorldSynth.getBuildCache().clearCachedData(wrapper);
		}
		return invalidatedSet;
	}

	private Set<ModuleWrapper> traverseForward(List<ModuleWrapper> wrappers) {
		Set<ModuleWrapper> traversed = Collections.newSetFromMap(new IdentityHashMap<>());
		for (ModuleWrapper wrapper: wrappers) {
			traverseForward(wrapper, traversed);
		}
		return traversed;
	}

	private void traverseForward(ModuleWrapper wrapper, Set<ModuleWrapper> traversed) {
		// Add the module to the traversed set
		traversed.add(wrapper);

		// Traverse patch forward from the wrapper to dependents
		for (ModuleWrapperIO output : wrapper.wrapperOutputs.values()) {
			List<ModuleConnector> connectors = wrapper.getParentPatch().getModuleConnectorsByWrapperIo(output);
			for (ModuleConnector con : connectors) {
				ModuleWrapper nextWrapper = con.module2;
				if (!traversed.contains(nextWrapper)) {
					traverseForward(nextWrapper, traversed);
				}
			}
		}
	}
}
