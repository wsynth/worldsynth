/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.editor.data;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.util.DefaultIndenter;
import com.fasterxml.jackson.core.util.DefaultPrettyPrinter;
import com.fasterxml.jackson.core.util.DefaultPrettyPrinter.Indenter;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import net.worldsynth.module.AbstractModule;

public class WorkspaceData {
	private static Logger logger = LogManager.getLogger(WorkspaceData.class);
	
	private File dataFile;
	
	public List<File> recentSynths = new LinkedList<File>();
	public Map<String, Integer> modulesUsage = new HashMap<String, Integer>();
	
	public static WorkspaceData construct(File workspaceDataFile) throws JsonParseException, JsonMappingException, IOException {
		ObjectMapper objectMapper = new ObjectMapper();
		
		WorkspaceData workspaceData;
		if (workspaceDataFile.exists()) {
			try {
				workspaceData = objectMapper.readValue(workspaceDataFile, WorkspaceData.class);
				logger.info("Loaded workspace data from file");
			} catch (Exception e) {
				logger.error("Could not load workspace data from file", e);
				workspaceData = new WorkspaceData();
			}
		}
		else {
			logger.info("Found no existing workspace data to load from file");
			workspaceData = new WorkspaceData();
		}
		
		workspaceData.setDataFile(workspaceDataFile);
		return workspaceData;
	}
	
	private void setDataFile(File dataFile) {
		this.dataFile = dataFile;
	}
	
	public void save() {
		DefaultPrettyPrinter printer = new DefaultPrettyPrinter();
		Indenter indenter = new DefaultIndenter("    ", DefaultIndenter.SYS_LF);
		printer.indentObjectsWith(indenter);
		printer.indentArraysWith(indenter);
		
		ObjectMapper objectMapper = new ObjectMapper();
		try {
			objectMapper.writer(printer).writeValue(dataFile, this);
		} catch (IOException e) {
			logger.error("Exception when writing workspace data to file", e);
		}
	}
	
	public List<File> getRecentSynths() {
		return recentSynths;
	}
	
	public void setMostRecentSynth(File newRecentlySynth) {
		if (recentSynths.contains(newRecentlySynth)) {
			recentSynths.remove(newRecentlySynth);
		}
		recentSynths.add(0, newRecentlySynth);
		save();
	}
	
	public void incrementModuleUsage(Class<? extends AbstractModule> module) {
		String key = module.getName();
		
		Object value = modulesUsage.get(key);
		int count = 0;
		
		if (value != null) {
			count = (Integer) value;
		}
		
		count++;
		modulesUsage.put(key, count);
		
		save();
	}
	
	public int getModuleUsage(String moduleClass) {
		Object value = modulesUsage.get(moduleClass);
		int count = 0;
		
		if (value != null) {
			count = (Integer) value;
		}
		
		return count;
	}
}
