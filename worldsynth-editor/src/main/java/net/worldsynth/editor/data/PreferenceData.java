/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.editor.data;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.fasterxml.jackson.core.util.DefaultIndenter;
import com.fasterxml.jackson.core.util.DefaultPrettyPrinter;
import com.fasterxml.jackson.core.util.DefaultPrettyPrinter.Indenter;
import com.fasterxml.jackson.databind.ObjectMapper;

public class PreferenceData {
	private static Logger logger = LogManager.getLogger(PreferenceData.class);
	
	private File prefFile;
	
	public Map<String, Object> preferenceMap = new HashMap<>();
	
	public static PreferenceData construct(File preferenceFile) throws IOException {
		ObjectMapper objectMapper = new ObjectMapper();

		PreferenceData preferenceData;
		if (preferenceFile.exists()) {
			try {
				preferenceData = objectMapper.readValue(preferenceFile, PreferenceData.class);
				logger.info("Loaded preferences from file");
			} catch (Exception e) {
				logger.error("Could not load preferences from file", e);
				preferenceData = new PreferenceData();
			}
		}
		else {
			logger.info("Found no existing preferences to load from file");
			preferenceData = new PreferenceData();
		}

		preferenceData.setDataFile(preferenceFile);
		return preferenceData;
	}
	
	private void setDataFile(File dataFile) {
		this.prefFile = dataFile;
	}
	
	public void save() {
		DefaultPrettyPrinter printer = new DefaultPrettyPrinter();
		Indenter indenter = new DefaultIndenter("    ", DefaultIndenter.SYS_LF);
		printer.indentObjectsWith(indenter);
		printer.indentArraysWith(indenter);
		
		ObjectMapper objectMapper = new ObjectMapper();
		try {
			objectMapper.writer(printer).writeValue(prefFile, this);
		} catch (IOException e) {
			logger.error("Exception when writing preferences to file", e);
		}
	}
	
	public Map<String, Object> getPreferenceMap() {
		return preferenceMap;
	}
}
