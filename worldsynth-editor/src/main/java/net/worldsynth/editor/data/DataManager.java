/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.editor.data;

import net.worldsynth.editor.WorldSynthEditorDirectoryConfig;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.IOException;

public class DataManager {
	private static Logger logger = LogManager.getLogger(DataManager.class);
	
	private static DataManager instance;
	
	private static File dataDirectory;
	
	private static WorkspaceData workspaceData;
	private static PreferenceData preferenceData;
	
	public DataManager(WorldSynthEditorDirectoryConfig directoryConfig) {
		if (instance != null) {
			throw new IllegalStateException("Can only construct the patcher data manager once");
		}
		instance = this;
		
		dataDirectory = directoryConfig.getDataDirectory();
		
		try {
			workspaceData = WorkspaceData.construct(new File(dataDirectory, "workspacedata.json"));
		} catch (IOException e) {
			logger.error(e);
		}
		
		try {
			preferenceData = PreferenceData.construct(new File(dataDirectory, "preferencedata.json"));
		} catch (IOException e) {
			logger.error(e);
		}
	}
	
	public static WorkspaceData getWorkspaceData() {
		return workspaceData;
	}
	
	public static PreferenceData getPreferenceData() {
		return preferenceData;
	}
}
