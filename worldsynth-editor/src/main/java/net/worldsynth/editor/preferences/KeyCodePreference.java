/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.editor.preferences;

import javafx.beans.value.ObservableValue;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination;
import net.worldsynth.fx.control.KeyCombinationEditor;
import org.controlsfx.control.PropertySheet;
import org.controlsfx.property.editor.AbstractPropertyEditor;
import org.controlsfx.property.editor.PropertyEditor;

import java.util.HashMap;
import java.util.Optional;

public class KeyCodePreference extends Preference<KeyCodeCombination> {

	public KeyCodePreference(KeyCodeCombination defaultValue, String pathName, String displayName, String category, String description) {
		super(defaultValue, KeyCodeCombination.class, pathName, displayName, category, description);
	}

	@Override
	protected KeyCodeCombination castDeserializedValue(Object o) {
		HashMap oMap = (HashMap) o;

		return new KeyCodeCombination(
				KeyCode.valueOf((String) oMap.get("code")),
				KeyCombination.ModifierValue.valueOf((String) oMap.get("shift")),
				KeyCombination.ModifierValue.valueOf((String) oMap.get("control")),
				KeyCombination.ModifierValue.valueOf((String) oMap.get("alt")),
				KeyCombination.ModifierValue.valueOf((String) oMap.get("meta")),
				KeyCombination.ModifierValue.valueOf((String) oMap.get("shortcut")));
	}

	@Override
	public Optional<Class<? extends PropertyEditor<?>>> getPropertyEditorClass() {
		return Optional.of(KeyCodePropertyEditor.class);
	}

	public static class KeyCodePropertyEditor extends AbstractPropertyEditor<KeyCodeCombination, KeyCombinationEditor> {

		public KeyCodePropertyEditor(PropertySheet.Item property) {
			super(property, new KeyCombinationEditor());
		}

		@Override protected ObservableValue<KeyCodeCombination> getObservableValue() {
			return getEditor().keyCombinationProperty();
		}

		@Override public void setValue(KeyCodeCombination value) {
			getEditor().setKeyCombination(value);
		}
	}
}
