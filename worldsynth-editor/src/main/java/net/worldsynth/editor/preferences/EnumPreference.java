/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.editor.preferences;

public class EnumPreference<T extends Enum<T>> extends Preference<T> {
	
	public EnumPreference(T defaultValue, Class<T> valueClass, String pathName, String displayName, String category, String description) {
		super(defaultValue, valueClass, pathName, displayName, category, description);
	}
	
	@Override
	protected T castDeserializedValue(Object o) {
		for (T enumConstant: getType().getEnumConstants()) {
			if (enumConstant.name().equals(o)) {
				return enumConstant;
			}
		}
		throw new ClassCastException("Could not cast value: \"" + o + "\" to enum of type " + getType());
	}
}
