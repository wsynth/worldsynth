/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.editor.preferences;

import com.sun.javafx.collections.ObservableListWrapper;
import javafx.beans.value.ObservableValue;
import javafx.scene.Scene;
import javafx.scene.control.SplitPane;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import net.worldsynth.fx.control.WsColorPicker;
import net.worldsynth.editor.WorldSynthEditorApp;
import net.worldsynth.standalone.ui.stage.UtilityStage;
import org.controlsfx.control.PropertySheet;
import org.controlsfx.control.PropertySheet.Item;
import org.controlsfx.property.editor.AbstractPropertyEditor;
import org.controlsfx.property.editor.DefaultPropertyEditorFactory;
import org.controlsfx.property.editor.PropertyEditor;

import java.util.ArrayList;
import java.util.List;

public class PreferenceEditor extends UtilityStage {
	
	private final SplitPane splitPane;

	private static ArrayList<Preferences> prefs = new ArrayList<>();
	public static void addPrefs(Preferences prefs) {
		PreferenceEditor.prefs.add(prefs);
	}
	
	public PreferenceEditor() {
		super();
		
		TreeItem<Preferences> rootItem = new TreeItem<>();
		addChildPrefs(rootItem, prefs);
		rootItem.setExpanded(true);
		
		TreeView<Preferences> treeView = new TreeView<>(rootItem);
		treeView.setShowRoot(false);
		
		splitPane = new SplitPane(treeView, new PropertySheet());
		splitPane.setPrefSize(800, 600);
		splitPane.setDividerPositions(0.3);
		
		treeView.getSelectionModel().selectedItemProperty().addListener((ob, ov, nv) -> {
			splitPane.getItems().set(1, getPropSheet(nv.getValue()));
		});
		
		treeView.getSelectionModel().select(0);
		
		Scene scene = new Scene(splitPane);
		scene.getStylesheets().add(WorldSynthEditorApp.stylesheet);
		setScene(scene);
		
		setTitle("Preferences");
		setResizable(true);
	}
	
	private PropertySheet getPropSheet(Preferences pref) {
		List<Item> items = new ArrayList<>(pref.getPreferences());
		PropertySheet propertySheet = new PropertySheet(new ObservableListWrapper<>(items));

		propertySheet.setPropertyEditorFactory(new CustomPropertyEditorFactory());

		return propertySheet;
	}
	
	private void addChildPrefs(TreeItem<Preferences> parent, List<Preferences> prefs) {
		for (Preferences pref: prefs) {
			TreeItem<Preferences> prefItem = new TreeItem<>(pref);
			if (pref.getSubPreferences() != null) {
				addChildPrefs(prefItem, pref.getSubPreferences());
			}
			parent.getChildren().add(prefItem);
			prefItem.setExpanded(true);
		}
	}

	private class CustomPropertyEditorFactory extends DefaultPropertyEditorFactory {

		@Override
		public PropertyEditor<?> call(Item item) {
			Class<?> type = item.getType();

			// Use the custom WorldSynth color picker instead of the default JFX color picker
			if (type == Color.class || type == Paint.class) {
				return createColorEditor(item);
			}

			return super.call(item);
		}

		private static PropertyEditor<?> createColorEditor(Item property ) {
			return new AbstractPropertyEditor<Color, WsColorPicker>(property, new WsColorPicker()) {

				@Override protected ObservableValue<Color> getObservableValue() {
					return getEditor().valueProperty();
				}

				@Override public void setValue(Color value) {
					getEditor().setValue(value);
				}
			};
		}
	}
}
