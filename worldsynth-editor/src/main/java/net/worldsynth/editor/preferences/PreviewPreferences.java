/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.editor.preferences;

import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination;
import javafx.scene.paint.Color;

public class PreviewPreferences extends Preferences {

	public static Preference<Integer> resolution = new Preference<>(256, Integer.class,
			"net.worldsynth.editor.preview.resolution",
			"Resolution", "Build",
			"The max resolution in the horizontal directions to build the preview at");

	public static Preference<Color> backgroundColor = new ColorPreference(Color.gray(0.2),
			"net.worldsynth.editor.preview.backgroundColor",
			"Background color", "Preview",
			"The background color for the preview window");

	public static Preference<KeyCodeCombination> previewKeybindLock = new KeyCodePreference(
			new KeyCodeCombination(KeyCode.F, KeyCombination.SHORTCUT_DOWN),
			"net.worldsynth.editor.preview.lock",
			"Lock preview", "Key bindings",
			"Key bind for locking preview");
	
	public PreviewPreferences() {
		super("Preview");
		
		registerPreference(resolution);
		registerPreference(backgroundColor);
		registerPreference(previewKeybindLock);
	}
}
