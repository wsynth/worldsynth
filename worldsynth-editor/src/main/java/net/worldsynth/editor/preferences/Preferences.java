/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.editor.preferences;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import net.worldsynth.editor.data.DataManager;

public abstract class Preferences {

	private final String name;
	private final List<Preferences> subPreferences;

	private final List<Preference> preferences = new ArrayList<>();

	public Preferences(String name) {
		this.name = name;
		this.subPreferences = null;
	}

	public Preferences(String name, Preferences... subPreferences) {
		this.name = name;
		this.subPreferences = Arrays.asList(subPreferences);
	}

	public final String getName() {
		return name;
	}

	public final List<Preference> getPreferences() {
		return preferences;
	}

	public final List<Preferences> getSubPreferences() {
		return subPreferences;
	}

	protected final void registerPreference(Preference<?> pref) {
		preferences.add(pref);
		pref.setPrefData(DataManager.getPreferenceData());
	}

	@Override
	public String toString() {
		return getName();
	}
}
