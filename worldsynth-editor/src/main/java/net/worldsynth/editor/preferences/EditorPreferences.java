/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.editor.preferences;

import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination;
import net.worldsynth.patcher.preferences.PatcherPreferences;

public class EditorPreferences extends Preferences {

	public static Preference<Boolean> showWelcomeSplashOnStartup = new Preference<>(
			true, Boolean.class,
			"net.worldsynth.patcher.showWelcomeSplashOnStartup",
			"Show welcome splash on startup", "Splash",
			null);

	public static Preference<KeyCodeCombination> editorKeybindSynthParameters = new KeyCodePreference(
			new KeyCodeCombination(KeyCode.P, KeyCombination.SHORTCUT_DOWN),
			"net.worldsynth.patcher.patcheditor.keybind.openSynthParameters",
			"Open synth parameters", "Key bindings",
			"Key bind for opening the synth parameters panel");

	public static Preference<KeyCodeCombination> editorKeybindPreferences = new KeyCodePreference(
			new KeyCodeCombination(KeyCode.P, KeyCombination.SHORTCUT_DOWN, KeyCombination.ALT_DOWN, KeyCodeCombination.SHIFT_DOWN),
			"net.worldsynth.patcher.patcheditor.keybind.openEditorPreferences",
			"Open editor preferences", "Key bindings",
			"Key bind for opening the preference editor");

	public static Preference<KeyCodeCombination> editorKeybindNew = new KeyCodePreference(
			new KeyCodeCombination(KeyCode.N, KeyCombination.SHORTCUT_DOWN),
			"net.worldsynth.editor.keybind.new",
			"New project", "Key bindings",
			"Key bind for creating new project");

	public static Preference<KeyCodeCombination> editorKeybindSave = new KeyCodePreference(
			new KeyCodeCombination(KeyCode.S, KeyCombination.SHORTCUT_DOWN),
			"net.worldsynth.editor.keybind.save",
			"Save project", "Key bindings",
			"Key bind for saving project");

	public static Preference<KeyCodeCombination> editorKeybindSaveAs = new KeyCodePreference(
			new KeyCodeCombination(KeyCode.S, KeyCombination.SHORTCUT_DOWN, KeyCodeCombination.SHIFT_DOWN),
			"net.worldsynth.editor.keybind.saveAs",
			"Save project as", "Key bindings",
			"Key bind for saving project as");

	public static Preference<KeyCodeCombination> editorKeybindOpen = new KeyCodePreference(
			new KeyCodeCombination(KeyCode.O, KeyCombination.SHORTCUT_DOWN),
			"net.worldsynth.editor.keybind.open",
			"Open project", "Key bindings",
			"Key bind for opening project");

	public static Preference<KeyCodeCombination> editorKeybindUndo = new KeyCodePreference(
			new KeyCodeCombination(KeyCode.Z, KeyCombination.SHORTCUT_DOWN),
			"net.worldsynth.editor.keybind.undo",
			"Undo", "Key bindings",
			"Key bind for undo operation");

	public static Preference<KeyCodeCombination> editorKeybindRedo = new KeyCodePreference(
			new KeyCodeCombination(KeyCode.Y, KeyCombination.SHORTCUT_DOWN),
			"net.worldsynth.editor.keybind.redo",
			"Redo", "Key bindings",
			"Key bind for redo operation");

	public EditorPreferences() {
		super("Editor", new PatcherPreferences(), new PreviewPreferences());
		
		registerPreference(showWelcomeSplashOnStartup);
		registerPreference(editorKeybindNew);
		registerPreference(editorKeybindSave);
		registerPreference(editorKeybindSaveAs);
		registerPreference(editorKeybindOpen);
		registerPreference(editorKeybindUndo);
		registerPreference(editorKeybindRedo);
		registerPreference(editorKeybindSynthParameters);
		registerPreference(editorKeybindPreferences);

		// Self-add as a root entry in the preference editor
		PreferenceEditor.addPrefs(this);
	}
}
