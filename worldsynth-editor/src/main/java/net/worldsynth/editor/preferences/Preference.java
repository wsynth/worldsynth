/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.editor.preferences;

import java.util.Optional;

import javafx.beans.property.SimpleObjectProperty;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.controlsfx.control.PropertySheet.Item;

import javafx.beans.value.ObservableValue;
import net.worldsynth.editor.data.PreferenceData;

public class Preference<T> extends SimpleObjectProperty<T> implements Item {

	public static final Logger LOGGER = LogManager.getLogger(Preference.class);

	private final String pathName;
	private final String displayName;
	private final String category;
	private final String description;

	private Class<T> valueClass;

	private PreferenceData prefData;

	public Preference(T defaultValue, Class<T> valueClass, String pathName, String displayName, String category, String description) {
		super(defaultValue);

		this.valueClass = valueClass;

		this.pathName = pathName;
		this.displayName = displayName;
		this.category = category;
		this.description = description;

		addListener((observable, oldValue, newValue) -> {
			if (prefData != null) {
				prefData.getPreferenceMap().put(pathName, get());
				prefData.save();
			}
		});
	}

	public final void setPrefData(PreferenceData prefData) {
		this.prefData = prefData;

		if (prefData.getPreferenceMap().containsKey(pathName)) {
			try {
				set(castDeserializedValue(prefData.getPreferenceMap().get(pathName)));
				LOGGER.info("Parsed preference: " + pathName);
			} catch (ClassCastException e) {
				LOGGER.error("Could not parse preference: " + pathName, e);
			}
		}
		else {
			prefData.getPreferenceMap().put(pathName, get());
		}
	}

	@SuppressWarnings("unchecked")
	protected T castDeserializedValue(Object o) throws ClassCastException {
		if (getType().isAssignableFrom(o.getClass())) {
			return (T) o;
		}
		throw new ClassCastException("Could not cast value: \"" + o + "\" to type " + getType());
	}

	public String getPathName() {
		return pathName;
	}

	@Override
	public Class<T> getType() {
		return valueClass;
	}

	@Override
	public String getCategory() {
		return category;
	}

	@Override
	public String getName() {
		return displayName;
	}

	@Override
	public String getDescription() {
		return description;
	}

	@SuppressWarnings("unchecked")
	@Override
	public void setValue(Object value) {
		super.setValue((T) value);
	}

	@Override
	public Optional<ObservableValue<? extends Object>> getObservableValue() {
		return Optional.empty();
	}
}
