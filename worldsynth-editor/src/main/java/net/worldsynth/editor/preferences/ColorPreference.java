/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.editor.preferences;

import javafx.scene.paint.Color;

import java.util.HashMap;

public class ColorPreference extends Preference<Color> {

	public ColorPreference(Color defaultValue, String pathName, String displayName, String category, String description) {
		super(defaultValue, Color.class, pathName, displayName, category, description);
	}
	
	@Override
	protected Color castDeserializedValue(Object o) {
		HashMap oMap = (HashMap) o;

		return new Color(
				(double) oMap.get("red"),
				(double) oMap.get("green"),
				(double) oMap.get("blue"),
				(double) oMap.get("opacity"));
	}
}
