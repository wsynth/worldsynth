/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.editor;

import javafx.application.Application;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import net.worldsynth.WorldSynth;
import net.worldsynth.addon.IAddon;
import net.worldsynth.composer.WorldSynthComposer;
import net.worldsynth.editor.addon.EditorAddonLoader;
import net.worldsynth.extent.WorldExtentManager;
import net.worldsynth.glpreview.util.GLUtil;
import net.worldsynth.editor.data.DataManager;
import net.worldsynth.editor.preferences.EditorPreferences;
import net.worldsynth.editor.resources.Resources;
import net.worldsynth.editor.ui.WorldSynthEditor;
import net.worldsynth.patcher.ui.patcheditor.PatchEditorPane;
import net.worldsynth.editor.ui.welcomesplash.WelcomeSplash;
import net.worldsynth.standalone.ui.stage.StageManager;
import net.worldsynth.util.ExecutionInfo;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.swing.*;
import java.awt.Window.Type;
import java.io.File;

public class WorldSynthEditorApp {
	public static final Logger LOGGER = LogManager.getLogger(WorldSynthEditorApp.class);

	public static String stylesheet;
	public static Stage primaryStage;
	
	private static IAddon[] injectedAddons;
	
	public static void main(String[] args) {
		parseArguments(args);
		injectedAddons = null;
		Application.launch(App.class, args);
	}
	
	/**
	 * Start WorldSynth Editor with module registers passed as arguments in addition to addons loaded from addon jars.
	 * This is practical for loading modules in addons under development.
	 * 
	 * @param args as in main args
	 * @param injectedAddons instances of module registers to be loaded
	 */
	public static void startEditor(String[] args, IAddon... injectedAddons) {
		parseArguments(args);
		WorldSynthEditorApp.injectedAddons = injectedAddons;
		Application.launch(App.class, args);
	}

	//Arguments
	private static boolean openSplash = !ExecutionInfo.systemIsMac();
	private static File biomesDirectory;
	private static File materialsDirectory;
	private static File addonsDirectory;
	private static File dataDirectory;
	private static File examplesDirectory;
	private static File templatesDirectory;
	private static File brushesDirectory;

	private static void parseArguments(String[] args) {
		for (int i = 0; i < args.length; i++) {
			String arg = args[i];
			if (arg.equals("--help") || arg.equals("-h")) {
				System.out.println("   --help             Print a list of available arguments");
				System.out.println("   -h                 Print a list of available arguments");
				System.out.println("   --nosplash         Start without showing splash");
				System.out.println("   biomes <path>      Path to biomes directory");
				System.out.println("   materials <path>   Path to materials directory");
				System.out.println("   addons <path>      Path to addon directory");
				System.out.println("   data <path>        Path to data directory");
				System.out.println("   examples <path>    Path to examples directory");
				System.out.println("   templates <path>   Path to templates directory");
				System.out.println("   brushes <path>     Path to brushes directory");
				System.exit(0);
			}
			else if (arg.equals("--nosplash")) {
				openSplash = false;
			}
			else if (arg.equals("biomes")) {
				biomesDirectory = new File(args[++i]);
			}
			else if (arg.equals("materials")) {
				materialsDirectory = new File(args[++i]);
			}
			else if (arg.equals("addons")) {
				addonsDirectory = new File(args[++i]);
			}
			else if (arg.equals("data")) {
				dataDirectory = new File(args[++i]);
			}
			else if (arg.equals("examples")) {
				examplesDirectory = new File(args[++i]);
			}
			else if (arg.equals("templates")) {
				templatesDirectory = new File(args[++i]);
			}
			else if (arg.equals("brushes")) {
				brushesDirectory = new File(args[++i]);
			}
			else {
				LOGGER.error("Invalid argument: " + arg);
				System.exit(1);
			}
		}
	}

	public static class App extends Application {
		@Override
		public void start(Stage primaryStage) throws Exception {
			LOGGER.info("JRE: " + System.getProperty("java.version"));
			LOGGER.info("OS: " + System.getProperty("os.name"));
			LOGGER.info("WorldSynth version: " + WorldSynth.getVersion());
			GLUtil.logGLProfiles();
			LOGGER.info("Starting WorldSynth Editor");

			JFrame splash = new JFrame();
			if (openSplash) {
				LOGGER.info("Displaying editor splash screen");
				splash.setLocationRelativeTo(null);
				splash.setUndecorated(true);
				splash.setType(Type.UTILITY);
				splash.add(new JLabel(new ImageIcon(Resources.getResourceURL("Splash.jpg"))));
				splash.pack();
				splash.setLocation(splash.getLocation().x-splash.getWidth()/2, splash.getLocation().y-splash.getHeight()/2);
				splash.setVisible(true);
			}

			LOGGER.info("Initializing engine");
			LOGGER.info("WorldSynth directory: " + ExecutionInfo.getWorldSynthDirectory());
			WorldSynthEditorDirectoryConfig directoryConfig = new WorldSynthEditorDirectoryConfig(
					ExecutionInfo.getWorldSynthDirectory(),
					addonsDirectory,
					materialsDirectory,
					biomesDirectory,
					dataDirectory,
					templatesDirectory,
					examplesDirectory,
					brushesDirectory);

			LOGGER.info("Initialize addon loader");
			EditorAddonLoader addonLoader = new EditorAddonLoader(directoryConfig.getAddonDirectory(), injectedAddons);

			LOGGER.info("Initialize WorldSynth");
			new WorldSynth(directoryConfig, addonLoader);

			LOGGER.info("Initializing editor");
			LOGGER.info("Initializing data manager");
			new DataManager(directoryConfig);
			LOGGER.info("Initializing preferences");
			new EditorPreferences();
			LOGGER.info("Initializing extent manager");
			new WorldExtentManager();
			LOGGER.info("Initializing stage manager");
			new StageManager();
			LOGGER.info("Initializing composer");
			new WorldSynthComposer(addonLoader);

			/////////////////////////////////////////////////////////////////////////////////

			LOGGER.info("Setting up editor");
			WorldSynthEditorApp.primaryStage = primaryStage;
			primaryStage.setTitle("WorldSynth Editor - Version " + WorldSynth.getVersion());
			Image stageIcon = new Image(Resources.getResourceStream("worldSynthIcon.png"));
			primaryStage.getIcons().add(stageIcon);

			LOGGER.debug("Registering primary stage with the stage manager");
			StageManager.setPrimaryStage(primaryStage);

			LOGGER.debug("Setting up root scene");
			StackPane root = new StackPane();
			LOGGER.debug("Creating editor");
			WorldSynthEditor worldSynthEditor = new WorldSynthEditor(addonLoader);
			LOGGER.debug("Add editor to root");
			root.getChildren().add(worldSynthEditor);
			LOGGER.debug("Check if welcome splash should show");
			if (EditorPreferences.showWelcomeSplashOnStartup.getValue()) {
				LOGGER.debug("Create and show welcome splash");
				Parent welcomeSplash = new WelcomeSplash(root, directoryConfig);
				root.getChildren().add(welcomeSplash);
			}
			LOGGER.debug("Creating scene");
			Scene scene = new Scene(root, 1500, 800);
			LOGGER.debug("Add scene event handler");
			scene.addEventHandler(KeyEvent.ANY, e -> {
				Node tabContentNode = worldSynthEditor.editorTabPane.getSelectionModel().getSelectedItem().getContent();
				if (tabContentNode instanceof PatchEditorPane) {
					PatchEditorPane currentEditor = (PatchEditorPane) tabContentNode;
					currentEditor.getKeyboardListener().handle(e);
				}
			});

			LOGGER.debug("Applying dark theme");
			stylesheet = Resources.getResourceURL("DarkThemeBase.css").toExternalForm();
			scene.getStylesheets().add(stylesheet);

			LOGGER.debug("Set stage scene and size, and show it");
			primaryStage.setScene(scene);
			primaryStage.setMinWidth(800);
			primaryStage.setMinHeight(700);
			primaryStage.show();

			if (openSplash) {
				LOGGER.info("Disposing editor splash screen");
				splash.dispose();
			}
		}
	}
}
