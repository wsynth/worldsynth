/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.editor.addon;

import net.worldsynth.addon.AddonLoader;
import net.worldsynth.addon.IAddon;
import net.worldsynth.composer.addon.IComposerAddon;
import net.worldsynth.composer.tool.Tool;
import net.worldsynth.composer.tool.addon.IToolAddon;
import net.worldsynth.editor.preview.addon.IPreviewAddon;
import net.worldsynth.editor.preview.addon.PreviewConstructor;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class EditorAddonLoader extends AddonLoader implements IPreviewAddon, IComposerAddon {

	public EditorAddonLoader(File addonDirectory, IAddon... injectedAddons) {
		super(addonDirectory, injectedAddons);
	}

	@Override
	public List<PreviewConstructor> getAddonPreviews() {
		ArrayList<PreviewConstructor> previews = new ArrayList<>();
		for (IAddon addon: addons) {
			if (addon instanceof IPreviewAddon previewAddon) {
				previews.addAll(previewAddon.getAddonPreviews());
			}
		}
		return previews;
	}

	@Override
	public List<Tool<?>> getAddonTools() {
		ArrayList<Tool<?>> tools = new ArrayList<>();
		for (IAddon addon: addons) {
			if (addon instanceof IToolAddon moduleAddon) {
				tools.addAll(moduleAddon.getAddonTools());
			}
		}
		return tools;
	}
}
