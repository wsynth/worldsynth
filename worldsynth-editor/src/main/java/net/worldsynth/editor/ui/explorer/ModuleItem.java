/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.editor.ui.explorer;

import javafx.event.Event;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TreeItem;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import net.worldsynth.editor.EditorSession;
import net.worldsynth.editor.resources.Resources;
import net.worldsynth.patch.ModuleWrapper;
import net.worldsynth.patch.event.WrapperEvent;
import net.worldsynth.standalone.ui.stage.ModuleParametersStage;
import net.worldsynth.standalone.ui.stage.NameEditorStage;

public class ModuleItem extends ProjectItem {

	public ModuleItem(ModuleWrapper module, EditorSession editorSession) {
		super(module, editorSession);

		Image iconImage = new Image(Resources.getResourceStream("explorerIcons/module.png"));
		ImageView iconView = new ImageView(iconImage);
		iconView.setFitWidth(16);
		iconView.setFitHeight(16);
		setGraphic(iconView);

		module.addEventHandler(WrapperEvent.WRAPPER_NAME_CHANGED, e -> {
			Event.fireEvent(this, new TreeModificationEvent<>(TreeItem.valueChangedEvent(), this));
		});
	}

	@Override
	public String getString() {
		return getValue().toString();
	}

	@Override
	public ContextMenu getContextMenu() {
		MenuItem editItem = new MenuItem("Edit");
		editItem.setOnAction(e -> {
			new ModuleParametersStage((ModuleWrapper) getValue(), e2 -> {
				// Commit changes and update preview on parameters edits being applied
				getEditorSession().commitHistory();
				getEditorSession().updatePreview();
			});
		});

		MenuItem renameItem = new MenuItem("Rename");
		renameItem.setOnAction(e -> {
			new NameEditorStage("Rename patch", new NameEditorStage.Nameable() {
				@Override
				public String getName() {
					return ((ModuleWrapper) getValue()).getCustomName();
				}

				@Override
				public void setName(String name) {
					((ModuleWrapper) getValue()).setCustomName(name);
					getEditorSession().commitHistory();
				}
			});
		});

		return new ContextMenu(editItem, renameItem);
	}

	@Override
	public void onDoubleClick() {

	}
}
