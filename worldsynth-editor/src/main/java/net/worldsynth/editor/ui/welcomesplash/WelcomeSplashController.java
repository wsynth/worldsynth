/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.editor.ui.welcomesplash;

import java.io.File;
import java.io.IOException;

import net.worldsynth.WorldSynth;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javafx.fxml.FXML;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.ScrollPane.ScrollBarPolicy;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.shape.Rectangle;
import net.worldsynth.editor.data.DataManager;
import net.worldsynth.editor.preferences.EditorPreferences;
import net.worldsynth.editor.ui.WorldSynthEditor;
import net.worldsynth.synth.Synth;
import net.worldsynth.synth.SynthManager;

public class WelcomeSplashController {
	private static Logger logger = LogManager.getLogger(WelcomeSplashController.class);
	
	@FXML
	private AnchorPane splashRoot;
	@FXML
	private ImageView splashImage;
	@FXML
	private Label splashVersion;
	@FXML
	private ScrollPane templateList;
	@FXML
	private ScrollPane recentList;
	@FXML
	private ScrollPane exampleList;
	@FXML
	private CheckBox openOnStartup;

	private final WelcomeSplash splash;
	private final StackPane rootStack;
	private final File templatesDirectory;
	private final File examplesDirectory;
	
	public WelcomeSplashController(WelcomeSplash welcomeSplash, StackPane rootStack, File templatesDirectory, File examplesDirectory) {
		this.splash = welcomeSplash;
		this.rootStack = rootStack;
		this.templatesDirectory = templatesDirectory;
		this.examplesDirectory = examplesDirectory;
	}

	@FXML
	public void initialize() {
		Rectangle clip = new Rectangle(600, 500);
		clip.setArcWidth(10);
		clip.setArcHeight(10);
		splashRoot.setClip(clip);

		// Set version text
		splashVersion.setText(WorldSynth.getVersion());

		// Add templates
		VBox templatesVBox = new VBox();
		if (templatesDirectory != null && templatesDirectory.exists()) {
			for (File file: templatesDirectory.listFiles()) {
				String name = file.getName();
				name = name.substring(0, name.lastIndexOf('.'));
				Label l = new Label(name);
				l.setMaxWidth(Double.MAX_VALUE);
				
				l.setOnMouseClicked(e -> {
					rootStack.getChildren().remove(splash);
					WorldSynthEditor.closeSynth(WorldSynthEditor.getOpenSynths().get(0));
					
					try {
						Synth openedSynth = SynthManager.readUnmanagedSynthFromFile(file);
						WorldSynthEditor.openSynth(openedSynth);
					} catch (IOException ex) {
						logger.error(ex);
					}
					
				});
				
				templatesVBox.getChildren().add(l);
			}
		}
		templateList.setHbarPolicy(ScrollBarPolicy.NEVER);
		templateList.setFitToWidth(true);
		templateList.setContent(templatesVBox);

		// Add recent
		VBox recentVBox = new VBox();
		for (File file: DataManager.getWorkspaceData().getRecentSynths()) {
			String name = file.getName();
			name = name.substring(0, name.lastIndexOf('.'));
			Label l = new Label(name);
			l.setMaxWidth(Double.MAX_VALUE);
			
			l.setOnMouseClicked(e -> {
				rootStack.getChildren().remove(splash);
				WorldSynthEditor.closeSynth(WorldSynthEditor.getOpenSynths().get(0));
				WorldSynthEditor.openSynth(file);
			});
			
			recentVBox.getChildren().add(l);
		}
		recentList.setHbarPolicy(ScrollBarPolicy.NEVER);
		recentList.setFitToWidth(true);
		recentList.setContent(recentVBox);

		// Add examples
		VBox examplesVBox = new VBox();
		if (examplesDirectory != null && examplesDirectory.exists()) {
			for (File file: examplesDirectory.listFiles()) {
				if (file.isDirectory() || !file.getName().endsWith(".wsynth")) {
					// Skip directories and non-worldsynth files
					continue;
				}

				String name = file.getName();
				name = name.substring(0, name.lastIndexOf('.'));
				Label l = new Label(name);
				l.setMaxWidth(Double.MAX_VALUE);
				
				l.setOnMouseClicked(e -> {
					rootStack.getChildren().remove(splash);
					WorldSynthEditor.closeSynth(WorldSynthEditor.getOpenSynths().get(0));
					WorldSynthEditor.openSynth(file);
				});
				
				examplesVBox.getChildren().add(l);
			}
		}
		exampleList.setHbarPolicy(ScrollBarPolicy.NEVER);
		exampleList.setFitToWidth(true);
		exampleList.setContent(examplesVBox);
		
		openOnStartup.selectedProperty().addListener((ob, oldValue, newValue) -> {
			EditorPreferences.showWelcomeSplashOnStartup.setValue(newValue);
		});
	}
	
	@FXML
	public void newSynth() {
		rootStack.getChildren().remove(splash);
	}
	
	@FXML
	public void openSynth() {
		rootStack.getChildren().remove(splash);
		Synth defaultSynth = WorldSynthEditor.getOpenSynths().get(0);
		if (WorldSynthEditor.openSynth()) {
			WorldSynthEditor.closeSynth(defaultSynth);
		}
	}
}
