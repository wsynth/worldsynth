/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.editor.ui.explorer;

import javafx.event.Event;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TreeItem;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import net.worldsynth.editor.EditorSession;
import net.worldsynth.editor.resources.Resources;
import net.worldsynth.patch.ModuleWrapper;
import net.worldsynth.patch.Patch;
import net.worldsynth.patch.event.PatchModuleAdditionEvent;
import net.worldsynth.patch.event.PatchModuleRemovalEvent;
import net.worldsynth.patch.event.PatchNameChangeEvent;
import net.worldsynth.standalone.ui.stage.NameEditorStage;

public class PatchItem extends ProjectItem {

	public PatchItem(Patch patch, EditorSession editorSession) {
		super(patch, editorSession);

		Image iconImage = new Image(Resources.getResourceStream("explorerIcons/patch.png"));
		ImageView iconView = new ImageView(iconImage);
		iconView.setFitWidth(16);
		iconView.setFitHeight(16);
		setGraphic(iconView);

		for (ModuleWrapper mw : patch.getModuleWrapperList()) {
			getChildren().add(new ModuleItem(mw, editorSession));
		}

		patch.addEventHandler(PatchModuleAdditionEvent.PATCH_MODULE_ADDED, e -> {
			PatchModuleAdditionEvent additionEvent = (PatchModuleAdditionEvent) e;
			getChildren().add(new ModuleItem(additionEvent.getWrapper(), editorSession));
		});

		patch.addEventHandler(PatchModuleRemovalEvent.PATCH_MODULE_REMOVED, e -> {
			PatchModuleRemovalEvent removalEvent = (PatchModuleRemovalEvent) e;
			for (TreeItem<Object> item : getChildren()) {
				if (item.getValue() == removalEvent.getWrapper()) {
					getChildren().remove(item);
					break;
				}
			}
		});

		patch.addEventHandler(PatchNameChangeEvent.PATCH_NAME_CHANGED, e -> {
			Event.fireEvent(this, new TreeModificationEvent<>(TreeItem.valueChangedEvent(), this));
		});
	}

	@Override
	public String getString() {
		return ((Patch) getValue()).getName();
	}

	@Override
	public ContextMenu getContextMenu() {
		MenuItem openItem = new MenuItem("Open");
		openItem.setOnAction(e -> {
			getEditorSession().openPatch((Patch) getValue());
		});

		MenuItem renameItem = new MenuItem("Rename");
		renameItem.setOnAction(e -> {
			new NameEditorStage("Rename patch", new NameEditorStage.Nameable() {
				@Override
				public String getName() {
					return ((Patch) getValue()).getName();
				}

				@Override
				public void setName(String name) {
					((Patch) getValue()).setName(name);
					getEditorSession().commitHistory();
				}
			});
		});

		return new ContextMenu(openItem, renameItem);
	}

	@Override
	public void onDoubleClick() {
		getEditorSession().openPatch((Patch) getValue());
	}
}
