/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.editor.ui.welcomesplash;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.StackPane;
import net.worldsynth.editor.WorldSynthEditorDirectoryConfig;
import net.worldsynth.editor.resources.Resources;

public class WelcomeSplash extends BorderPane {
	
	public WelcomeSplash(StackPane rootStack, WorldSynthEditorDirectoryConfig directoryConfig) throws Exception {
		FXMLLoader fxmlLoader = new FXMLLoader(Resources.getResourceURL("WelcomeSplashScene.fxml"));
		fxmlLoader.setController(new WelcomeSplashController(this, rootStack, directoryConfig.getTemplatesDirectory(), directoryConfig.getExamplesDirectory()));
		Parent welcomeSplash = fxmlLoader.load();
		setCenter(welcomeSplash);
	}
}
