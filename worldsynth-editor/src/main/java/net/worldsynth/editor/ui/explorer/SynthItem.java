/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.editor.ui.explorer;

import javafx.scene.control.ContextMenu;
import net.worldsynth.editor.EditorSession;
import net.worldsynth.synth.Synth;

public class SynthItem extends ProjectItem {

	public SynthItem(Synth synth, EditorSession editorSession) {
		super(synth, editorSession);

		getChildren().add(new PatchItem(synth.getPatch(), editorSession));

		if (synth.getComposition() != null) {
			getChildren().add(new CompositionItem(synth.getComposition(), editorSession));
		}
	}

	@Override
	public String getString() {
		return ((Synth) getValue()).getName();
	}

	@Override
	public ContextMenu getContextMenu() {
		return null;
	}

	@Override
	public void onDoubleClick() {

	}
}
