/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.editor.ui.explorer;

import javafx.scene.control.ContextMenu;
import javafx.scene.control.TreeItem;
import net.worldsynth.editor.EditorSession;

public abstract class ProjectItem extends TreeItem<Object> {

	private final EditorSession editorSession;

	public ProjectItem(Object value, EditorSession editorSession) {
		super(value);

		this.editorSession = editorSession;
	}

	public EditorSession getEditorSession() {
		return editorSession;
	}

	public abstract String getString();

	public abstract ContextMenu getContextMenu();

	public abstract void onDoubleClick();
}
