/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.editor.ui.explorer;

import javafx.event.Event;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TreeItem;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import net.worldsynth.composition.Composition;
import net.worldsynth.composition.event.CompositionLayerAdditionEvent;
import net.worldsynth.composition.event.CompositionLayerMoveEvent;
import net.worldsynth.composition.event.CompositionLayerRemovalEvent;
import net.worldsynth.composition.event.CompositionNameChangeEvent;
import net.worldsynth.composition.layer.Layer;
import net.worldsynth.composition.layer.LayerGroup;
import net.worldsynth.editor.EditorSession;
import net.worldsynth.editor.resources.Resources;
import net.worldsynth.standalone.ui.stage.NameEditorStage;

public class CompositionItem extends ProjectItem {

	public CompositionItem(Composition composition, EditorSession editorSession) {
		super(composition, editorSession);

		Image iconImage = new Image(Resources.getResourceStream("explorerIcons/composition.png"));
		ImageView iconView = new ImageView(iconImage);
		iconView.setFitWidth(16);
		iconView.setFitHeight(16);
		setGraphic(iconView);

		for (Layer l : composition.getRootGroup().getLayers()) {
			getChildren().add(0, new LayerItem(composition, l, editorSession));
		}

		composition.addEventHandler(CompositionLayerAdditionEvent.COMPOSITION_LAYER_ADDED, e -> {
			CompositionLayerAdditionEvent addEvent = (CompositionLayerAdditionEvent) e;
			recursiveAddLayer(composition, addEvent.getGroup(), addEvent.getLayer(), addEvent.getIndex(), null);
		});

		composition.addEventHandler(CompositionLayerRemovalEvent.COMPOSITION_LAYER_REMOVED, e -> {
			Layer layer = ((CompositionLayerRemovalEvent) e).getLayer();
			recursiveRemoveLayer(layer);
		});

		composition.addEventHandler(CompositionLayerMoveEvent.COMPOSITION_LAYER_MOVED, e -> {
			CompositionLayerMoveEvent moveEvent = (CompositionLayerMoveEvent) e;
			LayerItem movingItem  = recursiveRemoveLayer(moveEvent.getLayer());
			recursiveAddLayer(composition, moveEvent.getNewGroup(), moveEvent.getLayer(), moveEvent.getNewIndex(), movingItem);
		});

		composition.addEventHandler(CompositionNameChangeEvent.COMPOSITION_NAME_CHANGED, e -> {
			Event.fireEvent(this, new TreeModificationEvent<>(TreeItem.valueChangedEvent(), this));
		});
	}

	private void recursiveAddLayer(Composition composition, LayerGroup targetGroup, Layer layer, int index, LayerItem addItem) {
		if (composition.getRootGroup() == targetGroup) {
			addItem = addItem != null ? addItem : new LayerItem(composition, layer, getEditorSession());
			getChildren().add(getChildren().size() - index, addItem);
			return;
		}

		for (TreeItem<Object> item : getChildren()) {
			if (item instanceof LayerItem layerItem) {
				if (layerItem.recursiveAddLayer(composition, targetGroup, layer, index, addItem)) {
					return;
				}
			}
		}
	}

	private LayerItem recursiveRemoveLayer(Layer layer) {
		for (TreeItem<Object> item : getChildren()) {
			if (item instanceof LayerItem layerItem) {
				if (layerItem.getValue() == layer) {
					getChildren().remove(layerItem);
					return layerItem;
				}

				LayerItem removed = layerItem.recursiveRemoveLayer(layer);
				if (removed != null) {
					return removed;
				}
			}
		}

		return null;
	}

	@Override
	public String getString() {
		return ((Composition) getValue()).getName();
	}

	@Override
	public ContextMenu getContextMenu() {
		MenuItem openItem = new MenuItem("Open");
		openItem.setOnAction(e -> {
			getEditorSession().openComposition((Composition) getValue(), null);
		});

		MenuItem renameItem = new MenuItem("Rename");
		renameItem.setOnAction(e -> {
			new NameEditorStage("Rename composition", new NameEditorStage.Nameable() {
				@Override
				public String getName() {
					return ((Composition) getValue()).getName();
				}

				@Override
				public void setName(String name) {
					((Composition) getValue()).setName(name);
					getEditorSession().commitHistory();
				}
			});
		});

		return new ContextMenu(openItem, renameItem);
	}

	@Override
	public void onDoubleClick() {
		getEditorSession().openComposition((Composition) getValue(), null);
	}
}
