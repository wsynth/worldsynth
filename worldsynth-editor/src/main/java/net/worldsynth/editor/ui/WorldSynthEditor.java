/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.editor.ui;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.collections.ListChangeListener;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.layout.BorderPane;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import net.worldsynth.WorldSynth;
import net.worldsynth.build.BuildCache;
import net.worldsynth.composer.ui.CompositionEditor;
import net.worldsynth.editor.EditorSession;
import net.worldsynth.editor.WorldSynthEditorApp;
import net.worldsynth.editor.WorldSynthEditorDirectoryConfig;
import net.worldsynth.editor.addon.EditorAddonLoader;
import net.worldsynth.editor.data.DataManager;
import net.worldsynth.editor.preferences.EditorPreferences;
import net.worldsynth.editor.preferences.PreferenceEditor;
import net.worldsynth.editor.preferences.PreviewPreferences;
import net.worldsynth.editor.ui.explorer.ProjectExplorer;
import net.worldsynth.editor.ui.extentseditor.ExtentsEditor;
import net.worldsynth.patch.ModuleWrapper;
import net.worldsynth.patch.Patch;
import net.worldsynth.patcher.ui.patcheditor.PatchEditorPane;
import net.worldsynth.standalone.ui.stage.SynthParametersStage;
import net.worldsynth.synth.Synth;
import net.worldsynth.synth.SynthManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class WorldSynthEditor extends BorderPane {
	private static final Logger logger = LogManager.getLogger(WorldSynthEditor.class);

	private static WorldSynthEditor instance;
	
	private HashMap<Synth, EditorSession> editorSessions = new HashMap<Synth, EditorSession>();
	public EditorSession currentEditorSession;

	private BooleanProperty lockedPreview = new SimpleBooleanProperty(false);
	private ModuleWrapper currentPreviewWrapper;
	private boolean previewRefreshable = false;
	
	public TabPane editorTabPane;
	public ProjectExplorer projectTree;
	public PreviewPane previewPane;
	public ExtentsEditor extentsEditor;
	public Infobar infobar;

	public WorldSynthEditor(EditorAddonLoader addonLoader) {
		instance = this;

		logger.debug("Create menu bar");
		MenuBar menuBar = new WorldSynthEditorMenuBar();
		setTop(menuBar);

		logger.debug("Create preview");
		previewPane = new PreviewPane(addonLoader);

		logger.debug("Create tree explorer");
		projectTree = new ProjectExplorer();
		BorderPane leftPane = new BorderPane(projectTree, previewPane, null, null, null);
		setLeft(leftPane);

		logger.debug("Create info bar");
		infobar = new Infobar();
		setBottom(infobar);

		logger.debug("Create editor tab pane");
		editorTabPane = new TabPane();
		editorTabPane.getTabs().addListener((ListChangeListener.Change<? extends Tab> c) -> {
			if (editorTabPane.getTabs().size() == 1) {
				currentEditorSession = null;
				projectTree.setEditorSession(null);
			}
		});
		editorTabPane.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
			if (newValue.getContent() instanceof PatchEditorPane patchEditor) {
				currentEditorSession = (EditorSession) patchEditor.getEditorSession();
				projectTree.setEditorSession(currentEditorSession);
			}
			if (newValue.getContent() instanceof CompositionEditor compositionEditor) {
				currentEditorSession = (EditorSession) compositionEditor.getEditorSession();
				projectTree.setEditorSession(currentEditorSession);
			}
		});

		logger.debug("Create the extent editor");
		extentsEditor = new ExtentsEditor();
		Tab extentsEditorTab = new Tab("Extents editor", extentsEditor);
		extentsEditorTab.setClosable(false);
		editorTabPane.getTabs().add(extentsEditorTab);
		setCenter(editorTabPane);
		
		// Repaint patch editor when changing preview lock
		lockedPreviewProperty().addListener((ob, ov, nv) -> {
			if (getCurrentPreviewWrapper() != null) {
				Patch patch = getCurrentPreviewWrapper().getParentPatch();
				Synth synth = patch.getParentSynth();
				
				editorSessions.get(synth).repaintPatch(patch);
			}
		});

		setPrefWidth(1080);

		logger.debug("Create an initial blank synth");
		newSynth();
	}

	private class WorldSynthEditorMenuBar extends MenuBar {

		private Menu fileMenu = new Menu("File");
		private Menu editMenu = new Menu("Edit");
		private Menu helpMenu = new Menu("Help");

		public WorldSynthEditorMenuBar() {
			// File
			MenuItem newItem = new MenuItem("New");
			newItem.setAccelerator(EditorPreferences.editorKeybindNew.getValue());
			EditorPreferences.editorKeybindNew.addListener((observable, oldValue, newValue) -> {
				newItem.setAccelerator(newValue);
			});
			newItem.setOnAction(newItemHandler);

			MenuItem saveItem = new MenuItem("Save");
			saveItem.setAccelerator(EditorPreferences.editorKeybindSave.getValue());
			EditorPreferences.editorKeybindSave.addListener((observable, oldValue, newValue) -> {
				saveItem.setAccelerator(newValue);
			});
			saveItem.setOnAction(saveItemHandler);

			MenuItem saveAsItem = new MenuItem("Save as");
			saveAsItem.setAccelerator(EditorPreferences.editorKeybindSaveAs.getValue());
			EditorPreferences.editorKeybindSaveAs.addListener((observable, oldValue, newValue) -> {
				saveAsItem.setAccelerator(newValue);
			});
			saveAsItem.setOnAction(saveAsItemHandler);

			MenuItem openItem = new MenuItem("Open");
			openItem.setAccelerator(EditorPreferences.editorKeybindOpen.getValue());
			EditorPreferences.editorKeybindOpen.addListener((observable, oldValue, newValue) -> {
				openItem.setAccelerator(newValue);
			});
			openItem.setOnAction(openItemHandler);

			Menu openRecentMenu = new Menu("Open recent");
			fileMenu.setOnShowing(e -> {
				openRecentMenu.getItems().clear();
				for (File file: DataManager.getWorkspaceData().getRecentSynths()) {
					String name = file.getName();
					name = name.substring(0, name.lastIndexOf('.'));
					MenuItem r = new MenuItem(name);
					r.setOnAction(e2 -> {
						WorldSynthEditor.instance.openSynth(file);
					});
					openRecentMenu.getItems().add(r);
				}
			});

			Menu examplesMenu = new Menu("Examples");
			File examplesDirectory = ((WorldSynthEditorDirectoryConfig) WorldSynth.getDirectoryConfig()).getExamplesDirectory();
			if (examplesDirectory != null && examplesDirectory.exists()) {
				for (File file: examplesDirectory.listFiles()) {
					if (file.isDirectory() || !file.getName().endsWith(".wsynth")) {
						// Skip directories and non-worldsynth files
						continue;
					}

					String name = file.getName();
					name = name.substring(0, name.lastIndexOf('.'));
					MenuItem r = new MenuItem(name);
					r.setOnAction(e2 -> {
						WorldSynthEditor.instance.openSynth(file);
					});
					examplesMenu.getItems().add(r);
				}
			}

			fileMenu.getItems().addAll(
					newItem,
					new SeparatorMenuItem(),
					saveItem,
					saveAsItem,
					new SeparatorMenuItem(),
					openItem,
					openRecentMenu,
					examplesMenu);

			// Edit
			MenuItem undoItem = new MenuItem("Undo");
			undoItem.setAccelerator(EditorPreferences.editorKeybindUndo.getValue());
			EditorPreferences.editorKeybindUndo.addListener((observable, oldValue, newValue) -> {
				undoItem.setAccelerator(newValue);
			});
			undoItem.setOnAction(undoItemHandler);
			editMenu.getItems().add(undoItem);
			
			MenuItem redoItem = new MenuItem("Redo");
			redoItem.setAccelerator(EditorPreferences.editorKeybindRedo.getValue());
			EditorPreferences.editorKeybindRedo.addListener((observable, oldValue, newValue) -> {
				redoItem.setAccelerator(newValue);
			});
			redoItem.setOnAction(redoItemHandler);
			editMenu.getItems().add(redoItem);
			
			editMenu.getItems().add(new SeparatorMenuItem());

			MenuItem lockPreviewItem = new MenuItem("Lock preview");
			lockPreviewItem.setAccelerator(PreviewPreferences.previewKeybindLock.getValue());
			PreviewPreferences.previewKeybindLock.addListener((observable, oldValue, newValue) -> {
				lockPreviewItem.setAccelerator(newValue);
			});
			lockPreviewItem.setOnAction(e -> {
				setPreviewLocked(!isPreviewLocked());
			});
			editMenu.getItems().add(lockPreviewItem);

			editMenu.getItems().add(new SeparatorMenuItem());
			
			MenuItem synthParametersItem = new MenuItem("Synth parameters");
			synthParametersItem.setAccelerator(EditorPreferences.editorKeybindSynthParameters.getValue());
			EditorPreferences.editorKeybindSynthParameters.addListener((observable, oldValue, newValue) -> {
				synthParametersItem.setAccelerator(newValue);
			});
			synthParametersItem.setOnAction(synthParametersItemHandler);
			editMenu.getItems().add(synthParametersItem);
			
			editMenu.getItems().add(new SeparatorMenuItem());
			
			MenuItem editorPreferencesItem = new MenuItem("Editor preferences");
			editorPreferencesItem.setAccelerator(EditorPreferences.editorKeybindPreferences.getValue());
			EditorPreferences.editorKeybindPreferences.addListener((observable, oldValue, newValue) -> {
				editorPreferencesItem.setAccelerator(newValue);
			});
			editorPreferencesItem.setOnAction(editorPreferencesItemHandler);
			editMenu.getItems().add(editorPreferencesItem);

			// About
			MenuItem aboutItem = new MenuItem("About");
			aboutItem.setOnAction(aboutItemHandler);
			helpMenu.getItems().add(aboutItem);
			
			
			getMenus().addAll(fileMenu, editMenu, helpMenu);
		}

		private EventHandler<ActionEvent> newItemHandler = e -> {
			newSynth();
		};

		private EventHandler<ActionEvent> saveItemHandler = e -> {
			saveSynth();
		};

		private EventHandler<ActionEvent> saveAsItemHandler = e -> {
			saveSynthAs();
		};

		private EventHandler<ActionEvent> openItemHandler = e -> {
			openSynth();
		};

		private EventHandler<ActionEvent> undoItemHandler = e -> {
			if (currentEditorSession != null) {
				currentEditorSession.undo();

				Node n = editorTabPane.getSelectionModel().getSelectedItem().getContent();
				if (n instanceof PatchEditorPane) {
					((PatchEditorPane) n).repaint();
				}

				updatePreview();
			}
		};
		
		private EventHandler<ActionEvent> redoItemHandler = e -> {
			if (currentEditorSession != null) {
				currentEditorSession.redo();

				Node n = editorTabPane.getSelectionModel().getSelectedItem().getContent();
				if (n instanceof PatchEditorPane) {
					((PatchEditorPane) n).repaint();
				}

				updatePreview();
			}
		};
		
		private EventHandler<ActionEvent> synthParametersItemHandler = e -> {
			SynthParametersStage synthParametersStage = new SynthParametersStage(currentEditorSession.getSynth());
			synthParametersStage.show();
		};

		private PreferenceEditor prefEditorInstance;
		private EventHandler<ActionEvent> editorPreferencesItemHandler = e -> {
			if (prefEditorInstance == null) {
				prefEditorInstance = new PreferenceEditor();
			}
			prefEditorInstance.show();
		};

		private EventHandler<ActionEvent> aboutItemHandler = e -> {
			about();
		};
	}

	private static void newSynth() {
		openSynth(new Synth("Unnamed synth"));
	}
	
	private static void saveSynth() {
		instance.currentEditorSession.save();
	}
	
	private static void saveSynthAs() {
		instance.currentEditorSession.saveAs();
	}

	public static boolean openSynth() {
		FileChooser fileChooser = new FileChooser();
		fileChooser.setTitle("Open WorldSynth project");
		fileChooser.getExtensionFilters().add(new ExtensionFilter("WorldSynth", "*.wsynth"));

		File synthFile = fileChooser.showOpenDialog(WorldSynthEditorApp.primaryStage);

		logger.info(synthFile);
		if (synthFile != null) {
			openSynth(synthFile);
			return true;
		}
		return false;
	}

	public static void openSynth(File synthFile) {
		try {
			Synth openedSynth = SynthManager.openSynth(synthFile);
			
			DataManager.getWorkspaceData().setMostRecentSynth(synthFile);
			
			openSynth(openedSynth);
		} catch (IOException e) {
			logger.error(e);
		}
	}

	public static void openSynth(Synth synth) {
		logger.info("Opeing editor session for \"" + synth.getName() + "\"");
		if (!instance.editorSessions.containsKey(synth)) {
			logger.info("Creating new editor session for \"" + synth.getName() + "\"");
			EditorSession editorSession = new EditorSession(synth, instance);
			instance.editorSessions.put(synth, editorSession);
		}

		instance.currentEditorSession = instance.editorSessions.get(synth);
	}
	
	public static void closeSynth(Synth synth) {
		logger.info("Closing editor session for \"" + synth.getName() + "\"");
		instance.editorSessions.get(synth).closeAllPatches();
		instance.editorSessions.get(synth).closeAllCompositions();
		instance.editorSessions.remove(synth);
	}

	public static List<Synth> getOpenSynths() {
		return new ArrayList<>(instance.editorSessions.keySet());
	}
	
	private void about() {
		new AboutStage();
	}

	public static Infobar getInfobar() {
		return instance.infobar;
	}

	public static ProjectExplorer getPatchTreeView() {
		return instance.projectTree;
	}

	public static void updatePreview() {
		updatePreview(true);
	}

	public static void updatePreview(boolean clearExtentsEditorPreview) {
		instance.previewPane.updatePreview();
		instance.extentsEditor.worldPreview.updatePreview(clearExtentsEditorPreview);
		instance.previewRefreshable = false;
	}

	public static void updatePreview(ModuleWrapper wrapper, BuildCache buildCache) {
		if (instance.isPreviewLocked() && wrapper != instance.currentPreviewWrapper) {
			return;
		}
		else if (wrapper == instance.currentPreviewWrapper && !instance.previewRefreshable) {
			return;
		}

		instance.currentPreviewWrapper = wrapper;
		instance.previewRefreshable = false;
		
		Synth synth = wrapper.getParentPatch().getParentSynth();
		if (synth.getExtentManager() != instance.extentsEditor.getCurrentExtentManager()) {
			instance.extentsEditor.setEditorSession(instance.editorSessions.get(synth));
			instance.previewPane.setExtentManager(synth.getExtentManager());
		}

		instance.extentsEditor.worldPreview.updatePreview(synth, instance.currentPreviewWrapper);
		instance.previewPane.updatePreview(instance.currentPreviewWrapper, buildCache);
	}
	
	public static ModuleWrapper getCurrentPreviewWrapper() {
		return instance.currentPreviewWrapper;
	}
	
	public static BooleanProperty lockedPreviewProperty() {
		return instance.lockedPreview;
	}
	
	public static boolean isPreviewLocked() {
		return instance.lockedPreview.get();
	}
	
	public static void setPreviewLocked(boolean lock) {
		instance.lockedPreview.set(lock);
	}
	
	/**
	 * Called by editor sessions when any unsaved changes are registered. This lets
	 * the user refresh the preview by clicking the currently previewed module if it
	 * is not done automatically.
	 */
	public static void makePreviewRefreshable() {
		instance.previewRefreshable = true;
	}
}
