/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.editor.ui.explorer;

import javafx.scene.control.TreeCell;
import javafx.scene.control.TreeView;
import net.worldsynth.editor.EditorSession;

public class ProjectExplorer extends TreeView<Object> {

	private EditorSession editorSession;

	public void setEditorSession(EditorSession editorSession) {
		this.editorSession = editorSession;

		if (editorSession == null) {
			setRoot(null);
			return;
		}

		ProjectItem synthRootItem = editorSession.getSynthTreeItem();
		synthRootItem.setExpanded(true);

		setRoot(synthRootItem);
		setCellFactory(p -> new ExplorerCell());

		setFocusTraversable(false);
	}

	public void setSelection(Object[] objects) {

	}

	private static class ExplorerCell extends TreeCell<Object> {

		@Override
		protected void updateItem(Object item, boolean empty) {
			super.updateItem(item, empty);

			if (empty) {
				setText(null);
				setGraphic(null);
				setContextMenu(null);
				setOnMouseClicked(null);
			} else {
				setText(getString());
				setGraphic(getTreeItem().getGraphic());
				setContextMenu(((ProjectItem) getTreeItem()).getContextMenu());
				setOnMouseClicked(e -> {
					if (e.getClickCount() == 2) {
						((ProjectItem) getTreeItem()).onDoubleClick();
					}
				});
			}
		}

		private String getString() {
			return ((ProjectItem) getTreeItem()).getString();
		}
	}
}
