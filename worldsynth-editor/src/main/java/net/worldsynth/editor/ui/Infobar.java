/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.editor.ui;

import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import net.worldsynth.WorldSynth;

public class Infobar extends BorderPane {
	
	private final Label infoLabel;
	
	public Infobar() {
		// Add style class
		getStyleClass().add("info-bar");

		infoLabel = new Label("Welcome to WorldSynth - Version " + WorldSynth.getVersion());
		setCenter(infoLabel);
	}
	
	public void updateInfoText(String text) {
		infoLabel.setText(text);
	}
}
