/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.editor.ui;


import com.jogamp.opengl.awt.GLJPanel;
import javafx.beans.binding.Bindings;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.embed.swing.SwingNode;
import javafx.geometry.Pos;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.StackPane;
import net.worldsynth.build.BuildCache;
import net.worldsynth.datatype.AbstractDatatype;
import net.worldsynth.editor.WorldSynthEditorApp;
import net.worldsynth.editor.preferences.PreviewPreferences;
import net.worldsynth.editor.preview.GLPreview;
import net.worldsynth.editor.preview.NativePreviews;
import net.worldsynth.editor.preview.NullRender;
import net.worldsynth.editor.preview.UndefinedRender;
import net.worldsynth.editor.preview.addon.IPreviewAddon;
import net.worldsynth.editor.preview.addon.PreviewConstructor;
import net.worldsynth.editor.resources.Resources;
import net.worldsynth.extent.WorldExtent;
import net.worldsynth.extent.WorldExtentManager;
import net.worldsynth.module.ModuleOutput;
import net.worldsynth.module.ModuleOutputRequest;
import net.worldsynth.patch.ModuleWrapper;
import net.worldsynth.patch.ModuleWrapperIO;
import net.worldsynth.patcher.build.BuildTask;
import net.worldsynth.standalone.ui.preview.AbstractPreviewRender;
import net.worldsynth.util.event.build.BuildStatusListener;
import org.apache.logging.log4j.Logger;
import org.controlsfx.control.SegmentedButton;

import javax.swing.*;
import java.awt.*;
import java.util.*;
import java.util.concurrent.ExecutionException;

public class PreviewPane extends BorderPane {
	
	private final Logger LOGGER = WorldSynthEditorApp.LOGGER;
	
	private WorldExtentManager currentExtentsManager = null;
	private final ComboBox<WorldExtent> extentSelector = new ComboBox<>();

	private final StyleChooser styleChooser = new StyleChooser();
	
	private BuildTask mainBuildTask = null;
	
	private ModuleWrapper currentPreviewModuleWrapper = null;
	private BuildCache currentBuildCache = null;

	private final ArrayList<PreviewConstructor> previewConstructors = new ArrayList<>();
	private AbstractPreviewRender previewRender;
	
	private final StackPane previewStack = new StackPane();
	
	private final SwingNode swingNode = new SwingNode();
	private final JPanel swingPanel = new JPanel(new BorderLayout());
	
	public PreviewPane(IPreviewAddon addonLoader) {
		LOGGER.debug("Set up stack pane");
		previewStack.setPrefSize(512, 512);

		LOGGER.debug("Set up swing node");
		swingPanel.setPreferredSize(new Dimension(512, 512));
		swingNode.setContent(swingPanel);

		previewStack.getChildren().add(swingNode);
		setCenter(previewStack);
		
		setNewPreviewRender(new NullRender());
		
		extentSelector.setMaxWidth(Double.MAX_VALUE);
		setTop(extentSelector);
		
		// Lock preview button
		Image unlockedIcon = new Image(Resources.getResourceStream("unlocked16x16.png"));
		Image lockedIcon = new Image(Resources.getResourceStream("locked16x16.png"));
		ToggleButton lockPreview = new ToggleButton("", new ImageView(unlockedIcon));
		lockPreview.setTooltip(new Tooltip("Preview locking"));
		lockPreview.selectedProperty().bindBidirectional(WorldSynthEditor.lockedPreviewProperty());
		
		lockPreview.selectedProperty().addListener((observable, oldValue, newValue) -> {
			if (newValue) {
				lockPreview.setGraphic(new ImageView(lockedIcon));
			}
			else {
				lockPreview.setGraphic(new ImageView(unlockedIcon));
			}
		});

		BorderPane buttonsPane = new BorderPane(styleChooser);
		BorderPane.setAlignment(styleChooser, Pos.CENTER);
		lockPreview.prefHeightProperty().bind(buttonsPane.heightProperty());
		buttonsPane.setRight(lockPreview);
		
		setBottom(buttonsPane);


		// Add native previews
		previewConstructors.addAll(new NativePreviews().getAddonPreviews());

		// Add previews from addons
		previewConstructors.addAll(addonLoader.getAddonPreviews());
	}
	
	public void updatePreview() {
		updatePreview(currentPreviewModuleWrapper, currentBuildCache);
	}
	
	public void updatePreview(ModuleWrapper wrapper, BuildCache buildCache) {
		currentPreviewModuleWrapper = wrapper;
		currentBuildCache = buildCache;

		if (wrapper != null && currentExtentsManager.getCurrentWorldExtent() != null) {
			if (wrapper.wrapperOutputs.size() > 0) {
				ModuleWrapperIO primaryWrapperOutput = wrapper.wrapperOutputs.get(wrapper.module.getOutputs()[0].getName());
				if (primaryWrapperOutput != null) {
					double x = currentExtentsManager.getCurrentWorldExtent().getX();
					double y = currentExtentsManager.getCurrentWorldExtent().getY();
					double z = currentExtentsManager.getCurrentWorldExtent().getZ();
					
					double width = currentExtentsManager.getCurrentWorldExtent().getWidth();
					double length = currentExtentsManager.getCurrentWorldExtent().getLength();
					double height = currentExtentsManager.getCurrentWorldExtent().getHeight();
					
					double resolutionPitch = Math.max(Math.max(width, length) / PreviewPreferences.resolution.getValue(), 1.0);
					
					AbstractDatatype requestData = primaryWrapperOutput.getDatatype().getPreviewDatatype(x, y, z, width, height, length, resolutionPitch);
					ModuleOutput output = (ModuleOutput) primaryWrapperOutput.getIO();
					ModuleOutputRequest request = new ModuleOutputRequest(output, requestData);
					
					buildOnNewTreadAndPushToPreview(wrapper, request, null, buildCache);
				}
			}
		}
	}
	
	public void setExtentManager(WorldExtentManager manager) {
		if (currentExtentsManager != null) {
			currentExtentsManager.currentWorldExtentProperty().removeListener(extentManagerSelectionChangeListener);
			Bindings.unbindBidirectional(extentSelector.valueProperty(), currentExtentsManager.currentWorldExtentProperty());
			extentSelector.valueProperty().set(null);
			extentSelector.setItems(FXCollections.emptyObservableList());
		}
		currentExtentsManager = manager;
		if (manager != null) {
			currentExtentsManager.currentWorldExtentProperty().addListener(extentManagerSelectionChangeListener);
			extentSelector.setItems(manager.getObservableExtentsList());
			Bindings.bindBidirectional(extentSelector.valueProperty(), currentExtentsManager.currentWorldExtentProperty());
		}
	}
	
	/**
	 * This listener listens for change in the current worldextent in the extents manager.<br>
	 * The listener is registered to the appropriate properties inside {@link #setExtentManager(WorldExtentManager) setExtentManager}.
	 */
	private ChangeListener<WorldExtent> extentManagerSelectionChangeListener = (ObservableValue<? extends WorldExtent> observable, WorldExtent oldValue, WorldExtent newValue) -> {
		if (newValue == null) {
			return;
		}
		//extentSelector.getItems();
		updatePreview();
	};
	
	private void setNewPreviewRender(AbstractPreviewRender render) {
		if (previewRender instanceof GLPreview) {
			swingPanel.removeAll();
		}
		else {
			previewStack.getChildren().remove(previewRender);
		}
		
		previewRender = render;
		
		if (render instanceof GLPreview) {
			GLPreview glPreview = (GLPreview) previewRender;
			GLJPanel glJpanel = glPreview.getGLJpanel();
			swingPanel.add(glJpanel);
			swingPanel.revalidate();
		}
		else {
			previewStack.getChildren().add(previewRender);
		}
	}

	private LinkedHashMap<String, PreviewConstructor> getRendersForData(AbstractDatatype data) {
		LinkedHashMap<String, PreviewConstructor> previewConstructors = new LinkedHashMap<>();

		for (PreviewConstructor pc : this.previewConstructors) {
			if (pc.isApplicableTo(data)) {
				previewConstructors.put(pc.getStyleName(), pc);
			}
		}

		return previewConstructors;
	}
	
	private void buildOnNewTreadAndPushToPreview(ModuleWrapper wrapper, ModuleOutputRequest request, BuildStatusListener buildListener, BuildCache buildCache) {
		if (mainBuildTask != null) {
			mainBuildTask.cancel();
		}
		mainBuildTask = new BuildTask(wrapper, request, e -> {
			WorldSynthEditor.getInfobar().updateInfoText(e.getStatus().toString());
		}, buildCache);
		
		mainBuildTask.setOnFailed(e -> {
			LOGGER.error("Build failed", mainBuildTask.getException());
		});
		
		mainBuildTask.setOnSucceeded(e -> {
			LOGGER.info("Build succeded");
			
			try {
				AbstractDatatype builtData = mainBuildTask.get();

				styleChooser.setDatatype(builtData);
				AbstractPreviewRender newRender = styleChooser.getRenderForData(builtData);

				if (!previewRender.getClass().equals(newRender.getClass())) {
					setNewPreviewRender(newRender);
				}
				
				previewRender.pushDataToRender(builtData, wrapper.getParentPatch().getParentSynth().getParameters());
			} catch (InterruptedException | ExecutionException e1) {
				e1.printStackTrace();
			}
			
			mainBuildTask = null;
		});
		
		new Thread(mainBuildTask, "WorldSynth Patcher Preview Build Thread").start();
	}

	private class StyleChooser extends SegmentedButton {

		private final HashMap<Class<? extends AbstractDatatype>, String> lastStyleSelections = new HashMap<>();
		private Class<? extends AbstractDatatype> currentDataClass;

		public StyleChooser() {
		}

		public void setDatatype(AbstractDatatype data) {
			if (currentDataClass == data.getClass()) return;
			currentDataClass = data.getClass();

			setToggleGroup(new ToggleGroup());
			getButtons().clear();

			LinkedHashMap<String, PreviewConstructor> previewConstructors = getRendersForData(data);

			// No need for style buttons if there are no options
			if (previewConstructors.size() <= 1) return;

			String lastSelection = lastStyleSelections.get(data.getClass());
			for (String s : previewConstructors.keySet()) {
				ToggleButton styleButton = new ToggleButton(s);
				styleButton.setMinWidth(100.0);

				getButtons().add(styleButton);

				if (lastSelection == null) {
					getToggleGroup().selectToggle(styleButton);
					lastSelection = s;
					lastStyleSelections.put(data.getClass(), lastSelection);
				}
				else if (lastSelection.equals(s)) {
					getToggleGroup().selectToggle(styleButton);
				}
			}

			getToggleGroup().selectedToggleProperty().addListener((observable, oldValue, newValue) -> {
				if (newValue == null) return;

				String buttonText = ((ToggleButton) newValue).getText();
				lastStyleSelections.put(data.getClass(), buttonText);
				updatePreview();
			});
		}

		public AbstractPreviewRender getRenderForData(AbstractDatatype data) {
			LinkedHashMap<String, PreviewConstructor> previewConstructors = getRendersForData(data);

			if (!previewConstructors.isEmpty()) {
				String selectedStyle = lastStyleSelections.get(data == null ? null : data.getClass());
				if (selectedStyle == null) {
					selectedStyle = previewConstructors.entrySet().iterator().next().getValue().getStyleName();
					lastStyleSelections.put(data == null ? null : data.getClass(), selectedStyle);
				}

				return previewConstructors.get(selectedStyle).constructPreview();
			}
			else {
				return new UndefinedRender();
			}
		}
	}
}
