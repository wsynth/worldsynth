/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.editor.ui.explorer;

import javafx.event.Event;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TreeItem;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import net.worldsynth.composition.Composition;
import net.worldsynth.composition.event.LayerEvent;
import net.worldsynth.composition.layer.Layer;
import net.worldsynth.composition.layer.LayerGroup;
import net.worldsynth.composition.layer.colormap.LayerColormap;
import net.worldsynth.composition.layer.featuremap.LayerFeaturemap;
import net.worldsynth.composition.layer.heightmap.LayerHeightmap;
import net.worldsynth.composition.layer.materialmap.LayerMaterialmap;
import net.worldsynth.editor.EditorSession;
import net.worldsynth.editor.resources.Resources;

public class LayerItem extends ProjectItem {

	private final Composition composition;

	public LayerItem(Composition composition, Layer layer, EditorSession editorSession) {
		super(layer, editorSession);
		this.composition = composition;

		if (layer instanceof LayerHeightmap) {
			setLayerIcon("explorerIcons/layers/heightmap.png");
		}
		else if (layer instanceof LayerColormap) {
			setLayerIcon("explorerIcons/layers/colormap.png");
		}
		else if (layer instanceof LayerMaterialmap) {
			setLayerIcon("explorerIcons/layers/materialmap.png");
		}
		else if (layer instanceof LayerFeaturemap) {
			setLayerIcon("explorerIcons/layers/featuremap.png");
		}
		else if (layer instanceof LayerGroup layerGroup) {
			setLayerIcon("explorerIcons/layers/group.png");

			for (Layer l : layerGroup.getLayers()) {
				getChildren().add(0, new LayerItem(composition, l, editorSession));
			}
		}

		layer.addEventHandler(LayerEvent.LAYER_ANY, e -> {
			Event.fireEvent(this, new TreeModificationEvent<>(TreeItem.valueChangedEvent(), this));
		});
	}

	boolean recursiveAddLayer(Composition composition, LayerGroup targetGroup, Layer layer, int index, LayerItem addItem) {
		if (getValue() == targetGroup) {
			addItem = addItem != null ? addItem : new LayerItem(composition, layer, getEditorSession());
			getChildren().add(getChildren().size() - index, addItem);
			return true;
		}

		for (TreeItem<Object> item : getChildren()) {
			if (item instanceof LayerItem layerItem) {
				if (layerItem.recursiveAddLayer(composition, targetGroup, layer, index, addItem)) {
					return true;
				}
			}
		}

		return false;
	}

	LayerItem recursiveRemoveLayer(Layer layer) {
		for (TreeItem<Object> item : getChildren()) {
			if (item instanceof LayerItem layerItem) {
				if (layerItem.getValue() == layer) {
					getChildren().remove(layerItem);
					return layerItem;
				}

				LayerItem removed = layerItem.recursiveRemoveLayer(layer);
				if (removed != null) {
					return removed;
				}
			}
		}

		return null;
	}

	@Override
	public String getString() {
		return ((Layer) getValue()).getName();
	}

	private void setLayerIcon(String resource) {
		Image iconImage = new Image(Resources.getResourceStream(resource));
		ImageView iconView = new ImageView(iconImage);
		iconView.setFitWidth(16);
		iconView.setFitHeight(16);

		setGraphic(iconView);
	}

	@Override
	public ContextMenu getContextMenu() {
		MenuItem openItem = new MenuItem("Open");
		openItem.setOnAction(e -> {
			getEditorSession().openComposition(composition, (Layer) getValue());
		});

		return new ContextMenu(openItem);
	}

	@Override
	public void onDoubleClick() {
		getEditorSession().openComposition(composition, (Layer) getValue());
	}
}
