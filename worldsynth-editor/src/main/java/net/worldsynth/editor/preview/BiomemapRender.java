/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.editor.preview;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.PixelFormat;
import javafx.scene.image.PixelWriter;
import javafx.scene.image.WritableImage;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import net.worldsynth.biome.Biome;
import net.worldsynth.datatype.AbstractDatatype;
import net.worldsynth.datatype.DatatypeBiomemap;
import net.worldsynth.editor.preferences.PreviewPreferences;
import net.worldsynth.standalone.ui.preview.AbstractPreviewRenderCanvas;
import net.worldsynth.synth.SynthParameters;

public class BiomemapRender extends AbstractPreviewRenderCanvas {
	
	private Biome[][] biomemap;
	private WritableImage biomemapImage;
	
	private Biome mouseOverBiome = Biome.NULL;
	
	public BiomemapRender() {
		setOnMouseMoved(e -> {
			mouseOverBiome = biomeAtPixel((int) e.getX(), (int) e.getY());
			paint();
		});
	}
	
	@Override
	public void pushDataToRender(AbstractDatatype data, SynthParameters synthParameters) {
		DatatypeBiomemap castData = (DatatypeBiomemap) data;
		this.biomemap = castData.getBiomemap();

		int width = castData.mapPointsWidth;
		int length = castData.mapPointsLength;

		// Create image from biomemap
		biomemapImage = new WritableImage(width, length);
		PixelWriter pw = biomemapImage.getPixelWriter();
		int[] buffer = new int[width*length];

		for (int u = 0; u < width; u++) {
			for (int v = 0; v < length; v++) {
				Color c = biomemap[u][v].getFxColor();
				int red = (int) (c.getRed() * 255.0f);
				int green = (int) (c.getGreen() * 255.0f);
				int blue = (int) (c.getBlue() * 255.0f);
				buffer[u+v*width] = (255 << 24) | (red << 16) | (green << 8) | blue;
			}
		}
		pw.setPixels(0, 0, width, length, PixelFormat.getIntArgbPreInstance(), buffer, 0, width);

		paint();
	}
	
	public void paint() {
		GraphicsContext g = canvas.getGraphicsContext2D();
		g.setFill(PreviewPreferences.backgroundColor.getValue());
		g.fillRect(0, 0, getWidth(), getHeight());
		
		if (biomemap != null) {
			int width = biomemap.length;
			int length = biomemap[0].length;

			double minWidth = Math.min(width, getWidth());
			double minLength = Math.min(length, getHeight());
			double resPitch = Math.min(PreviewPreferences.resolution.getValue(), Math.max(minWidth, minLength)) / Math.max(width, length);
			double renderWidth = width*resPitch;
			double renderHeight = length*resPitch;

			double xOffset = (getWidth() - renderWidth) / 2.0;
			double yOffset = (getHeight() - renderHeight) / 2.0;

			g.drawImage(biomemapImage, xOffset, yOffset, renderWidth, renderHeight);

			// Indicate biome under the mouse cursor
			g.setFill(Color.WHITE);
			g.setFont(new Font("TimesRoman", 20));
			g.fillText(mouseOverBiome.getDisplayName(), 50, getHeight()-40);
		}
	}
	
	private Biome biomeAtPixel(int x, int y) {
		int width = biomemap.length;
		int length = biomemap[0].length;

		double minWidth = Math.min(width, getWidth());
		double minLength = Math.min(length, getHeight());
		double resPitch = Math.min(PreviewPreferences.resolution.getValue(), Math.max(minWidth, minLength)) / Math.max(width, length);
		double renderWidth = width*resPitch;
		double renderHeight = length*resPitch;

		double xOffset = (getWidth() - renderWidth) / 2.0;
		double yOffset = (getHeight() - renderHeight) / 2.0;

		int mapX = (int) ((x - xOffset) / resPitch);
		int mapY = (int) ((y - yOffset) / resPitch);
		
		if (mapX > 0 && mapY > 0 && mapX < biomemap.length && mapY < biomemap[0].length) {
			return biomemap[mapX][mapY];
		}
		
		return Biome.NULL;
	}
}
