/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.editor.preview;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import net.worldsynth.datatype.AbstractDatatype;
import net.worldsynth.datatype.DatatypeVectormap;
import net.worldsynth.editor.preferences.PreviewPreferences;
import net.worldsynth.standalone.ui.preview.AbstractPreviewRenderCanvas;
import net.worldsynth.synth.SynthParameters;

public class VectormapRender extends AbstractPreviewRenderCanvas {
	
	private float[][][] vectorfield;

	public double x, z, width, length;
	
	private float gain = 10.0f;
	
	double mouseX = 0, mouseY = 0;
	private float[] mouseOverVector = null;
	
	public VectormapRender() {
		setOnScroll(e -> {
			
			float maxGain = 100.0f;
			float minGain = 0.05f;
			
			double lastGain = gain;
			gain += e.getDeltaY()/e.getMultiplierY();
			if (gain < minGain) gain = minGain;
			else if (gain > maxGain) gain = maxGain;
			
			if (lastGain != gain) {
				paint();
			}
		});
		
		setOnMouseMoved(e -> {
			mouseX = e.getX();
			mouseY = e.getY();
			mouseOverVector = vectorAtPixel((int) e.getX(), (int) e.getY());
			paint();
		});
		
		setOnMouseExited(e -> {
			mouseOverVector = null;
			paint();
		});
	}
	
	@Override
	public void pushDataToRender(AbstractDatatype data, SynthParameters synthParameters) {
		DatatypeVectormap castData = (DatatypeVectormap) data;
		this.vectorfield = castData.getVectormap();

		this.x = castData.extent.getX();
		this.z = castData.extent.getZ();
		this.width = castData.mapPointsWidth;
		this.length = castData.mapPointsLength;

		paint();
	}
	
	@Override
	public void paint() {
		GraphicsContext g = getGraphicsContext2D();
		g.setLineWidth(1.0);
		g.setFill(PreviewPreferences.backgroundColor.getValue());
		g.fillRect(0, 0, getWidth(), getHeight());
		
		if (vectorfield != null) {
			double minWidth = Math.min(width, getWidth());
			double minLength = Math.min(length, getHeight());
			double resPitch = Math.min(PreviewPreferences.resolution.getValue(), Math.max(minWidth, minLength)) / Math.max(width, length);
			double renderWidth = width*resPitch;
			double renderHeight = length*resPitch;

			double xOffset = (getWidth() - renderWidth) / 2.0;
			double yOffset = (getHeight() - renderHeight) / 2.0;
			
			for (int x = 0; x < width; x+=10 / resPitch) {
				for (int y = 0; y < length; y+=10 / resPitch) {
					double xx = vectorfield[x][y][0];
					double yy = vectorfield[x][y][1];
					
					for (double i = 0.1; i <= 1.0; i+=0.1) {
						g.setLineWidth(i * 1.0);
						g.setStroke(Color.LIGHTGRAY);
						
						double x0 = x*resPitch+xOffset + xx * gain * (i - 0.1);
						double y0 = y*resPitch+yOffset + yy * gain * (i - 0.1);
						double x1 = x*resPitch+xOffset + xx * gain * i;
						double y1 = y*resPitch+yOffset + yy * gain * i;
						g.strokeLine(x0, y0, x1, y1);
					}
				}
			}

			if (mouseOverVector != null) {
				g.setFill(Color.gray(0.4, 0.4));
				g.fillOval(mouseX-gain*10, mouseY-gain*10, gain*20, gain*20);
				g.setStroke(Color.gray(1.0, 0.4));
				g.strokeOval(mouseX-gain, mouseY-gain, gain*2, gain*2);
				g.strokeOval(mouseX-gain*10, mouseY-gain*10, gain*20, gain*20);

				double x = (mouseX-xOffset)/resPitch;
				double y = (mouseY-yOffset)/resPitch;
				g.setStroke(Color.CYAN);
				for (int i = 0; i < 2000; i++) {
					if (x < 0 || y < 0 || x >= width || y >= length) break;
					float[] v = vectorfield[(int) x][(int) y];
					if (v == null) break;

					double nx = x + v[0];
					double ny = y + v[1];
					g.strokeLine(x*resPitch+xOffset, y*resPitch+yOffset, nx*resPitch+xOffset, ny*resPitch+yOffset);
					x = nx;
					y = ny;

				}

				x = (mouseX-xOffset)/resPitch;
				y = (mouseY-yOffset)/resPitch;
				float[] v = vectorfield[(int) x][(int) y];
				g.setLineWidth(2.0);
				g.setStroke(Color.WHITE);
				g.strokeLine(mouseX, mouseY, mouseX + v[0] * gain * 10, mouseY + v[1] * gain * 10);
			}
		}
	}
	
	private float[] vectorAtPixel(double x, double y) {
		double minWidth = Math.min(width, getWidth());
		double minLength = Math.min(length, getHeight());
		double resPitch = Math.min(PreviewPreferences.resolution.getValue(), Math.max(minWidth, minLength)) / Math.max(width, length);
		double renderWidth = width*resPitch;
		double renderHeight = length*resPitch;

		double xOffset = (getWidth() - renderWidth) / 2.0;
		double yOffset = (getHeight() - renderHeight) / 2.0;
		
		int mapX = (int) ((x - xOffset) / resPitch);
		int mapY = (int) ((y - yOffset) / resPitch);
		
		if (mapX > 0 && mapY > 0 && mapX < width && mapY < length) {
			return vectorfield[mapX][mapY];
		}
		
		return null;
	}
}
