/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.editor.preview;

import com.jogamp.opengl.awt.GLJPanel;

import net.worldsynth.datatype.AbstractDatatype;
import net.worldsynth.datatype.DatatypeHeightmap;
import net.worldsynth.glpreview.heightmap.Heightmap3DGLPreviewBufferedGL;
import net.worldsynth.standalone.ui.preview.AbstractPreviewRender;
import net.worldsynth.synth.SynthParameters;

public class Heightmap3DRender extends AbstractPreviewRender implements GLPreview {

	private final Heightmap3DGLPreviewBufferedGL openGL3DRenderer;
	
	private float[][] colorscale = {{0, 0, 0, 1},
			{0.2f, 1.0f, 1.0f, 0.0f},
			{0.5f, 0.0f, 1.0f, 0.0f},
			{0.7f, 0.75f, 0.56f, 0.2f},
			{0.8f, 0.5f, 0.5f, 0.5f},
			{1, 1, 1, 1}};
	
	public Heightmap3DRender() {
		openGL3DRenderer = new Heightmap3DGLPreviewBufferedGL();
	}
	
	@Override
	public void pushDataToRender(AbstractDatatype data, SynthParameters synthParameters) {
		DatatypeHeightmap castData = (DatatypeHeightmap) data;
		setHeightmap(castData, synthParameters.getNormalizedHeightmapHeight());
	}
	
	private void setHeightmap(DatatypeHeightmap heightmap, float normalizedHeight) {
		double size = Math.max(heightmap.extent.getWidth(), heightmap.extent.getLength());
		openGL3DRenderer.setHeightmap(heightmap.getHeightmap(), size, normalizedHeight);
	}
	
	public void setColorscale(float[][] colorscale) {
		this.colorscale = colorscale;
		openGL3DRenderer.setColorscale(this.colorscale);
	}

	@Override
	public GLJPanel getGLJpanel() {
		return openGL3DRenderer;
	}
}
