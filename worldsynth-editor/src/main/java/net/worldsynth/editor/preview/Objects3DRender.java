/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.editor.preview;

import com.jogamp.opengl.awt.GLJPanel;

import net.worldsynth.customobject.Block;
import net.worldsynth.customobject.CustomObject;
import net.worldsynth.datatype.AbstractDatatype;
import net.worldsynth.datatype.DatatypeObjects;
import net.worldsynth.glpreview.voxel.Object3DGLPreviewBufferedGL;
import net.worldsynth.standalone.ui.preview.AbstractPreviewRender;
import net.worldsynth.synth.SynthParameters;

public class Objects3DRender extends AbstractPreviewRender implements GLPreview {
	
	private final Object3DGLPreviewBufferedGL openGL3DRenderer;
	
	public Objects3DRender() {
		openGL3DRenderer = new Object3DGLPreviewBufferedGL();
	}
	
	@Override
	public void pushDataToRender(AbstractDatatype data, SynthParameters synthParameters) {
		DatatypeObjects castData = (DatatypeObjects) data;
		if (castData.objects.length > 0) {
			setObject(castData.objects[0].getObject(), synthParameters.getNormalizedHeightmapHeight());
		}
		else {
			setObject(new CustomObject(new Block[0]), synthParameters.getNormalizedHeightmapHeight());
		}
	}
	
	public void setObject(CustomObject object, float normalizedHeight) {
		openGL3DRenderer.setObject(object, normalizedHeight);
	}

	@Override
	public GLJPanel getGLJpanel() {
		return openGL3DRenderer;
	}
}
