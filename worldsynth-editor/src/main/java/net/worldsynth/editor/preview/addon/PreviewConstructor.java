/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.editor.preview.addon;

import net.worldsynth.datatype.AbstractDatatype;
import net.worldsynth.standalone.ui.preview.AbstractPreviewRender;

public abstract class PreviewConstructor {

	public abstract AbstractPreviewRender constructPreview();

	public abstract boolean isApplicableTo(AbstractDatatype data);

	public abstract String getStyleName();
}
