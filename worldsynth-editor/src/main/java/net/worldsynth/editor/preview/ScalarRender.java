/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.editor.preview;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.TextAlignment;
import net.worldsynth.datatype.AbstractDatatype;
import net.worldsynth.datatype.DatatypeScalar;
import net.worldsynth.editor.preferences.PreviewPreferences;
import net.worldsynth.standalone.ui.preview.AbstractPreviewRenderCanvas;
import net.worldsynth.synth.SynthParameters;

public class ScalarRender extends AbstractPreviewRenderCanvas {
	
	private double value;
	
	@Override
	public void pushDataToRender(AbstractDatatype data, SynthParameters synthParameters) {
		DatatypeScalar castData = (DatatypeScalar) data;
		this.value = castData.getValue();
		paint();
	}
	
	@Override
	public void paint() {
		GraphicsContext g = getGraphicsContext2D();
		g.setFill(PreviewPreferences.backgroundColor.getValue());
		g.fillRect(0, 0, getWidth(), getHeight());
		
		g.setFill(Color.WHITE);
		g.setFont(new Font("TimesRoman", 40));
		g.setTextAlign(TextAlignment.CENTER);
		g.fillText(String.valueOf(value), getWidth()/2, getHeight()/2);
	}
}
