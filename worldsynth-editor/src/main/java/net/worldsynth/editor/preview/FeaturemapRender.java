/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.editor.preview;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import net.worldsynth.datatype.AbstractDatatype;
import net.worldsynth.datatype.DatatypeFeaturemap;
import net.worldsynth.featurepoint.Featurepoint2D;
import net.worldsynth.editor.preferences.PreviewPreferences;
import net.worldsynth.standalone.ui.preview.AbstractPreviewRenderCanvas;
import net.worldsynth.synth.SynthParameters;

public class FeaturemapRender extends AbstractPreviewRenderCanvas {

	private Featurepoint2D[] points;
	
	public double x, z, width, length;
	
	@Override
	public void pushDataToRender(AbstractDatatype data, SynthParameters synthParameters) {
		DatatypeFeaturemap castData = (DatatypeFeaturemap) data;
		this.points = castData.getFeaturepoints();
		this.x = castData.extent.getX();
		this.z = castData.extent.getZ();
		this.width = castData.extent.getWidth();
		this.length = castData.extent.getLength();
		paint();
	}
	
	@Override
	public void paint() {
		GraphicsContext g = getGraphicsContext2D();
		g.setFill(PreviewPreferences.backgroundColor.getValue());
		g.fillRect(0, 0, getWidth(), getHeight());
		
		if (points != null) {
			double minWidth = Math.min(width, getWidth());
			double minLength = Math.min(length, getHeight());
			double resPitch = Math.min(PreviewPreferences.resolution.getValue(), Math.max(minWidth, minLength)) / Math.max(width, length);
			double renderWidth = width*resPitch;
			double renderHeight = length*resPitch;

			double xOffset = (getWidth() - renderWidth) / 2.0;
			double yOffset = (getHeight() - renderHeight) / 2.0;
			
			g.setFill(Color.BLACK);
			g.fillRect(xOffset, yOffset, width*resPitch, length*resPitch);

			g.setFill(Color.WHITE);
			for (Featurepoint2D p: points) {
				g.fillRect((p.getX()-x) * resPitch + xOffset - 1, (p.getZ()-z) * resPitch + yOffset - 1, 3, 3);
			}
		}
	}
}
