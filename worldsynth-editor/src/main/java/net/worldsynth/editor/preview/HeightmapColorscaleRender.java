/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.editor.preview;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.PixelFormat;
import javafx.scene.image.PixelWriter;
import javafx.scene.image.WritableImage;
import net.worldsynth.datatype.AbstractDatatype;
import net.worldsynth.datatype.DatatypeHeightmap;
import net.worldsynth.editor.preferences.PreviewPreferences;
import net.worldsynth.standalone.ui.preview.AbstractPreviewRenderCanvas;
import net.worldsynth.synth.SynthParameters;
import net.worldsynth.util.math.MathHelperScalar;

public class HeightmapColorscaleRender extends AbstractPreviewRenderCanvas {

	private float[][] heightmap;
	private WritableImage heightmapImage;
	
	private float[][] colorscale = {{0, 0, 0, 1},
			{0.2f, 1.0f, 1.0f, 0.0f},
			{0.5f, 0.0f, 1.0f, 0.0f},
			{0.7f, 0.75f, 0.56f, 0.2f},
			{0.8f, 0.5f, 0.5f, 0.5f},
			{1, 1, 1, 1}};
	
	@Override
	public void pushDataToRender(AbstractDatatype data, SynthParameters synthParameters) {
		DatatypeHeightmap castData = (DatatypeHeightmap) data;
		this.heightmap = castData.getHeightmap();

		int width = castData.mapPointsWidth;
		int length = castData.mapPointsLength;

		// Create image from heightmap
		heightmapImage = new WritableImage(width, length);
		PixelWriter pw = heightmapImage.getPixelWriter();
		int[] buffer = new int[width*length];

		float normalizedHeight = synthParameters.getNormalizedHeightmapHeight();
		for (int u = 0; u < width; u++) {
			for (int v = 0; v < length; v++) {
				buffer[u+v*width] = heightToColor(heightmap[u][v], normalizedHeight);
			}
		}
		pw.setPixels(0, 0, width, length, PixelFormat.getIntArgbPreInstance(), buffer, 0, width);

		paint();
	}
	
	public void setColorscale(float[][] colorscale) {
		this.colorscale = colorscale;
	}
	
	@Override
	public void paint() {
		GraphicsContext g = getGraphicsContext2D();
		g.setFill(PreviewPreferences.backgroundColor.getValue());
		g.fillRect(0, 0, getWidth(), getHeight());
		
		if (heightmap != null) {
			int width = heightmap.length;
			int length = heightmap[0].length;

			double minWidth = Math.min(width, getWidth());
			double minLength = Math.min(length, getHeight());
			double resPitch = Math.min(PreviewPreferences.resolution.getValue(), Math.max(minWidth, minLength)) / Math.max(width, length);
			double renderWidth = width*resPitch;
			double renderHeight = length*resPitch;

			double xOffset = (getWidth() - renderWidth) / 2.0;
			double yOffset = (getHeight() - renderHeight) / 2.0;

			g.drawImage(heightmapImage, xOffset, yOffset, renderWidth, renderHeight);
		}
	}
	
	private int heightToColor(float height, float normalizedHeight) {
		height = MathHelperScalar.clamp(height, 0.0f, normalizedHeight) / normalizedHeight;
		
		float[] lowRange = colorscale[0];
		float[] highRange = colorscale[1];
		for (int i = 1; i < colorscale.length; i++) {
			if (height <= colorscale[i][0] && height >= colorscale[i-1][0]) {
				lowRange = colorscale[i-1];
				highRange = colorscale[i];
			}
		}
		float[] colorspace = new float[3];
		float a = (height-lowRange[0]) / (highRange[0] - lowRange[0]);
		for (int j = 0; j < 3; j++) {
			colorspace[j] = lowRange[j+1] * (1-a) + highRange[j+1] * a;
		}
		return (255 << 24) | ((int) (colorspace[0] * 255.0f) << 16) | ((int) (colorspace[1] * 255.0f) << 8) | (int) (colorspace[2] * 255.0f);
	}
}
