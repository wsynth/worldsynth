/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.editor.preview;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.PixelFormat;
import javafx.scene.image.PixelWriter;
import javafx.scene.image.WritableImage;
import net.worldsynth.datatype.AbstractDatatype;
import net.worldsynth.datatype.DatatypeColormap;
import net.worldsynth.editor.preferences.PreviewPreferences;
import net.worldsynth.standalone.ui.preview.AbstractPreviewRenderCanvas;
import net.worldsynth.synth.SynthParameters;

public class ColormapRender extends AbstractPreviewRenderCanvas {
	
	private float[][][] colormap;
	private WritableImage colormapImage;
	
	@Override
	public void pushDataToRender(AbstractDatatype data, SynthParameters synthParameters) {
		DatatypeColormap castData = (DatatypeColormap) data;
		this.colormap = castData.getColormap();

		int width = castData.mapPointsWidth;
		int length = castData.mapPointsLength;

		// Create image from colormap
		colormapImage = new WritableImage(width, length);
		PixelWriter pw = colormapImage.getPixelWriter();
		int[] buffer = new int[width*length];

		for (int u = 0; u < width; u++) {
			for (int v = 0; v < length; v++) {
				float[] c = colormap[u][v];
				int red = (int) (c[0] * 255.0f);
				int green = (int) (c[1] * 255.0f);
				int blue = (int) (c[2] * 255.0f);
				buffer[u+v*width] = (255 << 24) | (red << 16) | (green << 8) | blue;
			}
		}
		pw.setPixels(0, 0, width, length, PixelFormat.getIntArgbPreInstance(), buffer, 0, width);

		paint();
	}
	
	@Override
	public void paint() {
		GraphicsContext g = getGraphicsContext2D();
		g.setFill(PreviewPreferences.backgroundColor.getValue());
		g.fillRect(0, 0, getWidth(), getHeight());
		
		if (colormap != null) {
			int width = colormap.length;
			int length = colormap[0].length;

			double minWidth = Math.min(width, getWidth());
			double minLength = Math.min(length, getHeight());
			double resPitch = Math.min(PreviewPreferences.resolution.getValue(), Math.max(minWidth, minLength)) / Math.max(width, length);
			double renderWidth = width*resPitch;
			double renderHeight = length*resPitch;

			double xOffset = (getWidth() - renderWidth) / 2.0;
			double yOffset = (getHeight() - renderHeight) / 2.0;

			g.drawImage(colormapImage, xOffset, yOffset, renderWidth, renderHeight);
		}
	}
}
