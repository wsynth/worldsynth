/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.editor.preview;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.PixelFormat;
import javafx.scene.image.PixelWriter;
import javafx.scene.image.WritableImage;
import net.worldsynth.datatype.AbstractDatatype;
import net.worldsynth.datatype.DatatypeHeightmap;
import net.worldsynth.editor.preferences.PreviewPreferences;
import net.worldsynth.standalone.ui.preview.AbstractPreviewRenderCanvas;
import net.worldsynth.synth.SynthParameters;

public class HeightmapGrayscaleRender extends AbstractPreviewRenderCanvas {
	
	private float[][] heightmap;
	private WritableImage heightmapImage;
	
	@Override
	public void pushDataToRender(AbstractDatatype data, SynthParameters synthParameters) {
		DatatypeHeightmap castData = (DatatypeHeightmap) data;
		this.heightmap = castData.getHeightmap();

		int width = castData.mapPointsWidth;
		int length = castData.mapPointsLength;

		// Create image from heightmap
		heightmapImage = new WritableImage(width, length);
		PixelWriter pw = heightmapImage.getPixelWriter();
		int[] buffer = new int[width*length];

		float normalizedHeight = synthParameters.getNormalizedHeightmapHeight();
		for (int u = 0; u < width; u++) {
			for (int v = 0; v < length; v++) {
				float s = heightmap[u][v] / normalizedHeight;
				int gray = (int) (Math.max(Math.min(s, 1.0f), 0.0f) * 255.0f);
				buffer[u+v*width] = (255 << 24) | (gray << 16) | (gray << 8) | gray;
			}
		}
		pw.setPixels(0, 0, width, length, PixelFormat.getIntArgbPreInstance(), buffer, 0, width);

		paint();
	}
	
	@Override
	public void paint() {
		GraphicsContext g = getGraphicsContext2D();
		g.setFill(PreviewPreferences.backgroundColor.getValue());
		g.fillRect(0, 0, getWidth(), getHeight());

		if (heightmap != null) {
			int width = heightmap.length;
			int length = heightmap[0].length;

			double minWidth = Math.min(width, getWidth());
			double minLength = Math.min(length, getHeight());
			double resPitch = Math.min(PreviewPreferences.resolution.getValue(), Math.max(minWidth, minLength)) / Math.max(width, length);
			double renderWidth = width*resPitch;
			double renderHeight = length*resPitch;

			double xOffset = (getWidth() - renderWidth) / 2.0;
			double yOffset = (getHeight() - renderHeight) / 2.0;

			g.drawImage(heightmapImage, xOffset, yOffset, renderWidth, renderHeight);
		}
	}
}
