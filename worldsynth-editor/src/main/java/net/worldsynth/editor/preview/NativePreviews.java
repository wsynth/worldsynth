/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.editor.preview;

import net.worldsynth.datatype.*;
import net.worldsynth.editor.preview.addon.IPreviewAddon;
import net.worldsynth.editor.preview.addon.PreviewConstructor;
import net.worldsynth.standalone.ui.preview.AbstractPreviewRender;

import java.util.ArrayList;
import java.util.List;

public class NativePreviews implements IPreviewAddon {

	@Override
	public List<PreviewConstructor> getAddonPreviews() {
		ArrayList<PreviewConstructor> previewConstructors = new ArrayList<>();

		// Null render
		previewConstructors.add(new PreviewConstructor() {
			@Override
			public AbstractPreviewRender constructPreview() {
				return new NullRender();
			}

			@Override
			public boolean isApplicableTo(AbstractDatatype data) {
				return data == null;
			}

			@Override
			public String getStyleName() {
				return "Null";
			}
		});

		// Heightmap grayscale render
		previewConstructors.add(new PreviewConstructor() {
			@Override
			public AbstractPreviewRender constructPreview() {
				return new HeightmapGrayscaleRender();
			}

			@Override
			public boolean isApplicableTo(AbstractDatatype data) {
				return data instanceof DatatypeHeightmap;
			}

			@Override
			public String getStyleName() {
				return "Grayscale";
			}
		});

		// Heightmap colorscale render
		previewConstructors.add(new PreviewConstructor() {
			@Override
			public AbstractPreviewRender constructPreview() {
				return new HeightmapColorscaleRender();
			}

			@Override
			public boolean isApplicableTo(AbstractDatatype data) {
				return data instanceof DatatypeHeightmap;
			}

			@Override
			public String getStyleName() {
				return "Colorscale";
			}
		});

		// Heightmap 3D render
		previewConstructors.add(new PreviewConstructor() {
			@Override
			public AbstractPreviewRender constructPreview() {
				return new Heightmap3DRender();
			}

			@Override
			public boolean isApplicableTo(AbstractDatatype data) {
				return data instanceof DatatypeHeightmap;
			}

			@Override
			public String getStyleName() {
				return "3D";
			}
		});

		// Scalar render
		previewConstructors.add(new PreviewConstructor() {
			@Override
			public AbstractPreviewRender constructPreview() {
				return new ScalarRender();
			}

			@Override
			public boolean isApplicableTo(AbstractDatatype data) {
				return data instanceof DatatypeScalar;
			}

			@Override
			public String getStyleName() {
				return "Scalar";
			}
		});

		// Colormap render
		previewConstructors.add(new PreviewConstructor() {
			@Override
			public AbstractPreviewRender constructPreview() {
				return new ColormapRender();
			}

			@Override
			public boolean isApplicableTo(AbstractDatatype data) {
				return data instanceof DatatypeColormap;
			}

			@Override
			public String getStyleName() {
				return "Colormap";
			}
		});

		// Overlay render
		previewConstructors.add(new PreviewConstructor() {
			@Override
			public AbstractPreviewRender constructPreview() {
				return new HeightmapOverlay3DRender();
			}

			@Override
			public boolean isApplicableTo(AbstractDatatype data) {
				return data instanceof DatatypeHeightmapOverlay;
			}

			@Override
			public String getStyleName() {
				return "Overlay";
			}
		});

		// Materialmap render
		previewConstructors.add(new PreviewConstructor() {
			@Override
			public AbstractPreviewRender constructPreview() {
				return new MaterialmapRender();
			}

			@Override
			public boolean isApplicableTo(AbstractDatatype data) {
				return data instanceof DatatypeMaterialmap;
			}

			@Override
			public String getStyleName() {
				return "Materialmap";
			}
		});

		// Biomemap render
		previewConstructors.add(new PreviewConstructor() {
			@Override
			public AbstractPreviewRender constructPreview() {
				return new BiomemapRender();
			}

			@Override
			public boolean isApplicableTo(AbstractDatatype data) {
				return data instanceof DatatypeBiomemap;
			}

			@Override
			public String getStyleName() {
				return "Biomemap";
			}
		});

		// Blockspace render
		previewConstructors.add(new PreviewConstructor() {
			@Override
			public AbstractPreviewRender constructPreview() {
				return new Blockspace3DRender();
			}

			@Override
			public boolean isApplicableTo(AbstractDatatype data) {
				return data instanceof DatatypeBlockspace;
			}

			@Override
			public String getStyleName() {
				return "Blockspace";
			}
		});

		// Custom object render
		previewConstructors.add(new PreviewConstructor() {
			@Override
			public AbstractPreviewRender constructPreview() {
				return new Objects3DRender();
			}

			@Override
			public boolean isApplicableTo(AbstractDatatype data) {
				return data instanceof DatatypeObjects;
			}

			@Override
			public String getStyleName() {
				return "Object";
			}
		});

		// Featuremap render
		previewConstructors.add(new PreviewConstructor() {
			@Override
			public AbstractPreviewRender constructPreview() {
				return new FeaturemapRender();
			}

			@Override
			public boolean isApplicableTo(AbstractDatatype data) {
				return data instanceof DatatypeFeaturemap;
			}

			@Override
			public String getStyleName() {
				return "Featuremap";
			}
		});

		// Featurespace render
		previewConstructors.add(new PreviewConstructor() {
			@Override
			public AbstractPreviewRender constructPreview() {
				return new Featurespace3DRender();
			}

			@Override
			public boolean isApplicableTo(AbstractDatatype data) {
				return data instanceof DatatypeFeaturespace;
			}

			@Override
			public String getStyleName() {
				return "Featurespace";
			}
		});

		// Vectormap render
		previewConstructors.add(new PreviewConstructor() {
			@Override
			public AbstractPreviewRender constructPreview() {
				return new VectormapRender();
			}

			@Override
			public boolean isApplicableTo(AbstractDatatype data) {
				return data instanceof DatatypeVectormap;
			}

			@Override
			public String getStyleName() {
				return "Vectormap";
			}
		});

		return previewConstructors;
	}
}
