/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.standalone.ui.stage;

import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleButton;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.*;
import net.worldsynth.editor.WorldSynthEditorApp;

public class NameEditorStage extends PinnableUtilityStage {
	private final TextField customNameField;

	public NameEditorStage(String title, Nameable nameable) {
		setTitle(title);

		BorderPane rootPane = new BorderPane();

		customNameField = new TextField(nameable.getName());
		customNameField.setPrefColumnCount(20);

		// Pin button
		Image pinIcon = new Image(NameEditorStage.class.getClassLoader().getResourceAsStream("pin16x16_white.png"));
		ToggleButton pinButton = new ToggleButton(null, new ImageView(pinIcon));
		pinButton.setOnAction(e -> {
			setPinned(pinButton.isSelected());
		});

		Button cancelButton = new Button("Cancel");
		cancelButton.setOnAction(e -> {
			close();
		});

		Button applyButton = new Button("Apply");
		applyButton.setOnAction(e -> {
			nameable.setName(customNameField.getText());
		});

		Button okButton = new Button("OK");
		okButton.setOnAction(e -> {
			nameable.setName(customNameField.getText());
			close();
		});

		HBox buttonsPane = new HBox();
		buttonsPane.setPadding(new Insets(0.0, 10.0, 5.0, 10.0));
		buttonsPane.getChildren().add(pinButton);

		Pane spacerPane = new Pane();
		HBox.setHgrow(spacerPane, Priority.SOMETIMES);
		buttonsPane.getChildren().add(spacerPane);

		buttonsPane.getChildren().add(cancelButton);
		buttonsPane.getChildren().add(applyButton);
		buttonsPane.getChildren().add(okButton);

		GridPane namingPane = new GridPane();
		namingPane.add(new Label("Custom name"), 0, 0);
		namingPane.add(customNameField, 1, 0);
		namingPane.setPadding(new Insets(10.0));
		namingPane.setHgap(10.0);

		// Setup keybinds
		namingPane.setOnKeyReleased(e -> {
			if (e.getCode() == KeyCode.ENTER) {
				nameable.setName(customNameField.getText());
			}
		});

		rootPane.setCenter(namingPane);
		rootPane.setBottom(buttonsPane);

		Scene scene = new Scene(rootPane, rootPane.getPrefWidth(), rootPane.getPrefHeight());
		scene.getStylesheets().add(WorldSynthEditorApp.stylesheet);
		setScene(scene);
		sizeToScene();
		show();
	}

	public interface Nameable {

		String getName();

		void setName(String name);
	}
}
