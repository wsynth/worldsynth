/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.patcher.ui.patcheditor;

import java.util.List;

import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import net.worldsynth.patch.ModuleConnector;
import net.worldsynth.patcher.ui.patcheditor.PatchEditorPane.MouseListener;

class MouseActionConnectorDragStart implements MouseAction {
	
	private static final MouseActionEventFilter FILTER = new MouseActionEventFilter(
			MouseEvent.MOUSE_DRAGGED, // Mouse event type
			MouseButton.PRIMARY, // Mouse button
			1, // Mouse click count
			EventFilterInputState.UNUSED, // Mouse over module
			EventFilterInputState.YES, // Mouse over module IO
			EventFilterInputState.NO, // Temp module active
			EventFilterInputState.NO, // Temp connector active
			EventFilterInputState.NO, // Temp patch active
			EventFilterInputState.NO); // Area selection active
	
	@Override
	public boolean filterEvent(PatchEditorPane editor, MouseListener t) {
		return FILTER.validateFilter(editor, t);
	}

	@Override
	public void action(PatchEditorPane editor, MouseListener t) {
		if (editor.wrapperIoOver.isInput()) {
			List<ModuleConnector> mc = editor.patch.getModuleConnectorsByWrapperIo(editor.wrapperIoOver);
			if (!mc.isEmpty()) {
				for (ModuleConnector c : mc) {
					editor.removeModuleConnector(c, true, true);
					t.replacingConnector = c;
				}
			}
			editor.tempConnector = new ModuleConnector(null, null, editor.wrapperOver, editor.wrapperIoOver);
		}
		else {
			editor.tempConnector = new ModuleConnector(editor.wrapperOver, editor.wrapperIoOver, null, null);
		}
		
		t.dragConnector = true;
		editor.repaint();editor.repaint();
	}
}