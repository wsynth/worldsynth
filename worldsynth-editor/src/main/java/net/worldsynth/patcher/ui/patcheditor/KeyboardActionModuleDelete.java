/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.patcher.ui.patcheditor;

import javafx.scene.input.KeyEvent;
import net.worldsynth.patch.ModuleWrapper;
import net.worldsynth.patcher.preferences.PatcherPreferences;
import net.worldsynth.patcher.ui.patcheditor.PatchEditorPane.KeyboardListener;

class KeyboardActionModuleDelete implements KeyboardAction {
	
	private static final KeyboardActionEventFilter FILTER_DELETE = new KeyboardActionEventFilter(
			KeyEvent.KEY_RELEASED, // Key event type
			PatcherPreferences.patcherKeybindDelete, // Key code combination
			EventFilterInputState.UNUSED, // Mouse over module
			EventFilterInputState.UNUSED, // Mouse over module IO
			EventFilterInputState.NO, // Temp module active
			EventFilterInputState.NO, // Temp connector active
			EventFilterInputState.NO, // Temp patch active
			EventFilterInputState.NO); // Area selection active
	
	@Override
	public boolean filterEvent(PatchEditorPane editor, KeyboardListener t) {
		return FILTER_DELETE.validateFilter(editor, t);
	}
	
	@Override
	public void action(PatchEditorPane editor, KeyboardListener t) {
		if (editor.selectedWrappers.size() == 1) {
			editor.removeModuleWrapper(editor.selectedWrappers.get(0), true, true, true);
		}
		else {
			editor.removeModuleWrappers(editor.selectedWrappers.toArray(new ModuleWrapper[0]), true, true);
		}
		editor.clearSelectedWrappers();

		// Update the preview after removing the module
		editor.getEditorSession().updatePreview();
	}
}
