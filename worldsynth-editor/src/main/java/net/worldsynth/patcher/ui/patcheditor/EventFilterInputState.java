/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.patcher.ui.patcheditor;

enum EventFilterInputState {
	YES(true),
	NO(false),
	UNUSED(false);
	
	private boolean state;
	
	EventFilterInputState(boolean state) {
		this.state = state;
	}
	
	public boolean getState() {
		return state;
	}
}
