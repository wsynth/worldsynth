/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.patcher.ui.patcheditor;

import java.awt.Polygon;
import java.util.ArrayList;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import net.worldsynth.patch.ModuleWrapper;
import net.worldsynth.editor.ui.navcanvas.Coordinate;
import net.worldsynth.editor.ui.navcanvas.Pixel;

class SelectionLasso {
	
	private ArrayList<double[]> points = new ArrayList<double[]>();
	
	public SelectionLasso(double startCoordinateX, double startCoordinateY) {
		double[] newPoint = {startCoordinateX, startCoordinateY};
		points.add(newPoint);
		points.add(newPoint.clone());
	}
	
	public void setCurrentCoordinate(double coordinateX, double coordinateY) {
		double[] latestPoint = points.get(points.size() - 2);
		double[] currentPoint = {coordinateX, coordinateY};
		points.set(points.size() - 1, currentPoint);
		if (dist(latestPoint, currentPoint) > 10) {
			points.add(currentPoint.clone());
		}
	}
	
	public double[][] getPoints() {
		double[][] pointsArray = new double[0][2];
		pointsArray = this.points.toArray(pointsArray);
		return pointsArray;
	}
	
	public ModuleWrapper[] getModuleWrappersInside(PatchEditorPane patchEditor) {
		ArrayList<ModuleWrapper> containedModuleWrappersList = new ArrayList<ModuleWrapper>();
		
		//Build polygon
		Polygon polygon = new Polygon();
		for (double[] p: points) {
			polygon.addPoint((int)p[0], (int)p[1]);
		}
		
		//check for containment
		for (ModuleWrapper mw: patchEditor.getPatch().getModuleWrapperList()) {
			if (polygon.contains(mw.posX, mw.posY, mw.wrapperWidth, mw.wrapperHeight)) {
				containedModuleWrappersList.add(mw);
			}
		}
		
		//Return contained devices
		ModuleWrapper[] list = new ModuleWrapper[0];
		return containedModuleWrappersList.toArray(list);
	}
	
	private float dist(double[] p1, double[] p2) {
		double dx = p2[0] - p1[0];
		double dy = p2[1] - p1[1];
		return (float) Math.sqrt((dx * dx) + (dy * dy));
	}
	
	public void paint(PatchEditorPane editor, GraphicsContext g) {
		double[][] lassoPoints = getPoints();
		double[] xPoints = new double[lassoPoints.length];
		double[] yPoints = new double[lassoPoints.length];
		for (int i = 0; i < getPoints().length; i++) {
			Coordinate coord = new Coordinate(lassoPoints[i][0], lassoPoints[i][1]);
			Pixel pixel = new Pixel(coord, editor);
			xPoints[i] = pixel.x;
			yPoints[i] = pixel.y;
		}
		
		g.setStroke(Color.WHITE);
		g.setLineWidth(2.0);
		g.setLineDashes(10.0, 10.0);
		g.strokePolygon(xPoints, yPoints, lassoPoints.length);
		g.setLineDashes();
	}
}
