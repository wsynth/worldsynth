/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.patcher.ui.patcheditor;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.event.EventType;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyEvent;

class KeyboardActionEventFilter {
	private final EventType<KeyEvent> keyEventType;
	private final ObjectProperty<KeyCodeCombination> keyCodeCombination;
	
	private final EventFilterInputState mouseOverModule;
	private final EventFilterInputState mouseOverIo;
	private final EventFilterInputState tempModule;
	private final EventFilterInputState tempConnector;
	private final EventFilterInputState tempPatch;
	
	private final EventFilterInputState areaSelector;

	public KeyboardActionEventFilter(EventType<KeyEvent> keyEventType, KeyCodeCombination keyCombination, EventFilterInputState mouseOverModule, EventFilterInputState mouseOverIo, EventFilterInputState tempModule, EventFilterInputState tempConnector, EventFilterInputState tempPatch, EventFilterInputState areaSelector) {
		this.keyEventType = keyEventType;
		this.keyCodeCombination = new SimpleObjectProperty<>(keyCombination);

		this.mouseOverModule = mouseOverModule;
		this.mouseOverIo = mouseOverIo;
		this.tempModule = tempModule;
		this.tempConnector = tempConnector;
		this.tempPatch = tempPatch;

		this.areaSelector = areaSelector;
	}

	public KeyboardActionEventFilter(EventType<KeyEvent> keyEventType, ObjectProperty<KeyCodeCombination> keyCombination, EventFilterInputState mouseOverModule, EventFilterInputState mouseOverIo, EventFilterInputState tempModule, EventFilterInputState tempConnector, EventFilterInputState tempPatch, EventFilterInputState areaSelector) {
		this.keyEventType = keyEventType;
		this.keyCodeCombination = keyCombination;

		this.mouseOverModule = mouseOverModule;
		this.mouseOverIo = mouseOverIo;
		this.tempModule = tempModule;
		this.tempConnector = tempConnector;
		this.tempPatch = tempPatch;

		this.areaSelector = areaSelector;
	}
	
	public final boolean validateFilter(PatchEditorPane editor, PatchEditorPane.KeyboardListener t) {
		if (keyEventType != t.currentEvent.getEventType()) {
			return false;
		}
		else if (!keyCodeCombination.get().match(t.currentEvent)) {
			return false;
		}

		else if (mouseOverModule != EventFilterInputState.UNUSED && mouseOverModule.getState() != (editor.wrapperOver != null)) {
			return false;
		}
		else if (mouseOverIo != EventFilterInputState.UNUSED && mouseOverIo.getState() != (editor.wrapperIoOver != null)) {
			return false;
		}
		else if (tempModule != EventFilterInputState.UNUSED && tempModule.getState() != (editor.tempWrapper != null)) {
			return false;
		}
		else if (tempConnector != EventFilterInputState.UNUSED && tempConnector.getState() != (editor.tempConnector != null)) {
			return false;
		}
		else if (tempPatch != EventFilterInputState.UNUSED && tempPatch.getState() != (editor.tempPatch != null)) {
			return false;
		}
		else if (areaSelector != EventFilterInputState.UNUSED && areaSelector.getState() != (editor.selectionRectangle != null || editor.selectionLasso != null)) {
			return false;
		}
		
		return true;
	}
}
