/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.patcher.ui.patcheditor;

import javafx.event.EventType;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import net.worldsynth.patcher.ui.patcheditor.PatchEditorPane.MouseListener;

class MouseActionEventFilter {
	private final EventType<MouseEvent> mouseEventType;
	private final MouseButton mouseButton;
	private final int mouseClickCount;
	
	private final EventFilterInputState mouseOverModule;
	private final EventFilterInputState mouseOverIo;
	private final EventFilterInputState tempModule;
	private final EventFilterInputState tempConnector;
	private final EventFilterInputState tempPatch;
	
	private final EventFilterInputState areaSelector;
	
	/**
	 * @param mouseEventType
	 * @param mouseButton
	 * @param mouseClickCount
	 * @param mouseOverModule
	 * @param mouseOverIo
	 * @param tempModule
	 * @param tempConnector
	 * @param tempPatch
	 * @param areaSelector
	 */
	public MouseActionEventFilter(EventType<MouseEvent> mouseEventType, MouseButton mouseButton, int mouseClickCount, EventFilterInputState mouseOverModule, EventFilterInputState mouseOverIo, EventFilterInputState tempModule, EventFilterInputState tempConnector, EventFilterInputState tempPatch, EventFilterInputState areaSelector) {
		this.mouseEventType = mouseEventType;
		this.mouseButton = mouseButton;
		this.mouseClickCount = mouseClickCount;
		
		this.mouseOverModule = mouseOverModule;
		this.mouseOverIo = mouseOverIo;
		this.tempModule = tempModule;
		this.tempConnector = tempConnector;
		this.tempPatch = tempPatch;
		
		this.areaSelector = areaSelector;
	}
	
	public final boolean validateFilter(PatchEditorPane editor, MouseListener t) {

		if (mouseEventType != t.currentEvent.getEventType()) {
			return false;
		}
		else if (mouseButton != t.currentEvent.getButton()) {
			return false;
		}
		else if (mouseClickCount > 0 && mouseClickCount != t.currentEvent.getClickCount()) {
			return false;
		}
		
		else if (mouseOverModule != EventFilterInputState.UNUSED && mouseOverModule.getState() != (editor.wrapperOver != null)) {
			return false;
		}
		else if (mouseOverIo != EventFilterInputState.UNUSED && mouseOverIo.getState() != (editor.wrapperIoOver != null)) {
			return false;
		}
		else if (tempModule != EventFilterInputState.UNUSED && tempModule.getState() != (editor.tempWrapper != null)) {
			return false;
		}
		else if (tempConnector != EventFilterInputState.UNUSED && tempConnector.getState() != (editor.tempConnector != null)) {
			return false;
		}
		else if (tempPatch != EventFilterInputState.UNUSED && tempPatch.getState() != (editor.tempPatch != null)) {
			return false;
		}
		else if (areaSelector != EventFilterInputState.UNUSED && areaSelector.getState() != (editor.selectionRectangle != null || editor.selectionLasso != null)) {
			return false;
		}
		
		return true;
	}
}
