/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.patcher.ui.patcheditor;

import java.io.File;
import java.util.ArrayList;

import com.fasterxml.jackson.databind.JsonNode;

import net.worldsynth.patch.ModuleConnector;
import net.worldsynth.patch.ModuleWrapper;
import net.worldsynth.patch.Patch;

public class TempPatch extends Patch {
	
	private final Patch parentPatch;

	public TempPatch(ArrayList<ModuleWrapper> moduleWrappers, ArrayList<ModuleConnector> moduleConnectors, Patch parentPatch) {
		super(parentPatch.getParentSynth());
		this.parentPatch = parentPatch;
		
		this.moduleWrappers = moduleWrappers;
		this.moduleConnectors = moduleConnectors;
		
		reinstance();
		recenterSynth();
	}
	
	public TempPatch(JsonNode patchJson, File source, Patch parentPatch) {
		super(parentPatch.getParentSynth());
		this.parentPatch = parentPatch;
		
		fromJson(patchJson, source);
		reidentify();
		recenterSynth();
	}
	
	public void reinstance() {
		fromJson(toJson(null), null);
		reidentify();
	}
	
	private void reidentify() {
		for (ModuleWrapper mw: moduleWrappers) {
			mw.wrapperID = getNewModuleId(mw);
		}
	}
	@Override
	public String getNewModuleId(ModuleWrapper module) {
		int i = 0;
		String name = module.module.getClass().getSimpleName().toLowerCase();
		
		while (parentPatch.getModuleWrapperByID(name + i) != null || (getModuleWrapperByID(name + i) != null && getModuleWrapperByID(name + i) != module)) {
			i++;
		}
		return name + i;
	}
	
	public void setCenterTo(double x, double y) {
		recenterSynth();
		
		for (ModuleWrapper d: getModuleWrapperList()) {
			d.posX = d.posX + x;
			d.posY = d.posY + y;
		}
	}
	
	private void recenterSynth() {
		//Average positions and find the averaged center of the blueprint
		float x = 0;
		float y = 0;
		
		for (ModuleWrapper d: getModuleWrapperList()) {
			x += d.posX;
			y += d.posY;
		}
		x /= getModuleWrapperList().size();
		y /= getModuleWrapperList().size();
		
		//Reidentify wrappers from the synth to avoid id conflictions and reposition
		for (ModuleWrapper d: getModuleWrapperList()) {
			d.posX = d.posX - x;
			d.posY = d.posY - y;
		}
	}
	
	@Override
	protected ArrayList<ModuleWrapper> extractModuleWrappers(JsonNode node, File source) {
		ArrayList<ModuleWrapper> devices = new ArrayList<ModuleWrapper>();
		
		for (JsonNode de: node) {
			devices.add(new ModuleWrapper(de, source, parentPatch));
		}
		
		return devices;
	}
	
	@Override
	protected ArrayList<ModuleConnector> extractModuleConnectors(JsonNode moduleConnectorsJson) {
		ArrayList<ModuleConnector> modulesConnectors = new ArrayList<ModuleConnector>();
		
		for (JsonNode dce: moduleConnectorsJson) {
			modulesConnectors.add(new ModuleConnector(dce, this));
		}
		
		return modulesConnectors;
	}
}
