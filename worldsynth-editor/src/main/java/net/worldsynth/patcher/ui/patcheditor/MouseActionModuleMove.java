/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.patcher.ui.patcheditor;

import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import net.worldsynth.patch.ModuleWrapper;
import net.worldsynth.patcher.ui.patcheditor.PatchEditorPane.MouseListener;
import net.worldsynth.editor.ui.navcanvas.Coordinate;

import java.util.LinkedHashMap;

class MouseActionModuleMove implements MouseAction {
	
	private static final MouseActionEventFilter FILTER = new MouseActionEventFilter(
			MouseEvent.MOUSE_DRAGGED, // Mouse event type
			MouseButton.PRIMARY, // Mouse button
			0, // Mouse click count
			EventFilterInputState.YES, // Mouse over module
			EventFilterInputState.NO, // Mouse over module IO
			EventFilterInputState.NO, // Temp module active
			EventFilterInputState.NO, // Temp connector active
			EventFilterInputState.NO, // Temp patch active
			EventFilterInputState.NO); // Area selection active
	
	@Override
	public boolean filterEvent(PatchEditorPane editor, MouseListener t) {
		return FILTER.validateFilter(editor, t);
	}

	@Override
	public void action(PatchEditorPane editor, MouseListener t) {
		double diffX = t.lastMouseCoordinateX - t.currentMouseCoordinateX;
		double diffY = t.lastMouseCoordinateY - t.currentMouseCoordinateY;
		
		// Create a move event with the old coordinates at first move
		if (t.moveOldCoordinates == null) {
			t.moveOldCoordinates = new LinkedHashMap<>();

			// Move multiple selected
			if (editor.selectedWrappers.contains(editor.wrapperOver) && !t.currentEvent.isShortcutDown()) {
				for (int i = 0; i < editor.selectedWrappers.size(); i++) {
					ModuleWrapper movingWrapper = editor.selectedWrappers.get(i);
					t.moveOldCoordinates.put(movingWrapper, new Coordinate(movingWrapper.posX, movingWrapper.posY));
				}
			}
			// Move single under cursor
			else {
				ModuleWrapper movingWrapper = editor.wrapperOver;
				t.moveOldCoordinates.put(movingWrapper, new Coordinate(movingWrapper.posX, movingWrapper.posY));
			}
		}

		// Move module wrappers
		for (ModuleWrapper subject: t.moveOldCoordinates.keySet()) {
			subject.posX -= diffX;
			subject.posY -= diffY;
		}
		
		editor.repaint();
	}

}
