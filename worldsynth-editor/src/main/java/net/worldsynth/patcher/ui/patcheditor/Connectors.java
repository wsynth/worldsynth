/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.patcher.ui.patcheditor;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import javafx.scene.shape.StrokeLineCap;
import net.worldsynth.patch.ModuleConnector;
import net.worldsynth.patcher.preferences.PatcherPreferences;
import net.worldsynth.editor.ui.navcanvas.Coordinate;
import net.worldsynth.editor.ui.navcanvas.Pixel;
import net.worldsynth.util.math.MathHelperScalar;

/**
 * This class contains various methods used in relations to connectors in the
 * patch editor.
 */
class Connectors {
	
	/**
	 * Paints the connector in the given editor with the graphics context.
	 * 
	 * @param editor
	 * @param g
	 * @param connector
	 */
	public static void paintConnector(PatchEditorPane editor, GraphicsContext g, ModuleConnector connector) {
		paintConnector(editor, g, connector, false);
	}

	/**
	 * Paints the connector which may be highlighted in the given editor with the graphics context.
	 * 
	 * @param editor
	 * @param g
	 * @param connector
	 * @param highlight
	 */
	public static void paintConnector(PatchEditorPane editor, GraphicsContext g, ModuleConnector connector, boolean highlight) {
		double startX = 0;
		double startY = 0;
		double endX = 0;
		double endY = 0;
		
		if (connector.module1 != null && connector.module1Io != null) {
			// Connector is connected to the output of a wrapper
			double x = connector.module1.posX + connector.module1Io.posX;
			double y = connector.module1.posY + connector.module1Io.posY;
			
			Pixel pixel = new Pixel(new Coordinate(x, y), editor);
			startX = pixel.x;
			startY = pixel.y;
		}
		else if (editor.wrapperIoOver != null) {
			// An IO has active hover over in the editor
			double x = editor.wrapperOver.posX + editor.wrapperIoOver.posX;
			double y = editor.wrapperOver.posY + editor.wrapperIoOver.posY;
			
			Pixel pixel = new Pixel(new Coordinate(x, y), editor);
			startX = pixel.x;
			startY = pixel.y;
		}
		else {
			// Draw the connector to the mouse pointer
			startX = editor.mouseListener.currentEvent.getX();
			startY = editor.mouseListener.currentEvent.getY();
		}
		
		if (connector.module2 != null && connector.module2Io != null) {
			// Connector is connected to the input of a wrapper
			double x = connector.module2.posX + connector.module2Io.posX;
			double y = connector.module2.posY + connector.module2Io.posY;
			
			Pixel pixel = new Pixel(new Coordinate(x, y), editor);
			endX = pixel.x;
			endY = pixel.y;
		}
		else if (editor.wrapperIoOver != null) {
			// An IO has active hover over in the editor
			double x = editor.wrapperOver.posX + editor.wrapperIoOver.posX;
			double y = editor.wrapperOver.posY + editor.wrapperIoOver.posY;
			
			Pixel pixel = new Pixel(new Coordinate(x, y), editor);
			endX = pixel.x;
			endY = pixel.y;
		}
		else {
			// Draw the connector to the mouse pointer
			endX = editor.mouseListener.currentEvent.getX();
			endY = editor.mouseListener.currentEvent.getY();
		}
		
		paintConnector(editor, g, startX, startY, endX, endY, highlight);
	}
	
	private static void paintConnector(PatchEditorPane editor, GraphicsContext g, double startX, double startY, double endX, double endY, boolean highlight) {
		g.setLineCap(StrokeLineCap.ROUND);
		g.setStroke(Color.DARKGRAY);
		g.setLineWidth(2*editor.zoom);
		if (highlight) {
			g.setStroke(Color.WHITE);
			g.setLineWidth(4*editor.zoom);
		}
		
		switch (PatcherPreferences.connectorStyle.getValue()) {
			case STRAIGHT:
				g.strokeLine(startX, startY, endX, endY);
				break;
			case ANGULAR:
				g.strokeLine(startX, startY, startX+30*editor.zoom, startY);
				g.strokeLine(startX+30*editor.zoom, startY, endX-30*editor.zoom, endY);
				g.strokeLine(endX-30*editor.zoom, endY, endX, endY);
				break;
			case SQUARE:
				double midpointX = (startX + endX) / 2.0;
				g.strokeLine(startX, startY, midpointX, startY);
				g.strokeLine(midpointX, startY, midpointX, endY);
				g.strokeLine(midpointX, endY, endX, endY);
				break;
			case BEZIER:
				double x1 = startX, x2 = Math.max(startX + 50.0*editor.zoom, startX + (endX-startX)/2.0), x3 = Math.min(endX - 50.0*editor.zoom, endX - (endX-startX)/2.0), x4 = endX;
				double y1 = startY, y2 = startY, y3 = endY, y4 = endY;
				
				double x = x1, y = y1;
				for (double b = 0.0; b < 1.0; b += 0.05) {
					double nx = p(x1, x2, x3, x4, b+0.05);
					double ny = p(y1, y2, y3, y4, b+0.05);
					g.strokeLine(x, y, nx, ny);
					x = nx;
					y = ny;
				}
				break;
			case METRO:
				midpointX = (startX + endX) / 2.0;
				double diffX = endX - startX;
				double diffY = endY - startY;
				double signY = Math.signum(diffY);
				double diagonal = Math.min(Math.abs(diffX), Math.abs(diffY));
				if (diffX > 0) {
					if (diffX > diagonal) {
						g.strokeLine(startX, startY, midpointX-diagonal/2, startY);
						g.strokeLine(midpointX-diagonal/2, startY, midpointX+diagonal/2, endY);
						g.strokeLine(midpointX+diagonal/2, endY, endX, endY);
					}
					else {
						g.strokeLine(midpointX-diagonal/2, startY, midpointX, startY+signY*diagonal/2);
						g.strokeLine(midpointX, startY+signY*diagonal/2, midpointX, endY-signY*diagonal/2);
						g.strokeLine(midpointX, endY-signY*diagonal/2, midpointX+diagonal/2, endY);
					}
				}
				else {
					g.strokeLine(startX, startY, startX-diagonal/2, startY+signY*diagonal/2);
					g.strokeLine(startX-diagonal/2, startY+signY*diagonal/2, endX+diagonal/2, endY-signY*diagonal/2);
					g.strokeLine(endX+diagonal/2, endY-signY*diagonal/2, endX, endY);
				}
				break;
			default:
				g.strokeLine(startX, startY, endX, endY);
		}
	}

	public static boolean isCoordinateOverConector(PatchEditorPane editor, ModuleConnector connector, double xCoord, double yCoord, double offset) {
		// Connector is connected to the output of a wrapper
		double startX = connector.module1.posX + connector.module1Io.posX;
		double startY = connector.module1.posY + connector.module1Io.posY;

		// Connector is connected to the input of a wrapper
		double endX = connector.module2.posX + connector.module2Io.posX;
		double endY = connector.module2.posY + connector.module2Io.posY;

		// Find the bounding vox of the connector
		double x1 = Math.min(startX, endX);
		double y1 = Math.min(startY, endY);
		double x2 = Math.max(startX, endX);
		double y2 = Math.max(startY, endY);

		if (PatcherPreferences.connectorStyle.getValue() == PatcherPreferences.ConnectorStyle.ANGULAR) {
			// special case for angular style connectors
			x1 -= 30*editor.zoom;
			x2 += 30*editor.zoom;
		}

		// Check if mouse is within bounding box and then the distance to the connector
		if (xCoord+offset >= x1 && xCoord-offset <= x2 && yCoord+offset >= y1 && yCoord-offset <= y2) {
			double d = connectorDist(editor, xCoord, yCoord, startX, startY, endX, endY, PatcherPreferences.connectorStyle.getValue());
			return d <= offset;
		}
		return false;
	}

	/**
	 * Calculate the distance from the mouse to the drawn connector between two points
	 */
	private static double connectorDist(PatchEditorPane editor, double mouseX, double mouseY, double startX, double startY, double endX, double endY, PatcherPreferences.ConnectorStyle style) {
		double dist = Double.POSITIVE_INFINITY;

		switch (style) {
			case STRAIGHT:
				dist = lineDistMin(mouseX, mouseY, startX, startY, endX, endY, dist);
				break;
			case ANGULAR:
				dist = lineDistMin(mouseX, mouseY, startX, startY, startX+30*editor.zoom, startY, dist);
				dist = lineDistMin(mouseX, mouseY,startX+30*editor.zoom, startY, endX-30*editor.zoom, endY, dist);
				dist = lineDistMin(mouseX, mouseY,endX-30*editor.zoom, endY, endX, endY, dist);
				break;
			case SQUARE:
				double midpointX = (startX + endX) / 2.0;
				dist = lineDistMin(mouseX, mouseY, startX, startY, midpointX, startY, dist);
				dist = lineDistMin(mouseX, mouseY, midpointX, startY, midpointX, endY, dist);
				dist = lineDistMin(mouseX, mouseY, midpointX, endY, endX, endY, dist);
				break;
			case BEZIER:
				double x1 = startX, x2 = Math.max(startX + 50.0*editor.zoom, startX + (endX-startX)/2.0), x3 = Math.min(endX - 50.0*editor.zoom, endX - (endX-startX)/2.0), x4 = endX;
				double y1 = startY, y2 = startY, y3 = endY, y4 = endY;

				double x = x1, y = y1;
				for (double b = 0.0; b < 1.0; b += 0.05) {
					double nx = p(x1, x2, x3, x4, b+0.05);
					double ny = p(y1, y2, y3, y4, b+0.05);
					dist = lineDistMin(mouseX, mouseY, x, y, nx, ny, dist);
					x = nx;
					y = ny;
				}
				break;
			case METRO:
				midpointX = (startX + endX) / 2.0;
				double diffX = endX - startX;
				double diffY = endY - startY;
				double signY = Math.signum(diffY);
				double diagonal = Math.min(Math.abs(diffX), Math.abs(diffY));
				if (diffX > 0) {
					if (diffX > diagonal) {
						dist = lineDistMin(mouseX, mouseY, startX, startY, midpointX-diagonal/2, startY, dist);
						dist = lineDistMin(mouseX, mouseY, midpointX-diagonal/2, startY, midpointX+diagonal/2, endY, dist);
						dist = lineDistMin(mouseX, mouseY, midpointX+diagonal/2, endY, endX, endY, dist);
					}
					else {
						dist = lineDistMin(mouseX, mouseY, midpointX-diagonal/2, startY, midpointX, startY+signY*diagonal/2, dist);
						dist = lineDistMin(mouseX, mouseY, midpointX, startY+signY*diagonal/2, midpointX, endY-signY*diagonal/2, dist);
						dist = lineDistMin(mouseX, mouseY, midpointX, endY-signY*diagonal/2, midpointX+diagonal/2, endY, dist);
					}
				}
				else {
					dist = lineDistMin(mouseX, mouseY, startX, startY, startX-diagonal/2, startY+signY*diagonal/2, dist);
					dist = lineDistMin(mouseX, mouseY, startX-diagonal/2, startY+signY*diagonal/2, endX+diagonal/2, endY-signY*diagonal/2, dist);
					dist = lineDistMin(mouseX, mouseY, endX+diagonal/2, endY-signY*diagonal/2, endX, endY, dist);
				}
				break;
			default:
				dist = lineDistMin(mouseX, mouseY, startX, startY, endX, endY, dist);
		}
		return dist;
	}

	/**
	 * Calculate the minimum of the distance from (px, py) to a line between (ax, ay) and (bx, by),
	 * then take the min of that dist and the input dist.
	 * Used for getting the min dist to several lines.
	 */
	private static double lineDistMin(double px, double py, double ax, double ay, double bx, double by, double dist) {
		return Math.min(lineDist(px, py, ax, ay, bx, by), dist);
	}

	/**
	 * Calculate the minimum of the distance from (px, py) to a line between (ax, ay) and (bx, by).
	 */
	private static double lineDist(double px, double py, double ax, double ay, double bx, double by) {
		double pax = px - ax;
		double pay = py - ay;
		double bax = bx - ax;
		double bay = by - ay;

		double dp_pa_ba = pax * bax + pay * bay;
		double dp_ba_ba = bax * bax + bay * bay;
		double h = dp_pa_ba / dp_ba_ba;
		h = MathHelperScalar.clamp(h, 0.0, 1.0);

		double dx = pax - bax*h;
		double dy = pay - bay*h;

		return Math.sqrt(dx * dx + dy * dy);

	}
	
	/**
	 * For bezier connectors
	 */
	private static double p(double a1, double a2, double a3, double a4, double b) {
		return lerp(lerp(lerp(a1, a2, b), lerp(a2, a3, b), b), lerp(lerp(a2, a3, b), lerp(a3, a4, b), b), b);
	}
	
	/**
	 * For bezier connectors
	 */
	private static double lerp(double a1, double a2, double b) {
		return a1 + b * (a2 - a1);
	}
}
