/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.patcher.ui.patcheditor;

import java.util.ArrayList;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import net.worldsynth.patch.ModuleWrapper;
import net.worldsynth.editor.ui.navcanvas.Coordinate;
import net.worldsynth.editor.ui.navcanvas.Pixel;

public class SelectionRectangle {
	private double startCoordinateX;
	private double startCoordinateY;
	private double endCoordinateX;
	private double endCoordinateY;
	
	public SelectionRectangle(double startCoordinateX, double startCoordinateY) {
		this.endCoordinateX = this.startCoordinateX = startCoordinateX;
		this.endCoordinateY = this.startCoordinateY = startCoordinateY;
	}
	
	public void setEndCoordinate(double endCoordinateX, double endCoordinateY) {
		this.endCoordinateX = endCoordinateX;
		this.endCoordinateY = endCoordinateY;
	}
	
	public double getX1() {
		if (startCoordinateX < endCoordinateX) {
			return startCoordinateX;
		}
		return endCoordinateX;
	}
	
	public double getY1() {
		if (startCoordinateY < endCoordinateY) {
			return startCoordinateY;
		}
		return endCoordinateY;
	}
	
	public double getX2() {
		if (startCoordinateX > endCoordinateX) {
			return startCoordinateX;
		}
		return endCoordinateX;
	}
	
	public double getY2() {
		if (startCoordinateY > endCoordinateY) {
			return startCoordinateY;
		}
		return endCoordinateY;
	}
	
	public ModuleWrapper[] getModuleWrappersInside(PatchEditorPane patchEditor) {
		ArrayList<ModuleWrapper> containedModuleWrappersList = new ArrayList<ModuleWrapper>();
		for (ModuleWrapper mw: patchEditor.getPatch().getModuleWrapperList()) {
			if (contains(mw.posX, mw.posY) && contains(mw.posX+mw.wrapperWidth, mw.posY+mw.wrapperHeight)) {
				containedModuleWrappersList.add(mw);
			}
		}
		ModuleWrapper[] list = new ModuleWrapper[0];
		return containedModuleWrappersList.toArray(list);
	}
	
	private boolean contains(double x, double y) {
		if (x < getX1() || x > getX2()) {
			return false;
		}
		if (y < getY1() || y > getY2()) {
			return false;
		}
		return true;
	}
	
	public void paint(PatchEditorPane editor, GraphicsContext g) {
		Coordinate startCoord = new Coordinate(getX1(), getY1());
		Pixel startPixel = new Pixel(startCoord, editor);
		Coordinate endCoord = new Coordinate(getX2(), getY2());
		Pixel endPixel = new Pixel(endCoord, editor);
		
		g.setStroke(Color.WHITE);
		g.setLineWidth(2.0);
		g.setLineDashes(10.0, 10.0);
		g.strokeRect(startPixel.x, startPixel.y, endPixel.x-startPixel.x, endPixel.y-startPixel.y);
		g.setLineDashes();
	}
}
