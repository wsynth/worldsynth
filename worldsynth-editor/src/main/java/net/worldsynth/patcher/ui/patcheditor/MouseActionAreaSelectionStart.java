/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.patcher.ui.patcheditor;

import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import net.worldsynth.patcher.ui.patcheditor.PatchEditorPane.MouseListener;

class MouseActionAreaSelectionStart implements MouseAction {
	
	private static final MouseActionEventFilter FILTER = new MouseActionEventFilter(
			MouseEvent.MOUSE_DRAGGED, // Mouse event type
			MouseButton.PRIMARY, // Mouse button
			0, // Mouse click count
			EventFilterInputState.NO, // Mouse over module
			EventFilterInputState.NO, // Mouse over module IO
			EventFilterInputState.NO, // Temp module active
			EventFilterInputState.NO, // Temp connector active
			EventFilterInputState.NO, // Temp patch active
			EventFilterInputState.NO); // Area selection active
	
	@Override
	public boolean filterEvent(PatchEditorPane editor, MouseListener t) {
		return FILTER.validateFilter(editor, t);
	}
	
	@Override
	public void action(PatchEditorPane editor, MouseListener t) {
		if (t.currentEvent.isShortcutDown()) {
			editor.selectionLasso = new SelectionLasso(t.lastMouseCoordinateX, t.lastMouseCoordinateY);
			editor.selectionLasso.setCurrentCoordinate(t.currentMouseCoordinateX, t.currentMouseCoordinateY);
		}
		else {
			editor.selectionRectangle = new SelectionRectangle(t.lastMouseCoordinateX, t.lastMouseCoordinateY);
			editor.selectionRectangle.setEndCoordinate(t.currentMouseCoordinateX, t.currentMouseCoordinateY);
		}
		editor.repaint();
	}
}
