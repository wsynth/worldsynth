/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.patcher.ui.patcheditor;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import javafx.scene.input.Clipboard;
import javafx.scene.input.KeyEvent;
import net.worldsynth.patcher.preferences.PatcherPreferences;
import net.worldsynth.patcher.ui.patcheditor.PatchEditorPane.KeyboardListener;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

class KeyboardActionPaste implements KeyboardAction {
	private static Logger logger = LogManager.getLogger(KeyboardActionPaste.class);

	private static final KeyboardActionEventFilter FILTER = new KeyboardActionEventFilter(
			KeyEvent.KEY_RELEASED, // Key event type
			PatcherPreferences.patcherKeybindPaste, // Key code combination
			EventFilterInputState.UNUSED, // Mouse over module
			EventFilterInputState.UNUSED, // Mouse over module IO
			EventFilterInputState.UNUSED, // Temp module active
			EventFilterInputState.UNUSED, // Temp connector active
			EventFilterInputState.UNUSED, // Temp patch active
			EventFilterInputState.UNUSED); // Area selection active
	
	@Override
	public boolean filterEvent(PatchEditorPane editor, KeyboardListener t) {
		return FILTER.validateFilter(editor, t);
	}
	
	@Override
	public void action(PatchEditorPane editor, KeyboardListener t) {
		final Clipboard cliphboard = Clipboard.getSystemClipboard();
		String tempPatchString = cliphboard.getString();
		if (tempPatchString == null) {
			return;
		}
		else if (!tempPatchString.startsWith("{")) {
			return;
		}
		
		ObjectMapper objectMapper = new ObjectMapper();
		JsonNode patchNode;
		try {
			patchNode = objectMapper.readTree(tempPatchString);
		} catch (JsonProcessingException e) {
			logger.error(e);
			return;
		}
		
		TempPatch tmp = new TempPatch(patchNode, null, editor.patch);
		editor.setTempPatch(tmp);
	}
}
