/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.patcher.ui.patcheditor;

import java.util.Iterator;
import java.util.Map;

import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import net.worldsynth.datatype.Datatypes;
import net.worldsynth.patch.ModuleConnector;
import net.worldsynth.patch.ModuleWrapper;
import net.worldsynth.patch.ModuleWrapperIO;
import net.worldsynth.patcher.ui.patcheditor.PatchEditorPane.MouseListener;

class MouseActionMouseMove implements MouseAction {
	
	private static final MouseActionEventFilter FILTER = new MouseActionEventFilter(
			MouseEvent.MOUSE_MOVED, // Mouse event type
			MouseButton.NONE, // Mouse button
			0, // Mouse click count
			EventFilterInputState.UNUSED, // Mouse over module
			EventFilterInputState.UNUSED, // Mouse over module IO
			EventFilterInputState.UNUSED, // Temp module active
			EventFilterInputState.UNUSED, // Temp connector active
			EventFilterInputState.UNUSED, // Temp patch active
			EventFilterInputState.UNUSED); // Area selection active
	
	private static final MouseActionEventFilter FILTER_CONNECTOR_DRAG = new MouseActionEventFilter(
			MouseEvent.MOUSE_DRAGGED, // Mouse event type
			MouseButton.PRIMARY, // Mouse button
			0, // Mouse click count
			EventFilterInputState.UNUSED, // Mouse over module
			EventFilterInputState.UNUSED, // Mouse over module IO
			EventFilterInputState.NO, // Temp module active
			EventFilterInputState.YES, // Temp connector active
			EventFilterInputState.NO, // Temp patch active
			EventFilterInputState.NO); // Area selection active
	
	@Override
	public boolean filterEvent(PatchEditorPane editor, MouseListener t) {
		return FILTER.validateFilter(editor, t) 
				|| FILTER_CONNECTOR_DRAG.validateFilter(editor, t);
	}
	
	@Override
	public void action(PatchEditorPane editor, MouseListener t) {
		// Is mouse over a device
		boolean foundWrapper = false;
		boolean foundWrapperIo = false;
		ModuleWrapper lastWrapper = editor.wrapperOver;
		ModuleWrapperIO lastWrapperIo = editor.wrapperIoOver;
		for (ModuleWrapper mw: editor.patch.getModuleWrapperList()) {
			if (Wrappers.isCoordinateInsideWrapper(editor, mw, t.currentMouseCoordinateX, t.currentMouseCoordinateY, 5)) {
				if (Wrappers.isCoordinateInsideWrapper(editor, mw, t.currentMouseCoordinateX, t.currentMouseCoordinateY)) {
					editor.wrapperOver = mw;
					foundWrapper = true;
				}
				
				// Is mouse over an IO
				if (mw.wrapperInputs.size() > 0) {
					for (ModuleWrapperIO io: mw.wrapperInputs.values()) {
						if (!io.getIO().isVisible()) {
							continue;
						}
						if (Wrappers.isCoordinateInsideIo(editor, mw, io, t.currentMouseCoordinateX, t.currentMouseCoordinateY)) {
							editor.wrapperOver = mw;
							editor.wrapperIoOver = io;
							foundWrapper = true;
							foundWrapperIo = true;
							break;
						}
					}
				}
				if (mw.wrapperOutputs.size() > 0) {
					for (ModuleWrapperIO io: mw.wrapperOutputs.values()) {
						if (!io.getIO().isVisible()) {
							continue;
						}
						if (Wrappers.isCoordinateInsideIo(editor, mw, io, t.currentMouseCoordinateX, t.currentMouseCoordinateY)) {
							editor.wrapperOver = mw;
							editor.wrapperIoOver = io;
							foundWrapper = true;
							foundWrapperIo = true;
							break;
						}
					}
				}
				
				// Snap over main IO if making connection if hovering over
				if (editor.tempConnector != null && !foundWrapperIo && foundWrapper) {
					if (editor.tempConnector.module2 == null && !editor.wrapperOver.wrapperInputs.isEmpty()) {
						// Temp connector is not connected to the input of a wrapper
						// Snap it to the first available available and valid input
						Iterator<Map.Entry<String, ModuleWrapperIO>> it = editor.wrapperOver.wrapperInputs.entrySet().iterator();
						while (it.hasNext()) {
							ModuleWrapperIO io = it.next().getValue();
							if (!io.getIO().isVisible()) {
								continue; // IO must be visible to snap
							}
							if (!editor.patch.getModuleConnectorsByWrapperIo(io).isEmpty()) {
								continue; // An input must be unconnected to snap
							}
							if (Datatypes.verifyCompatibleTypes(editor.tempConnector.getModule1IoDatatype(), io.getDatatype())) {
								// If the datatypes of such a connection are compatible, snap to the IO
								editor.wrapperIoOver = io;
								foundWrapperIo = true;
								break;
							}
						}
					}
					else if (editor.tempConnector.module1 == null && !editor.wrapperOver.wrapperOutputs.isEmpty()) {
						// Temp connector is not connected to the output of a wrapper
						// Snap it to the first available valid output
						Iterator<Map.Entry<String, ModuleWrapperIO>> it = editor.wrapperOver.wrapperOutputs.entrySet().iterator();
						while (it.hasNext()) {
							ModuleWrapperIO io = it.next().getValue();
							if (!io.getIO().isVisible()) {
								continue; // IO must be visible to snap
							}
							if (Datatypes.verifyCompatibleTypes(io.getDatatype(), editor.tempConnector.getModule2IoDatatype())) {
								// If the datatypes of such a connection are compatible, snap to the IO
								editor.wrapperIoOver = io;
								foundWrapperIo = true;
								break;
							}
						}
					}
				}
			}
		}
		if (!foundWrapper) {
			editor.wrapperOver = null;
		}
		if (!foundWrapperIo) {
			editor.wrapperIoOver = null;
		}

		boolean foundConnector = false;
		ModuleConnector lastConnector = editor.connectorOver;
		if (editor.tempConnector == null && !foundWrapper && !foundWrapperIo) {
			// Is mouse over a connector
			for (ModuleConnector mc: editor.patch.getModuleConnectorList()) {
				if (Connectors.isCoordinateOverConector(editor, mc, t.currentMouseCoordinateX, t.currentMouseCoordinateY, 5/editor.zoom)) {
					editor.connectorOver = mc;
					foundConnector = true;
					break;
				}
			}
		}

		if (!foundConnector) {
			editor.connectorOver = null;
		}

		if (editor.wrapperOver != lastWrapper || editor.wrapperIoOver != lastWrapperIo || editor.tempConnector != null || editor.connectorOver != lastConnector) {
			editor.repaint();
		}
	}
}
