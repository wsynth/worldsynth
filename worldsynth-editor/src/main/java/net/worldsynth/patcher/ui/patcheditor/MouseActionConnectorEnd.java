/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.patcher.ui.patcheditor;

import java.util.List;

import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import net.worldsynth.patch.ModuleConnector;
import net.worldsynth.patcher.ui.patcheditor.PatchEditorPane.MouseListener;

class MouseActionConnectorEnd implements MouseAction {
	
	private static final MouseActionEventFilter FILTER = new MouseActionEventFilter(
			MouseEvent.MOUSE_CLICKED, // Mouse event type
			MouseButton.PRIMARY, // Mouse button
			1, // Mouse click count
			EventFilterInputState.UNUSED, // Mouse over module
			EventFilterInputState.YES, // Mouse over module IO
			EventFilterInputState.UNUSED, // Temp module active
			EventFilterInputState.YES, // Temp connector active
			EventFilterInputState.UNUSED, // Temp patch active
			EventFilterInputState.UNUSED); // Area selection active
	
	@Override
	public boolean filterEvent(PatchEditorPane editor, MouseListener t) {
		return FILTER.validateFilter(editor, t);
	}
	
	@Override
	public void action(PatchEditorPane editor, MouseListener t) {
		if (editor.tempConnector.module2 != null && editor.tempConnector.module2Io != null && !editor.wrapperIoOver.isInput() && editor.tempConnector.module2 != editor.wrapperOver) {
			// If the mouse is over an output that is not on the already connected device
			// and the connected io is an input
			editor.tempConnector.setOutputWrapper(editor.wrapperOver, editor.wrapperIoOver);
			if (editor.tempConnector.verifyInputOutputType()) {
				editor.addModuleConnector(editor.tempConnector, true, true);
				editor.getEditorSession().updatePreview();
			}
			editor.tempConnector = null;
		}
		else if (editor.tempConnector.module1 != null && editor.tempConnector.module1Io != null && editor.wrapperIoOver.isInput() && editor.tempConnector.module1 != editor.wrapperOver) {
			// If the mouse is over an input that is not on the already connected device and
			// the connected io is an output
			List<ModuleConnector> mc = editor.patch.getModuleConnectorsByWrapperIo(editor.wrapperIoOver);
			
			editor.tempConnector.setInputWrapper(editor.wrapperOver, editor.wrapperIoOver);
			
			if (editor.tempConnector.verifyInputOutputType()) {
				if (!mc.isEmpty()) {
					// If there is already a connector on the input remove it so it gets replaced
					for (ModuleConnector c : mc) {
						editor.removeModuleConnector(c, true, false);
					}
				}
				editor.addModuleConnector(editor.tempConnector, true, true);
				editor.getEditorSession().updatePreview();
			}
			editor.tempConnector = null;
		}
	}
}