/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.patcher.preferences;

import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination;
import net.worldsynth.editor.preferences.EnumPreference;
import net.worldsynth.editor.preferences.KeyCodePreference;
import net.worldsynth.editor.preferences.Preference;
import net.worldsynth.editor.preferences.Preferences;

public class PatcherPreferences extends Preferences {

	public static Preference<ConnectorStyle> connectorStyle = new EnumPreference<>(
			ConnectorStyle.BEZIER, ConnectorStyle.class,
			"net.worldsynth.patcher.patcheditor.connectorStyle",
			"Connector style", "View",
			"Style to use for rendering module connectors");

	public static Preference<GridStyle> gridStyle = new EnumPreference<>(
			GridStyle.DOTS, GridStyle.class,
			"net.worldsynth.patcher.patcheditor.gridStyle",
			"Grid style", "View",
			"Node editor background grid");

	public static Preference<KeyCodeCombination> patcherKeybindCut = new KeyCodePreference(
			new KeyCodeCombination(KeyCode.X, KeyCombination.SHORTCUT_DOWN),
			"net.worldsynth.patcher.patcheditor.keybind.cut",
			"Cut", "Key bindings",
			"Key bind for cut operation");

	public static Preference<KeyCodeCombination> patcherKeybindCopy = new KeyCodePreference(
			new KeyCodeCombination(KeyCode.C, KeyCombination.SHORTCUT_DOWN),
			"net.worldsynth.patcher.patcheditor.keybind.copy",
			"Copy", "Key bindings",
			"Key bind for copy operation");

	public static Preference<KeyCodeCombination> patcherKeybindPaste = new KeyCodePreference(
			new KeyCodeCombination(KeyCode.V, KeyCombination.SHORTCUT_DOWN),
			"net.worldsynth.patcher.patcheditor.keybind.paste",
			"Paste", "Key bindings",
			"Key bind for paste operation");

	public static Preference<KeyCodeCombination> patcherKeybindDelete = new KeyCodePreference(
			new KeyCodeCombination(KeyCode.DELETE),
			"net.worldsynth.patcher.patcheditor.keybind.delete",
			"Delete", "Key bindings",
			"Key bind for delete operation");

	public static Preference<KeyCodeCombination> patcherKeybindRename = new KeyCodePreference(
			new KeyCodeCombination(KeyCode.R, KeyCombination.SHORTCUT_DOWN),
			"net.worldsynth.patcher.patcheditor.keybind.rename",
			"Rename", "Key bindings",
			"Key bind for rename operation");

	public static Preference<KeyCodeCombination> patcherKeybindBypass = new KeyCodePreference(
			new KeyCodeCombination(KeyCode.B, KeyCombination.SHORTCUT_DOWN),
			"net.worldsynth.patcher.patcheditor.keybind.bypass",
			"Bypass", "Key bindings",
			"Key bind for bypass toggle");

	public static Preference<KeyCodeCombination> patcherKeybindEdit = new KeyCodePreference(
			new KeyCodeCombination(KeyCode.E, KeyCombination.SHORTCUT_DOWN),
			"net.worldsynth.patcher.patcheditor.keybind.editModule",
			"Edit", "Key bindings",
			"Key bind for opening the module parameter editor");

	public static Preference<KeyCodeCombination> patcherKeybindSearchModule = new KeyCodePreference(
			new KeyCodeCombination(KeyCode.SPACE),
			"net.worldsynth.patcher.patcheditor.keybind.searchModule",
			"Search module", "Key bindings",
			"Key bind for adding new module through search");

	public static Preference<KeyCodeCombination> patcherKeybindApplyParameters = new KeyCodePreference(
			new KeyCodeCombination(KeyCode.ENTER),
			"net.worldsynth.patcher.patcheditor.keybind.applyParameters",
			"Apply parameters", "Key bindings",
			"Key bind for applying parameters in the module parameters editor");

	public PatcherPreferences() {
		super("Patcher");

		registerPreference(connectorStyle);
		registerPreference(gridStyle);
		registerPreference(patcherKeybindCut);
		registerPreference(patcherKeybindCopy);
		registerPreference(patcherKeybindPaste);
		registerPreference(patcherKeybindDelete);
		registerPreference(patcherKeybindRename);
		registerPreference(patcherKeybindBypass);
		registerPreference(patcherKeybindEdit);
		registerPreference(patcherKeybindSearchModule);
		registerPreference(patcherKeybindApplyParameters);
	}

	public enum ConnectorStyle {
		STRAIGHT,
		ANGULAR,
		SQUARE,
		BEZIER,
		METRO
	}

	public enum GridStyle {
		NONE,
		DARK_GRID,
		LIGHT_GRID,
		DOTS
	}
}
