![WorldSynth logo and title](readme-images/LogoTitle.png "")

# WorldSynth

WorldSynth is a graphical terrain authoring tool.

![Synth example](readme-images/SynthExampleImage.png "Example of a synth generating caverns")

## Building with gradlew

Run: `./gradlew build`

## Running with gradlew

WorldSynth can be run with gradle using the editor module as entry point with the command:  
`./gradlew run`

> **Note:**  
> WorldSynth might not start properly without access to any material and biome files.  
> See the section on [adding materials and biomes](#adding-materials-and-biomes).


Or run with overrides for resource folders with a command on a form like for example:  
`./gradlew run --args='materials <path> biomes <path> examples <path>'`  
If a path contains space characters, the path should be enclosed in double quotes ( " ).

For examole for a user on linux, this may look something like:  
`./gradlew run --args='materials /home/user/Documents/WorldSynth/worldsynth-materials-minecraft/materials biomes /home/user/Documents/WorldSynth/worldsynth-materials-minecraft/biomes examples /home/user/Documents/WorldSynth/worldsynth-examples/examples'`

### Generated resource folders
On the first time running the project, it will create a set of additional folders in the editor module. These are as
follows:

| Folder    | Description                                                                       | Application argument overrides |
|-----------|-----------------------------------------------------------------------------------|--------------------------------|
| addons    | A folder for putting addon jars into                                              |                                |
| biomes    | A folder for putting biome files in a .json format into                           | biomes \<path>                 |
| brushes   | A folder for putting brush image files into                                       | brushes \<path>                |
| data      | A folder where WorldSynth stores instance data, like recent files and preferences | data \<path>                   |
| examples  | A folder holding example files                                                    | examples \<path>               |
| logs      | A folder where WorldSynth stores log files                                        |                                |
| materials | A folder for putting material files in a .json format into                        | materials \<path>              |
| templates | A folder holding project template files                                           | templates \<path>              |

### Adding materials and biomes

Though it's possible to add material and biome files to the generated resource folders after the first run of the
program, the recommendation is rather to clone an adjacent materials repository
like [WorldSynth Materials Minecraft](https://gitlab.com/wsynth/worldsynth-materials-minecraft), to provide a set of
materials and biomes, and then point WorldSynth toward using the path to the biomes and materials folders from the
cloned repository with the help of application arguments.
