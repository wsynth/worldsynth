/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.synth.event;

import net.worldsynth.event.Event;
import net.worldsynth.event.EventType;
import net.worldsynth.event.HistoricalEvent;
import net.worldsynth.synth.Synth;
import net.worldsynth.composition.event.CompositionEvent;
import net.worldsynth.patch.event.PatchEvent;
import net.worldsynth.extent.event.ExtentManagerEvent;

public class SynthEvent extends Event implements HistoricalEvent {

	public static final EventType<SynthEvent> SYNTH_ANY = new EventType<>(Event.ANY, "SYNTH_ANY");
	public static final EventType<SynthEvent> SYNTH_PATCH = new EventType<>(SynthEvent.SYNTH_ANY, "SYNTH_PATCH");
	public static final EventType<SynthEvent> SYNTH_COMPOSITION = new EventType<>(SynthEvent.SYNTH_ANY, "SYNTH_COMPOSITION");
	public static final EventType<SynthEvent> SYNTH_EXTENT = new EventType<>(SynthEvent.SYNTH_ANY, "SYNTH_EXTENT");

	private PatchEvent patchEvent;
	private CompositionEvent compositionEvent;
	private ExtentManagerEvent extentManagerEvent;

	public SynthEvent(Synth source, PatchEvent patchEvent) {
		super(source, SYNTH_PATCH);
		this.patchEvent = patchEvent;
	}

	public SynthEvent(Synth source, CompositionEvent compositionEvent) {
		super(source, SYNTH_COMPOSITION);
		this.compositionEvent = compositionEvent;
	}

	public SynthEvent(Synth source, ExtentManagerEvent extentManagerEvent) {
		super(source, SYNTH_EXTENT);
		this.extentManagerEvent = extentManagerEvent;
	}

	public PatchEvent getPatchEvent() {
		return patchEvent;
	}

	public CompositionEvent getCompositionEvent() {
		return compositionEvent;
	}

	public ExtentManagerEvent getExtentManagerEvent() {
		return extentManagerEvent;
	}

	@Override
	public void undo() {
		Synth sourceSynth = (Synth) getSource();

		if (getEventType() == SYNTH_PATCH) {
			getPatchEvent().undo();
		}
		else if (getEventType() == SYNTH_COMPOSITION) {
			getCompositionEvent().undo();
		}
		else if (getEventType() == SYNTH_EXTENT) {
			getExtentManagerEvent().undo();
		}
	}

	@Override
	public void redo() {
		Synth sourceSynth = (Synth) getSource();

		if (getEventType() == SYNTH_PATCH) {
			getPatchEvent().redo();
		}
		else if (getEventType() == SYNTH_COMPOSITION) {
			getCompositionEvent().redo();
		}
		else if (getEventType() == SYNTH_EXTENT) {
			getExtentManagerEvent().redo();
		}
	}
}
