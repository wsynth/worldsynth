/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.synth;

import java.io.File;
import java.util.ArrayList;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

import net.worldsynth.parameter.AbstractParameter;
import net.worldsynth.parameter.FloatParameter;

public class SynthParameters {
	
	/**
	 * Scaling constant used for downscaling heightmap values to a [0, 1] range by division.
	 */
	private FloatParameter normalizedHeightmapHeight = new FloatParameter("normalizedheightmapheight", "Normalized Heightmap Height", null, 256.0f, 1.0f, Float.POSITIVE_INFINITY, 1.0f, 4096.0f);
	
	public SynthParameters() {}
	
	public SynthParameters(JsonNode jsonNode, File source) {
		fromJson(jsonNode, source);
	}
	
	public final ArrayList<AbstractParameter<?>> getParameters() {
		ArrayList<AbstractParameter<?>> parameters = new ArrayList<>();
		
		parameters.add(normalizedHeightmapHeight);
		
		return parameters;
	}
	
	public float getNormalizedHeightmapHeight() {
		return normalizedHeightmapHeight.getValue();
	}
	
	public JsonNode toJson(File destination) {
		ObjectMapper objectMapper = new ObjectMapper();
		ObjectNode node = objectMapper.createObjectNode();
		
		for (AbstractParameter<?> p: getParameters()) {
			node.set(p.getName(), p.toJson(destination));
		}
			
		return node;
	}
	
	protected void fromJson(JsonNode node, File source) {
		for (AbstractParameter<?> p: getParameters()) {
			if (node.has(p.getName())) {
				p.fromJson(node.get(p.getName()), source);
			}
		}
	}
}
