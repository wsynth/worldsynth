/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.synth;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.fasterxml.jackson.core.util.DefaultIndenter;
import com.fasterxml.jackson.core.util.DefaultPrettyPrinter;
import com.fasterxml.jackson.core.util.DefaultPrettyPrinter.Indenter;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

public class SynthManager {
	private static final Logger logger = LogManager.getLogger(SynthManager.class);
	
	private static HashMap<File, Synth> synths = new HashMap<File, Synth>();
	
	public static Synth getSynth(File file) throws IOException {
		return openSynth(file);
	}
	
	public static Synth openSynth(File file) throws IOException {
		if (!synths.containsKey(file)) {
			Synth synth = readUnmanagedSynthFromFile(file);
			synth.setFile(file);
			synths.put(file, synth);
		}
		return synths.get(file);
	}
	
	public static Synth readUnmanagedSynthFromFile(File file) throws IOException {
		ObjectMapper objectMapper = new ObjectMapper();
		JsonNode synthNode = objectMapper.readTree(file);
		
		Synth synth = new Synth(synthNode, file);
		return synth;
	}
	
	public static void saveSynth(Synth synth) {
		if (synths.containsValue(synth)) {
			if (!synths.containsKey(synth.getFile())) {
				// If the instance is saved to a new file, make sure to un-associate the instance from the old file, then associate it with the new file.
				synths.entrySet().removeIf(e -> e.getValue() == synth);
				synths.put(synth.getFile(), synth);
			}
		}
		else {
			// If the instance is not associated to a file (aka. not opened from file and never saved), associate the synth to the save file.
			synths.put(synth.getFile(), synth);
		}
		writeSynthToFile(synth);
	}
	
	private static void writeSynthToFile(Synth synth) {
		JsonNode synthNode = synth.toJson(synth.getFile());
		
		DefaultPrettyPrinter printer = new DefaultPrettyPrinter();
		Indenter indenter = new DefaultIndenter("    ", DefaultIndenter.SYS_LF);
		printer.indentObjectsWith(indenter);
		printer.indentArraysWith(indenter);
		
		ObjectMapper objectMapper = new ObjectMapper();
		try {
			objectMapper.writer(printer).writeValue(synth.getFile(), synthNode);
			
			if (synth.composition != null) {
				synth.composition.save();
			}
		} catch (IOException e) {
			logger.error(e);
		}
	}
}
