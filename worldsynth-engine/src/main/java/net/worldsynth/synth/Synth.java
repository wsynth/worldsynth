/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.synth;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import net.worldsynth.WorldSynth;
import net.worldsynth.composition.Composition;
import net.worldsynth.event.EventDispatcher;
import net.worldsynth.event.EventHandler;
import net.worldsynth.event.EventType;
import net.worldsynth.extent.WorldExtentManager;
import net.worldsynth.extent.event.ExtentManagerEvent;
import net.worldsynth.patch.Patch;
import net.worldsynth.saveformat.SaveUpdater;
import net.worldsynth.synth.event.SynthEvent;
import net.worldsynth.composition.event.CompositionEvent;
import net.worldsynth.patch.event.PatchEvent;

import java.io.File;

public class Synth {

	public static final int SAVE_FORMAT_VERSION = 2;

	private EventDispatcher<SynthEvent> eventDispatcher = new EventDispatcher<>();
	
	private String name = "";
	private File file;

	protected WorldExtentManager extentManager = new WorldExtentManager();
	protected SynthParameters parameters = new SynthParameters();
	protected Patch patch = new Patch(this);
	protected Composition composition;

	public Synth(String name) {
		this.name = name;
		patch.addEventHandler(PatchEvent.PATCH_ANY, e -> {
			eventDispatcher.dispatchEvent(new SynthEvent(this, e));
		});
	}
	
	public Synth(JsonNode jsonNode, File source) throws JsonProcessingException {
		fromJson(jsonNode, source);
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getName() {
		return name;
	}
	
	public void setFile(File synthFile) {
		this.file = synthFile;

		if (composition != null) {
			File compDir = new File(getFile().getParentFile(), getName() + "_composition");
			composition.setDirectory(compDir);
		}
	}
	
	public File getFile() {
		return file;
	}
	
	public WorldExtentManager getExtentManager() {
		return extentManager;
	}
	
	public Patch getPatch() {
		return patch;
	}

	public Composition getComposition() {
		if (composition == null) {
			composition = new Composition(this);
			composition.addEventHandler(CompositionEvent.COMPOSITION_ANY, e -> {
				eventDispatcher.dispatchEvent(new SynthEvent(this, e));
			});
		}
		return composition;
	}

	public void addEventHandler(EventType<? extends SynthEvent> eventType, EventHandler<SynthEvent> eventHandler) {
		eventDispatcher.addEventHandler(eventType, eventHandler);
	}
	
	public void removeEventHandler(EventType<? extends SynthEvent> eventType, EventHandler<SynthEvent> eventHandler) {
		eventDispatcher.removeEventHandler(eventType, eventHandler);
	}
	
	public JsonNode toJson(File destination) {
		ObjectMapper objectMapper = new ObjectMapper();
		ObjectNode synthNode = objectMapper.createObjectNode();

		ObjectNode versionNode = objectMapper.createObjectNode();
		versionNode.put("worldsynth", WorldSynth.getVersion());
		versionNode.put("saveformat", SAVE_FORMAT_VERSION);
		synthNode.set("version", versionNode);

		synthNode.put("name", name);
		synthNode.set("extents", getExtentManager().toJson());
		synthNode.set("parameters", getParameters().toJson(destination));

		ArrayNode compositionsNode = objectMapper.createArrayNode();
		if (composition != null) {
			compositionsNode.add(composition.toJson());
		}
		synthNode.set("compositions", compositionsNode);

		ArrayNode patchesNode = objectMapper.createArrayNode();
		patchesNode.add(patch.toJson(destination));
		synthNode.set("patches", patchesNode);
		
		return synthNode;
	}
	
	protected void fromJson(JsonNode synthNode, File source) throws JsonProcessingException {
		synthNode = SaveUpdater.updateSynth(synthNode);

		name = synthNode.get("name").asText();
		extentManager = new WorldExtentManager(synthNode.get("extents"));
		extentManager.addEventHandler(ExtentManagerEvent.EXTENT_ANY, e -> {
			eventDispatcher.dispatchEvent(new SynthEvent(this, e));
		});

		parameters = new SynthParameters(synthNode.get("parameters"), source);

		JsonNode compositionsNode = synthNode.get("compositions");
		if (compositionsNode.size() > 0) {
			composition = new Composition(compositionsNode.get(0), this);
			composition.addEventHandler(CompositionEvent.COMPOSITION_ANY, e -> {
				eventDispatcher.dispatchEvent(new SynthEvent(this, e));
			});
		}

		JsonNode patchesNode = synthNode.get("patches");
		patch = new Patch(patchesNode.get(0), source, this);
		patch.addEventHandler(PatchEvent.PATCH_ANY, e -> {
			eventDispatcher.dispatchEvent(new SynthEvent(this, e));
		});
	}
	
	public SynthParameters getParameters() {
		return parameters;
	}
	
	@Override
	public boolean equals(Object obj) {
		// TODO Improve equals check
		return super.equals(obj);
	}
}
