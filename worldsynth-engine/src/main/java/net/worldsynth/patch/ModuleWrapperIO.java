/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.patch;

import net.worldsynth.datatype.AbstractDatatype;
import net.worldsynth.module.ModuleIO;

public class ModuleWrapperIO {
	
	private ModuleIO io;
	private boolean isInput;
	
	public static final float IO_RENDERSIZE = 10;
	
	/**
	 * Relative position in module wrapper
	 */
	public float posX;
	
	/**
	 * Relative position in module wrapper
	 */
	public float posY;
	
	public ModuleWrapperIO(ModuleIO io, float posX, float posY, boolean isInput) {
		this.io = io;
		this.posX = posX;
		this.posY = posY;
		this.isInput = isInput;
	}
	
	public boolean isInput() {
		return isInput;
	}

	public AbstractDatatype getDatatype() {
		return io.getData();
	}
	
	public ModuleIO getIO() {
		return io;
	}
	
	public String getName() {
		return io.getName();
	}
	
	@Override
	public String toString() {
		return io.getName() + " <" + io.getData().getDatatypeName() + ">";
	}
}
