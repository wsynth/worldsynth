/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.patch.event;

import net.worldsynth.event.Event;
import net.worldsynth.event.EventType;
import net.worldsynth.event.HistoricalEvent;
import net.worldsynth.patch.ModuleWrapper;

public class WrapperEvent extends Event implements HistoricalEvent {

	public static final EventType<WrapperEvent> WRAPPER_ANY = new EventType<>(Event.ANY, "WRAPPER_ANY");
	public static final EventType<WrapperEvent> WRAPPER_IO_CHANGED = new EventType<>(WrapperEvent.WRAPPER_ANY, "WRAPPER_IO_CHANGED");
	public static final EventType<WrapperEvent> WRAPPER_NAME_CHANGED = new EventType<>(WrapperEvent.WRAPPER_ANY, "WRAPPER_NAME_CHANGED");
	public static final EventType<WrapperEvent> WRAPPER_BYPASS_CHANGED = new EventType<>(WrapperEvent.WRAPPER_ANY, "WRAPPER_BYPASS_CHANGED");
	public static final EventType<WrapperEvent> WRAPPER_POSITION_CHANGED = new EventType<>(WrapperEvent.WRAPPER_ANY, "WRAPPER_POSITION_CHANGED");
	public static final EventType<WrapperEvent> WRAPPER_MODULE_MODIFIED = new EventType<>(WrapperEvent.WRAPPER_ANY, "WRAPPER_MODULE_MODIFIED");

	private ModuleEvent moduleEvent;

	private double oldPosX, newPosX;
	private double oldPosY, newPosY;
	private String oldName, newName;
	private boolean oldBypass, newBypass;

	public WrapperEvent(ModuleWrapper source, EventType<WrapperEvent> eventType) {
		super(source, eventType);
	}

	public WrapperEvent(ModuleWrapper source, EventType<WrapperEvent> eventType, ModuleEvent moduleEvent) {
		super(source, eventType);
		this.moduleEvent = moduleEvent;
	}

	public WrapperEvent(ModuleWrapper source, EventType<WrapperEvent> eventType, double oldPosX, double oldPosY, double newPosX, double newPosY) {
		super(source, eventType);
		this.oldPosX = oldPosX;
		this.oldPosY = oldPosY;
		this.newPosX = newPosX;
		this.newPosY = newPosY;
	}

	public WrapperEvent(ModuleWrapper source, EventType<WrapperEvent> eventType, String oldName, String newName) {
		super(source, eventType);
		this.oldName = oldName;
		this.newName = newName;
	}

	public WrapperEvent(ModuleWrapper source, EventType<WrapperEvent> eventType, boolean oldBypass, boolean newBypass) {
		super(source, eventType);
		this.oldBypass = oldBypass;
		this.newBypass = newBypass;
	}

	public ModuleEvent getModuleEvent() {
		return moduleEvent;
	}

	public double getOldPosX() {
		return oldPosX;
	}

	public double getOldPosY() {
		return oldPosY;
	}

	public double getNewPosX() {
		return newPosX;
	}

	public double getNewPosY() {
		return newPosY;
	}

	@Override
	public void undo() {
		ModuleWrapper sourceWrapper = (ModuleWrapper) getSource();

		if (getEventType() == WRAPPER_NAME_CHANGED) {
			sourceWrapper.setCustomName(oldName);
		}
		else if (getEventType() == WRAPPER_BYPASS_CHANGED) {
			sourceWrapper.setBypassed(oldBypass);
		}
		else if (getEventType() == WRAPPER_POSITION_CHANGED) {
			sourceWrapper.posX = oldPosX;
			sourceWrapper.posY = oldPosY;
			sourceWrapper.commitMove(newPosX, newPosY);
		}
		else if (getEventType() == WRAPPER_MODULE_MODIFIED) {
			getModuleEvent().undo();
		}
	}

	@Override
	public void redo() {
		ModuleWrapper sourceWrapper = (ModuleWrapper) getSource();

		if (getEventType() == WRAPPER_NAME_CHANGED) {
			sourceWrapper.setCustomName(newName);
		}
		else if (getEventType() == WRAPPER_BYPASS_CHANGED) {
			sourceWrapper.setBypassed(newBypass);
		}
		else if (getEventType() == WRAPPER_POSITION_CHANGED) {
			sourceWrapper.posX = newPosX;
			sourceWrapper.posY = newPosY;
			sourceWrapper.commitMove(oldPosX, oldPosY);
		}
		else if (getEventType() == WRAPPER_MODULE_MODIFIED) {
			getModuleEvent().redo();
		}
	}
}
