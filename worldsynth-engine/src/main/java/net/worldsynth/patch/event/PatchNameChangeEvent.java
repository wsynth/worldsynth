/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.patch.event;

import net.worldsynth.event.EventType;
import net.worldsynth.patch.Patch;

public class PatchNameChangeEvent extends PatchEvent {
	public static final EventType<PatchNameChangeEvent> PATCH_NAME_CHANGED = new EventType<>(PatchEvent.PATCH_ANY, "PATCH_NAME_CHANGED");

	private final String oldName, newName;

	public PatchNameChangeEvent(Patch source, String oldName, String newName) {
		super(source, PATCH_NAME_CHANGED);
		this.oldName = oldName;
		this.newName = newName;
	}

	public String getOldName() {
		return oldName;
	}

	public String getNewName() {
		return newName;
	}

	@Override
	public void undo() {
		Patch sourcePatch = (Patch) getSource();
		sourcePatch.setName(oldName);
	}

	@Override
	public void redo() {
		Patch sourcePatch = (Patch) getSource();
		sourcePatch.setName(newName);
	}
}
