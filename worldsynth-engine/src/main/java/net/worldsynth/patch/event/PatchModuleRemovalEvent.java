/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.patch.event;

import net.worldsynth.event.EventType;
import net.worldsynth.patch.ModuleWrapper;
import net.worldsynth.patch.Patch;

public class PatchModuleRemovalEvent extends PatchEvent {
	public static final EventType<PatchModuleRemovalEvent> PATCH_MODULE_REMOVED = new EventType<>(PatchEvent.PATCH_ANY, "PATCH_MODULE_REMOVED");

	private final ModuleWrapper wrapper;

	public PatchModuleRemovalEvent(Patch source, ModuleWrapper wrapper) {
		super(source, PATCH_MODULE_REMOVED);
		this.wrapper = wrapper;
	}

	public ModuleWrapper getWrapper() {
		return wrapper;
	}

	@Override
	public void undo() {
		Patch sourcePatch = (Patch) getSource();
		sourcePatch.addModuleWrapper(getWrapper());
	}

	@Override
	public void redo() {
		Patch sourcePatch = (Patch) getSource();
		sourcePatch.removeModuleWrapper(getWrapper());
	}
}
