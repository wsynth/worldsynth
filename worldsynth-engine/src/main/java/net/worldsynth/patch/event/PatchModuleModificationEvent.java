/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.patch.event;

import net.worldsynth.event.EventType;
import net.worldsynth.patch.ModuleWrapper;
import net.worldsynth.patch.Patch;

public class PatchModuleModificationEvent extends PatchEvent {
	public static final EventType<PatchModuleModificationEvent> PATCH_MODULE_MODIFIED = new EventType<>(PatchEvent.PATCH_ANY, "PATCH_MODULE_MODIFIED");

	private final ModuleWrapper wrapper;
	private final WrapperEvent wrapperEvent;

	public PatchModuleModificationEvent(Patch source, WrapperEvent wrapperEvent) {
		super(source, PATCH_MODULE_MODIFIED);
		this.wrapperEvent = wrapperEvent;
		this.wrapper = (ModuleWrapper) wrapperEvent.getSource();
	}

	public ModuleWrapper getWrapper() {
		return wrapper;
	}

	public WrapperEvent getWrapperEvent() {
		return wrapperEvent;
	}

	@Override
	public void undo() {
		getWrapperEvent().undo();
	}

	@Override
	public void redo() {
		getWrapperEvent().redo();
	}
}
