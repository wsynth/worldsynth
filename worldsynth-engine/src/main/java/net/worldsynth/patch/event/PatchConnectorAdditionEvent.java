/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.patch.event;

import net.worldsynth.event.EventType;
import net.worldsynth.patch.ModuleConnector;
import net.worldsynth.patch.Patch;

public class PatchConnectorAdditionEvent extends PatchEvent {
	public static final EventType<PatchConnectorAdditionEvent> PATCH_CONNECTOR_ADDED = new EventType<>(PatchEvent.PATCH_ANY, "PATCH_CONNECTOR_ADDED");

	private final ModuleConnector connector;

	public PatchConnectorAdditionEvent(Patch source, ModuleConnector connector) {
		super(source, PATCH_CONNECTOR_ADDED);
		this.connector = connector;
	}

	public ModuleConnector getConnector() {
		return connector;
	}

	@Override
	public void undo() {
		Patch sourcePatch = (Patch) getSource();
		sourcePatch.removeModuleConnector(getConnector());
	}

	@Override
	public void redo() {
		Patch sourcePatch = (Patch) getSource();
		sourcePatch.addModuleConnector(getConnector());
	}
}
