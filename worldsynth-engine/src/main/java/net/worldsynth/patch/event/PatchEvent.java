/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.patch.event;

import net.worldsynth.event.Event;
import net.worldsynth.event.EventType;
import net.worldsynth.event.HistoricalEvent;
import net.worldsynth.patch.Patch;

public abstract class PatchEvent extends Event implements HistoricalEvent {
	public static final EventType<PatchEvent> PATCH_ANY = new EventType<>(Event.ANY, "PATCH_ANY");

	public PatchEvent(Patch source, EventType<? extends PatchEvent> eventType) {
		super(source, eventType);
	}
}
