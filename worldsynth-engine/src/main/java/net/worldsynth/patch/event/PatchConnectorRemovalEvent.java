/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.patch.event;

import net.worldsynth.event.EventType;
import net.worldsynth.patch.ModuleConnector;
import net.worldsynth.patch.Patch;

public class PatchConnectorRemovalEvent extends PatchEvent {
	public static final EventType<PatchConnectorRemovalEvent> PATCH_CONNECTOR_REMOVED = new EventType<>(PatchEvent.PATCH_ANY, "PATCH_CONNECTOR_REMOVED");

	private final ModuleConnector connector;

	public PatchConnectorRemovalEvent(Patch source, ModuleConnector connector) {
		super(source, PATCH_CONNECTOR_REMOVED);
		this.connector = connector;
	}

	public ModuleConnector getConnector() {
		return connector;
	}

	@Override
	public void undo() {
		Patch sourcePatch = (Patch) getSource();
		sourcePatch.addModuleConnector(getConnector());
	}

	@Override
	public void redo() {
		Patch sourcePatch = (Patch) getSource();
		sourcePatch.removeModuleConnector(getConnector());
	}
}
