/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.patch;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import net.worldsynth.WorldSynth;
import net.worldsynth.build.Build;
import net.worldsynth.datatype.AbstractDatatype;
import net.worldsynth.datatype.DatatypeMultitype;
import net.worldsynth.event.EventDispatcher;
import net.worldsynth.event.EventHandler;
import net.worldsynth.event.EventType;
import net.worldsynth.module.*;
import net.worldsynth.module.AbstractModuleRegister.ModuleEntry;
import net.worldsynth.patch.event.ModuleEvent;
import net.worldsynth.patch.event.WrapperEvent;

import java.io.File;
import java.lang.reflect.InvocationTargetException;
import java.util.LinkedHashMap;
import java.util.List;

/**
 * The {@link ModuleWrapper} is a wrapper for the {@link AbstractModule}.
 */

public class ModuleWrapper {
	
	private Patch parentPatch = null;
	
	public String wrapperID;

	private boolean bypassed = false;
	private String customName = "";
	
	public double posX;
	public double posY;
	
	public float wrapperWidth = 100;
	public float wrapperHeight = 35;
	
	public final AbstractModule module;
	
	public LinkedHashMap<String, ModuleWrapperIO> wrapperInputs;
	public LinkedHashMap<String, ModuleWrapperIO> wrapperOutputs;

	private final EventDispatcher<WrapperEvent> eventDispatcher = new EventDispatcher<>();
	
	public ModuleWrapper(ModuleEntry moduleEntry, Patch parentPatch, double posX, double posY) throws Exception {
		try {
			this.parentPatch = parentPatch;
			this.posX = posX;
			this.posY = posY;
			module = ModuleConstruction.constructModule(moduleEntry, this);
			buildWrapperForModule(module);
			wrapperID = parentPatch.getNewModuleId(this);
		} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException | SecurityException | InstantiationException e) {
			throw new Exception("Could not create instance of ModuleWrapper", e);
		}
	}
	
	public ModuleWrapper(Class<? extends AbstractModule> moduleClass, Patch parentPatch, double posX, double posY) throws Exception {
		try {
			this.parentPatch = parentPatch;
			this.posX = posX;
			this.posY = posY;
			module = ModuleConstruction.constructModule(moduleClass, this);
			buildWrapperForModule(module);
			wrapperID = parentPatch.getNewModuleId(this); 
		} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException | SecurityException | InstantiationException e) {
			throw new Exception("Could not create instance of ModuleWrapper", e);
		}
	}
	
	public ModuleWrapper(JsonNode jsonNode, File source, Patch parentPatch) {
		this.parentPatch = parentPatch;
		module = fromJson(jsonNode, source);
		buildWrapperForModule(module);
	}
	
	public Patch getParentPatch() {
		return parentPatch;
	}
	
	private void buildWrapperForModule(AbstractModule module) {
		// Get longest input and output names for width calculations
		String longestInputname = "";
		if (module.getInputs() != null) {
			for (ModuleIO input : module.getInputs()) {
				if (input.getName().length() > longestInputname.length()) {
					longestInputname = input.getName();
				}
			}
		}
		
		String longestOutputName = "";
		if (module.getOutputs() != null) {
			for (ModuleIO output : module.getOutputs()) {
				if (output.getName().length() > longestOutputName.length()) {
					longestOutputName = output.getName();
				}
			}
		}
		
		// Calculate the graphical width of the device
		wrapperWidth = (longestInputname.length() + longestOutputName.length()) * 5.0f + 50.0f;
		
		wrapperInputs = new LinkedHashMap<String, ModuleWrapperIO>();
		if (module.getInputs() != null) {
			for (int i = 0; i < module.getInputs().length; i++) {
				ModuleIO io = module.getInputs()[i];
				ModuleWrapperIO wrapperInput = new ModuleWrapperIO(io, 0, 25 + 15*i, true);
				wrapperInputs.put(io.getName(), wrapperInput);
			}
		}
		
		wrapperOutputs = new LinkedHashMap<String, ModuleWrapperIO>();
		if (module.getOutputs() != null) {
			for (int i = 0; i < module.getOutputs().length; i++) {
				ModuleIO io = module.getOutputs()[i];
				ModuleWrapperIO wrapperOutput = new ModuleWrapperIO(io, wrapperWidth, 25 + 15*i, false);
				wrapperOutputs.put(io.getName(), wrapperOutput);
			}
		}
		
		// Calculate the graphical height of the device
		wrapperHeight = (float) Math.max(wrapperInputs.size(), wrapperOutputs.size()) * 15 + 25;

		// Add event handler for parameter change
		module.addEventHandler(ModuleEvent.MODULE_PARAMETERS_CHANGED, e -> {
			eventDispatcher.dispatchEvent(new WrapperEvent(this, WrapperEvent.WRAPPER_MODULE_MODIFIED, e));
		});

		// Add event handler for composition layer change
		module.addEventHandler(ModuleEvent.MODULE_COMPOSITION_LAYER_CHANGED, e -> {
			eventDispatcher.dispatchEvent(new WrapperEvent(this, WrapperEvent.WRAPPER_MODULE_MODIFIED, e));
		});

		// Add event handler for IO change
		module.addEventHandler(ModuleEvent.MODULE_IO_CHANGED, e -> {
			reBuildWrapperForModule(module);
			eventDispatcher.dispatchEvent(new WrapperEvent(this, WrapperEvent.WRAPPER_IO_CHANGED, e));
		});
	}
	
	private void reBuildWrapperForModule(AbstractModule module) {
		//Make a list of the current device connectors that are connected to the device so
		//connectors connected to later removed IOs can be removed.
		List<ModuleConnector> currentConnectors = parentPatch.getModuleConnectorsByWrapper(this);
		
		// Get longest input and output names for width calculations
		String longestInputname = "";
		if (module.getInputs() != null) {
			for (ModuleIO input : module.getInputs()) {
				if (input.getName().length() > longestInputname.length()) {
					longestInputname = input.getName();
				}
			}
		}
		
		String longestOutputName = "";
		if (module.getOutputs() != null) {
			for (ModuleIO output : module.getOutputs()) {
				if (output.getName().length() > longestOutputName.length()) {
					longestOutputName = output.getName();
				}
			}
		}
		
		// Calculate the graphical width of the device
		wrapperWidth = (longestInputname.length() + longestOutputName.length()) * 5.0f + 50.0f;
		
		//Only create new ModuleWrapperIO objects for ModuleIO that is new, the rest can be reused.
		if (module.getInputs() != null) {
			LinkedHashMap<String, ModuleWrapperIO> newWrapperInputs = new LinkedHashMap<String, ModuleWrapperIO>();
			
			int i = 0;
			for (ModuleIO moduleInput: module.getInputs()) {
				String inputName = moduleInput.getName();
				
				//Check if there is an existing ModuleWrapperIO for this ModuleIO and overwrite
				//the new ModuleWrapperIO with the existing.
				//This check is done using the name of the ModuleIO.
				if (wrapperInputs.containsKey(inputName)) {
					ModuleWrapperIO wrapperInput = wrapperInputs.get(inputName);
					wrapperInput.posY = 25 + 15*i;
					newWrapperInputs.put(inputName, wrapperInput);
				}
				else {
					newWrapperInputs.put(inputName, new ModuleWrapperIO(moduleInput, 0, 25 + 15*i, true));
				}
				i++;
			}
			wrapperInputs = newWrapperInputs;
		}
		//Only create new ModuleWrapperIO objects for ModuleIO that is new, the rest can be reused.
		if (module.getOutputs() != null) {
			LinkedHashMap<String, ModuleWrapperIO> newWrapperOutputs = new LinkedHashMap<String, ModuleWrapperIO>();
			
			int i = 0;
			for (ModuleIO moduleOutput: module.getOutputs()) {
				String outputName = moduleOutput.getName();
				
				//Check if there is an existing ModuleWrapperIO for this ModuleIO and overwrite
				//the new ModuleWrapperIO with the existing.
				//This check is done using the name of the ModuleIO.
				if (wrapperOutputs.containsKey(outputName)) {
					ModuleWrapperIO wrapperOutput = wrapperOutputs.get(outputName);
					wrapperOutput.posY = 25 + 15*i;
					wrapperOutput.posX = wrapperWidth;
					newWrapperOutputs.put(outputName, wrapperOutput);
				}
				else {
					newWrapperOutputs.put(outputName, new ModuleWrapperIO(moduleOutput, wrapperWidth, 25 + 15*i, false));
				}
				i++;
			}
			
			wrapperOutputs = newWrapperOutputs;
		}
		
		//calculate the graphical dimensions of the device
		wrapperHeight = (float) Math.max(wrapperInputs.size(), wrapperOutputs.size()) * 15 + 25;
		
		//TODO Consider if this should be done in the synth itself by a iochange listener
		//Cleanup the synth this module is a member of for the new unconnected connectors
		for (ModuleConnector mc: currentConnectors) {
			boolean connected = false;
			for (ModuleWrapperIO mio: wrapperInputs.values()) {
				if (mc.module2Io == mio) {
					connected = true;
					break;
				}
			}
			for (ModuleWrapperIO mio: wrapperOutputs.values()) {
				if (mc.module1Io == mio) {
					connected = true;
					break;
				}
			}
			
			if (!connected) {
				parentPatch.removeModuleConnector(mc);
			}
		}
	}
	
	public boolean isBypassable() {
		return module.isBypassable();
	}
	
	public void setBypassed(boolean bypass) {
		if (isBypassable() && this.bypassed != bypass) {
			bypassed = bypass && isBypassable();
			eventDispatcher.dispatchEvent(new WrapperEvent(this, WrapperEvent.WRAPPER_BYPASS_CHANGED, !bypass, bypass));
		}
	}
	
	public boolean isBypassed() {
		return bypassed;
	}
	
	public void setCustomName(String customName) {
		String oldName = this.customName;
		if (!this.customName.equals(customName)) {
			this.customName = customName;
			eventDispatcher.dispatchEvent(new WrapperEvent(this, WrapperEvent.WRAPPER_NAME_CHANGED, oldName, customName));
		}
	}
	
	public String getCustomName() {
		return customName;
	}

	public void commitMove(double oldPosX, double oldPosY) {
		eventDispatcher.dispatchEvent(new WrapperEvent(this, WrapperEvent.WRAPPER_POSITION_CHANGED, oldPosX, oldPosY, posX, posY));
	}
	
	public ModuleWrapperIO getWrapperIoByModuleIo(ModuleIO moduleIo) {
		if (wrapperInputs.containsKey(moduleIo.getName())) {
			return wrapperInputs.get(moduleIo.getName());
		}
		else if (wrapperOutputs.containsKey(moduleIo.getName())) {
			return wrapperOutputs.get(moduleIo.getName());
		}
		return null;
	}

	public final void addEventHandler(EventType<? extends WrapperEvent> eventType, final EventHandler<WrapperEvent> eventHandler) {
		eventDispatcher.addEventHandler(eventType, eventHandler);
	}

	public final void removeEventHandler(EventType<? extends WrapperEvent> eventType, final EventHandler<WrapperEvent> eventHandler) {
		eventDispatcher.removeEventHandler(eventType, eventHandler);
	}

	public String toString() {
		if (!customName.equals("")) {
			if (module.getModuleMetaTag() != null) {
				return customName + " (" + module.getModuleName() + " [" + module.getModuleMetaTag()+ "])";
			}
			return customName + " (" + module.getModuleName()+ ")";
		}
		if (module.getModuleMetaTag() != null) {
			return module.getModuleName() + " [" + module.getModuleMetaTag()+ "]";
		}
		return module.getModuleName();
	}
	
	public JsonNode toJson(File destination) {
		ObjectMapper objectMapper = new ObjectMapper();
		ObjectNode node = objectMapper.createObjectNode();
		
		node.put("id", wrapperID);
		node.put("name", customName);
		node.put("x", posX);
		node.put("y", posY);
		node.put("bypass", bypassed);
		node.put("moduleclass", module.getModuleClassString());
		node.set("parameters", module.toJson(destination));
		
		return node;
	}
	
	public AbstractModule fromJson(JsonNode node, File source) {
		AbstractModule module = null;
		customName = node.get("name").asText();
		wrapperID = node.get("id").asText();
		posX = node.get("x").asDouble();
		posY = node.get("y").asDouble();
		bypassed = node.get("bypass").asBoolean();
		
		String moduleclass = node.get("moduleclass").asText();
		JsonNode parametersNode = node.get("parameters");
		
		if (moduleclass != null && parametersNode != null) {
			try {
				for (ModuleEntry moduleEntry: WorldSynth.getModuleRegister().getRegisteredModuleEntries()) {
					Class<? extends AbstractModule> c = moduleEntry.getModuleClass();
					if (c.getName().equals(moduleclass)) {
						module = ModuleConstruction.constructModule(c, parametersNode, source, this);
						break;
					}
				}
			} catch (NoSuchMethodException | SecurityException | InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e1) {
				e1.printStackTrace();
			}
			
			//If the module is not in the library
			if (module == null) {
				module = new ModuleUnknown(moduleclass, parametersNode);
			}
		}

		return module;
	}
	
	public AbstractDatatype buildInputData(ModuleInputRequest request, Build build) {
		//Transform inputrequests into output requests if possible
		ModuleWrapperIO input = getWrapperIoByModuleIo(request.getInput());
		List<ModuleConnector> connectors = parentPatch.getModuleConnectorsByWrapperIo(input);
		ModuleConnector connector = null;
		if (!connectors.isEmpty()) {
			connector = connectors.get(0);
		}
		if (connector == null) {
			return null;
		}
		ModuleWrapper requestedWrapper = connector.module1;
		ModuleOutput requestedModuleOutput = (ModuleOutput) connector.module1Io.getIO();
		
		ModuleOutputRequest outputRequest = new ModuleOutputRequest(requestedModuleOutput, request.getData());
		if (request.getData() instanceof DatatypeMultitype) {
			for (AbstractDatatype datatype: ((DatatypeMultitype) request.getData()).getDatatypes()) {
				if (requestedModuleOutput.getData().getClass().equals(datatype.getClass())) {
					outputRequest = new ModuleOutputRequest(requestedModuleOutput, datatype);
					break;
				}
			}
		}

		return build.getModuleOutput(requestedWrapper, outputRequest);
	}
}
