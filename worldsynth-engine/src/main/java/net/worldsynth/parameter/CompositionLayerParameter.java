/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.parameter;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import net.worldsynth.composition.Composition;
import net.worldsynth.composition.layer.Layer;
import net.worldsynth.module.ModuleCompositionLayer;
import net.worldsynth.standalone.ui.parameters.CompositionLayerParameterDropdownSelector;
import net.worldsynth.standalone.ui.parameters.ParameterUiElement;
import net.worldsynth.event.EventHandler;
import net.worldsynth.parameter.event.ParameterEvent;
import net.worldsynth.composition.event.CompositionEvent;
import net.worldsynth.composition.event.CompositionLayerRemovalEvent;

import java.io.File;
import java.util.function.Supplier;

public class CompositionLayerParameter<T extends Layer> extends AbstractParameter<T> {

	private final ModuleCompositionLayer<T> parentModule;
	private final Class<T> layerClass;
	private final Supplier<T> layerConstructor;

	private final EventHandler<CompositionEvent> compositionHandler;

	public CompositionLayerParameter(String name, String displayName, String description, ModuleCompositionLayer<T> parentModule, Class<T> layerClass, Supplier layerConstructor) {
		super(name, displayName, description, null);
		this.layerClass = layerClass;
		this.parentModule = parentModule;
		this.layerConstructor = layerConstructor;
		
		compositionHandler = e -> {
			JsonNode oldJson = toJson(null);

			if (e.getEventType() == CompositionLayerRemovalEvent.COMPOSITION_LAYER_REMOVED) {
				setValue(null);
			}

			eventDispatcher.dispatchEvent(new ParameterEvent(this, ParameterEvent.PARAMETER_CHANGED, oldJson, toJson(null)));
		};
	}
	
	private Composition getComposition() {
		return parentModule.getComposition();
	}

	@Override
	public void setValue(T value) {
		if (getValue() != null) {
			getComposition().removeEventHandler(CompositionEvent.COMPOSITION_ANY, compositionHandler);
		}

		super.setValue(value);

		if (value != null) {
			getComposition().addEventHandler(CompositionEvent.COMPOSITION_ANY, compositionHandler);
		}
	}

	@Override
	public ParameterUiElement<T> getUi() {
		return new CompositionLayerParameterDropdownSelector<T>(this, layerClass, layerConstructor, getComposition());
	}

	@Override
	public JsonNode toJson(File destination) {
		ObjectMapper objectMapper = new ObjectMapper();

		if (getValue() != null) {
			return objectMapper.convertValue(getValue().getLayerId(), JsonNode.class);
		} else {
			return objectMapper.convertValue(null, JsonNode.class);
		}
	}

	@Override
	public void fromJson(JsonNode node, File source) {
		int layerId = node.asInt();
		setValue((T) getComposition().getLayer(layerId));
	}
}
