/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.parameter;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import net.worldsynth.standalone.ui.parameters.ParameterUiElement;
import net.worldsynth.standalone.ui.parameters.StringParameterField;

import java.io.File;

public class StringParameter extends AbstractParameter<String> {
	
	public StringParameter(String name, String displayName, String description, String defaultValue) {
		super(name, displayName, description, defaultValue);
	}
	
	@Override
	public ParameterUiElement<String> getUi() {
		return new StringParameterField(this);
	}
	
	@Override
	public JsonNode toJson(File destination) {
		ObjectMapper objectMapper = new ObjectMapper();
		return objectMapper.convertValue(getValue(), JsonNode.class);
	}

	@Override
	public void fromJson(JsonNode node, File source) {
		setValue(node.asText());
	}
}
