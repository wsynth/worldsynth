/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.parameter.table;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import javafx.scene.control.CheckBox;
import javafx.scene.control.Control;

public abstract class BooleanColumn<P> extends TableColumn<P, Boolean> {

	public BooleanColumn(String name, String displayName, String description) {
		super(name, displayName, description);
	}

	@Override
	public Control getNewUiControl(Boolean value, OnChange notifier) {
		CheckBox checkBox = new CheckBox();
		checkBox.setSelected(value);
		checkBox.selectedProperty().addListener((observable, oldValue, newValue) -> {
			notifier.call();
		});
		return checkBox;
	}
	
	@Override
	public Boolean getColumnValueFromUiControl(Control control) {
		CheckBox checkBox = (CheckBox) control;
		return checkBox.isSelected();
	}
	
	@Override
	public JsonNode valueToJson(Boolean value) {
		ObjectMapper objectMapper = new ObjectMapper();
		return objectMapper.convertValue(value, JsonNode.class);
	}
	
	@Override
	public Boolean valueFromJson(JsonNode node) {
		return node.asBoolean();
	}
}
