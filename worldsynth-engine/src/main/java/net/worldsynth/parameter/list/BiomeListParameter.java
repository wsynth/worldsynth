/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.parameter.list;

import java.util.List;
import java.util.function.Supplier;

import net.worldsynth.biome.Biome;
import net.worldsynth.parameter.table.BiomeColumn;
import net.worldsynth.parameter.table.TableColumn;

public class BiomeListParameter extends ListParameter<Biome> {

	public BiomeListParameter(String name, String displayName, String description, List<Biome> defaultValue) {
		super(name, displayName, description, defaultValue, new Supplier<TableColumn<Row<Biome>, Biome>>() {

			@Override
			public BiomeColumn<Row<Biome>> get() {
				return new BiomeColumn<Row<Biome>>(name, displayName, null) {
					
					@Override
					public void setColumnValue(Row<Biome> row, Biome value) {
						row.rowValue = value;
					}
					
					@Override
					public Biome getColumnValue(Row<Biome> row) {
						return row.rowValue;
					}
				};
			}
		});
	}

	@Override
	public Row<Biome> newRow() {
		return new Row<Biome>(Biome.NULL);
	}
	
}
