/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.parameter;

import com.fasterxml.jackson.databind.JsonNode;

import net.worldsynth.color.WsColor;
import net.worldsynth.standalone.ui.parameters.ColorParameterSelector;
import net.worldsynth.standalone.ui.parameters.ParameterUiElement;

import java.io.File;

public class ColorParameter extends AbstractParameter<WsColor> {

	public ColorParameter(String name, String displayName, String description, WsColor defaultValue) {
		super(name, displayName, description, defaultValue);
	}

	@Override
	public ParameterUiElement<WsColor> getUi() {
		return new ColorParameterSelector(this);
	}

	@Override
	public JsonNode toJson(File destination) {
		return getValue().toJson();
	}

	@Override
	public void fromJson(JsonNode node, File source) {
		setValue(new WsColor(node));
	}
}
