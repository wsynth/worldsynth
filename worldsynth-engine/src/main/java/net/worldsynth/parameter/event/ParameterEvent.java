/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.parameter.event;

import com.fasterxml.jackson.databind.JsonNode;
import net.worldsynth.event.Event;
import net.worldsynth.event.EventType;
import net.worldsynth.event.HistoricalEvent;
import net.worldsynth.parameter.AbstractParameter;

public class ParameterEvent extends Event implements HistoricalEvent {

	public static final EventType<ParameterEvent> PARAMETER_ANY = new EventType<>(Event.ANY, "PARAMETER_ANY");
	public static final EventType<ParameterEvent> PARAMETER_CHANGED = new EventType<>(ParameterEvent.PARAMETER_ANY, "PARAMETER_CHANGED");

	private JsonNode oldJson;
	private JsonNode newJson;

	public ParameterEvent(AbstractParameter<?> source, EventType<ParameterEvent> eventType) {
		super(source, eventType);
	}

	public ParameterEvent(AbstractParameter<?> source, EventType<ParameterEvent> eventType, JsonNode oldValue, JsonNode newValue) {
		super(source, eventType);
		this.oldJson = oldValue;
		this.newJson = newValue;
	}

	public JsonNode getOldJson() {
		return oldJson;
	}

	public JsonNode getNewJson() {
		return newJson;
	}

	@Override
	public void undo() {
		AbstractParameter<?> sourceParameter = (AbstractParameter<?>) getSource();

		if (getEventType() == PARAMETER_CHANGED) {
			sourceParameter.fromJson(oldJson, null);
		}
	}

	@Override
	public void redo() {
		AbstractParameter<?> sourceParameter = (AbstractParameter<?>) getSource();

		if (getEventType() == PARAMETER_CHANGED) {
			sourceParameter.fromJson(newJson, null);
		}
	}
}
