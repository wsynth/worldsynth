/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.parameter;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import javafx.stage.FileChooser.ExtensionFilter;
import net.worldsynth.standalone.ui.parameters.ParameterUiElement;
import net.worldsynth.standalone.ui.parameters.FileListParameterUi;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class FileListParameter extends AbstractParameter<List<File>> {

	protected List<ExtensionFilter> fileExtensions;

	public FileListParameter(String name, String displayName, String description, List<File> defaultValue, List<ExtensionFilter> fileExtensions) {
		super(name, displayName, description, defaultValue);

		this.fileExtensions = fileExtensions;
	}

	@Override
	public ParameterUiElement<List<File>> getUi() {
		return new FileListParameterUi(this);
	}

	@Override
	public JsonNode toJson(File destination) {
		ObjectMapper objectMapper = new ObjectMapper();

		ArrayNode filesNode = objectMapper.createArrayNode();
		for (File f : getValue()) {
			filesNode.add(getPath(f, destination));
		}

		return filesNode;
	}

	private String getPath(File value, File destination) {
		if (destination != null && fileIsChild(destination.getParentFile(), value)) {
			return destination.toPath().relativize(value.toPath()).toString();
		}
		else {
			return value.getAbsolutePath();
		}
	}

	private boolean fileIsChild(final File parent, File child) {
		while (child.getParentFile() != null) {
			child = child.getParentFile();
			if (parent.equals(child)) {
				return true;
			}
		}
		return false;
	}

	@Override
	public void fromJson(JsonNode node, File source) {
		List<File> objectFiles = new ArrayList<>();
		for (JsonNode e : node) {
			String path = e.asText();
			if (path.startsWith("..")) {
				// Path is relative
				objectFiles.add(source.toPath().resolve(path).normalize().toFile());
			}
			else {
				// Path is absolute
				objectFiles.add(new File(path));
			}
		}
		setValue(objectFiles);
	}

	public List<ExtensionFilter> getFilters() {
		return fileExtensions;
	}
}
