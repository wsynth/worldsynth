/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.parameter;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import net.worldsynth.biome.Biome;
import net.worldsynth.biome.BiomeRegistry;
import net.worldsynth.standalone.ui.parameters.BiomeParameterSelector;
import net.worldsynth.standalone.ui.parameters.ParameterUiElement;

import java.io.File;

public class BiomeParameter extends AbstractParameter<Biome> {
	
	public BiomeParameter(String name, String displayName, String description, Biome defaultValue) {
		super(name, displayName, description, defaultValue);
	}
	
	@Override
	public ParameterUiElement<Biome> getUi() {
		return new BiomeParameterSelector(this);
	}

	@Override
	public JsonNode toJson(File destination) {
		ObjectMapper objectMapper = new ObjectMapper();
		return objectMapper.convertValue(getValue().getIdName(), JsonNode.class);
	}

	@Override
	public void fromJson(JsonNode node, File source) {
		setValue(BiomeRegistry.getBiome(node.asText()));
	}
}
