/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.parameter;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import net.worldsynth.standalone.ui.parameters.ObjectParameterDropdownSelector;
import net.worldsynth.standalone.ui.parameters.ParameterUiElement;

import java.io.File;

public class ObjectParameter extends AbstractParameter<Object> {
	
	private Object[] selectables;
	
	public ObjectParameter(String name, String displayName, String description, Object defaultValue, Object... selectables) {
		super(name, displayName, description, defaultValue);
		
		this.selectables = selectables;
	}
	
	@Override
	public ParameterUiElement<Object> getUi() {
		return new ObjectParameterDropdownSelector(this);
	}
	
	@Override
	public JsonNode toJson(File destination) {
		ObjectMapper objectMapper = new ObjectMapper();
		return objectMapper.convertValue(getValue().getClass().toString(), JsonNode.class);
	}

	@Override
	public void fromJson(JsonNode node, File source) {
		for (Object selectable: selectables) {
			if (node.asText().equals(selectable.getClass().toString())) {
				setValue(selectable);
				return;
			}
		}
	}
	
	public Object[] getSelectables() {
		return selectables;
	}
}
