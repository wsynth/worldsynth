/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.addon;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import net.worldsynth.module.AbstractModuleRegister;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import java.util.zip.ZipEntry;

public class AddonJarLoader {
	private static final Logger LOGGER = LogManager.getLogger(AddonJarLoader.class);

	private ArrayList<Class<?>> loadedClasses = new ArrayList<Class<?>>();

	public AddonJarLoader(File jar) throws IOException, ClassNotFoundException {
		// Make an addon class loader
		URL[] urls = { jar.toURI().toURL() };
		ClassLoader addonClassLoader = URLClassLoader.newInstance(urls, AbstractModuleRegister.class.getClassLoader());
		
		// Load all classes in the jar
		LOGGER.debug("Loading jar file: " + jar);
		JarFile jarFile = new JarFile(jar);

		LOGGER.debug("Looking for addon json");
		JsonNode addonJson = getAddonJson(jarFile);
		if (addonJson != null) {
			// Load addon from defined entry point addon json is present
			LOGGER.debug("Found addon properties");

			JsonNode entryPoints = addonJson.get("addon-entrypoints");
			for (JsonNode e : entryPoints) {
				String entry = e.asText();
				LOGGER.debug("Addon entrypoint: " + entry);
				loadedClasses.add(addonClassLoader.loadClass(entry));
			}
		}
		else {
			// Fallback to old exhaustive enumeration method if addon json is not present
			LOGGER.debug("Did not find addon properties");
			LOGGER.debug("Enumerating entries");
			Enumeration<JarEntry> allJarEntries = jarFile.entries();
			while (allJarEntries.hasMoreElements()) {
				JarEntry jarEntry = allJarEntries.nextElement();
				LOGGER.debug(jarEntry);
				if (jarEntry.isDirectory() || !jarEntry.getName().endsWith(".class")) {
					continue;
				}

				String className = jarEntry.getName().substring(0, jarEntry.getName().length() - 6);
				className = className.replace('/', '.');
				loadedClasses.add(addonClassLoader.loadClass(className));
			}
		}

		jarFile.close();
	}

	private JsonNode getAddonJson(JarFile jarFile) throws IOException {
		ZipEntry addonJsonZip = jarFile.getEntry("worldsynth.addon.json");
		if (addonJsonZip == null) {
			return null;
		}

		InputStream addonJsonStream = jarFile.getInputStream(addonJsonZip);
		return jsonFromStream(addonJsonStream);
	}

	private JsonNode jsonFromStream(InputStream stream) throws IOException {
		String addonJson = streamToString(stream);

		ObjectMapper mapper = new ObjectMapper();
		return mapper.readTree(addonJson);
	}

	private String streamToString(InputStream stream) throws IOException {
		StringBuilder sb = new StringBuilder();
		for (int ch; (ch = stream.read()) != -1; ) {
			sb.append((char) ch);
		}
		return sb.toString();
	}

	public <T> List<T> getInstancesAssignableFrom(Class<T> classType) throws NoSuchMethodException, SecurityException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		ArrayList<T> instances = new ArrayList<T>();
		for (Class<?> c: loadedClasses) {
			if (classType.isAssignableFrom(c)) {
				@SuppressWarnings("unchecked")
				Class<T> assignableClass = (Class<T>) c;
				Constructor<T> classConstructor = assignableClass.getConstructor();
				instances.add(classConstructor.newInstance());
			}
		}
		return instances;
	}
}
