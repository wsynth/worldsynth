/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.addon;

import net.worldsynth.WorldSynthDirectoryConfig;
import net.worldsynth.biome.BiomeProfile;
import net.worldsynth.biome.addon.IBiomeAddon;
import net.worldsynth.customobject.CustomObjectFormat;
import net.worldsynth.customobject.addon.ICustomObjectFormatAddon;
import net.worldsynth.material.MaterialProfile;
import net.worldsynth.material.addon.IMaterialAddon;
import net.worldsynth.module.AbstractModuleRegister;
import net.worldsynth.module.addon.IModuleAddon;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class AddonLoader implements IAddon, IBiomeAddon, IMaterialAddon, IModuleAddon, ICustomObjectFormatAddon {
	private static final Logger logger = LogManager.getLogger(AddonLoader.class);
	
	private final ArrayList<AddonJarLoader> jarLoaders = new ArrayList<>();
	protected final ArrayList<IAddon> addons = new ArrayList<>();
	
	public AddonLoader(File addonDirectory, IAddon... injectedAddons) {
		if (injectedAddons != null) {
			logger.debug("Loading injected addons");
			this.addons.addAll(Arrays.asList(injectedAddons));
		}
		logger.debug("Loading injected addons");
		loadAddonsFromDirectory(addonDirectory);
		try {
			addons.addAll(getInstancesAssignableFrom(IAddon.class));
		} catch (NoSuchMethodException | SecurityException | InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
			logger.error("Problem during instancing of addons", e);
		}
	}
	
	private void loadAddonsFromDirectory(File addonDirectory) {
		for (File sub: addonDirectory.listFiles()) {
			if (sub.isDirectory()) {
				loadAddonsFromDirectory(sub);
			}
			else if (sub.getAbsolutePath().endsWith(".jar")) {
				try {
					logger.debug("Found addon: " + sub.getName());
					loadAddon(sub);
				} catch (ClassNotFoundException | IOException e) {
					logger.error("Problem loading addon: " + sub.getName(), e);
				}
			}
		}
	}
	
	private void loadAddon(File jar) throws ClassNotFoundException, IOException {
		AddonJarLoader jarLoader = new AddonJarLoader(jar);
		jarLoaders.add(jarLoader);
	}
	
	private <T> List<T> getInstancesAssignableFrom(Class<T> classType) throws NoSuchMethodException, SecurityException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		ArrayList<T> instances = new ArrayList<>();
		for (AddonJarLoader jarLoader: jarLoaders) {
			instances.addAll(jarLoader.getInstancesAssignableFrom(classType));
		}
		return instances;
	}
	
	public void initAddons(WorldSynthDirectoryConfig directoryConfig) {
		ArrayList<IAddon> addonLoaders = new ArrayList<>();
		for (IAddon addon: addons) {
			addonLoaders.add(addon);
		}
		
		for (IAddon addon: addonLoaders) {
			addon.initAddon(directoryConfig);
		}
	}
	
	@Override
	public void initAddon(WorldSynthDirectoryConfig directoryConfig) {}
	
	@Override
	public List<AbstractModuleRegister> getAddonModuleRegisters() {
		ArrayList<AbstractModuleRegister> moduleRegisters = new ArrayList<>();
		for (IAddon addon: addons) {
			if (addon instanceof IModuleAddon moduleAddon) {
				moduleRegisters.addAll(moduleAddon.getAddonModuleRegisters());
			}
		}
		return moduleRegisters;
	}

	@Override
	public List<MaterialProfile<?, ?>> getAddonMaterialProfiles() {
		ArrayList<MaterialProfile<?, ?>> materialProfiles = new ArrayList<>();
		for (IAddon addon: addons) {
			if (addon instanceof IMaterialAddon materialAddon) {
				materialProfiles.addAll(materialAddon.getAddonMaterialProfiles());
			}
		}
		return materialProfiles;
	}

	@Override
	public List<BiomeProfile<?>> getAddonBiomeProfiles() {
		ArrayList<BiomeProfile<?>> biomeProfiles = new ArrayList<>();
		for (IAddon addon: addons) {
			if (addon instanceof IBiomeAddon biomeAddon) {
				biomeProfiles.addAll(biomeAddon.getAddonBiomeProfiles());
			}
		}
		return biomeProfiles;
	}
	
	@Override
	public List<CustomObjectFormat> getAddonCustomObjectFormats() {
		ArrayList<CustomObjectFormat> objectFormats = new ArrayList<>();
		for (IAddon addon: addons) {
			if (addon instanceof ICustomObjectFormatAddon objectFormatAddon) {
				objectFormats.addAll(objectFormatAddon.getAddonCustomObjectFormats());
			}
		}
		return objectFormats;
	}
}
