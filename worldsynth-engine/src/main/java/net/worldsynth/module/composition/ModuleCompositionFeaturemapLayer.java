/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.module.composition;

import net.worldsynth.composition.layer.featuremap.LayerFeaturemap;
import net.worldsynth.datatype.AbstractDatatype;
import net.worldsynth.datatype.DatatypeFeaturemap;
import net.worldsynth.extent.Extent;
import net.worldsynth.featurepoint.Featurepoint2D;
import net.worldsynth.module.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ModuleCompositionFeaturemapLayer extends ModuleCompositionLayer<LayerFeaturemap> {

	public ModuleCompositionFeaturemapLayer() {
		super(() -> new LayerFeaturemap("Featuremap layer"));
	}

	@Override
	public Class<LayerFeaturemap> getLayerClass() {
		return LayerFeaturemap.class;
	}
	
	@Override
	public AbstractDatatype buildModule(Map<String, AbstractDatatype> inputs, ModuleOutputRequest request) {
		DatatypeFeaturemap requestData = (DatatypeFeaturemap) request.data;
		
		//----------READ INPUTS----------//
		
		//----------BUILD----------//
		
		List<Featurepoint2D> generatedPoints = getFeatures(requestData.extent);
		
		Featurepoint2D[] points = new Featurepoint2D[generatedPoints.size()];
		generatedPoints.toArray(points);
		requestData.setFeaturepoints(points);
		
		return requestData;
	}
	
	public List<Featurepoint2D> getFeatures(Extent extent) {
		if (getLayer() == null) return new ArrayList<>();
		return getLayer().getFeatures(extent);
	}
	
	@Override
	public Map<String, ModuleInputRequest> getInputRequests(ModuleOutputRequest outputRequest) {
		HashMap<String, ModuleInputRequest> inputRequests = new HashMap<>();
		return inputRequests;
	}

	@Override
	public String getModuleName() {
		if (getLayer() == null) return "Featuremap layer";
		return getLayer().getName();
	}
	
	@Override
	public String getModuleMetaTag() {
		return "FL";
	}

	@Override
	public IModuleCategory getModuleCategory() {
		return ModuleCategory.GENERATOR;
	}

	@Override
	public ModuleInput[] registerInputs() {
		ModuleInput[] i = {};
		return i;
	}

	@Override
	public ModuleOutput[] registerOutputs() {
		ModuleOutput[] o = {
				new ModuleOutput(new DatatypeFeaturemap(), "Output")
				};
		return o;
	}

	@Override
	public boolean isBypassable() {
		return false;
	}
}
