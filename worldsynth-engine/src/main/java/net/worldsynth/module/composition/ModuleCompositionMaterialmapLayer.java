/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.module.composition;

import net.worldsynth.composition.layer.materialmap.LayerMaterialmap;
import net.worldsynth.datatype.AbstractDatatype;
import net.worldsynth.datatype.DatatypeMaterialmap;
import net.worldsynth.material.MaterialState;
import net.worldsynth.module.*;

import java.util.HashMap;
import java.util.Map;

public class ModuleCompositionMaterialmapLayer extends ModuleCompositionLayer<LayerMaterialmap> {

	public ModuleCompositionMaterialmapLayer() {
		super(() -> new LayerMaterialmap("Materialmap layer"));
	}

	@Override
	public Class<LayerMaterialmap> getLayerClass() {
		return LayerMaterialmap.class;
	}

	@Override
	public AbstractDatatype buildModule(Map<String, AbstractDatatype> inputs, ModuleOutputRequest request) {
		DatatypeMaterialmap requestData = (DatatypeMaterialmap) request.data;
		
		double x = requestData.extent.getX();
		double z = requestData.extent.getZ();
		double res = requestData.resolution;
		int mpw = requestData.mapPointsWidth;
		int mpl = requestData.mapPointsLength;
		
		//----------READ INPUTS----------//
		
		//----------BUILD----------//
		
		MaterialState<?, ?>[][] map = new MaterialState<?, ?>[mpw][mpl];
		
		for (int u = 0; u < mpw; u++) {
			for (int v = 0; v < mpl; v++) {
				map[u][v] = getMaterialAt(x+u*res, z+v*res);
			}
		}
		
		requestData.setMaterialmap(map);
		
		return requestData;
	}
	
	public MaterialState<?, ?> getMaterialAt(double x, double z) {
		if (getLayer() == null) return null;
		return getLayer().getValueAt((int) x, (int) z);
	}
	
	@Override
	public Map<String, ModuleInputRequest> getInputRequests(ModuleOutputRequest outputRequest) {
		HashMap<String, ModuleInputRequest> inputRequests = new HashMap<>();
		return inputRequests;
	}
	
	@Override
	public String getModuleName() {
		if (getLayer() == null) return "Materialmap layer";
		return getLayer().getName();
	}
	
	@Override
	public String getModuleMetaTag() {
		return "ML";
	}

	@Override
	public IModuleCategory getModuleCategory() {
		return ModuleCategory.GENERATOR;
	}

	@Override
	public ModuleInput[] registerInputs() {
		ModuleInput[] i = {};
		return i;
	}

	@Override
	public ModuleOutput[] registerOutputs() {
		ModuleOutput[] o = {
				new ModuleOutput(new DatatypeMaterialmap(), "Output")
				};
		return o;
	}

	@Override
	public boolean isBypassable() {
		return false;
	}
}
