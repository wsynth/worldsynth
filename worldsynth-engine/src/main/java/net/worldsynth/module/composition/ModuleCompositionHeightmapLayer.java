/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.module.composition;

import net.worldsynth.composition.layer.heightmap.LayerHeightmap;
import net.worldsynth.datatype.AbstractDatatype;
import net.worldsynth.datatype.DatatypeHeightmap;
import net.worldsynth.module.*;
import net.worldsynth.util.HeightmapUtil;

import java.util.HashMap;
import java.util.Map;

public class ModuleCompositionHeightmapLayer extends ModuleCompositionLayer<LayerHeightmap> {

	public ModuleCompositionHeightmapLayer() {
		super(() -> new LayerHeightmap("Heightmap layer"));
	}

	@Override
	public Class<LayerHeightmap> getLayerClass() {
		return LayerHeightmap.class;
	}
	
	@Override
	public AbstractDatatype buildModule(Map<String, AbstractDatatype> inputs, ModuleOutputRequest request) {
		DatatypeHeightmap requestData = (DatatypeHeightmap) request.data;
		
		double x = requestData.extent.getX();
		double z = requestData.extent.getZ();
		double res = requestData.resolution;
		int mpw = requestData.mapPointsWidth;
		int mpl = requestData.mapPointsLength;
		
		//----------READ INPUTS----------//
		
		float normalizedHeight = getSynthParameters().getNormalizedHeightmapHeight();
		
		//Read in mask
		float[][] mask = null;
		if (inputs.get("mask") != null) {
			mask = ((DatatypeHeightmap) inputs.get("mask")).getHeightmap();
		}
		
		//----------BUILD----------//
		
		float[][] map = new float[mpw][mpl];
		
		for (int u = 0; u < mpw; u++) {
			for (int v = 0; v < mpl; v++) {
				map[u][v] = getHeightAt(x+u*res, z+v*res);
			}
		}
		
		//Apply mask
		if (mask != null) {
			HeightmapUtil.applyMask(map, mask, normalizedHeight);
		}
		
		requestData.setHeightmap(map);
		
		return requestData;
	}
	
	public float getHeightAt(double x, double z) {
		if (getLayer() == null) return 0.0f;
		return getLayer().getValueAt((int) x, (int) z);
	}
	
	@Override
	public Map<String, ModuleInputRequest> getInputRequests(ModuleOutputRequest outputRequest) {
		HashMap<String, ModuleInputRequest> inputRequests = new HashMap<>();
		
		inputRequests.put("mask", new ModuleInputRequest(getInput("Mask"), outputRequest.data));
		return inputRequests;
	}

	@Override
	public String getModuleName() {
		if (getLayer() == null) return "Heightmap layer";
		return getLayer().getName();
	}
	
	@Override
	public String getModuleMetaTag() {
		return "HL";
	}

	@Override
	public IModuleCategory getModuleCategory() {
		return ModuleCategory.GENERATOR;
	}

	@Override
	public ModuleInput[] registerInputs() {
		ModuleInput[] i = {
				new ModuleInput(new DatatypeHeightmap(), "Mask")
				};
		return i;
	}

	@Override
	public ModuleOutput[] registerOutputs() {
		ModuleOutput[] o = {
				new ModuleOutput(new DatatypeHeightmap(), "Output")
				};
		return o;
	}

	@Override
	public boolean isBypassable() {
		return false;
	}
}
