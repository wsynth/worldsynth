/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.module.vectormap;

import java.util.HashMap;
import java.util.Map;

import net.worldsynth.datatype.AbstractDatatype;
import net.worldsynth.datatype.DatatypeScalar;
import net.worldsynth.datatype.DatatypeVectormap;
import net.worldsynth.extent.Extent;
import net.worldsynth.module.AbstractModule;
import net.worldsynth.module.IModuleCategory;
import net.worldsynth.module.ModuleCategory;
import net.worldsynth.module.ModuleInput;
import net.worldsynth.module.ModuleInputRequest;
import net.worldsynth.module.ModuleOutput;
import net.worldsynth.module.ModuleOutputRequest;
import net.worldsynth.parameter.AbstractParameter;
import net.worldsynth.parameter.DoubleParameter;

public class ModuleVectorfieldTranslate extends AbstractModule {
	
	private DoubleParameter xTranslate = new DoubleParameter("xtranslate", "Translate x", null, 0.0, Double.NEGATIVE_INFINITY, Double.POSITIVE_INFINITY, -100.0, 100.0);
	private DoubleParameter zTranslate = new DoubleParameter("ztranslate", "Translate z", null, 0.0, Double.NEGATIVE_INFINITY, Double.POSITIVE_INFINITY, -100.0, 100.0);
	
	@Override
	public AbstractParameter<?>[] registerParameters() {
		AbstractParameter<?>[] p = {
				xTranslate,
				zTranslate
				};
		return p;
	}
	
	@Override
	public Map<String, ModuleInputRequest> getInputRequests(ModuleOutputRequest outputRequest) {
		HashMap<String, ModuleInputRequest> inputRequests = new HashMap<String, ModuleInputRequest>();
		
		double xt = xTranslate.getValue();
		double zt = zTranslate.getValue();
		
		DatatypeScalar scalarDataX = (DatatypeScalar) buildInputData(new ModuleInputRequest(getInput("X"), new DatatypeScalar()));
		DatatypeScalar scalarDataZ = (DatatypeScalar) buildInputData(new ModuleInputRequest(getInput("Z"), new DatatypeScalar()));
		
		if (scalarDataX != null) {
			xt = scalarDataX.getValue();
		}
		if (scalarDataZ != null) {
			zt = scalarDataZ.getValue();
		}
		
		DatatypeVectormap ord = (DatatypeVectormap) outputRequest.data;
		DatatypeVectormap translatedRequestData = new DatatypeVectormap(Extent.translatedBuildExtent(ord.extent, -xt, 0, -zt), ord.resolution);
		
		inputRequests.put("input", new ModuleInputRequest(getInput("Input"), translatedRequestData));
		
		return inputRequests;
	}
	
	@Override
	public AbstractDatatype buildModule(Map<String, AbstractDatatype> inputs, ModuleOutputRequest request) {
		DatatypeVectormap requestData = (DatatypeVectormap) request.data;
		DatatypeVectormap inputData = (DatatypeVectormap) inputs.get("input");

		if (inputData == null) {
			// Return null if there is no input
			return null;
		}

		requestData.setVectormap(inputData.getVectormap());
		return requestData;
	}

	@Override
	public String getModuleName() {
		return "Translate";
	}

	@Override
	public IModuleCategory getModuleCategory() {
		return ModuleCategory.MODIFIER;
	}

	@Override
	public ModuleInput[] registerInputs() {
		ModuleInput[] i = {
				new ModuleInput(new DatatypeVectormap(), "Input"),
				new ModuleInput(new DatatypeScalar(), "X"),
				new ModuleInput(new DatatypeScalar(), "Z")
				};
		return i;
	}

	@Override
	public ModuleOutput[] registerOutputs() {
		ModuleOutput[] o = {
				new ModuleOutput(new DatatypeVectormap(), "Output")
				};
		return o;
	}

	@Override
	public boolean isBypassable() {
		return true;
	}
}
