/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.module.vectormap;

import java.util.HashMap;
import java.util.Map;

import net.worldsynth.datatype.AbstractDatatype;
import net.worldsynth.datatype.DatatypeHeightmap;
import net.worldsynth.datatype.DatatypeVectormap;
import net.worldsynth.module.AbstractModule;
import net.worldsynth.module.IModuleCategory;
import net.worldsynth.module.ModuleCategory;
import net.worldsynth.module.ModuleInput;
import net.worldsynth.module.ModuleInputRequest;
import net.worldsynth.module.ModuleOutput;
import net.worldsynth.module.ModuleOutputRequest;
import net.worldsynth.parameter.AbstractParameter;
import net.worldsynth.parameter.EnumParameter;
import net.worldsynth.parameter.FloatParameter;

public class ModuleVectorfieldHeightmapComponents extends AbstractModule {
	
	private FloatParameter gain = new FloatParameter("gain", "Gain", null, 1.0f, 0.0f, Float.MAX_VALUE, 0.0f, 1.0f);
	private EnumParameter<CoordinateSystem> coordinateSystem = new EnumParameter<CoordinateSystem>("coordinatesystem", "Component type", null, CoordinateSystem.class, CoordinateSystem.CARTESIAN);
	
	@Override
	public AbstractParameter<?>[] registerParameters() {
		AbstractParameter<?>[] p = {
				gain,
				coordinateSystem
				};
		return p;
	}
	
	@Override
	public AbstractDatatype buildModule(Map<String, AbstractDatatype> inputs, ModuleOutputRequest request) {
		DatatypeVectormap requestData = (DatatypeVectormap) request.data;
		
		int mpw = requestData.mapPointsWidth;
		int mpl = requestData.mapPointsLength;
		
		//----------READ INPUTS----------//
		
		float normalizedHeight = getSynthParameters().getNormalizedHeightmapHeight();
		
		float gain = this.gain.getValue();
		CoordinateSystem coordinateSystem = this.coordinateSystem.getValue();
		
		if (inputs.get("pcomponent") == null || inputs.get("scomponent") == null) {
			//If at least one of the component inputs are null, there is not enough input and then just return null
			return null;
		}
		
		float[][] inputMapPComponent = ((DatatypeHeightmap) inputs.get("pcomponent")).getHeightmap();
		float[][] inputMapSComponent = ((DatatypeHeightmap) inputs.get("scomponent")).getHeightmap();
		
		//----------BUILD----------//
		
		float[][][] field = new float[mpw][mpl][2];
		
		for (int u = 0; u < mpw; u++) {
			for (int v = 0; v < mpl; v++) {
				float p = inputMapPComponent[u][v]/normalizedHeight;
				float s = inputMapSComponent[u][v]/normalizedHeight;
				
				if (coordinateSystem == CoordinateSystem.CARTESIAN) {
					field[u][v] = new float[] {
							p * gain,
							s * gain
							};
				}
				else if (coordinateSystem == CoordinateSystem.POLAR) {
					field[u][v] = new float[] {
							p * (float) Math.cos(s * Math.PI * 2.0) * gain,
							p * (float) Math.sin(s * Math.PI * 2.0) * gain
							};
				}
			}
		}
		
		requestData.setVectormap(field);
		
		return requestData;
	}

	@Override
	public Map<String, ModuleInputRequest> getInputRequests(ModuleOutputRequest outputRequest) {
		HashMap<String, ModuleInputRequest> inputRequests = new HashMap<String, ModuleInputRequest>();
		
		DatatypeVectormap ord = (DatatypeVectormap) outputRequest.data;
		
		DatatypeHeightmap heightmapRequestData = new DatatypeHeightmap(ord.extent, ord.resolution);
		
		inputRequests.put("pcomponent", new ModuleInputRequest(getInput("Primary"), heightmapRequestData));
		inputRequests.put("scomponent", new ModuleInputRequest(getInput("Secondary"), heightmapRequestData));
		
		return inputRequests;
	}

	@Override
	public String getModuleName() {
		return "Vectormap component";
	}

	@Override
	public IModuleCategory getModuleCategory() {
		return ModuleCategory.GENERATOR;
	}

	@Override
	public ModuleInput[] registerInputs() {
		ModuleInput[] i = {
				new ModuleInput(new DatatypeHeightmap(), "Primary"),
				new ModuleInput(new DatatypeHeightmap(), "Secondary")
				};
		return i;
	}

	@Override
	public ModuleOutput[] registerOutputs() {
		ModuleOutput[] o = {
				new ModuleOutput(new DatatypeVectormap(), "Output")
				};
		return o;
	}

	@Override
	public boolean isBypassable() {
		return false;
	}
	
	private enum CoordinateSystem {
		CARTESIAN,
		POLAR;
	}
}
