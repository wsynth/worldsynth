/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.module.colormap;

import java.util.HashMap;
import java.util.Map;

import net.worldsynth.datatype.AbstractDatatype;
import net.worldsynth.datatype.DatatypeColormap;
import net.worldsynth.datatype.DatatypeHeightmap;
import net.worldsynth.module.AbstractModule;
import net.worldsynth.module.IModuleCategory;
import net.worldsynth.module.ModuleCategory;
import net.worldsynth.module.ModuleInput;
import net.worldsynth.module.ModuleInputRequest;
import net.worldsynth.module.ModuleOutput;
import net.worldsynth.module.ModuleOutputRequest;
import net.worldsynth.parameter.AbstractParameter;
import net.worldsynth.parameter.EnumParameter;
import net.worldsynth.util.math.MathHelperScalar;

public class ModuleColormapSelector extends AbstractModule {
	
	private EnumParameter<SelectOrder> selectOrder = new EnumParameter<ModuleColormapSelector.SelectOrder>("selectorder", "High value selects", null, SelectOrder.class, SelectOrder.PRIMARY);
	
	@Override
	public AbstractParameter<?>[] registerParameters() {
		AbstractParameter<?>[] p = {
				selectOrder
		};
		return p;
	}
	
	@Override
	public AbstractDatatype buildModule(Map<String, AbstractDatatype> inputs, ModuleOutputRequest request) {
		DatatypeColormap requestData = (DatatypeColormap) request.data;
		
		int mpw = requestData.mapPointsWidth;
		int mpl = requestData.mapPointsLength;
		
		//----------READ INPUTS----------//
		
		float normalizedHeight = getSynthParameters().getNormalizedHeightmapHeight();
		
		SelectOrder selectOrder = this.selectOrder.getValue();
		
		if (inputs.get("selector") == null || inputs.get("primary") == null || inputs.get("secondary") == null) {
			//If any of the inputs are null, there is not enough input and then just return null
			return null;
		}
		
		float[][] inputMap0 = ((DatatypeHeightmap) inputs.get("selector")).getHeightmap();
		float[][][] inputMap1 = ((DatatypeColormap) inputs.get("primary")).getColormap();
		float[][][] inputMap2 = ((DatatypeColormap) inputs.get("secondary")).getColormap();
		
		//----------BUILD----------//
		
		float[][][] map = new float[mpw][mpl][3];
		
		for (int u = 0; u < mpw; u++) {
			for (int v = 0; v < mpl; v++) {
				float i0 = MathHelperScalar.clamp(inputMap0[u][v]/normalizedHeight, 0.0f, 1.0f);
				float l = selectOrder == SelectOrder.PRIMARY ? i0 : 1.0f - i0;
				map[u][v][0] = lerp(inputMap1[u][v][0], inputMap2[u][v][0], l);
				map[u][v][1] = lerp(inputMap1[u][v][1], inputMap2[u][v][1], l);
				map[u][v][2] = lerp(inputMap1[u][v][2], inputMap2[u][v][2], l);
			}
		}
		
		requestData.setColormap(map);
		
		return requestData;
	}
	
	private float lerp(float a, float b, float x) {
	    return a*x + b*(1.0f-x);
	}
	
	@Override
	public Map<String, ModuleInputRequest> getInputRequests(ModuleOutputRequest outputRequest) {
		HashMap<String, ModuleInputRequest> inputRequests = new HashMap<String, ModuleInputRequest>();
		
		DatatypeColormap ord = (DatatypeColormap) outputRequest.data;
		
		DatatypeHeightmap heightmapRequestData = new DatatypeHeightmap(ord.extent, ord.resolution);
		
		inputRequests.put("selector", new ModuleInputRequest(getInput("Selector"), heightmapRequestData));
		inputRequests.put("primary", new ModuleInputRequest(getInput("Primary"), ord));
		inputRequests.put("secondary", new ModuleInputRequest(getInput("Secondary"), ord));
		
		return inputRequests;
	}

	@Override
	public String getModuleName() {
		return "Selector";
	}

	@Override
	public IModuleCategory getModuleCategory() {
		return ModuleCategory.COMBINER;
	}

	@Override
	public ModuleInput[] registerInputs() {
		ModuleInput[] i = {
				new ModuleInput(new DatatypeHeightmap(), "Selector"),
				new ModuleInput(new DatatypeColormap(), "Primary"),
				new ModuleInput(new DatatypeColormap(), "Secondary")
		};
		return i;
	}

	@Override
	public ModuleOutput[] registerOutputs() {
		ModuleOutput[] o = {
				new ModuleOutput(new DatatypeColormap(), "Output")
				};
		return o;
	}

	@Override
	public boolean isBypassable() {
		return false;
	}
	
	private enum SelectOrder {
		PRIMARY, SECONDARY;
	}
}
