/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.module.colormap;

import java.util.HashMap;
import java.util.Map;

import net.worldsynth.color.WsColor;
import net.worldsynth.datatype.AbstractDatatype;
import net.worldsynth.datatype.DatatypeColormap;
import net.worldsynth.datatype.DatatypeMaterialmap;
import net.worldsynth.material.MaterialState;
import net.worldsynth.module.AbstractModule;
import net.worldsynth.module.IModuleCategory;
import net.worldsynth.module.ModuleCategory;
import net.worldsynth.module.ModuleInput;
import net.worldsynth.module.ModuleInputRequest;
import net.worldsynth.module.ModuleOutput;
import net.worldsynth.module.ModuleOutputRequest;
import net.worldsynth.parameter.AbstractParameter;

public class ModuleColormapFromMaterialmap extends AbstractModule {
	
	@Override
	public AbstractParameter<?>[] registerParameters() {
		AbstractParameter<?>[] p = {};
		return p;
	}
	
	@Override
	public AbstractDatatype buildModule(Map<String, AbstractDatatype> inputs, ModuleOutputRequest request) {
		DatatypeColormap requestData = (DatatypeColormap) request.data;
		
		int mpw = requestData.mapPointsWidth;
		int mpl = requestData.mapPointsLength;
		
		if (inputs.get("input") == null) {
			//If the input is null, there is not enough input and then just return null
			return null;
		}
		
		MaterialState<?, ?>[][] inputMaterialmap0 = ((DatatypeMaterialmap) inputs.get("input")).getMaterialmap();
		
		float[][][] map = new float[mpw][mpl][3];
		
		for (int u = 0; u < mpw; u++) {
			for (int v = 0; v < mpl; v++) {
				WsColor c = inputMaterialmap0[u][v].getWsColor();
				float red = (float) c.getRed();
				float green = (float) c.getGreen();
				float blue = (float) c.getBlue();
				map[u][v][0] = red;
				map[u][v][1] = green;
				map[u][v][2] = blue;
			}
		}
		
		requestData.setColormap(map);
		
		return requestData;
	}
	
	@Override
	public Map<String, ModuleInputRequest> getInputRequests(ModuleOutputRequest outputRequest) {
		HashMap<String, ModuleInputRequest> inputRequests = new HashMap<String, ModuleInputRequest>();
		
		DatatypeColormap ord = (DatatypeColormap) outputRequest.data;
		
		DatatypeMaterialmap materialmapRequestData = new DatatypeMaterialmap(ord.extent, ord.resolution);

		inputRequests.put("input", new ModuleInputRequest(getInput(0), materialmapRequestData));
		
		return inputRequests;
	}

	@Override
	public String getModuleName() {
		return "Colormap from materialmap";
	}

	@Override
	public IModuleCategory getModuleCategory() {
		return ModuleCategory.GENERATOR;
	}

	@Override
	public ModuleInput[] registerInputs() {
		ModuleInput[] i = {
				new ModuleInput(new DatatypeMaterialmap(), "Input"),
				};
		return i;
	}

	@Override
	public ModuleOutput[] registerOutputs() {
		ModuleOutput[] o = {
				new ModuleOutput(new DatatypeColormap(), "Output")
				};
		return o;
	}

	@Override
	public boolean isBypassable() {
		return false;
	}
}
