/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.module.heightmap;

import net.worldsynth.datatype.*;
import net.worldsynth.module.*;
import net.worldsynth.parameter.AbstractParameter;
import net.worldsynth.parameter.DoubleParameter;
import net.worldsynth.parameter.EnumParameter;
import net.worldsynth.parameter.LongParameter;
import net.worldsynth.util.HeightmapUtil;
import net.worldsynth.util.gen.Permutation;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

public class ModuleHeightmapCells extends AbstractModule {
	
	private LongParameter seed = new LongParameter("seed", "Seed", null, 0, Long.MIN_VALUE, Long.MAX_VALUE);
	private DoubleParameter scale = new DoubleParameter("scale", "Scale", null, 100.0, 1.0, Double.POSITIVE_INFINITY, 1.0, 1000.0);
	private DoubleParameter amplitude = new DoubleParameter("amplitude", "Amplitude", null, 256.0, Double.NEGATIVE_INFINITY, Double.POSITIVE_INFINITY, 0.0, 512.0);
	private DoubleParameter offset = new DoubleParameter("offset", "Offset", null, 0.0, Double.NEGATIVE_INFINITY, Double.POSITIVE_INFINITY, -256.0, 256.0);
	private DoubleParameter distortion = new DoubleParameter("distortion", "Distortion", null, 1.0, Double.NEGATIVE_INFINITY, Double.POSITIVE_INFINITY, 0.0, 100.0);
	
	private EnumParameter<CellType> cellType = new EnumParameter<>("type", "Cell type", null, CellType.class, CellType.VORONOI);
	private EnumParameter<DistanceFunction> distanceFunction = new EnumParameter<>("distancefunction", "Distance function", null, DistanceFunction.class, DistanceFunction.EUCLIDEAN);
	private EnumParameter<Feature> feature = new EnumParameter<>("feature", "Feature function", null, Feature.class, Feature.F1);
	
	private final double S60 = Math.sin(Math.toRadians(60));

	private Permutation permutation;
	
	@Override
	public AbstractParameter<?>[] registerParameters() {
		
		seed.setOnChange(newValue -> {
			permutation = new Permutation(newValue, 256, 3);
		});
		seed.setValue(new Random().nextLong());
		
		AbstractParameter<?>[] p = {
				cellType,
				scale,
				amplitude,
				offset,
				distanceFunction,
				feature,
				seed,
				distortion
				};
		return p;
	}
	
	@Override
	public AbstractDatatype buildModule(Map<String, AbstractDatatype> inputs, ModuleOutputRequest request) {
		DatatypeHeightmap requestData = (DatatypeHeightmap) request.data;
		
		double x = requestData.extent.getX();
		double z = requestData.extent.getZ();
		double res = requestData.resolution;
		int mpw = requestData.mapPointsWidth;
		int mpl = requestData.mapPointsLength;
		
		//----------READ INPUTS----------//
		
		float normalizedHeight = getSynthParameters().getNormalizedHeightmapHeight();

		double scale = this.scale.getValue();
		double amplitude = this.amplitude.getValue();
		double offset = this.offset.getValue();
		double distortion = this.distortion.getValue();
		CellType cellType = this.cellType.getValue();
		DistanceFunction distanceFunction = this.distanceFunction.getValue();
		Feature feature = this.feature.getValue();
		
		float[][] amplitudeMap = null;
		float[][] offsetMap = null;
		float[][][] distortionMap = null;
		float[][] mask = null;
		
		//Read in scale
		if (inputs.get("scale") != null) {
			scale = ((DatatypeScalar) inputs.get("scale")).getValue();
		}
		
		//Read in amplitude
		if (inputs.get("amplitude") != null) {
			if (inputs.get("amplitude") instanceof DatatypeScalar) {
				amplitude = ((DatatypeScalar) inputs.get("amplitude")).getValue();
			}
			else {
				amplitudeMap = ((DatatypeHeightmap) inputs.get("amplitude")).getHeightmap();
			}
		}
		
		//Read in offset
		if (inputs.get("offset") != null) {
			if (inputs.get("offset") instanceof DatatypeScalar) {
				offset = ((DatatypeScalar) inputs.get("offset")).getValue();
			}
			else {
				offsetMap = ((DatatypeHeightmap) inputs.get("offset")).getHeightmap();
			}
		}
		
		//Read in distortion
		if (inputs.get("distortion") != null) {
			if (inputs.get("distortion") instanceof DatatypeVectormap) {
				distortionMap = ((DatatypeVectormap) inputs.get("distortion")).getVectormap();
			}
			else {
				float[][] distortionHeightmap = ((DatatypeHeightmap) inputs.get("distortion")).getHeightmap();
				distortionMap = new float[mpw][mpl][2];
				for (int u = 0; u < mpw; u++) {
					for (int v = 0; v < mpl; v++) {
						distortionMap[u][v][0] = (float) Math.cos(distortionHeightmap[u][v]/normalizedHeight * Math.PI * 2.0);
						distortionMap[u][v][1] = (float) Math.sin(distortionHeightmap[u][v]/normalizedHeight * Math.PI * 2.0);
					}
				}
			}
		}
		
		//Read in mask
		if (inputs.get("mask") != null) {
			mask = ((DatatypeHeightmap) inputs.get("mask")).getHeightmap();
		}
		
		//----------BUILD----------//
		
		float[][] map = new float[mpw][mpl];
		
		//Has some input maps
		if (amplitudeMap != null || offsetMap != null || distortionMap != null) {
			double xDistortion = 0.0;
			double zDistortion = 0.0;
			double amplitudeLocal = amplitude;
			double offsetLocal = offset;
			for (int u = 0; u < mpw; u++) {
				for (int v = 0; v < mpl; v++) {
					if (amplitudeMap != null) amplitudeLocal = amplitudeMap[u][v]/normalizedHeight * amplitude;
					if (offsetMap != null) offsetLocal = offsetMap[u][v] + offset;
					if (distortionMap != null) {
						xDistortion = distortionMap[u][v][0] * distortion;
						zDistortion = distortionMap[u][v][1] * distortion;
					}
					
					map[u][v] = (float) getHeightAt(x+u*res+xDistortion, z+v*res+zDistortion, scale, amplitudeLocal, offsetLocal, cellType, distanceFunction, feature);
				}
			}
		}
		//Has only values and no map
		else {
			for (int u = 0; u < mpw; u++) {
				for (int v = 0; v < mpl; v++) {
					map[u][v] = (float) getHeightAt(x+u*res, z+v*res, scale, amplitude, offset, cellType, distanceFunction, feature);
				}
			}
		}
		
		//Apply mask
		if (mask != null) {
			HeightmapUtil.applyMask(map, mask, normalizedHeight);
		}
		
		requestData.setHeightmap(map);
		
		return requestData;
	}
	
	@Override
	public Map<String, ModuleInputRequest> getInputRequests(ModuleOutputRequest outputRequest) {
		HashMap<String, ModuleInputRequest> inputRequests = new HashMap<>();
		
		inputRequests.put("scale", new ModuleInputRequest(getInput("Scale"), new DatatypeScalar()));
		inputRequests.put("amplitude", new ModuleInputRequest(getInput("Amplitude"), new DatatypeMultitype(new AbstractDatatype[] {new DatatypeScalar(), (DatatypeHeightmap)outputRequest.data})));
		inputRequests.put("offset", new ModuleInputRequest(getInput("Offset"), new DatatypeMultitype(new AbstractDatatype[] {new DatatypeScalar(), (DatatypeHeightmap)outputRequest.data})));
		
		DatatypeHeightmap ord = (DatatypeHeightmap) outputRequest.data;
		DatatypeVectormap requestVectorMap = new DatatypeVectormap(ord.extent, ord.resolution);
		inputRequests.put("distortion", new ModuleInputRequest(getInput("Distortion"), new DatatypeMultitype(new AbstractDatatype[] {(DatatypeHeightmap)outputRequest.data, requestVectorMap})));
		
		inputRequests.put("mask", new ModuleInputRequest(getInput("Mask"), outputRequest.data));
		
		return inputRequests;
	}
	
	public double getHeightAt(double x, double y, double scale, double amplitude, double offset, CellType cellType, DistanceFunction distanceFunction, Feature feature) {
		return cells(x/scale, y/scale, amplitude, offset, cellType, distanceFunction, feature);
	}
	
	private double cells(double x, double y, double amplitude, double offset, CellType cellType, DistanceFunction distanceFunction, Feature feature) {
		if (cellType == CellType.HEXAGON) {
			y *= 1.0/S60;
		}

		// Calculate the coordinates for the unit square that the coordinates is inside
		int xi = (int) Math.floor(x);
		int yi = (int) Math.floor(y);

		// Calculate the local coordinates inside the unit square
		double xf = x - xi;
		double yf = y - yi;

		double dist1 = Double.POSITIVE_INFINITY;
		double dist2 = Double.POSITIVE_INFINITY;
		double dist3 = Double.POSITIVE_INFINITY;
		double height1 = 0;
		double height2 = 0;
		double height3 = 0;

		for (int ix = -1; ix <= 1; ix++) {
			for (int iy = -1; iy <= 1; iy++) {
				double xoffset = 0.0;
				double yoffset = 0.0;

				int cx = xi + ix;
				int cy = yi + iy;
				if (cellType == CellType.VORONOI) {
					int[] offsets = permutation.lHashAll(cx & 255, cy & 255);
					xoffset = offsets[0] / 255.0;
					yoffset = offsets[1] / 255.0;
				}
				else if (cellType == CellType.HEXAGON) {
					if ((yi & 1) == 1) {
						double c = 0.5;
						double m = 0 + Math.abs(iy-1);
						xoffset =  c * m;
					}
					else {
						double c = 0.5;
						double m = 1 - Math.abs(iy-1);
						xoffset =  c * m;
					}
					yoffset = 0;
				}

				double xDist = ix + xoffset - xf;
				double yDist = iy + yoffset - yf;

				if (cellType == CellType.HEXAGON) {
					yDist *= S60;
				}
				
				//Distance from point
				double dist = Double.POSITIVE_INFINITY;
				switch (distanceFunction) {
				case EUCLIDEAN:
					dist = xDist*xDist + yDist*yDist;
					break;
				case MANHATTAN:
					dist = Math.abs(xDist) + Math.abs(yDist);
					break;
				case CHEBYSHEV:
					dist = Math.max(Math.abs(xDist), Math.abs(yDist));
					break;
				case MIN:
					dist = Math.min(Math.abs(xDist), Math.abs(yDist));
					break;
				}
				
				double height = permutation.lHash(2, cx & 255, cy & 255)/(double) 256;

				if (dist < dist3) {
					dist3 = dist;
					height3 = height;

					if (dist3 < dist2) {
						double d = dist2;
						double h = height2;
						dist2 = dist3;
						height2 = height3;
						dist3 = d;
						height3 = h;

						if (dist2 < dist1) {
							d = dist1;
							h = height1;
							dist1 = dist2;
							height1 = height2;
							dist2 = d;
							height2 = h;
						}
					}
				}
			}
		}

		double h = 0;
		switch (feature) {
		case F1:
			h = height1;
			break;
		case F2:
			h = height2;
			break;
		case F3:
			h = height3;
			break;
		}

		h *= amplitude;
		h += offset;
		return h;
	}

	@Override
	public String getModuleName() {
		return "Cells";
	}

	@Override
	public IModuleCategory getModuleCategory() {
		return ModuleCategory.GENERATOR;
	}

	@Override
	public ModuleInput[] registerInputs() {
		ModuleInput[] i = {
				new ModuleInput(new DatatypeScalar(), "Scale"),
				new ModuleInput(new DatatypeMultitype(new AbstractDatatype[] {new DatatypeScalar(), new DatatypeHeightmap()}), "Amplitude"),
				new ModuleInput(new DatatypeMultitype(new AbstractDatatype[] {new DatatypeScalar(), new DatatypeHeightmap()}), "Offset"),
				new ModuleInput(new DatatypeMultitype(new AbstractDatatype[] {new DatatypeHeightmap(), new DatatypeVectormap()}), "Distortion"),
				new ModuleInput(new DatatypeHeightmap(), "Mask")
				};
		return i;
	}

	@Override
	public ModuleOutput[] registerOutputs() {
		ModuleOutput[] o = {
				new ModuleOutput(new DatatypeHeightmap(), "Output")
				};
		return o;
	}

	@Override
	public boolean isBypassable() {
		return false;
	}
	
	private enum DistanceFunction {
		EUCLIDEAN,
		MANHATTAN,
		CHEBYSHEV,
		MIN;
	}

	private enum Feature {
		F1,
		F2,
		F3;
	}
	
	private enum CellType {
		VORONOI, HEXAGON, SQUARE;
	}
}
