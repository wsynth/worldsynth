/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.module.heightmap;

import java.util.HashMap;
import java.util.Map;

import net.worldsynth.datatype.AbstractDatatype;
import net.worldsynth.datatype.DatatypeBlockspace;
import net.worldsynth.datatype.DatatypeHeightmap;
import net.worldsynth.material.MaterialState;
import net.worldsynth.module.AbstractModule;
import net.worldsynth.module.IModuleCategory;
import net.worldsynth.module.ModuleCategory;
import net.worldsynth.module.ModuleInput;
import net.worldsynth.module.ModuleInputRequest;
import net.worldsynth.module.ModuleOutput;
import net.worldsynth.module.ModuleOutputRequest;
import net.worldsynth.parameter.AbstractParameter;

public class ModuleHeightmapFromBlockspace extends AbstractModule {
	
	@Override
	public AbstractParameter<?>[] registerParameters() {
		AbstractParameter<?>[] p = {};
		return p;
	}
	
	@Override
	public AbstractDatatype buildModule(Map<String, AbstractDatatype> inputs, ModuleOutputRequest request) {
		DatatypeHeightmap requestData = (DatatypeHeightmap) request.data;
		
		int mpw = requestData.mapPointsWidth;
		int mpl = requestData.mapPointsLength;
		
		//----------READ INPUTS----------//
		
		if (inputs.get("input") == null) {
			return null;
		}
		
		DatatypeBlockspace inputData = (DatatypeBlockspace) inputs.get("input");
		MaterialState<?, ?>[][][] inputBlockspace = inputData.getBlockspace();
		int blockspaceHeightLength = inputBlockspace[0].length;
		double blockspaceFloor = inputData.extent.getY();
		double blockspaceRes = inputData.resolution;
		
		//----------BUILD----------//
		
		float[][] map = new float[mpw][mpl];
		
		for (int u = 0; u < mpw; u++) {
			for (int w = 0; w < mpl; w++) {
				for (int v = blockspaceHeightLength-1; v >= 0; v--) {
					if (!inputBlockspace[u][v][w].isAir()) {
						map[u][w] = (float) (v * blockspaceRes + blockspaceFloor);
						break;
					}
				}
			}
		}
		
		requestData.setHeightmap(map);
		
		return requestData;
	}

	@Override
	public Map<String, ModuleInputRequest> getInputRequests(ModuleOutputRequest outputRequest) {
		HashMap<String, ModuleInputRequest> inputRequests = new HashMap<String, ModuleInputRequest>();
		
		DatatypeHeightmap ord = (DatatypeHeightmap) outputRequest.data;
		
		DatatypeBlockspace blockspaceRequestData = new DatatypeBlockspace(ord.extent, ord.resolution);
		
		inputRequests.put("input", new ModuleInputRequest(getInput(0), blockspaceRequestData));
		
		return inputRequests;
	}

	@Override
	public String getModuleName() {
		return "Heightmap from blockspace";
	}

	@Override
	public IModuleCategory getModuleCategory() {
		return ModuleCategory.GENERATOR;
	}

	@Override
	public ModuleInput[] registerInputs() {
		ModuleInput[] i = {
				new ModuleInput(new DatatypeBlockspace(), "Input")
				};
		return i;
	}

	@Override
	public ModuleOutput[] registerOutputs() {
		ModuleOutput[] o = {
				new ModuleOutput(new DatatypeHeightmap(), "Output")
				};
		return o;
	}

	@Override
	public boolean isBypassable() {
		return false;
	}
}
