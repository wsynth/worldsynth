/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.module.heightmap;

import net.worldsynth.datatype.AbstractDatatype;
import net.worldsynth.datatype.DatatypeHeightmap;
import net.worldsynth.extent.Extent;
import net.worldsynth.module.*;
import net.worldsynth.parameter.*;
import net.worldsynth.util.Tuple;
import net.worldsynth.util.gen.Permutation;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

public class ModuleHeightmapCellTerrace extends AbstractModule {
	
	private LongParameter seed = new LongParameter("seed", "Seed", null, 0, Long.MIN_VALUE, Long.MAX_VALUE);
	private DoubleTupleParameter scale = new DoubleTupleParameter("scale", "Scale", null, 0.0, Double.POSITIVE_INFINITY, 32.0, 32.0, 32.0);
	private EnumParameter<DistanceFunction> distanceFunction = new EnumParameter<>("distancefunction", "Distance function", null, DistanceFunction.class, DistanceFunction.EUCLIDEAN);

	private Permutation permutation;
	
	@Override
	public AbstractParameter<?>[] registerParameters() {
		
		seed.setOnChange(newValue -> {
			permutation = new Permutation(newValue, 256, 3);
		});
		seed.setValue(new Random().nextLong());
		
		AbstractParameter<?>[] p = {
				scale,
				distanceFunction,
				seed
				};
		return p;
	}
	
	@Override
	public AbstractDatatype buildModule(Map<String, AbstractDatatype> inputs, ModuleOutputRequest request) {
		DatatypeHeightmap requestData = (DatatypeHeightmap) request.data;
		
		double x = requestData.extent.getX();
		double z = requestData.extent.getZ();
		double res = requestData.resolution;
		int mpw = requestData.mapPointsWidth;
		int mpl = requestData.mapPointsLength;
		
		//----------READ INPUTS----------//

		float normalizedHeight = getSynthParameters().getNormalizedHeightmapHeight();
		
		Tuple<Double> scale = this.scale.getValue();
		DistanceFunction distanceFunction = this.distanceFunction.getValue();
		
		//Read in input
		if (inputs.get("input") == null) {
			//If the main input (index 0) is null, there is no input and then just return null
			return null;
		}
		DatatypeHeightmap input = (DatatypeHeightmap) inputs.get("input");

		//Read in mask
		float[][] mask = null;
		if (inputs.get("mask") != null) {
			mask = ((DatatypeHeightmap) inputs.get("mask")).getHeightmap();
		}
		
		//----------BUILD----------//
		
		float[][] map = new float[mpw][mpl];
		
		for (int u = 0; u < mpw; u++) {
			for (int v = 0; v < mpl; v++) {
				map[u][v] = (float) getValueAt(x+u*res, z+v*res, scale, input, distanceFunction);

				if (mask != null) {
					float in = input.getGlobalHeight(x+u*res, z+v*res);
					map[u][v] = (map[u][v] - in) * mask[u][v]/normalizedHeight + in;
				}
			}
		}
		
		requestData.setHeightmap(map);
		
		return requestData;
	}
	
	@Override
	public Map<String, ModuleInputRequest> getInputRequests(ModuleOutputRequest outputRequest) {
		HashMap<String, ModuleInputRequest> inputRequests = new HashMap<>();

		DatatypeHeightmap ord = (DatatypeHeightmap)outputRequest.data;
		
		double expandX = Math.ceil(Math.abs(scale.getValue().getValue(0) * 2.0) / ord.resolution) * ord.resolution;
		double expandZ = Math.ceil(Math.abs(scale.getValue().getValue(2) * 2.0) / ord.resolution) * ord.resolution;
		DatatypeHeightmap inputRequestDatatype = new DatatypeHeightmap(Extent.expandedBuildExtent(ord.extent, expandX, 0, expandZ), ord.resolution);
		inputRequests.put("input", new ModuleInputRequest(getInput("Input"), inputRequestDatatype));

		inputRequests.put("mask", new ModuleInputRequest(getInput("Mask"), outputRequest.data));
		
		return inputRequests;
	}

	public double getValueAt(double x, double z, Tuple<Double> scale, DatatypeHeightmap heightmap, DistanceFunction distanceFunction) {
		Point3D p = closestPoint(x/scale.getValue(0), heightmap.getGlobalLerpHeight(x, z)/scale.getValue(1), z/scale.getValue(2), distanceFunction);

//		double sx = p.x * scale.getValue(0);
		double sy = p.y * scale.getValue(1);
//		double sz = p.z * scale.getValue(2);
		return sy;
	}
	
	private Point3D closestPoint(final double x, final double y, final double z, DistanceFunction distanceFunction) {
		// Calculate the coordinates for the unit square that the coordinates is inside
		int xi = (int) Math.floor(x);
		int yi = (int) Math.floor(y);
		int zi = (int) Math.floor(z);

		// Calculate the local coordinates inside the unit square
		double xf = x - xi;
		double yf = y - yi;
		double zf = z - zi;
		
		double minDist = Double.POSITIVE_INFINITY;
		double minDistX = 0, minDistY = 0, minDistZ = 0;
		for (int ix = -1; ix <= 1; ix++) {
			for (int iy = -1; iy <= 1; iy++) {
				for (int iz = -1; iz <= 1; iz++) {
					int cx = xi + ix;
					int cy = yi + iy;
					int cz = zi + iz;

					int[] offsets = permutation.lHashAll(cx & 255, cy & 255, cz & 255);
					double xOffset = offsets[0] / 255.0;
					double yOffset = offsets[1] / 255.0;
					double zOffset = offsets[2] / 255.0;

					double xDist = ix + xOffset - xf;
					double yDist = iy + yOffset - yf;
					double zDist = iz + zOffset - zf;
					
					//Distance from point
					double dist = 0;
					switch (distanceFunction) {
					case EUCLIDEAN:
						dist = xDist*xDist + yDist*yDist + zDist*zDist;
						break;
					case MANHATTAN:
						dist = Math.abs(xDist) + Math.abs(yDist) + Math.abs(zDist);
						break;
					case CHEBYSHEV:
						dist = Math.max(Math.max(Math.abs(xDist), Math.abs(yDist)), Math.abs(zDist));
						break;
					}
					
					if (dist < minDist) {
						minDistX = xDist;
						minDistY = yDist;
						minDistZ = zDist;
						minDist = dist;
					}
				}
			}
		}
		
		return new Point3D(x + minDistX, y + minDistY, z + minDistZ);
	}

	@Override
	public String getModuleName() {
		return "Cell terrace";
	}

	@Override
	public IModuleCategory getModuleCategory() {
		return ModuleCategory.MODIFIER;
	}

	@Override
	public ModuleInput[] registerInputs() {
		ModuleInput[] i = {
				new ModuleInput(new DatatypeHeightmap(), "Input"),
				new ModuleInput(new DatatypeHeightmap(), "Mask")
				};
		return i;
	}

	@Override
	public ModuleOutput[] registerOutputs() {
		ModuleOutput[] o = {
				new ModuleOutput(new DatatypeHeightmap(), "Output")
				};
		return o;
	}

	@Override
	public boolean isBypassable() {
		return true;
	}
	
	private enum DistanceFunction {
		EUCLIDEAN,
		MANHATTAN,
		CHEBYSHEV
	}
	
	private class Point3D {
		public double x, y, z;
		public Point3D(double x, double y, double z) {
			this.x = x;
			this.y = y;
			this.z = z;
		}
	}
}
