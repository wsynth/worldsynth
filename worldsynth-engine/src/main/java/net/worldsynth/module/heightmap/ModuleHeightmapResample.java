package net.worldsynth.module.heightmap;

import java.util.HashMap;
import java.util.Map;

import net.worldsynth.datatype.AbstractDatatype;
import net.worldsynth.datatype.DatatypeHeightmap;
import net.worldsynth.extent.Extent;
import net.worldsynth.module.AbstractModule;
import net.worldsynth.module.IModuleCategory;
import net.worldsynth.module.ModuleCategory;
import net.worldsynth.module.ModuleInput;
import net.worldsynth.module.ModuleInputRequest;
import net.worldsynth.module.ModuleOutput;
import net.worldsynth.module.ModuleOutputRequest;
import net.worldsynth.parameter.AbstractParameter;
import net.worldsynth.parameter.IntegerParameter;

public class ModuleHeightmapResample extends AbstractModule {
	
	private IntegerParameter resamples = new IntegerParameter("resamples", "Resamples", null, 8, 1, Integer.MAX_VALUE, 1, 32);
	
	@Override
	public AbstractParameter<?>[] registerParameters() {
		AbstractParameter<?>[] p = {
				resamples
				};
		return p;
	}
	
	@Override
	public AbstractDatatype buildModule(Map<String, AbstractDatatype> inputs, ModuleOutputRequest request) {
		DatatypeHeightmap requestData = (DatatypeHeightmap) request.data;
		
		double x = requestData.extent.getX();
		double z = requestData.extent.getZ();
		double res = requestData.resolution;
		int mpw = requestData.mapPointsWidth;
		int mpl = requestData.mapPointsLength;
		
		//----------READ INPUTS----------//
		
		//Read in primary input
		if (inputs.get("input") == null) {
			//If the main input (index 0) is null, there is no input and then just return null
			return null;
		}
		DatatypeHeightmap i1 = (DatatypeHeightmap) inputs.get("input");
		
		//----------BUILD----------//
		
		float[][] map = new float[mpw][mpl];
		for (int u = 0; u < mpw; u++) {
			for (int v = 0; v < mpl; v++) {
				map[u][v] = i1.getGlobalLerpHeight(x + u*res, z + v*res);
			}
		}
		
		requestData.setHeightmap(map);
		
		return requestData;
	}

	@Override
	public Map<String, ModuleInputRequest> getInputRequests(ModuleOutputRequest outputRequest) {
		HashMap<String, ModuleInputRequest> inputRequests = new HashMap<String, ModuleInputRequest>();
		
		DatatypeHeightmap ord = (DatatypeHeightmap) outputRequest.data;
		double res = Math.max(resamples.getValue(), ord.resolution);
		Extent sampleExtent = getSampleExtent(ord.extent, resamples.getValue());
		
		DatatypeHeightmap inputHeightmapRequestData = new DatatypeHeightmap(sampleExtent, res);
		inputRequests.put("input", new ModuleInputRequest(getInput("Input"), inputHeightmapRequestData));
		
		return inputRequests;
	}
	
	private Extent getSampleExtent(Extent extent, double sampleGridSize) {
		double x1 = Math.floor(extent.getX() / sampleGridSize);
		x1 *= sampleGridSize;
		double z1 = Math.floor(extent.getZ() / sampleGridSize);
		z1 *= sampleGridSize;

		double x2 = Math.ceil((extent.getX() + extent.getWidth()) / sampleGridSize) + 1.0;
		x2 *= sampleGridSize;
		double z2 = Math.ceil((extent.getZ() + extent.getLength()) / sampleGridSize) + 1.0;
		z2 *= sampleGridSize;
		
		double width = x2 - x1;
		double length = z2 - z1;
		
		return new Extent(x1, extent.getY(), z1, width, extent.getHeight(), length);
	}

	@Override
	public String getModuleName() {
		return "Resample";
	}

	@Override
	public IModuleCategory getModuleCategory() {
		return ModuleCategory.MODIFIER;
	}

	@Override
	public ModuleInput[] registerInputs() {
		ModuleInput[] i = {
				new ModuleInput(new DatatypeHeightmap(), "Input")
				};
		return i;
	}

	@Override
	public ModuleOutput[] registerOutputs() {
		ModuleOutput[] o = {
				new ModuleOutput(new DatatypeHeightmap(), "Output")
				};
		return o;
	}

	@Override
	public boolean isBypassable() {
		return true;
	}
}
