/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.module.heightmap;

import java.util.HashMap;
import java.util.Map;

import net.worldsynth.datatype.AbstractDatatype;
import net.worldsynth.datatype.DatatypeHeightmap;
import net.worldsynth.extent.Extent;
import net.worldsynth.module.AbstractModule;
import net.worldsynth.module.IModuleCategory;
import net.worldsynth.module.ModuleCategory;
import net.worldsynth.module.ModuleInput;
import net.worldsynth.module.ModuleInputRequest;
import net.worldsynth.module.ModuleOutput;
import net.worldsynth.module.ModuleOutputRequest;
import net.worldsynth.parameter.AbstractParameter;
import net.worldsynth.parameter.IntegerParameter;

public class ModuleHeightmapSmoothen extends AbstractModule {
	
	private IntegerParameter kernelRadius = new IntegerParameter("radius", "Radius", null, 2, 1, Integer.MAX_VALUE, 1, 20);
	
	@Override
	public AbstractParameter<?>[] registerParameters() {
		AbstractParameter<?>[] p = {
				kernelRadius
				};
		return p;
	}
	
	@Override
	public AbstractDatatype buildModule(Map<String, AbstractDatatype> inputs, ModuleOutputRequest request) {
		DatatypeHeightmap requestData = (DatatypeHeightmap) request.data;
		
		double res = requestData.resolution;
		int mpw = requestData.mapPointsWidth;
		int mpl = requestData.mapPointsLength;
		
		//----------READ INPUTS----------//
		
		float normalizedHeight = getSynthParameters().getNormalizedHeightmapHeight();
		
		//Read in primary input
		if (inputs.get("input") == null) {
			//If the main input is null, there is not enough input and then just return null
			return null;
		}
		float[][] inputMap = ((DatatypeHeightmap) inputs.get("input")).getHeightmap();
		
		//Read mask
		float[][] mask = null;
		if (inputs.get("mask") != null) {
			mask = ((DatatypeHeightmap) inputs.get("mask")).getHeightmap();
		}
		
		//----------BUILD----------//
		
		float[][] outputMap = new float[mpw][mpl];
		int activeKernelRadius = (int) Math.floor((double) kernelRadius.getValue()/res);
		double[][] kernel = generateKernel(activeKernelRadius);
		
		for (int u = 0; u < mpw; u++) {
			for (int v = 0; v < mpl; v++) {
				outputMap[u][v] = (float)convolution(kernel, activeKernelRadius, inputMap, u+activeKernelRadius, v+activeKernelRadius);
			}
		}
		
		//Apply mask
		if (mask != null) {
			for (int u = 0; u < mpw; u++) {
				for (int v = 0; v < mpl; v++) {
					outputMap[u][v] = (outputMap[u][v] - inputMap[u+activeKernelRadius][v+activeKernelRadius]) * mask[u][v]/normalizedHeight + inputMap[u+activeKernelRadius][v+activeKernelRadius];
				}
			}
		}
		
		requestData.setHeightmap(outputMap);
		
		return requestData;
	}
	
	private double[][] generateKernel(int kernelRadius) {
		int size = 1 + (int)kernelRadius * 2;
		double[][] kernel = new double[size][size];
		
		if (kernelRadius == 0) {
			kernel[0][0] = 1.0;
			return kernel;
		}
		
		for (int u = 0; u < size; u++) {
			for (int v = 0; v < size; v++) {
				double dist = Math.sqrt(Math.pow(u-kernelRadius, 2) + Math.pow(v-kernelRadius, 2));
				if (dist > kernelRadius) dist = kernelRadius;
				double a = 1.0-(dist/kernelRadius);
				kernel[u][v] = a;
			}
		}
		
		return kernel;
	}
	
	private double convolution(double[][] kernel, int kernelRadius, float[][] heightmap, int x, int y) {
		
		double n = 0;
		double sum = 0;
		
		for (int kx = -kernelRadius; kx <= kernelRadius; kx++) {
			for (int ky = -kernelRadius; ky <= kernelRadius; ky++) {
				
				double a = (double)heightmap[x+kx][y+ky];
				double k = kernel[kx+kernelRadius][ky+kernelRadius];
				sum += a*k;
				n += k;
			}
		}
		
		return sum/(double)n;
	}

	@Override
	public Map<String, ModuleInputRequest> getInputRequests(ModuleOutputRequest outputRequest) {
		HashMap<String, ModuleInputRequest> inputRequests = new HashMap<String, ModuleInputRequest>();
		
		DatatypeHeightmap d = (DatatypeHeightmap)outputRequest.data;
		double expandRadius = Math.floor((double)kernelRadius.getValue()/d.resolution)*d.resolution;
		DatatypeHeightmap inputRequestDatatype = new DatatypeHeightmap(Extent.expandedBuildExtent(d.extent, expandRadius, 0.0, expandRadius), d.resolution);
		inputRequests.put("input", new ModuleInputRequest(getInput("Input"), inputRequestDatatype));
		
		inputRequests.put("mask", new ModuleInputRequest(getInput("Mask"), outputRequest.data));
		
		return inputRequests;
	}

	@Override
	public String getModuleName() {
		return "Smoothen";
	}

	@Override
	public IModuleCategory getModuleCategory() {
		return ModuleCategory.MODIFIER;
	}

	@Override
	public ModuleInput[] registerInputs() {
		ModuleInput[] i = {
				new ModuleInput(new DatatypeHeightmap(), "Input"),
				new ModuleInput(new DatatypeHeightmap(), "Mask")
				};
		return i;
	}

	@Override
	public ModuleOutput[] registerOutputs() {
		ModuleOutput[] o = {new ModuleOutput(new DatatypeHeightmap(), "Output")};
		return o;
	}

	@Override
	public boolean isBypassable() {
		return true;
	}
}
