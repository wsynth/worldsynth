/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.module.heightmap;

import java.util.HashMap;
import java.util.Map;

import net.worldsynth.datatype.AbstractDatatype;
import net.worldsynth.datatype.DatatypeHeightmap;
import net.worldsynth.module.AbstractModule;
import net.worldsynth.module.IModuleCategory;
import net.worldsynth.module.ModuleCategory;
import net.worldsynth.module.ModuleInput;
import net.worldsynth.module.ModuleInputRequest;
import net.worldsynth.module.ModuleOutput;
import net.worldsynth.module.ModuleOutputRequest;
import net.worldsynth.parameter.AbstractParameter;
import net.worldsynth.util.HeightmapUtil;

public class ModuleHeightmapDither extends AbstractModule {
	
	//TODO Add options for other dither matrixes
	
	float[][] thresholdMap = {
			{0,  8,  2,  10},
			{12, 4,  14, 6 },
			{3,  11, 1,  9 },
			{15, 7,  13, 5 }};
	
	@Override
	public AbstractParameter<?>[] registerParameters() {
		AbstractParameter<?>[] p = {};
		return p;
	}
	
	@Override
	public AbstractDatatype buildModule(Map<String, AbstractDatatype> inputs, ModuleOutputRequest request) {
		DatatypeHeightmap requestData = (DatatypeHeightmap) request.data;
		
		double x = requestData.extent.getX();
		double z = requestData.extent.getX();
		double res = requestData.resolution;
		int mpw = requestData.mapPointsWidth;
		int mpl = requestData.mapPointsLength;
		
		//----------READ INPUTS----------//
		
		float normalizedHeight = getSynthParameters().getNormalizedHeightmapHeight();
		
		//Read in primary input
		if (inputs.get("input") == null) {
			//If the main input (index 0) is null, there is no input and then just return null
			return null;
		}
		float[][] inputMap = ((DatatypeHeightmap) inputs.get("input")).getHeightmap();
		
		//Read mask
		float[][] mask = null;
		if (inputs.get("mask") != null) {
			mask = ((DatatypeHeightmap) inputs.get("mask")).getHeightmap();
		}
		
		//----------BUILD----------//
		
		float[][] ditheredMap = new float[mpw][mpl];
		
		//Has only values and no map
		for (int u = 0; u < mpw; u++) {
			for (int v = 0; v < mpl; v++) {
				ditheredMap[u][v] = dither(inputMap[u][v], x + (double) u / res, z + (double) v / res, normalizedHeight);
			}
		}
		
		//Apply mask
		if (mask != null) {
			HeightmapUtil.applyMask(ditheredMap, inputMap, mask, normalizedHeight);
		}
		
		requestData.setHeightmap(ditheredMap);
		
		return requestData;
	}
	
	private float dither(float height, double globalX, double globalZ, float normalizedHeight) {
		if (height > thresholdMap[mod(globalX, 4)][mod(globalZ, 4)] * normalizedHeight / 16.0f) return normalizedHeight;
		else return 0.0f;
	}
	
	private int mod(double val, int mod) {
		return (int)val & (mod-1);
	}
	
	@Override
	public Map<String, ModuleInputRequest> getInputRequests(ModuleOutputRequest outputRequest) {
		HashMap<String, ModuleInputRequest> inputRequests = new HashMap<String, ModuleInputRequest>();
		
		inputRequests.put("input", new ModuleInputRequest(getInput("Input"), outputRequest.data));
		inputRequests.put("mask", new ModuleInputRequest(getInput("Mask"), outputRequest.data));
		
		return inputRequests;
	}

	@Override
	public String getModuleName() {
		return "Dither";
	}

	@Override
	public IModuleCategory getModuleCategory() {
		return ModuleCategory.MODIFIER;
	}

	@Override
	public ModuleInput[] registerInputs() {
		ModuleInput[] i = {
				new ModuleInput(new DatatypeHeightmap(), "Input"),
				new ModuleInput(new DatatypeHeightmap(), "Mask")
				};
		return i;
	}

	@Override
	public ModuleOutput[] registerOutputs() {
		ModuleOutput[] o = {
				new ModuleOutput(new DatatypeHeightmap(), "Output")
				};
		return o;
	}

	@Override
	public boolean isBypassable() {
		return true;
	}
}
