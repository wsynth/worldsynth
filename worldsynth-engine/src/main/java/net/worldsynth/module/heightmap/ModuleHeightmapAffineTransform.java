/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.module.heightmap;

import net.worldsynth.datatype.AbstractDatatype;
import net.worldsynth.datatype.DatatypeHeightmap;
import net.worldsynth.extent.Extent;
import net.worldsynth.module.*;
import net.worldsynth.parameter.AbstractParameter;
import net.worldsynth.parameter.DoubleTupleParameter;
import net.worldsynth.util.Tuple;
import net.worldsynth.util.math.MathHelperScalar;
import org.apache.commons.math3.geometry.euclidean.twod.Vector2D;
import org.apache.commons.math3.linear.MatrixUtils;
import org.apache.commons.math3.linear.RealMatrix;

import java.util.HashMap;
import java.util.Map;

public class ModuleHeightmapAffineTransform extends AbstractModule {

	private final DoubleTupleParameter xTransform = new DoubleTupleParameter("x", "X", null, Double.NEGATIVE_INFINITY, Double.POSITIVE_INFINITY, 1.0, 0.0, 0.0);
	private final DoubleTupleParameter zTransform = new DoubleTupleParameter("z", "Z", null, Double.NEGATIVE_INFINITY, Double.POSITIVE_INFINITY, 0.0, 1.0, 0.0);

	@Override
	public AbstractParameter<?>[] registerParameters() {
		return new AbstractParameter<?>[]{
				xTransform,
				zTransform
		};
	}

	@Override
	public Map<String, ModuleInputRequest> getInputRequests(ModuleOutputRequest outputRequest) {
		HashMap<String, ModuleInputRequest> inputRequests = new HashMap<>();

		DatatypeHeightmap ord = (DatatypeHeightmap) outputRequest.data;

		FastAffineMatrix affineMatrix = getAffineMatrix().invert();
		Extent transformExtent = transformBoundingExtent(affineMatrix, ord.extent);
		DatatypeHeightmap translatedRequestData = new DatatypeHeightmap(transformExtent, ord.resolution);

		inputRequests.put("input", new ModuleInputRequest(getInput(0), translatedRequestData));

		return inputRequests;
	}

	private FastAffineMatrix getAffineMatrix()  {
		Tuple<Double> xTransform = this.xTransform.getValue();
		Tuple<Double> zTransform = this.zTransform.getValue();

		return new FastAffineMatrix(xTransform, zTransform);
	}

	private Vector2D transformCoordinate(FastAffineMatrix transform, double x, double z) {
		return transform.applyAffine(new Vector2D(x, z));
	}

	private Extent transformBoundingExtent(FastAffineMatrix transform, Extent extent) {
		Vector2D x0z0 = transformCoordinate(transform, extent.getX(), extent.getZ());
		Vector2D x0z1 = transformCoordinate(transform, extent.getX(), extent.getZ() + extent.getLength());
		Vector2D x1z0 = transformCoordinate(transform, extent.getX() + extent.getWidth(), extent.getZ());
		Vector2D x1z1 = transformCoordinate(transform, extent.getX() + extent.getWidth(), extent.getZ() + extent.getLength());

		double x0 = Math.floor(MathHelperScalar.min(x0z0.getX(), x0z1.getX(), x1z0.getX(), x1z1.getX()));
		double x1 = Math.ceil(MathHelperScalar.max(x0z0.getX(), x0z1.getX(), x1z0.getX(), x1z1.getX()));
		double z0 = Math.floor(MathHelperScalar.min(x0z0.getY(), x0z1.getY(), x1z0.getY(), x1z1.getY()));
		double z1 = Math.ceil(MathHelperScalar.max(x0z0.getY(), x0z1.getY(), x1z0.getY(), x1z1.getY()));

		return new Extent(x0, extent.getY(), z0, x1 - x0, extent.getHeight(), z1 - z0);
	}

	@Override
	public AbstractDatatype buildModule(Map<String, AbstractDatatype> inputs, ModuleOutputRequest request) {
		DatatypeHeightmap requestData = (DatatypeHeightmap) request.data;

		double x = requestData.extent.getX();
		double z = requestData.extent.getZ();
		double res = requestData.resolution;
		int mpw = requestData.mapPointsWidth;
		int mpl = requestData.mapPointsLength;

		//----------READ INPUTS----------//

		DatatypeHeightmap inputData = (DatatypeHeightmap) inputs.get("input");

		if (inputData == null) {
			// Return null if there is no input
			return null;
		}

		//----------BUILD----------//

		float[][] map = new float[mpw][mpl];

		FastAffineMatrix affineMatrix = getAffineMatrix().invert();

		for (int u = 0; u < mpw; u++) {
			for (int v = 0; v < mpl; v++) {
				Vector2D sampleCoordinate = transformCoordinate(affineMatrix, x + u * res, z + v * res);
				map[u][v] = inputData.getGlobalHeight(sampleCoordinate.getX(), sampleCoordinate.getY());
			}
		}

		requestData.setHeightmap(map);
		return requestData;
	}

	@Override
	public String getModuleName() {
		return "Affine transform";
	}

	@Override
	public IModuleCategory getModuleCategory() {
		return ModuleCategory.MODIFIER;
	}

	@Override
	public ModuleInput[] registerInputs() {
		return new ModuleInput[]{
				new ModuleInput(new DatatypeHeightmap(), "Input")
		};
	}

	@Override
	public ModuleOutput[] registerOutputs() {
		return new ModuleOutput[]{
				new ModuleOutput(new DatatypeHeightmap(), "Output")
		};
	}

	@Override
	public boolean isBypassable() {
		return true;
	}

	/**
	 * Because apache matrix multiplication is slow.
	 */
	private static class FastAffineMatrix {
		private final double[][] matrix;

		public FastAffineMatrix(Tuple<Double> xTransform, Tuple<Double> zTransform) {
			matrix = new double[][]{
					{xTransform.getValue(0), zTransform.getValue(0), 0},
					{xTransform.getValue(1), zTransform.getValue(1), 0},
					{xTransform.getValue(2), zTransform.getValue(2), 1},
			};
		}

		public FastAffineMatrix(RealMatrix matrix) {
			this.matrix = new double[3][3];

			for (int i = 0; i < 3; i++) {
				for (int j = 0; j < 3; j++) {
					this.matrix[i][j] = matrix.getEntry(i, j);
				}
			}
		}

		public FastAffineMatrix invert() {
			return new FastAffineMatrix(MatrixUtils.inverse(MatrixUtils.createRealMatrix(matrix)));
		}

		public Vector2D applyAffine(Vector2D v) {
			double x = v.getX() * matrix[0][0] + v.getY() * matrix[1][0] + matrix[2][0];
			double z = v.getX() * matrix[0][1] + v.getY() * matrix[1][1] + matrix[2][1];

			return new Vector2D(x, z);
		}
	}
}
