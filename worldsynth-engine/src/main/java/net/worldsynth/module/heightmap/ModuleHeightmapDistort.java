/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.module.heightmap;

import java.util.HashMap;
import java.util.Map;

import net.worldsynth.datatype.AbstractDatatype;
import net.worldsynth.datatype.DatatypeHeightmap;
import net.worldsynth.datatype.DatatypeVectormap;
import net.worldsynth.extent.Extent;
import net.worldsynth.module.AbstractModule;
import net.worldsynth.module.IModuleCategory;
import net.worldsynth.module.ModuleCategory;
import net.worldsynth.module.ModuleInput;
import net.worldsynth.module.ModuleInputRequest;
import net.worldsynth.module.ModuleOutput;
import net.worldsynth.module.ModuleOutputRequest;
import net.worldsynth.parameter.AbstractParameter;
import net.worldsynth.parameter.DoubleParameter;
import net.worldsynth.parameter.EnumParameter;

public class ModuleHeightmapDistort extends AbstractModule {
	
	private DoubleParameter distortion = new DoubleParameter("distortion", "Distortion", null, 0.0, Double.NEGATIVE_INFINITY, Double.POSITIVE_INFINITY, 0.0, 100.0);
	private EnumParameter<InputSampling> inputSampling = new EnumParameter<InputSampling>("inputsampling", "Input sampling", null, InputSampling.class, InputSampling.LERP);
	
	@Override
	public AbstractParameter<?>[] registerParameters() {
		AbstractParameter<?>[] p = {
				distortion,
				inputSampling
				};
		return p;
	}

	@Override
	public Map<String, ModuleInputRequest> getInputRequests(ModuleOutputRequest outputRequest) {
		HashMap<String, ModuleInputRequest> inputRequests = new HashMap<String, ModuleInputRequest>();
		
		DatatypeHeightmap ord = (DatatypeHeightmap) outputRequest.data;
		
		// Expand in whole sample distance lengths
		double distortionExpansion = Math.ceil(Math.abs(this.distortion.getValue()) / ord.resolution) * ord.resolution;
		DatatypeHeightmap heightmapRequestData = new DatatypeHeightmap(Extent.expandedBuildExtent(ord.extent, distortionExpansion, 0.0, distortionExpansion), ord.resolution);
		inputRequests.put("input", new ModuleInputRequest(getInput("Input"), heightmapRequestData));
		
		inputRequests.put("distortion", new ModuleInputRequest(getInput("Distortion"), ord));
		
		return inputRequests;
	}

	@Override
	public AbstractDatatype buildModule(Map<String, AbstractDatatype> inputs, ModuleOutputRequest request) {
		DatatypeHeightmap requestData = (DatatypeHeightmap) request.data;
		
		int mpw = requestData.mapPointsWidth;
		int mpl = requestData.mapPointsLength;
		
		//----------READ INPUTS----------//
		
		float normalizedHeight = getSynthParameters().getNormalizedHeightmapHeight();
		
		double distortion = this.distortion.getValue();
		InputSampling inputSampling = this.inputSampling.getValue();
		
		// Check necessary inputs
		if (inputs.get("input") == null || inputs.get("distortion") == null) {
			// If the main input or distortion input is null, there is not enough input and then just return null
			return null;
		}
		
		// Read in input
		DatatypeHeightmap input = (DatatypeHeightmap) inputs.get("input");
		
		// Read in distortion
		float[][][] distortionMap = null;
		if (inputs.get("distortion") instanceof DatatypeVectormap) {
			distortionMap = ((DatatypeVectormap) inputs.get("distortion")).getVectormap();
		}
		else {
			DatatypeHeightmap distortionHeightmap = (DatatypeHeightmap) inputs.get("distortion");
			distortionMap = new float[2][mpw][mpl];
			for (int u = 0; u < mpw; u++) {
				for (int v = 0; v < mpl; v++) {
					distortionMap[0][u][v] = (float) Math.cos(distortionHeightmap.getLocalHeight(u, v)/normalizedHeight * Math.PI * 2.0);
					distortionMap[1][u][v] = (float) Math.sin(distortionHeightmap.getLocalHeight(u, v)/normalizedHeight * Math.PI * 2.0);
				}
			}
		}
		
		//----------BUILD----------//
		
		float[][] heightmap = new float[mpw][mpl];
		
		double samplesOffset = Math.ceil(Math.abs(distortion) / requestData.resolution);
		
		for (int u = 0; u < mpw; u++) {
			for (int v = 0; v < mpl; v++) {
				double xSampleDistortion = distortionMap[0][u][v]*distortion / requestData.resolution;
				double zSampleDistortion = distortionMap[1][u][v]*distortion / requestData.resolution;
				
				float o = 0.0f;
				if (inputSampling == InputSampling.LERP) {
					o = input.getLocalLerpHeight((u+samplesOffset + xSampleDistortion), (v+samplesOffset + zSampleDistortion));
				}
				else {
					o = input.getLocalHeight((int) (u+samplesOffset + xSampleDistortion), (int) (v+samplesOffset + zSampleDistortion));
				}
				
				heightmap[u][v] = o;
			}
		}
		
		requestData.setHeightmap(heightmap);
		
		return requestData;
	}

	@Override
	public String getModuleName() {
		return "Distort";
	}

	@Override
	public IModuleCategory getModuleCategory() {
		return ModuleCategory.MODIFIER;
	}

	@Override
	public ModuleInput[] registerInputs() {
		ModuleInput[] i = {
				new ModuleInput(new DatatypeHeightmap(), "Input"),
				new ModuleInput(new DatatypeHeightmap(), "Distortion")
				};
		return i;
	}

	@Override
	public ModuleOutput[] registerOutputs() {
		ModuleOutput[] o = {
				new ModuleOutput(new DatatypeHeightmap(), "Output")
				};
		return o;
	}

	@Override
	public boolean isBypassable() {
		return true;
	}
	
	private enum InputSampling {
		LERP, DISCRETE;
	}
}
