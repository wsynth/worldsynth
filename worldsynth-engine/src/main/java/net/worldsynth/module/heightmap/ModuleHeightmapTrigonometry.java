/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.module.heightmap;

import net.worldsynth.datatype.AbstractDatatype;
import net.worldsynth.datatype.DatatypeHeightmap;
import net.worldsynth.module.*;
import net.worldsynth.parameter.AbstractParameter;
import net.worldsynth.parameter.BooleanParameter;
import net.worldsynth.parameter.EnumParameter;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

public class ModuleHeightmapTrigonometry extends AbstractModule {
	
	private EnumParameter<TrigonometryFunction> function = new EnumParameter<TrigonometryFunction>("function", "Function", null, TrigonometryFunction.class, TrigonometryFunction.SIN);
	private BooleanParameter asRadians = new BooleanParameter("radians", "As radians", null, false);
	private BooleanParameter normalized = new BooleanParameter("normalized", "Normalized", null, true);
	
	@Override
	public AbstractParameter<?>[] registerParameters() {
		AbstractParameter<?>[] p = {
				function,
				asRadians,
				normalized
		};
		return p;
	}
	
	@Override
	public AbstractDatatype buildModule(Map<String, AbstractDatatype> inputs, ModuleOutputRequest request) {
		DatatypeHeightmap requestData = (DatatypeHeightmap) request.data;
		
		int spw = requestData.mapPointsWidth;
		int spl = requestData.mapPointsLength;
		
		//----------READ INPUTS----------//
		
		float normalizedHeight = getSynthParameters().getNormalizedHeightmapHeight();
		
		TrigonometryFunction function = this.function.getValue();
		boolean normalized = this.normalized.getValue();
		boolean asRadians = this.asRadians.getValue();
		
		//Read in input
		if (inputs.get("input") == null) {
			// If the input is null, there is no input and then just return null
			return null;
		}
		DatatypeHeightmap input = ((DatatypeHeightmap) inputs.get("input"));
		
		//----------BUILD----------//
		
		float[][] heightmap = new float[spw][spl];
		
		
		// Apply function
		final Function<Float, Float> f;
		if (asRadians) {
			f = t -> (float) function.apply(t);
		}
		else {
			f = t -> (float) function.apply(Math.toRadians(t));
		}
		
		// Apply normalization
		final Function<Float, Float> ff;
		if (normalized) {
			ff = t -> f.apply(t) * normalizedHeight;
		}
		else {
			ff = f;
		}
		
		for (int u = 0; u < spw; u++) {
			for (int v = 0; v < spl; v++) {
				heightmap[u][v] = ff.apply(input.getLocalHeight(u, v));
			}
		}
		
		requestData.setHeightmap(heightmap);
		
		return requestData;
	}

	@Override
	public Map<String, ModuleInputRequest> getInputRequests(ModuleOutputRequest outputRequest) {
		HashMap<String, ModuleInputRequest> inputRequests = new HashMap<String, ModuleInputRequest>();
		
		inputRequests.put("input", new ModuleInputRequest(getInput("Input"), outputRequest.data));
		
		return inputRequests;
	}

	@Override
	public String getModuleName() {
		return "Trigonometry";
	}
	
	@Override
	public String getModuleMetaTag() {
		return function.getValue().name();
	}

	@Override
	public IModuleCategory getModuleCategory() {
		return ModuleCategory.MODIFIER;
	}

	@Override
	public ModuleInput[] registerInputs() {
		ModuleInput[] i = {
				new ModuleInput(new DatatypeHeightmap(), "Input")
				};
		return i;
	}

	@Override
	public ModuleOutput[] registerOutputs() {
		ModuleOutput[] o = {
				new ModuleOutput(new DatatypeHeightmap(), "Output")
				};
		return o;
	}

	@Override
	public boolean isBypassable() {
		return true;
	}
	
	private enum TrigonometryFunction {
		COS(t -> Math.cos(t)),
		SIN(t -> Math.sin(t)),
		TAN(t -> Math.tan(t)),
		ACOS(t -> Math.acos(t)),
		ASIN(t -> Math.asin(t)),
		ATAN(t -> Math.atan(t)),
		COSH(t -> Math.cosh(t)),
		SINH(t -> Math.sinh(t)),
		TANH(t -> Math.tanh(t));
		
		private Function<Double, Double> function;
		
		private TrigonometryFunction(Function<Double, Double> function) {
			this.function = function;
		}
		
		public double apply(double t) {
			return function.apply(t);
		}
	}
}
