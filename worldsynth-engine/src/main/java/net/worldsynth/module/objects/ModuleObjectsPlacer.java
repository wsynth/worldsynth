/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.module.objects;

import java.io.File;
import java.io.IOException;
import java.util.*;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import javafx.beans.binding.Bindings;
import javafx.beans.property.*;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.MenuItem;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import net.worldsynth.customobject.Block;
import net.worldsynth.customobject.CustomObject;
import net.worldsynth.customobject.CustomObjectFormat;
import net.worldsynth.customobject.CustomObjectFormatRegistry;
import net.worldsynth.customobject.LocatedCustomObject;
import net.worldsynth.datatype.AbstractDatatype;
import net.worldsynth.datatype.DatatypeBlockspace;
import net.worldsynth.datatype.DatatypeFeaturemap;
import net.worldsynth.datatype.DatatypeFeaturespace;
import net.worldsynth.datatype.DatatypeMultitype;
import net.worldsynth.datatype.DatatypeObjects;
import net.worldsynth.extent.Extent;
import net.worldsynth.featurepoint.Featurepoint2D;
import net.worldsynth.featurepoint.Featurepoint3D;
import net.worldsynth.material.MaterialRegistry;
import net.worldsynth.material.MaterialState;
import net.worldsynth.module.AbstractModule;
import net.worldsynth.module.IModuleCategory;
import net.worldsynth.module.ModuleCategory;
import net.worldsynth.module.ModuleInput;
import net.worldsynth.module.ModuleInputRequest;
import net.worldsynth.module.ModuleOutput;
import net.worldsynth.module.ModuleOutputRequest;
import net.worldsynth.parameter.AbstractParameter;
import net.worldsynth.parameter.BooleanParameter;
import net.worldsynth.parameter.IntegerParameter;
import net.worldsynth.standalone.ui.parameters.ParameterUiElement;

public class ModuleObjectsPlacer extends AbstractModule {

	private final BooleanParameter placeAir = new BooleanParameter("placeair", "Place air", null, false);
	private final BooleanParameter replaceBlocks = new BooleanParameter("replaceblocks", "Replace blocks", null, true);
	private final IntegerParameter placementRadius = new IntegerParameter("placementradius", "Placement radius", null, 16, 0, Integer.MAX_VALUE, 0, 32);
	private final ObjectLayersParameter layers = new ObjectLayersParameter("layers", "Layers", null);
	
	@Override
	public AbstractParameter<?>[] registerParameters() {
		AbstractParameter<?>[] p = {
				placeAir,
				replaceBlocks,
				placementRadius,
				layers
		};
		return p;
	}

	@Override
	protected void onParametersChange() {
		reregisterIO();
	}
	
	@Override
	public Map<String, ModuleInputRequest> getInputRequests(ModuleOutputRequest outputRequest) {
		HashMap<String, ModuleInputRequest> inputRequests = new HashMap<>();
		
		DatatypeBlockspace ord = (DatatypeBlockspace) outputRequest.data;
		Extent inputExtent = Extent.expandedBuildExtent(ord.extent, placementRadius.getValue(), 0.0, placementRadius.getValue());
		
		DatatypeBlockspace blockspaceInput = new DatatypeBlockspace(inputExtent, ord.resolution);
		inputRequests.put("input", new ModuleInputRequest(getInput(0), blockspaceInput));
		
		for (ObjectLayer layer: layers.getValue()) {
			AbstractDatatype placementRequestData = new DatatypeMultitype(new DatatypeFeaturemap(inputExtent, ord.resolution), new DatatypeFeaturespace(inputExtent, ord.resolution));
			inputRequests.put(layer.getName() + " placement", new ModuleInputRequest(getInput(layer.getName() + " placement"), placementRequestData));
		}
		
		return inputRequests;
	}
	
	@Override
	public AbstractDatatype buildModule(Map<String, AbstractDatatype> inputs, ModuleOutputRequest request) {
		DatatypeBlockspace requestData = (DatatypeBlockspace) request.data;
		
		Extent extent = requestData.extent;
		double y = requestData.extent.getY();
		double res = requestData.resolution;
		
		//----------READ INPUTS----------//
		
		DatatypeBlockspace inputBlockspace = (DatatypeBlockspace) inputs.get("input");
		
		boolean placeAir = this.placeAir.getValue();
		boolean replaceBlocks = this.replaceBlocks.getValue();
		
		//----------BUILD----------//
		
		DatatypeBlockspace outputBlockspace = extractSubspace(inputBlockspace, (DatatypeBlockspace) request.data);
		
		// Iterate on all the layers
		for (ObjectLayer layer: layers.getValue()) {
			if (inputs.get(layer.name + " placement") == null) {
				continue;
			}
			
			Featurepoint3D[] featurepoints = null;
			if (inputs.get(layer.name + " placement") instanceof DatatypeFeaturemap) {
				DatatypeFeaturemap featuremap = (DatatypeFeaturemap) inputs.get(layer.name + " placement");
				Featurepoint2D[] featuremapPoints = featuremap.getFeaturepoints();
				
				// Convert Featurepoint2D to Featurepoint3D
				featurepoints = new Featurepoint3D[featuremapPoints.length];
				for (int i = 0; i < featurepoints.length; i++) {
					if (inputBlockspace == null) {
						// Place feature at the bottom of the output extent if no blockspace input is provided
						featurepoints[i] = new Featurepoint3D(
								featuremapPoints[i].getX(),
								y,
								featuremapPoints[i].getZ(),
								featuremapPoints[i].getSeed());
					}
					else {
						// Place features at the surface block when a blockspace input is provided
						featurepoints[i] = new Featurepoint3D(
								featuremapPoints[i].getX(),
								getBlockspaceSurfaceHeightAt(inputBlockspace, featuremapPoints[i].getX(), featuremapPoints[i].getZ()),
								featuremapPoints[i].getZ(),
								featuremapPoints[i].getSeed());
					}
				}
			}
			else if (inputs.get(layer.name + " placement") instanceof DatatypeFeaturespace) {
				DatatypeFeaturespace featurespace = (DatatypeFeaturespace) inputs.get(layer.name + " placement");
				featurepoints = featurespace.getFeaturepoints();
			}
			
			if (layer.hasInternalObjects()) {
				// Place objects from layer object set
				for (Featurepoint3D point : featurepoints) {
					// Get an object from the layer cached objects according to the feature seed
					long objectSeed = point.getSeed() >= 0 ? point.getSeed() : point.getSeed() - Long.MIN_VALUE;
					CustomObject object = layer.getCachedObjects().get((int) (objectSeed % layer.getCachedObjects().size()));
					// Create a located object
					LocatedCustomObject locatedObject = new LocatedCustomObject(point.getX(), point.getY(), point.getZ(), point.getSeed(), object);
					// Place the object into the blockspace
					placeObjectInBlockspace(outputBlockspace, locatedObject, placeAir, replaceBlocks);
				}
			}
			else {
				// Request a set of objects from the layer objects input
				LocatedCustomObject[] requestObjects = new LocatedCustomObject[featurepoints.length];
				for (int i = 0; i < featurepoints.length; i++) {
					//Create a located custom object for use in the objects input request
					requestObjects[i] = new LocatedCustomObject(featurepoints[i].getX(), featurepoints[i].getY(), featurepoints[i].getZ(), featurepoints[i].getSeed());
				}
				// Request the objects to be placed at by this layer
				Extent objectsExtent = Extent.expandedBuildExtent(extent, placementRadius.getValue(), 0.0, placementRadius.getValue());
				DatatypeObjects layerObjects = (DatatypeObjects) buildInputData(new ModuleInputRequest(getInput(layer.getName() + " objects"), new DatatypeObjects(requestObjects, objectsExtent, res)));
				if (layerObjects == null) {
					continue;
				}
				
				// Place objects
				for (LocatedCustomObject locatedObject: layerObjects.getLocatedObjects()) {
					placeObjectInBlockspace(outputBlockspace, locatedObject, placeAir, replaceBlocks);
				}
			}
		}
		
		return outputBlockspace;
	}
	
	private DatatypeBlockspace extractSubspace(DatatypeBlockspace parentSpace, DatatypeBlockspace subspace) {
		MaterialState<?, ?>[][][] subspaceMaterials = new MaterialState<?, ?>[subspace.spacePointsWidth][subspace.spacePointsHeight][subspace.spacePointsLength];
		
		MaterialState<?, ?> defaultAir = MaterialRegistry.getDefaultAir().getDefaultState();
		
		int blockspaceExtension = (int) Math.ceil(placementRadius.getValue()/subspace.resolution);
		
		for (int u = 0; u < subspace.spacePointsWidth; u++) {
			for (int v = 0; v < subspace.spacePointsHeight; v++) {
				for (int w = 0; w < subspace.spacePointsLength; w++) {
					if (parentSpace == null) {
						subspaceMaterials[u][v][w] = defaultAir;
					}
					else {
						subspaceMaterials[u][v][w] = parentSpace.getLocalMaterial(u+blockspaceExtension, v, w+blockspaceExtension);
					}
				}
			}
		}
		
		subspace.setBlockspace(subspaceMaterials);
		return subspace;
	}
	
	private double getBlockspaceSurfaceHeightAt(DatatypeBlockspace blockspace, double x, double z) {
		// Convert global coordinates to local coordinates
		x -= blockspace.extent.getX();
		x /= blockspace.resolution;
		int ix = (int) Math.floor(x);
		
		z -= blockspace.extent.getZ();
		z /= blockspace.resolution;
		int iz = (int) Math.floor(z);
		
		// Find local surface elevation
		int iy = blockspace.spacePointsHeight-1;
		while (blockspace.getLocalMaterial(ix, iy, iz).isAir() && iy > 0) {
			iy--;
		}
		
		// Convert local elevation to global elevation
		double y = iy;
		y *= blockspace.resolution;
		y += blockspace.extent.getY();
		return y;
	}
	
	private void placeObjectInBlockspace(DatatypeBlockspace blockspace, LocatedCustomObject locatedObject, boolean placeAir, boolean replaceBlocks) {
		double x = locatedObject.getX();
		double y = locatedObject.getY();
		double z = locatedObject.getZ();
		Block[] blocks = locatedObject.getObject().getBlocks();
		
		double blockspaceX = blockspace.extent.getX();
		double blockspaceY = blockspace.extent.getY();
		double blockspaceZ = blockspace.extent.getZ();
		double blockspaceRes = blockspace.resolution;
		
		int spw = blockspace.spacePointsWidth;
		int sph = blockspace.spacePointsHeight;
		int spl = blockspace.spacePointsLength;
		
		// Convert global object position to local blockspace coordinates
		x -= blockspaceX;
		x /= blockspaceRes;
		
		y -= blockspaceY;
		y /= blockspaceRes;
		
		z -= blockspaceZ;
		z /= blockspaceRes;
		
		// Iterate through the blocks
		for (Block b: blocks) {
			MaterialState<?, ?> material = b.material;
			if (!placeAir && material.isAir()) continue;
			
			// Convert object block position to local blockspace coordinates
			double bx = ((double) b.x) / blockspaceRes + x;
			double by = ((double) b.y) / blockspaceRes + y;
			double bz = ((double) b.z) / blockspaceRes + z;
			
			int ix = (int) Math.floor(bx);
			int iy = (int) Math.floor(by);
			int iz = (int) Math.floor(bz);
			
			// Ignore blocks that land outside the blockspace
			if (ix < 0 || iy < 0 || iz < 0 || ix >= spw || iy >= sph || iz >= spl) {
				continue;
			}

			if (replaceBlocks || blockspace.getLocalMaterial(ix, iy, iz).isAir()) {
				blockspace.getBlockspace()[ix][iy][iz] = material;
			}
		}
	}
	
	@Override
	public String getModuleName() {
		return "Objects placer";
	}

	@Override
	public IModuleCategory getModuleCategory() {
		return ModuleCategory.MODIFIER;
	}

	@Override
	public ModuleInput[] registerInputs() {
		ArrayList<ModuleInput> inputs = new ArrayList<>();
		inputs.add(new ModuleInput(new DatatypeBlockspace(), "Blockspace"));
		for (ObjectLayer layer: layers.getValue()) {
			inputs.add(new ModuleInput(new DatatypeMultitype(new DatatypeFeaturemap(), new DatatypeFeaturespace()), layer.getName() + " placement"));
			if (!layer.hasInternalObjects()) {
				inputs.add(new ModuleInput(new DatatypeObjects(), layer.getName() + " objects"));
			}
		}
		
		return inputs.toArray(new ModuleInput[inputs.size()]);
	}

	@Override
	public ModuleOutput[] registerOutputs() {
		ModuleOutput[] o = {
				new ModuleOutput(new DatatypeBlockspace(), "Output")
				};
		return o;
	}

	@Override
	public boolean isBypassable() {
		return true;
	}

	private class ObjectLayer {
		private String name;
		private List<File> objectFiles;
		private List<CustomObject> cachedObjects = null;
		
		public ObjectLayer(String name, List<File> objectFiles) {
			this.name = name;
			this.objectFiles = objectFiles;
		}
		
		public ObjectLayer(JsonNode node, File source) {
			fromJson(node, source);
		}
		
		public String getName() {
			return name;
		}
		
		public boolean hasInternalObjects() {
			return objectFiles.size() > 0;
		}
		
		public List<CustomObject> getCachedObjects() {
			if (cachedObjects == null && objectFiles.size() > 0) {
				cachedObjects = readObjectsFromFile(objectFiles);
			}
			return cachedObjects;
		}
		
		private List<CustomObject> readObjectsFromFile(List<File> objectFiles) {
			ArrayList<CustomObject> objects = new ArrayList<>();
			
			for (File objectFile: objectFiles) {
				try {
					objects.add(CustomObjectFormatRegistry.getFormatForFile(objectFile).readObjectFromFile(objectFile));
				} catch (IOException ex) {
					ex.printStackTrace();
					return null;
				}
			}
			
			return objects;
		}
		
		public JsonNode toJson(File destination) {
			ObjectMapper objectMapper = new ObjectMapper();
			ObjectNode node = objectMapper.createObjectNode();
			
			node.put("name", name);
			
			ArrayNode filesNode = objectMapper.createArrayNode();
			if (objectFiles != null && !objectFiles.isEmpty()) {
				for (File f: objectFiles) {
					filesNode.add(getPath(f, destination));
				}
			}
			node.set("files", filesNode);
			
			return node;
		}

		private String getPath(File value, File destination) {
			if (destination != null && fileIsChild(destination.getParentFile(), value)) {
				return destination.toPath().relativize(value.toPath()).toString();
			}
			else {
				return value.getAbsolutePath();
			}
		}

		private boolean fileIsChild(final File parent, File child) {
			while (child.getParentFile() != null) {
				child = child.getParentFile();
				if (parent.equals(child)) {
					return true;
				}
			}
			return false;
		}
		
		public void fromJson(JsonNode node, File source) {
			name = node.get("name").asText();
			
			objectFiles = new ArrayList<>();
			JsonNode filePaths = node.get("files");
			for (JsonNode e: filePaths) {
				String path = e.asText();
				if (path.startsWith("..")) {
					// Path is relative
					objectFiles.add(source.toPath().resolve(path).normalize().toFile());
				}
				else {
					// Path is absolute
					objectFiles.add(new File(path));
				}
			}
			cachedObjects = null;
		}
		
		@Override
		protected ObjectLayer clone() throws CloneNotSupportedException {
			ObjectLayer clone = new ObjectLayer(name, new ArrayList<>());
			if (objectFiles != null) {
				for (File f: objectFiles) {
					clone.objectFiles.add(new File(f, ""));
				}
			}
			return clone;
		}
	}
	
	private class ObjectLayersParameter extends AbstractParameter<List<ObjectLayer>> {
		public ObjectLayersParameter(String name, String displayName, String description) {
			super(name, displayName, description, new ArrayList<>(Arrays.asList(new ObjectLayer("Default", new ArrayList<>()))));
		}

		@Override
		public ParameterUiElement<List<ObjectLayer>> getUi() {
			return new ObjectLayersParameterUi(this);
		}

		@Override
		public JsonNode toJson(File destination) {
			ObjectMapper objectMapper = new ObjectMapper();

			ArrayNode layersNode = objectMapper.createArrayNode();
			for (ObjectLayer layer: getValue()) {
				layersNode.add(layer.toJson(destination));
			}

			return layersNode;
		}

		@Override
		public void fromJson(JsonNode node, File source) {
			getValue().clear();
			for (JsonNode layerNode: node) {
				getValue().add(new ObjectLayer(layerNode, source));
			}
		}
	}

	private class ObjectLayersParameterUi extends ParameterUiElement<List<ObjectLayer>> {

		private GridPane gridPane = new GridPane();
		private ListView<UiObjectLayer> layerListView = new ListView<>();
		private ListView<File> objectListView = new ListView<>();

		public ObjectLayersParameterUi(ObjectLayersParameter parameter) {
			super(parameter);

			///////////////////////
			// Layer list editor //
			///////////////////////

			gridPane.add(new Label("Layers"), 0, 2);

			layerListView.setPrefSize(300, 400);
			layerListView.setCellFactory(param -> {
				LayerCell cell = new LayerCell();
				cell.setOnEdit(e -> {
					layerListView.getSelectionModel().select(cell.getItem());
				});
				cell.setOnDelete(e -> {
					layerListView.getItems().remove(cell.getItem());
				});
				return cell;
			});

			layerListView.getItems().setAll(getEditorLayers());
			for (UiObjectLayer layer: layerListView.getItems()) {
				layer.layerName.addListener((observable, oldValue, newValue) -> {
					applyEditorLayers(layerListView.getItems());
				});
			}
			layerListView.getItems().addListener((ListChangeListener<UiObjectLayer>) c -> {
				while (c.next()) {
					for (UiObjectLayer newLayer: c.getAddedSubList()) {
						newLayer.layerName.addListener((observable, oldValue, newValue) -> {
							applyEditorLayers(layerListView.getItems());
						});
					}
				}

				applyEditorLayers(layerListView.getItems());
			});

			ScrollPane layerListPane = new ScrollPane(layerListView);
			GridPane.setColumnSpan(layerListPane, 2);
			gridPane.add(layerListPane, 0, 3);

			Button addLayerButton = new Button("Add layer");
			addLayerButton.setOnAction(e -> {
				String newLayerName = "New Layer";
				int iterativeName = 0;
				while (hasLayerWithName(newLayerName)) {
					newLayerName = "New Layer " + ++iterativeName;
				}

				layerListView.getItems().add(new UiObjectLayer(newLayerName, new ArrayList<>()));
			});
			gridPane.add(addLayerButton, 0, 4);

			////////////////////////////
			// Layer file list editor //
			////////////////////////////

			Button addObjectsButton = new Button("Add objects");
			addObjectsButton.setDisable(true);
			Button removeObjectsButton = new Button("Remove objects");
			removeObjectsButton.setDisable(true);

			Label fileListLabel = new Label("File list          Layer:");
			GridPane.setColumnSpan(fileListLabel, 3);
			gridPane.add(fileListLabel, 2, 2);

			layerListView.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
				fileListLabel.textProperty().unbind();
				if (newValue == null) {
					fileListLabel.setText("File list          Layer:");
					objectListView.setItems(null);
					addObjectsButton.setDisable(true);
					removeObjectsButton.setDisable(true);
				}
				else {
					fileListLabel.textProperty().bind(Bindings.concat("File list          Layer: ", newValue.layerName));
					objectListView.setItems(newValue.objectFiles);
					addObjectsButton.setDisable(false);
					removeObjectsButton.setDisable(false);
				}
			});

			objectListView.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
			objectListView.setPrefSize(600, 400);
			GridPane.setColumnSpan(objectListView, 3);
			gridPane.add(objectListView, 2, 3);

			addObjectsButton.setOnAction(e -> {
				FileChooser fileChooser = new FileChooser();
				ArrayList<String> allExtensions = new ArrayList<>();
				for (CustomObjectFormat format: CustomObjectFormatRegistry.getFormats()) {
					allExtensions.add("*." + format.formatSuffix());
				}
				fileChooser.getExtensionFilters().add(new ExtensionFilter("All custom objects", allExtensions));
				for (CustomObjectFormat format: CustomObjectFormatRegistry.getFormats()) {
					fileChooser.getExtensionFilters().add(new ExtensionFilter(format.formatName(), "*." + format.formatSuffix()));
				}

				List<File> selectedFiles = fileChooser.showOpenMultipleDialog(addObjectsButton.getScene().getWindow());
				if (selectedFiles != null) {
					layerListView.getSelectionModel().getSelectedItem().objectFiles.addAll(selectedFiles);
				}

				applyEditorLayers(layerListView.getItems());
			});

			removeObjectsButton.setOnAction(e -> {
				layerListView.getSelectionModel().getSelectedItem().objectFiles.removeAll(objectListView.getSelectionModel().getSelectedItems());
				applyEditorLayers(layerListView.getItems());

			});

			gridPane.add(addObjectsButton, 2, 4);
			gridPane.add(removeObjectsButton, 3, 4);
		}

		private boolean hasLayerWithName(String name) {
			for (UiObjectLayer l: layerListView.getItems()) {
				if (l.layerName.getValue().equals(name)) return true;
			}
			return false;
		}

		public List<UiObjectLayer> getEditorLayers() {
			List<UiObjectLayer> editorLayers = new ArrayList<>();
			for (ObjectLayer layer: parameter.getValue()) {
				editorLayers.add(new UiObjectLayer(layer.name, layer.objectFiles));
			}
			return editorLayers;
		}

		public void applyEditorLayers(List<UiObjectLayer> layers) {
			List<ObjectLayer> applyLayers = new ArrayList<>();
			for (UiObjectLayer layer: layers) {
				applyLayers.add(new ObjectLayer(layer.layerName.getValue(), new ArrayList<>(layer.objectFiles)));
			}

			uiValue = applyLayers;
		}

		@Override
		public void setDisable(boolean disable) {

		}

		@Override
		public void addToGrid(GridPane pane, int row) {
			pane.add(gridPane, 0, row, 4, 1);
		}
	}

	private class UiObjectLayer {
		public final StringProperty layerName;
		public final ObservableList<File> objectFiles;

		public UiObjectLayer(String name, List<File> files) {
			this.layerName = new SimpleStringProperty(name);
			this.objectFiles = FXCollections.observableArrayList(files);
		}
	}
	
	private class LayerCell extends ListCell<UiObjectLayer> {
		private final HBox hbox;
		private final TextField nameField = new TextField("");
		
		public LayerCell() {
			HBox.setHgrow(nameField, Priority.ALWAYS);
			nameField.textProperty().addListener((ob, ov, nv) -> {
				getItem().layerName.setValue(nv);
			});

			Button editButton = new Button("Edit");
			editButton.setOnAction(e -> {
				getOnEdit().handle(e);
			});

			MenuItem deleteLayerItem = new MenuItem("Delete");
			deleteLayerItem.setOnAction(e -> {
				getOnDelete().handle(e);
			});
			editButton.setContextMenu(new ContextMenu(deleteLayerItem));

			hbox = new HBox(nameField, editButton);
		}

		private ObjectProperty<EventHandler<ActionEvent>> onEdit = new SimpleObjectProperty<>();

		/**
		 * The action which is invoked whenever the edit button is fired.
		 */
		public final ObjectProperty<EventHandler<ActionEvent>> onEditProperty() {
			return onEdit;
		}

		public final void setOnEdit(EventHandler<ActionEvent> value) {
			onEditProperty().set(value);
		}

		public final EventHandler<ActionEvent> getOnEdit() {
			return onEditProperty().get();
		}

		private ObjectProperty<EventHandler<ActionEvent>> onDelete = new SimpleObjectProperty<>();

		/**
		 * The action which is invoked whenever the delete button is fired.
		 */
		public final ObjectProperty<EventHandler<ActionEvent>> onDeleteProperty() {
			return onDelete;
		}

		public final void setOnDelete(EventHandler<ActionEvent> value) {
			onDeleteProperty().set(value);
		}

		public final EventHandler<ActionEvent> getOnDelete() {
			return onDeleteProperty().get();
		}

		@Override
		protected void updateItem(UiObjectLayer item, boolean empty) {
			super.updateItem(item, empty);
			setText(null);
			setGraphic(null);
			
			if (item != null) {
				nameField.textProperty().setValue(item.layerName.getValue());
				setGraphic(hbox);
			}
		}
	}
}
