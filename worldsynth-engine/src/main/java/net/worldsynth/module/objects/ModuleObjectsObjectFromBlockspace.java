/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.module.objects;

import net.worldsynth.customobject.Block;
import net.worldsynth.customobject.CustomObject;
import net.worldsynth.customobject.LocatedCustomObject;
import net.worldsynth.datatype.AbstractDatatype;
import net.worldsynth.datatype.DatatypeBlockspace;
import net.worldsynth.datatype.DatatypeObjects;
import net.worldsynth.extent.Extent;
import net.worldsynth.material.MaterialState;
import net.worldsynth.module.*;
import net.worldsynth.parameter.AbstractParameter;
import net.worldsynth.parameter.BooleanParameter;
import net.worldsynth.parameter.WorldExtentParameter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class ModuleObjectsObjectFromBlockspace extends AbstractModule {

	private final WorldExtentParameter objectExtent = new WorldExtentParameter("extent", "Object extent", null, this);
	private final BooleanParameter includeAir = new BooleanParameter("includeair", "Include air", null, false);

	@Override
	public AbstractParameter<?>[] registerParameters() {
		return new AbstractParameter<?>[]{
				objectExtent,
				includeAir
		};
	}

	@Override
	public Map<String, ModuleInputRequest> getInputRequests(ModuleOutputRequest outputRequest) {
		HashMap<String, ModuleInputRequest> inputRequests = new HashMap<>();

		DatatypeBlockspace blockspaceInput = new DatatypeBlockspace(objectExtent.getValue().asExtent(), 1.0);
		inputRequests.put("input", new ModuleInputRequest(getInput("Blockspace"), blockspaceInput));

		return inputRequests;
	}

	@Override
	public AbstractDatatype buildModule(Map<String, AbstractDatatype> inputs, ModuleOutputRequest request) {
		DatatypeObjects requestData = (DatatypeObjects) request.data;

		//----------READ INPUTS----------//

		boolean includeAir = this.includeAir.getValue();

		DatatypeBlockspace inputBlockspace = (DatatypeBlockspace) inputs.get("input");

		//----------BUILD----------//

		CustomObject blockspaceObject = convertToCustomObject(inputBlockspace, includeAir);

		//Clone objects for return value based on seed
		for (LocatedCustomObject locatedObject : requestData.getLocatedObjects()) {
			locatedObject.setObject(blockspaceObject);
		}

		return requestData;
	}

	private CustomObject convertToCustomObject(DatatypeBlockspace blockspace, boolean includeAir) {
		Extent objectExtent = blockspace.extent;

		int x0 = (int) objectExtent.getX();
		int y0 = (int) objectExtent.getY();
		int z0 = (int) objectExtent.getZ();

		ArrayList<Block> objectBlocks = new ArrayList<>();
		for (int x = 0; x < blockspace.spacePointsWidth; x++) {
			for (int y = 0; y < blockspace.spacePointsHeight; y++) {
				for (int z = 0; z < blockspace.spacePointsLength; z++) {
					MaterialState<?, ?> material = blockspace.getLocalMaterial(x, y, z);

					if (includeAir && material.isAir()) continue;

					objectBlocks.add(new Block(x0 + x, y0 + y, z0 + z, material));
				}
			}
		}

		return new CustomObject(objectBlocks);
	}

	@Override
	public String getModuleName() {
		return "Object from blockspace";
	}

	@Override
	public IModuleCategory getModuleCategory() {
		return ModuleCategory.GENERATOR;
	}

	@Override
	public ModuleInput[] registerInputs() {
		return new ModuleInput[]{
				new ModuleInput(new DatatypeBlockspace(), "Blockspace")
		};
	}

	@Override
	public ModuleOutput[] registerOutputs() {
		return new ModuleOutput[]{
				new ModuleOutput(new DatatypeObjects(), "Output")
		};
	}

	@Override
	public boolean isBypassable() {
		return false;
	}
}
