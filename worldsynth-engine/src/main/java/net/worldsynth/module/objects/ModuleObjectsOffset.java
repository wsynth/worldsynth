/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.module.objects;

import net.worldsynth.customobject.Block;
import net.worldsynth.customobject.CustomObject;
import net.worldsynth.customobject.LocatedCustomObject;
import net.worldsynth.datatype.AbstractDatatype;
import net.worldsynth.datatype.DatatypeObjects;
import net.worldsynth.module.*;
import net.worldsynth.parameter.AbstractParameter;
import net.worldsynth.parameter.IntegerTupleParameter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.IdentityHashMap;
import java.util.Map;

public class ModuleObjectsOffset extends AbstractModule {

	private IntegerTupleParameter offset = new IntegerTupleParameter("offset", "Offset", null, Integer.MIN_VALUE, Integer.MAX_VALUE, 0, 0, 0);

	@Override
	public AbstractParameter<?>[] registerParameters() {
		AbstractParameter<?>[] p = {
				offset
				};
		return p;
	}

	@Override
	public AbstractDatatype buildModule(Map<String, AbstractDatatype> inputs, ModuleOutputRequest request) {
		DatatypeObjects requestData = (DatatypeObjects) request.data;
		
		//----------READ INPUTS----------//
		
		int xOffset = offset.getValue().getValue(0);
		int yOffset = offset.getValue().getValue(1);
		int zOffset = offset.getValue().getValue(2);
		
		//Check if inputs are available
		if (inputs.get("objects") == null) {
			//If the objects input is null, there is not enough input and then just return null
			return null;
		}
		
		LocatedCustomObject[] inputObjects = ((DatatypeObjects) inputs.get("objects")).getLocatedObjects();
		
		//----------BUILD----------//
		
		LocatedCustomObject[] outputObjects = new LocatedCustomObject[inputObjects.length];
		
		IdentityHashMap<CustomObject, CustomObject> centeredObjects = new IdentityHashMap<CustomObject, CustomObject>();
		
		for (int i = 0; i < inputObjects.length; i++) {
			LocatedCustomObject locatedObject = inputObjects[i];
			double x = locatedObject.getX();
			double y = locatedObject.getY();
			double z = locatedObject.getZ();
			long seed = locatedObject.getSeed();
			CustomObject object = locatedObject.getObject();
			
			if (!centeredObjects.containsKey(object)) {
				centeredObjects.put(object, offset(object, xOffset, yOffset, zOffset));
			}
			
			outputObjects[i] = new LocatedCustomObject(x, y, z, seed, centeredObjects.get(object));
		}
		
		return new DatatypeObjects(outputObjects, requestData.extent, requestData.resolution);
	}
	
	private CustomObject offset(CustomObject object, int xOffset, int yOffset, int zOffset) {
		// Offset blocks
		ArrayList<Block> blocks = new ArrayList<>();
		for (Block b: object.getBlocks()) {
			blocks.add(new Block(b.x+xOffset, b.y+yOffset, b.z+zOffset, b.material));
		}
		
		return new CustomObject(blocks.toArray(new Block[blocks.size()]));
	}

	@Override
	public Map<String, ModuleInputRequest> getInputRequests(ModuleOutputRequest outputRequest) {
		HashMap<String, ModuleInputRequest> inputRequests = new HashMap<String, ModuleInputRequest>();
		
		inputRequests.put("objects", new ModuleInputRequest(getInput("Input"), outputRequest.data));
		
		return inputRequests;
	}

	@Override
	public String getModuleName() {
		return "Objects offset";
	}

	@Override
	public IModuleCategory getModuleCategory() {
		return ModuleCategory.MODIFIER;
	}

	@Override
	public ModuleInput[] registerInputs() {
		ModuleInput[] i = {
				new ModuleInput(new DatatypeObjects(), "Input")
				};
		return i;
	}

	@Override
	public ModuleOutput[] registerOutputs() {
		ModuleOutput[] o = {
				new ModuleOutput(new DatatypeObjects(), "Output")
				};
		return o;
	}

	@Override
	public boolean isBypassable() {
		return true;
	}
}
