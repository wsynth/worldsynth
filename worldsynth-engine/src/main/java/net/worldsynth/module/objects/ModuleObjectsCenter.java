/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.module.objects;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.IdentityHashMap;
import java.util.Map;

import net.worldsynth.customobject.Block;
import net.worldsynth.customobject.CustomObject;
import net.worldsynth.customobject.LocatedCustomObject;
import net.worldsynth.datatype.AbstractDatatype;
import net.worldsynth.datatype.DatatypeObjects;
import net.worldsynth.module.AbstractModule;
import net.worldsynth.module.IModuleCategory;
import net.worldsynth.module.ModuleCategory;
import net.worldsynth.module.ModuleInput;
import net.worldsynth.module.ModuleInputRequest;
import net.worldsynth.module.ModuleOutput;
import net.worldsynth.module.ModuleOutputRequest;
import net.worldsynth.parameter.AbstractParameter;
import net.worldsynth.parameter.BooleanParameter;
import net.worldsynth.parameter.EnumParameter;

public class ModuleObjectsCenter extends AbstractModule {
	
	private BooleanParameter centerX = new BooleanParameter("centerx", "Center X", null, true);
	private BooleanParameter centerY = new BooleanParameter("centery", "Center Y", null, false);
	private BooleanParameter centerZ = new BooleanParameter("centerz", "Center Z", null, true);
	private EnumParameter<CenteringMethod> centeringMethod = new EnumParameter<CenteringMethod>("method", "Centering method", null, CenteringMethod.class, CenteringMethod.BOUND);
	
	@Override
	public AbstractParameter<?>[] registerParameters() {
		AbstractParameter<?>[] p = {
				centerX,
				centerY,
				centerZ,
				centeringMethod
				};
		return p;
	}

	@Override
	public AbstractDatatype buildModule(Map<String, AbstractDatatype> inputs, ModuleOutputRequest request) {
		DatatypeObjects requestData = (DatatypeObjects) request.data;
		
		//----------READ INPUTS----------//
		
		boolean centerX = this.centerX.getValue();
		boolean centerY = this.centerY.getValue();
		boolean centerZ = this.centerZ.getValue();
		CenteringMethod centeringMethod = this.centeringMethod.getValue();
		
		//Check if inputs are available
		if (inputs.get("objects") == null) {
			//If the objects input is null, there is not enough input and then just return null
			return null;
		}
		
		LocatedCustomObject[] inputObjects = ((DatatypeObjects) inputs.get("objects")).getLocatedObjects();
		
		//----------BUILD----------//
		
		LocatedCustomObject[] outputObjects = new LocatedCustomObject[inputObjects.length];
		
		IdentityHashMap<CustomObject, CustomObject> centeredObjects = new IdentityHashMap<CustomObject, CustomObject>();
		
		for (int i = 0; i < inputObjects.length; i++) {
			LocatedCustomObject locatedObject = inputObjects[i];
			double x = locatedObject.getX();
			double y = locatedObject.getY();
			double z = locatedObject.getZ();
			long seed = locatedObject.getSeed();
			CustomObject object = locatedObject.getObject();
			
			if (!centeredObjects.containsKey(object)) {
				if (centeringMethod == CenteringMethod.BOUND) {
					centeredObjects.put(object, centerObjectBound(object, centerX, centerY, centerZ));
				}
				else {
					centeredObjects.put(object, centerObjectVolume(object, centerX, centerY, centerZ));
				}
			}
			
			outputObjects[i] = new LocatedCustomObject(x, y, z, seed, centeredObjects.get(object));
		}
		
		return new DatatypeObjects(outputObjects, requestData.extent, requestData.resolution);
	}
	
	private CustomObject centerObjectBound(CustomObject object, boolean centerX, boolean centerY, boolean centerZ) {
		// Find bound center
		int xOffset = centerX ? object.width / 2 + object.xMin : 0;
		int yOffset = centerY ? object.height / 2 + object.yMin : 0;
		int zOffset = centerZ ? object.length / 2 + object.zMin : 0;
		
		// Offset blocks
		ArrayList<Block> blocks = new ArrayList<Block>();
		for (Block b: object.getBlocks()) {
			blocks.add(new Block(b.x-xOffset, b.y-yOffset, b.z-zOffset, b.material));
		}
		
		return new CustomObject(blocks.toArray(new Block[blocks.size()]));
	}
	
	private CustomObject centerObjectVolume(CustomObject object, boolean centerX, boolean centerY, boolean centerZ) {
		// Find volume center
		int xOffset = 0, yOffset = 0, zOffset = 0;
		int massiveBlocks = 0;
		for (Block b: object.getBlocks()) {
			if (! b.material.isAir()) {
				xOffset += b.x;
				yOffset += b.y;
				zOffset += b.z;
				massiveBlocks++;
			}
		}
		xOffset = centerX ? roundedIntDiv(xOffset, massiveBlocks) : 0;
		yOffset = centerY ? roundedIntDiv(yOffset, massiveBlocks) : 0;
		zOffset = centerZ ? roundedIntDiv(zOffset, massiveBlocks) : 0;
		
		// Offset blocks
		ArrayList<Block> blocks = new ArrayList<Block>();
		for (Block b: object.getBlocks()) {
			blocks.add(new Block(b.x-xOffset, b.y-yOffset, b.z-zOffset, b.material));
		}
		
		return new CustomObject(blocks.toArray(new Block[blocks.size()]));
	}
	
	private int roundedIntDiv(int i0, int i1) {
		return (int) Math.round((double) i0 / (double) i1);
	}

	@Override
	public Map<String, ModuleInputRequest> getInputRequests(ModuleOutputRequest outputRequest) {
		HashMap<String, ModuleInputRequest> inputRequests = new HashMap<String, ModuleInputRequest>();
		
		inputRequests.put("objects", new ModuleInputRequest(getInput("Input"), outputRequest.data));
		
		return inputRequests;
	}

	@Override
	public String getModuleName() {
		return "Objects center";
	}

	@Override
	public IModuleCategory getModuleCategory() {
		return ModuleCategory.MODIFIER;
	}

	@Override
	public ModuleInput[] registerInputs() {
		ModuleInput[] i = {
				new ModuleInput(new DatatypeObjects(), "Input")
				};
		return i;
	}

	@Override
	public ModuleOutput[] registerOutputs() {
		ModuleOutput[] o = {
				new ModuleOutput(new DatatypeObjects(), "Output")
				};
		return o;
	}

	@Override
	public boolean isBypassable() {
		return true;
	}
	
	private enum CenteringMethod {
		BOUND,
		VOLUME;
	}
}
