/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.module.objects;

import javafx.stage.FileChooser;
import net.worldsynth.customobject.CustomObject;
import net.worldsynth.customobject.CustomObjectFormat;
import net.worldsynth.customobject.CustomObjectFormatRegistry;
import net.worldsynth.customobject.LocatedCustomObject;
import net.worldsynth.datatype.AbstractDatatype;
import net.worldsynth.datatype.DatatypeObjects;
import net.worldsynth.module.*;
import net.worldsynth.parameter.AbstractParameter;
import net.worldsynth.parameter.FileListParameter;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ModuleObjectsImporter extends AbstractModule {

	private static List<FileChooser.ExtensionFilter> extensionFilters = new ArrayList<>();
	static {
		if (!CustomObjectFormatRegistry.getFormats().isEmpty()) {
			// Create a common extension filer for all registered custom object formats
			ArrayList<String> allExtensions = new ArrayList<String>();
			for (CustomObjectFormat format : CustomObjectFormatRegistry.getFormats()) {
				allExtensions.add("*." + format.formatSuffix());
			}
			extensionFilters.add(new FileChooser.ExtensionFilter("All custom objects", allExtensions));

			// Create individual extension filers for all registered custom object formats
			for (CustomObjectFormat format : CustomObjectFormatRegistry.getFormats()) {
				extensionFilters.add(new FileChooser.ExtensionFilter(format.formatName(), "*." + format.formatSuffix()));
			}
		}
	}

	private final FileListParameter objectFiles = new FileListParameter("files", "", null, new ArrayList<>(), extensionFilters);

	private List<CustomObject> cachedObjects;

	@Override
	public AbstractParameter<?>[] registerParameters() {
		AbstractParameter<?>[] p = {
				objectFiles
		};
		return p;
	}

	@Override
	protected void onParametersChange() {
		cachedObjects = null;
	}

	@Override
	public AbstractDatatype buildModule(Map<String, AbstractDatatype> inputs, ModuleOutputRequest request) {
		DatatypeObjects requestData = (DatatypeObjects) request.data;

		if (objectFiles == null) {
			return null;
		}
		else if (objectFiles.getValue().isEmpty()) {
			return null;
		}

		//Check if the object files are cached, if not read them from file to cache
		if (cachedObjects == null) {
			cachedObjects = new ArrayList<CustomObject>();
			for (File objectFile : objectFiles.getValue()) {
				try {
					cachedObjects.add(CustomObjectFormatRegistry.getFormatForFile(objectFile).readObjectFromFile(objectFile));
				} catch (IOException e) {
					e.printStackTrace();
					cachedObjects = null;
					return null;
				}
			}
		}

		//Clone objects for return value based on seed
		for (LocatedCustomObject locatedObject : requestData.getLocatedObjects()) {
			locatedObject.setObject(cachedObjects.get((int) (locatedObject.getSeed() % cachedObjects.size())));
		}

		return requestData;
	}

	@Override
	public Map<String, ModuleInputRequest> getInputRequests(ModuleOutputRequest outputRequest) {
		HashMap<String, ModuleInputRequest> inputRequests = new HashMap<String, ModuleInputRequest>();
		return inputRequests;
	}

	@Override
	public String getModuleName() {
		return "Object importer";
	}

	@Override
	public IModuleCategory getModuleCategory() {
		return ModuleCategory.GENERATOR;
	}

	@Override
	public ModuleInput[] registerInputs() {
		return null;
	}

	@Override
	public ModuleOutput[] registerOutputs() {
		ModuleOutput[] o = {
				new ModuleOutput(new DatatypeObjects(), "Output")
		};
		return o;
	}

	@Override
	public boolean isBypassable() {
		return false;
	}
}
