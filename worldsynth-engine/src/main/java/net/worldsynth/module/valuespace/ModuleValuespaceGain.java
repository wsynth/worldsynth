/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.module.valuespace;

import java.util.HashMap;
import java.util.Map;

import net.worldsynth.datatype.AbstractDatatype;
import net.worldsynth.datatype.DatatypeMultitype;
import net.worldsynth.datatype.DatatypeScalar;
import net.worldsynth.datatype.DatatypeValuespace;
import net.worldsynth.module.AbstractModule;
import net.worldsynth.module.IModuleCategory;
import net.worldsynth.module.ModuleCategory;
import net.worldsynth.module.ModuleInput;
import net.worldsynth.module.ModuleInputRequest;
import net.worldsynth.module.ModuleOutput;
import net.worldsynth.module.ModuleOutputRequest;
import net.worldsynth.parameter.AbstractParameter;
import net.worldsynth.parameter.DoubleParameter;
import net.worldsynth.parameter.FloatParameter;

public class ModuleValuespaceGain extends AbstractModule {
	
	private DoubleParameter gain = new DoubleParameter("gain", "Gain", null, 1.0, Double.NEGATIVE_INFINITY, Double.POSITIVE_INFINITY, 0.0, 2.0);
	private DoubleParameter offset = new DoubleParameter("offset", "Offset", null, 0.0, Double.NEGATIVE_INFINITY, Double.POSITIVE_INFINITY, -1.0, 1.0);
	private FloatParameter gainCentre = new FloatParameter("gaincentre", "Gain centre", null, 0.0f, Float.NEGATIVE_INFINITY, Float.POSITIVE_INFINITY, -1.0f, 1.0f);
	
	@Override
	public AbstractParameter<?>[] registerParameters() {
		AbstractParameter<?>[] p = {
				gain,
				offset,
				gainCentre
				};
		return p;
	}
	
	@Override
	public AbstractDatatype buildModule(Map<String, AbstractDatatype> inputs, ModuleOutputRequest request) {
		DatatypeValuespace requestData = (DatatypeValuespace) request.data;
		
		int spw = requestData.spacePointsWidth;
		int sph = requestData.spacePointsHeight;
		int spl = requestData.spacePointsLength;
		
		//----------READ INPUTS----------//
		
		double gain = this.gain.getValue();
		double offset = this.offset.getValue();
		float gainCentre = this.gainCentre.getValue();
		
		//Read in input
		if (inputs.get("input") == null) {
			// If the input is null, there is no input and then just return null
			return null;
		}
		float[][][] inputSpace = ((DatatypeValuespace) inputs.get("input")).getValuespace();
		
		//Read in gain
		float[][][] gainSpace = null;
		if (inputs.get("gain") != null) {
			if (inputs.get("gain") instanceof DatatypeScalar) {
				gain = ((DatatypeScalar) inputs.get("gain")).getValue();
			}
			else {
				gainSpace = ((DatatypeValuespace) inputs.get("gain")).getValuespace();
			}
		}
		
		//Read in offset
		float[][][] offsetSpace = null;
		if (inputs.get("offset") != null) {
			if (inputs.get("offset") instanceof DatatypeScalar) {
				offset = ((DatatypeScalar) inputs.get("offset")).getValue();
			}
			else {
				offsetSpace = ((DatatypeValuespace) inputs.get("offset")).getValuespace();
			}
		}
		
		//----------BUILD----------//
		
		float[][][] valuespace = new float[spw][sph][spl];
		
		//Has modulation inputs
		if (gainSpace != null || offsetSpace != null) {
			double localGain = gain;
			double localOffset = offset;
			for (int u = 0; u < spw; u++) {
				for (int v = 0; v < sph; v++) {
					for (int w = 0; w < spl; w++) {
						if (gainSpace != null) localGain = gainSpace[u][v][w] * gain;
						if (offsetSpace != null) localOffset = offsetSpace[u][v][w] + offset;
						
						valuespace[u][v][w] = gain(inputSpace[u][v][w], gainCentre, localGain, localOffset);
					}
				}
			}
		}
		// Has only input space
		else {
			for (int u = 0; u < spw; u++) {
				for (int v = 0; v < sph; v++) {
					for (int w = 0; w < spl; w++) {
						valuespace[u][v][w] = gain(inputSpace[u][v][w], gainCentre, gain, offset);
					}
				}
			}
		}
		
		requestData.setValuespace(valuespace);
		
		return requestData;
	}

	private float gain(float value, float gainCentre, double gain, double offset) {
		value -= gainCentre;
		value *= gain;
		value += gainCentre;
		value += offset;
		return value;
	}

	@Override
	public Map<String, ModuleInputRequest> getInputRequests(ModuleOutputRequest outputRequest) {
		HashMap<String, ModuleInputRequest> inputRequests = new HashMap<String, ModuleInputRequest>();
		
		inputRequests.put("input", new ModuleInputRequest(getInput("Input"), outputRequest.data));
		inputRequests.put("gain", new ModuleInputRequest(getInput("Gain"), new DatatypeMultitype(new DatatypeScalar(), outputRequest.data)));
		inputRequests.put("offset", new ModuleInputRequest(getInput("Offset"), new DatatypeMultitype(new DatatypeScalar(), outputRequest.data)));
		
		return inputRequests;
	}

	@Override
	public String getModuleName() {
		return "Valuespace gain";
	}

	@Override
	public IModuleCategory getModuleCategory() {
		return ModuleCategory.MODIFIER;
	}

	@Override
	public ModuleInput[] registerInputs() {
		ModuleInput[] i = {
				new ModuleInput(new DatatypeValuespace(), "Input"),
				new ModuleInput(new DatatypeMultitype(new DatatypeScalar(), new DatatypeValuespace()), "Gain"),
				new ModuleInput(new DatatypeMultitype(new DatatypeScalar(), new DatatypeValuespace()), "Offset")
				};
		return i;
	}

	@Override
	public ModuleOutput[] registerOutputs() {
		ModuleOutput[] o = {
				new ModuleOutput(new DatatypeValuespace(), "Output")
				};
		return o;
	}

	@Override
	public boolean isBypassable() {
		return true;
	}
}
