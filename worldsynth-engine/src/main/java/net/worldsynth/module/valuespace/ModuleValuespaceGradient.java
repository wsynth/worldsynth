/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.module.valuespace;

import java.util.HashMap;
import java.util.Map;

import net.worldsynth.datatype.AbstractDatatype;
import net.worldsynth.datatype.DatatypeHeightmap;
import net.worldsynth.datatype.DatatypeMultitype;
import net.worldsynth.datatype.DatatypeValuespace;
import net.worldsynth.module.AbstractModule;
import net.worldsynth.module.IModuleCategory;
import net.worldsynth.module.ModuleCategory;
import net.worldsynth.module.ModuleInput;
import net.worldsynth.module.ModuleInputRequest;
import net.worldsynth.module.ModuleOutput;
import net.worldsynth.module.ModuleOutputRequest;
import net.worldsynth.parameter.AbstractParameter;
import net.worldsynth.parameter.DoubleParameter;
import net.worldsynth.parameter.EnumParameter;
import net.worldsynth.util.math.MathHelperScalar;

public class ModuleValuespaceGradient extends AbstractModule {
	
	private DoubleParameter scale = new DoubleParameter("scale", "Scale", null, 100.0, 0.0, Double.MAX_VALUE, 1.0, 100.0);
	private DoubleParameter direction = new DoubleParameter("rotation", "Direction", null, 0.0, -360.0, 360.0, 0.0, 360.0);
	private DoubleParameter tilt = new DoubleParameter("tilt", "Tilt", null, 0.0, -90, 90, 0.0, 90.0);
	private EnumParameter<GradientTiling> tiling = new EnumParameter<GradientTiling>("tiling", "Tiling", null, GradientTiling.class, GradientTiling.NONE);
	private DoubleParameter distortion = new DoubleParameter("distortion", "Distortion", null, 1.0, Double.NEGATIVE_INFINITY, Double.POSITIVE_INFINITY, 0.0, 100.0);
	
	@Override
	public AbstractParameter<?>[] registerParameters() {
		AbstractParameter<?>[] p = {
				scale,
				direction,
				tilt,
				tiling,
				distortion
				};
		return p;
	}
	
	@Override
	public AbstractDatatype buildModule(Map<String, AbstractDatatype> inputs, ModuleOutputRequest request) {
		DatatypeValuespace requestData = (DatatypeValuespace) request.data;
		
		double x = requestData.extent.getX();
		double y = requestData.extent.getY();
		double z = requestData.extent.getZ();
		double res = requestData.resolution;
		int spw = requestData.spacePointsWidth;
		int sph = requestData.spacePointsHeight;
		int spl = requestData.spacePointsLength;
		
		//----------READ INPUTS----------//
		
		double scale = this.scale.getValue();
		double direction = this.direction.getValue();
		double tilt = this.tilt.getValue();
		GradientTiling tiling = this.tiling.getValue();
		double distortion = this.distortion.getValue();
		
		// Read in distortion
		boolean useHeightmap = false;
		float[][] inputMap0 = null;
		boolean useValuespace = false;
		float[][][] inputSpace1 = null;
		if (inputs.get("distortion") != null) {
			if (inputs.get("distortion") instanceof DatatypeValuespace) {
				useValuespace = true;
				inputSpace1 = ((DatatypeValuespace) inputs.get("distortion")).getValuespace();
			}
			else {
				useHeightmap = true;
				inputMap0 = ((DatatypeHeightmap) inputs.get("distortion")).getHeightmap();
			}
		}
		
		//----------BUILD----------//

		float[][][] space = new float[spw][sph][spl];
		for (int u = 0; u < spw; u++) {
			for (int v = 0; v < sph; v++) {
				for (int w = 0; w < spl; w++) {
					float offset = 0;
					if (useHeightmap) {
						offset += inputMap0[u][w];
					}
					if (useValuespace) {
						offset += inputSpace1[u][v][w];
					}
					
					float o = (float) getValueAt(x+u*res, y+v*res, z+w*res, scale, offset*distortion, direction, tilt, tiling);
					space[u][v][w] = o;
				}
			}
		}
		
		requestData.setValuespace(space); 
		
		return requestData;
	}
	
	public double getValueAt(double x, double y, double z, double scale, double offset, double direction, double tilt, GradientTiling tiling) {
		double rr = Math.toRadians(-direction);
		double tr = Math.toRadians(tilt);
		
		//Rotate around y
		if (direction != 0.0) {
			x = x*cos(rr) + 0 + z*sin(rr);
		}
		//Tilt around z
		if (tilt != 0.0) {
			y = x*sin(tr) + y*cos(tr) + 0;
		}
		
		return gradient(y - offset, scale, tiling);
	}
	
	private double cos(double a) {
		return Math.cos(a);
	}
	
	private double sin(double a) {
		return Math.sin(a);
	}
	
	private double gradient(double x, double scale, GradientTiling tiling) {
		 double h = x/scale;
		 
		if (tiling == GradientTiling.NONE) {
			return h;
		}
		else if (tiling == GradientTiling.CLAMPED) {
			h = MathHelperScalar.clamp(h, 0.0, 1.0);
		}
		else if (tiling == GradientTiling.TILING) {
			h -= Math.floor(h);
		}
		else if (tiling == GradientTiling.CONTINOUS) {
			h -= Math.floor(h);
			h *= 2;
			if (h > 1) {
				h = 2 - h;
			}
		}
		
		return h;
	}
	
	@Override
	public Map<String, ModuleInputRequest> getInputRequests(ModuleOutputRequest outputRequest) {
		HashMap<String, ModuleInputRequest> inputRequests = new HashMap<String, ModuleInputRequest>();
		
		DatatypeValuespace ord = (DatatypeValuespace) outputRequest.data;
		DatatypeHeightmap heightmapRequestData = new DatatypeHeightmap(ord.extent, ord.resolution);
		inputRequests.put("distortion", new ModuleInputRequest(getInput("Distortion"), new DatatypeMultitype(new AbstractDatatype[] {ord, heightmapRequestData})));
		
		return inputRequests;
	}

	@Override
	public String getModuleName() {
		return "Gradient";
	}

	@Override
	public IModuleCategory getModuleCategory() {
		return ModuleCategory.GENERATOR;
	}

	@Override
	public ModuleInput[] registerInputs() {
		ModuleInput[] i = {
				new ModuleInput(new DatatypeMultitype(new DatatypeValuespace(), new DatatypeHeightmap()), "Distortion"),
				};
		return i;
	}

	@Override
	public ModuleOutput[] registerOutputs() {
		ModuleOutput[] o = {
				new ModuleOutput(new DatatypeValuespace(), "Output")
				};
		return o;
	}

	@Override
	public boolean isBypassable() {
		return false;
	}

	private enum GradientTiling {
		NONE, CLAMPED, TILING, CONTINOUS;
	}
}
