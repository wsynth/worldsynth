/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.module.valuespace;

import net.worldsynth.datatype.AbstractDatatype;
import net.worldsynth.datatype.DatatypeValuespace;
import net.worldsynth.module.*;
import net.worldsynth.parameter.*;
import net.worldsynth.util.Tuple;
import net.worldsynth.util.gen.Permutation;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

public class ModuleValuespaceWorley extends AbstractModule {
	
	private LongParameter seed = new LongParameter("seed", "Seed", null, 0, Long.MIN_VALUE, Long.MAX_VALUE);
	private DoubleTupleParameter scale = new DoubleTupleParameter("scale", "Scale", null, 0.0, Double.POSITIVE_INFINITY, 100.0, 100.0, 100.0);
	private DoubleParameter amplitude = new DoubleParameter("amplitude", "Amplitude", null, 1.0, 0.0, Double.MAX_VALUE, 0.0, 5.0);
	private DoubleParameter offset = new DoubleParameter("offset", "Offset", null, 0.0, Double.NEGATIVE_INFINITY, Double.POSITIVE_INFINITY, -1.0, 1.0, 256.0);
	private EnumParameter<FeatureDistrubution> featureDistrubution = new EnumParameter<>("distrubution", "Cell type", null, FeatureDistrubution.class, FeatureDistrubution.VORONOI);
	private EnumParameter<DistanceFunction> distanceFunction = new EnumParameter<>("distancefunction", "Distance function", null, DistanceFunction.class, DistanceFunction.EUCLIDEAN);
	private EnumParameter<Feature> feature = new EnumParameter<Feature>("feature", "Feature function", null, Feature.class, Feature.F1);

	private Permutation permutation;
	
	@Override
	public AbstractParameter<?>[] registerParameters() {
		
		seed.setOnChange(newValue -> {
			permutation = new Permutation(newValue, 256, 3);
		});
		seed.setValue(new Random().nextLong());
		
		AbstractParameter<?>[] p = {
				scale,
				amplitude,
				offset,
				distanceFunction,
				feature,
				featureDistrubution,
				seed
				};
		return p;
	}
	
	@Override
	public AbstractDatatype buildModule(Map<String, AbstractDatatype> inputs, ModuleOutputRequest request) {
		DatatypeValuespace requestData = (DatatypeValuespace) request.data;
		
		double x = requestData.extent.getX();
		double y = requestData.extent.getY();
		double z = requestData.extent.getZ();
		double res = requestData.resolution;
		int spw = requestData.spacePointsWidth;
		int sph = requestData.spacePointsHeight;
		int spl = requestData.spacePointsLength;

		Tuple<Double> scale = this.scale.getValue();
		double amplitude = this.amplitude.getValue();
		double offset = this.offset.getValue();
		FeatureDistrubution featureDistrubution = this.featureDistrubution.getValue();
		DistanceFunction distanceFunction = this.distanceFunction.getValue();
		Feature feature = this.feature.getValue();
		
		float[][][] values = new float[spw][sph][spl];
		
		for (int u = 0; u < spw; u++) {
			for (int v = 0; v < sph; v++) {
				for (int w = 0; w < spl; w++) {
					values[u][v][w] = (float) getValueAt(x+u*res, y+v*res, z+w*res, scale, amplitude, offset, featureDistrubution, distanceFunction, feature);
				}
			}
		}
		
		requestData.setValuespace(values);
		
		return requestData;
	}
	
	@Override
	public Map<String, ModuleInputRequest> getInputRequests(ModuleOutputRequest outputRequest) {
		HashMap<String, ModuleInputRequest> inputRequests = new HashMap<>();
		
		return inputRequests;
	}
	
	public double getValueAt(double x, double y, double z, Tuple<Double> scale, double amplitude, double offset, FeatureDistrubution featureDistrubution, DistanceFunction distanceFunction, Feature feature) {
		return worley3d(x/scale.getValue(0), y/scale.getValue(1), z/scale.getValue(2), amplitude, offset, featureDistrubution, distanceFunction, feature);
	}
	
	private double worley3d(double x, double y, double z, double amplitude, double offset, FeatureDistrubution featureDistrubution, DistanceFunction distanceFunction, Feature feature) {
		// Calculate the coordinates for the unit square that the coordinates is inside
		int xi = (int) Math.floor(x);
		int yi = (int) Math.floor(y);
		int zi = (int) Math.floor(z);

		// Calculate the local coordinates inside the unit square
		double xf = x - xi;
		double yf = y - yi;
		double zf = z - zi;

		double dist1 = Double.POSITIVE_INFINITY;
		double dist2 = Double.POSITIVE_INFINITY;
		double dist3 = Double.POSITIVE_INFINITY;
		
		for (int ix = -1; ix <= 1; ix++) {
			for (int iy = -1; iy <= 1; iy++) {
				for (int iz = -1; iz <= 1; iz++) {
					double xoffset = 0.5;
					double yoffset = 0.5;
					double zoffset = 0.5;
					
					if (featureDistrubution == FeatureDistrubution.VORONOI) {
						int cx = xi + ix;
						int cy = yi + iy;
						int cz = zi + iz;

						int[] offsets = permutation.lHashAll(cx & 255, cy & 255, cz & 255);
						xoffset = offsets[0] / 255.0;
						yoffset = offsets[1] / 255.0;
						zoffset = offsets[2] / 255.0;
					}
					
					double xDist = ix + xoffset - xf;
					double yDist = iy + yoffset - yf;
					double zDist = iz + zoffset - zf;
					
					//Distance from point
					double dist = Double.POSITIVE_INFINITY;
					switch (distanceFunction) {
					case EUCLIDEAN:
						dist = Math.sqrt(xDist*xDist + yDist*yDist + zDist*zDist);
						break;
					case MANHATTAN:
						dist = Math.abs(xDist) + Math.abs(yDist) + Math.abs(zDist);
						break;
					case EUCLIDEAN_SQUARED:
						dist = xDist*xDist + yDist*yDist + zDist*zDist;
						break;
					case CHEBYSHEV:
						dist = Math.max(Math.max(Math.abs(xDist), Math.abs(yDist)), Math.abs(zDist));
						break;
					case MIN:
						dist = Math.min(Math.min(Math.abs(xDist), Math.abs(yDist)), Math.abs(zDist));
						break;
					}

					if (dist < dist3) {
						dist3 = dist;

						if (dist3 < dist2) {
							double d = dist2;
							dist2 = dist3;
							dist3 = d;

							if (dist2 < dist1) {
								d = dist1;
								dist1 = dist2;
								dist2 = d;
							}
						}
					}
				}
			}
		}
		
		double height = 0;
		switch (feature) {
		case F1:
			height = dist1;
			break;
		case F2:
			height = dist2;
			break;
		case F3:
			height = dist3;
			break;
		case F2_F1:
			height = dist2 - dist1;
			break;
		case F3_F1:
			height = dist3 - dist1;
			break;
		case F3_F2:
			height = dist3 - dist2;
			break;
		}
		
		height *= amplitude;
		height += offset;
		return height;
	}

	@Override
	public String getModuleName() {
		return "Valuespace worley";
	}

	@Override
	public IModuleCategory getModuleCategory() {
		return ModuleCategory.GENERATOR;
	}

	@Override
	public ModuleInput[] registerInputs() {
		return null;
	}

	@Override
	public ModuleOutput[] registerOutputs() {
		ModuleOutput[] o = {
				new ModuleOutput(new DatatypeValuespace(), "Output")
				};
		return o;
	}

	@Override
	public boolean isBypassable() {
		return false;
	}
	
	private enum DistanceFunction {
		EUCLIDEAN,
		MANHATTAN,
		EUCLIDEAN_SQUARED,
		CHEBYSHEV,
		MIN;
	}
	
	private enum Feature {
		F1,
		F2,
		F3,
		F2_F1,
		F3_F1,
		F3_F2;
	}
	
	private enum FeatureDistrubution {
		VORONOI,
		SQUARE;
	}
}
