/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.module.valuespace;

import net.worldsynth.datatype.AbstractDatatype;
import net.worldsynth.datatype.DatatypeValuespace;
import net.worldsynth.module.*;
import net.worldsynth.parameter.AbstractParameter;

import java.util.HashMap;
import java.util.Map;

public class ModuleValuespaceAbsolute extends AbstractModule {

	@Override
	public AbstractParameter<?>[] registerParameters() {
		AbstractParameter<?>[] p = {
		};
		return p;
	}

	@Override
	public AbstractDatatype buildModule(Map<String, AbstractDatatype> inputs, ModuleOutputRequest request) {
		DatatypeValuespace requestData = (DatatypeValuespace) request.data;
		
		int spw = requestData.spacePointsWidth;
		int sph = requestData.spacePointsHeight;
		int spl = requestData.spacePointsLength;

		//----------READ INPUTS----------//

		//Read in input
		if (inputs.get("input") == null) {
			// If the input is null, there is no input and then just return null
			return null;
		}
		float[][][] inputSpace = ((DatatypeValuespace) inputs.get("input")).getValuespace();

		//----------BUILD----------//

		float[][][] valuespace = new float[spw][sph][spl];

		for (int u = 0; u < spw; u++) {
			for (int v = 0; v < sph; v++) {
				for (int w = 0; w < spl; w++) {
					valuespace[u][v][w] = Math.abs(inputSpace[u][v][w]);
				}
			}
		}

		requestData.setValuespace(valuespace);

		return requestData;
	}

	private float gain(float value, float gainCentre, double gain, double offset) {
		value -= gainCentre;
		value *= gain;
		value += gainCentre;
		value += offset;
		return value;
	}

	@Override
	public Map<String, ModuleInputRequest> getInputRequests(ModuleOutputRequest outputRequest) {
		HashMap<String, ModuleInputRequest> inputRequests = new HashMap<>();

		inputRequests.put("input", new ModuleInputRequest(getInput("Input"), outputRequest.data));

		return inputRequests;
	}

	@Override
	public String getModuleName() {
		return "Absolute";
	}

	@Override
	public IModuleCategory getModuleCategory() {
		return ModuleCategory.MODIFIER;
	}

	@Override
	public ModuleInput[] registerInputs() {
		ModuleInput[] i = {
				new ModuleInput(new DatatypeValuespace(), "Input")
				};
		return i;
	}

	@Override
	public ModuleOutput[] registerOutputs() {
		ModuleOutput[] o = {
				new ModuleOutput(new DatatypeValuespace(), "Output")
				};
		return o;
	}

	@Override
	public boolean isBypassable() {
		return true;
	}
}
