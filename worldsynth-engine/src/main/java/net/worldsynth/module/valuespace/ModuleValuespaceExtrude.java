/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.module.valuespace;

import java.util.HashMap;
import java.util.Map;

import net.worldsynth.datatype.AbstractDatatype;
import net.worldsynth.datatype.DatatypeHeightmap;
import net.worldsynth.datatype.DatatypeValuespace;
import net.worldsynth.extent.Extent;
import net.worldsynth.module.AbstractModule;
import net.worldsynth.module.IModuleCategory;
import net.worldsynth.module.ModuleCategory;
import net.worldsynth.module.ModuleInput;
import net.worldsynth.module.ModuleInputRequest;
import net.worldsynth.module.ModuleOutput;
import net.worldsynth.module.ModuleOutputRequest;
import net.worldsynth.parameter.AbstractParameter;
import net.worldsynth.parameter.BooleanParameter;
import net.worldsynth.parameter.DoubleParameter;
import net.worldsynth.parameter.EnumParameter;
import net.worldsynth.util.math.FastTrigonometry;

public class ModuleValuespaceExtrude extends AbstractModule {
	
	private BooleanParameter normalized = new BooleanParameter("normalized", "Normalized", null, true);
	private DoubleParameter distortion = new DoubleParameter("distortion", "Distortion", null, 0.0, Double.NEGATIVE_INFINITY, Double.POSITIVE_INFINITY, 0.0, 100.0);
	private EnumParameter<DistortionQuality> distortionQuality = new EnumParameter<DistortionQuality>("distortionquality", "Distortion quality", null, DistortionQuality.class, DistortionQuality.LERP);
	private EnumParameter<InputSampling> inputSampling = new EnumParameter<InputSampling>("inputsampling", "Input sampling", null, InputSampling.class, InputSampling.LERP);
	
	
	@Override
	public AbstractParameter<?>[] registerParameters() {
		AbstractParameter<?>[] p = {
				normalized,
				distortion,
				distortionQuality,
				inputSampling
				};
		return p;
	}

	@Override
	public AbstractDatatype buildModule(Map<String, AbstractDatatype> inputs, ModuleOutputRequest request) {
		DatatypeValuespace requestData = (DatatypeValuespace) request.data;
		
		int spw = requestData.spacePointsWidth;
		int sph = requestData.spacePointsHeight;
		int spl = requestData.spacePointsLength;
		
		//----------READ INPUTS----------//
		
		float normalizedHeight = getSynthParameters().getNormalizedHeightmapHeight();
		
		boolean normalized = this.normalized.getValue();
		double distortion = this.distortion.getValue();
		DistortionQuality distortionQuality = this.distortionQuality.getValue();
		InputSampling inputSampling = this.inputSampling.getValue();
		
		//Read in input
		if (inputs.get("input") == null) {
			//If the main input (index 0) is null, there is no input and then just return null
			return null;
		}
		DatatypeHeightmap input = (DatatypeHeightmap) inputs.get("input");
		
		//Read in distortion
		float[][][][] distortionSpace = null;
		if (inputs.get("distortion") != null) {
			float[][][] distortionInput = ((DatatypeValuespace) inputs.get("distortion")).getValuespace();
			distortionSpace = new float[2][spw][sph][spl];

			if (distortionQuality == DistortionQuality.PRECISE) {
				// Precise distortion
				for (int u = 0; u < spw; u++) {
					for (int v = 0; v < sph; v++) {
						for (int w = 0; w < spl; w++) {
							distortionSpace[0][u][v][w] = (float) Math.cos(distortionInput[u][v][w] * Math.PI * 2.0);
							distortionSpace[1][u][v][w] = (float) Math.sin(distortionInput[u][v][w] * Math.PI * 2.0);
						}
					}
				}
			}
			else if (distortionQuality == DistortionQuality.LERP) {
				// Lerped distortion
				for (int u = 0; u < spw; u++) {
					for (int v = 0; v < sph; v++) {
						for (int w = 0; w < spl; w++) {
							distortionSpace[0][u][v][w] = (float) FastTrigonometry.COS.applyLerp(distortionInput[u][v][w] * Math.PI * 2.0);
							distortionSpace[1][u][v][w] = (float) FastTrigonometry.SIN.applyLerp(distortionInput[u][v][w] * Math.PI * 2.0);
						}
					}
				}
			}
			else {
				// Discrete distortion
				for (int u = 0; u < spw; u++) {
					for (int v = 0; v < sph; v++) {
						for (int w = 0; w < spl; w++) {
							distortionSpace[0][u][v][w] = (float) FastTrigonometry.COS.applyDiscrete(distortionInput[u][v][w] * Math.PI * 2.0);
							distortionSpace[1][u][v][w] = (float) FastTrigonometry.SIN.applyDiscrete(distortionInput[u][v][w] * Math.PI * 2.0);
						}
					}
				}
			}
		}
		
		//----------BUILD----------//
		
		float[][][] valuespace = new float[spw][sph][spl];
		
		// Has distortion input
		if (distortionSpace != null && distortion != 0.0) {
			double samplesOffset = Math.ceil(Math.abs(distortion) / requestData.resolution);
			
			for (int u = 0; u < spw; u++) {
				for (int w = 0; w < spl; w++) {
					for (int v = 0; v < sph; v++) {
						double xSampleDistortion = distortionSpace[0][u][v][w]*distortion / requestData.resolution;
						double zSampleDistortion = distortionSpace[1][u][v][w]*distortion / requestData.resolution;
						
						float o = 0.0f;
						if (inputSampling == InputSampling.LERP) {
							o = input.getLocalLerpHeight((u+samplesOffset + xSampleDistortion), (w+samplesOffset + zSampleDistortion));
						}
						else {
							o = input.getLocalHeight((int) (u+samplesOffset + xSampleDistortion), (int) (w+samplesOffset + zSampleDistortion));
						}
						
						if(normalized) {
							o /= normalizedHeight;
						}
						
						valuespace[u][v][w] = o;
					}
				}
			}
		}
		// Has only input to be extruded
		else {
			int samplesOffset = (int) Math.ceil(Math.abs(distortion) / requestData.resolution);
			
			for (int u = 0; u < spw; u++) {
				for (int w = 0; w < spl; w++) {
					
					float o = 0.0f;
					if (distortion == 0.0) {
						o = input.getLocalHeight(u, w);
					}
					else {
						o = input.getLocalHeight(u+samplesOffset, w+samplesOffset);
					}
					
					if(normalized) {
						o /= normalizedHeight;
					}
					
					for (int v = 0; v < sph; v++) {
						valuespace[u][v][w] = o;
					}
				}
			}
		}
		
		requestData.setValuespace(valuespace);
		
		return requestData;
	}

	@Override
	public Map<String, ModuleInputRequest> getInputRequests(ModuleOutputRequest outputRequest) {
		HashMap<String, ModuleInputRequest> inputRequests = new HashMap<String, ModuleInputRequest>();
		
		DatatypeValuespace ord = (DatatypeValuespace) outputRequest.data;
		
		// Expand in whole sample distance lengths
		double distortionExpansion = Math.ceil(Math.abs(this.distortion.getValue()) / ord.resolution) * ord.resolution;
		DatatypeHeightmap heightmapRequestData = new DatatypeHeightmap(Extent.expandedBuildExtent(ord.extent, distortionExpansion, 0.0, distortionExpansion), ord.resolution);
		inputRequests.put("input", new ModuleInputRequest(getInput("Input"), heightmapRequestData));
		
		inputRequests.put("distortion", new ModuleInputRequest(getInput("Distortion"), ord));
		
		return inputRequests;
	}

	@Override
	public String getModuleName() {
		return "Extrude";
	}

	@Override
	public IModuleCategory getModuleCategory() {
		return ModuleCategory.GENERATOR;
	}

	@Override
	public ModuleInput[] registerInputs() {
		ModuleInput[] i = {
				new ModuleInput(new DatatypeHeightmap(), "Input"),
				new ModuleInput(new DatatypeValuespace(), "Distortion")
				};
		return i;
	}

	@Override
	public ModuleOutput[] registerOutputs() {
		ModuleOutput[] o = {
				new ModuleOutput(new DatatypeValuespace(), "Output")
				};
		return o;
	}

	@Override
	public boolean isBypassable() {
		return false;
	}

	private enum DistortionQuality {
		PRECISE, LERP, DISCRETE;
	}
	
	private enum InputSampling {
		LERP, DISCRETE;
	}
}
