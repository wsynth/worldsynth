/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.module.scalar;

import java.util.HashMap;
import java.util.Map;

import net.worldsynth.datatype.AbstractDatatype;
import net.worldsynth.datatype.DatatypeScalar;
import net.worldsynth.module.AbstractModule;
import net.worldsynth.module.IModuleCategory;
import net.worldsynth.module.ModuleCategory;
import net.worldsynth.module.ModuleInput;
import net.worldsynth.module.ModuleInputRequest;
import net.worldsynth.module.ModuleOutput;
import net.worldsynth.module.ModuleOutputRequest;
import net.worldsynth.parameter.AbstractParameter;
import net.worldsynth.parameter.DoubleParameter;

public class ModuleScalarClamp extends AbstractModule {
	
	private DoubleParameter highClamp = new DoubleParameter("highclamp", "High clamp", null, 1.0, Double.NEGATIVE_INFINITY, Double.POSITIVE_INFINITY, 0.0, 1.0);
	private DoubleParameter lowClamp = new DoubleParameter("lowclamp", "Low clamp", null, 0.0, Double.NEGATIVE_INFINITY, Double.POSITIVE_INFINITY, 0.0, 1.0);
	
	@Override
	public AbstractParameter<?>[] registerParameters() {
		AbstractParameter<?>[] p = {
				highClamp,
				lowClamp
				};
		return p;
	}

	@Override
	public AbstractDatatype buildModule(Map<String, AbstractDatatype> inputs, ModuleOutputRequest request) {
		DatatypeScalar requestData = (DatatypeScalar) request.data;
		
		//----------READ INPUTS----------//
		
		if (inputs.get("input") == null) {
			//If the main input is null, there is not enough input and then just return null
			return null;
		}
		double input = ((DatatypeScalar) inputs.get("input")).getValue();
		
		double highClamp = this.highClamp.getValue();
		if (inputs.get("high") != null) {
			highClamp = ((DatatypeScalar) inputs.get("high")).getValue();
		}
		
		double lowClamp = this.lowClamp.getValue();
		if (inputs.get("low") != null) {
			lowClamp = ((DatatypeScalar) inputs.get("low")).getValue();
		}
		
		//----------BUILD----------//
		
		double value = clamp(input, lowClamp, highClamp);
		
		requestData.setValue(value);
		
		return requestData;
	}
	
	private double clamp(double height, double lowClamp, double highClamp) {
		if (height > highClamp) height = highClamp;
		else if (height < lowClamp) height = lowClamp;
		return height;
	}

	@Override
	public Map<String, ModuleInputRequest> getInputRequests(ModuleOutputRequest outputRequest) {
		HashMap<String, ModuleInputRequest> inputRequests = new HashMap<String, ModuleInputRequest>();
		
		inputRequests.put("input", new ModuleInputRequest(getInput(0), outputRequest.data));
		inputRequests.put("high", new ModuleInputRequest(getInput(1), outputRequest.data));
		inputRequests.put("low", new ModuleInputRequest(getInput(2), outputRequest.data));
		
		return inputRequests;
	}

	@Override
	public String getModuleName() {
		return "Scalar clamp";
	}

	@Override
	public IModuleCategory getModuleCategory() {
		return ModuleCategory.MODIFIER;
	}

	@Override
	public ModuleInput[] registerInputs() {
		ModuleInput[] i = {
				new ModuleInput(new DatatypeScalar(), "Input"),
				new ModuleInput(new DatatypeScalar(), "High"),
				new ModuleInput(new DatatypeScalar(), "Low")
				};
		return i;
	}

	@Override
	public ModuleOutput[] registerOutputs() {
		ModuleOutput[] o = {
				new ModuleOutput(new DatatypeScalar(), "Output")
				};
		return o;
	}

	@Override
	public boolean isBypassable() {
		return true;
	}
}
