/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.module.scalar;

import java.util.HashMap;
import java.util.Map;

import net.worldsynth.datatype.AbstractDatatype;
import net.worldsynth.datatype.DatatypeScalar;
import net.worldsynth.module.AbstractModule;
import net.worldsynth.module.IModuleCategory;
import net.worldsynth.module.ModuleCategory;
import net.worldsynth.module.ModuleInput;
import net.worldsynth.module.ModuleInputRequest;
import net.worldsynth.module.ModuleOutput;
import net.worldsynth.module.ModuleOutputRequest;
import net.worldsynth.parameter.AbstractParameter;
import net.worldsynth.parameter.DoubleParameter;

public class ModuleScalarConstat extends AbstractModule {
	
	private DoubleParameter constant = new DoubleParameter("constant", "Constant", null, 0.0, Double.NEGATIVE_INFINITY, Double.POSITIVE_INFINITY, 0.0, 1.0);
	
	@Override
	public AbstractParameter<?>[] registerParameters() {
		AbstractParameter<?>[] p = {
				constant
				};
		return p;
	}
	
	@Override
	public AbstractDatatype buildModule(Map<String, AbstractDatatype> inputs, ModuleOutputRequest request) {
		DatatypeScalar requestData = (DatatypeScalar) request.data;
		
		requestData.setValue(constant.getValue());
		
		return requestData;
	}
	
	@Override
	public Map<String, ModuleInputRequest> getInputRequests(ModuleOutputRequest outputRequest) {
		HashMap<String, ModuleInputRequest> inputRequests = new HashMap<String, ModuleInputRequest>();
		return inputRequests;
	}

	@Override
	public ModuleInput[] registerInputs() {
		return null;
	}

	@Override
	public ModuleOutput[] registerOutputs() {
		ModuleOutput[] o = {
				new ModuleOutput(new DatatypeScalar(), "Output")
				};
		return o;
	}

	@Override
	public String getModuleName() {
		return "Scalar constant";
	}

	@Override
	public IModuleCategory getModuleCategory() {
		return ModuleCategory.GENERATOR;
	}

	@Override
	public boolean isBypassable() {
		return false;
	}
}
