/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.module.featurespace;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import net.worldsynth.datatype.AbstractDatatype;
import net.worldsynth.datatype.DatatypeBlockspace;
import net.worldsynth.datatype.DatatypeFeaturespace;
import net.worldsynth.datatype.DatatypeValuespace;
import net.worldsynth.extent.Extent;
import net.worldsynth.featurepoint.Featurepoint3D;
import net.worldsynth.module.AbstractModule;
import net.worldsynth.module.IModuleCategory;
import net.worldsynth.module.ModuleCategory;
import net.worldsynth.module.ModuleInput;
import net.worldsynth.module.ModuleInputRequest;
import net.worldsynth.module.ModuleOutput;
import net.worldsynth.module.ModuleOutputRequest;
import net.worldsynth.parameter.AbstractParameter;
import net.worldsynth.parameter.DoubleParameter;
import net.worldsynth.parameter.LongParameter;
import net.worldsynth.util.gen.Permutation;

public class ModuleFeaturespaceDistributeOnBlocks extends AbstractModule {
	
	private LongParameter seed = new LongParameter("seed", "Seed", null, 0, Long.MIN_VALUE, Long.MAX_VALUE);
	private DoubleParameter scale = new DoubleParameter("scale", "Scale", null, 10.0, 1.0, Double.MAX_VALUE, 1.0, 100.0);
	private DoubleParameter probability = new DoubleParameter("probability", "Probability", null, 0.2, 0.0, 1.0);
	
	private final int permutationSize = 1024;
	private final int repeat = permutationSize;
	private Permutation permutation;
	
	@Override
	public AbstractParameter<?>[] registerParameters() {
		
		seed.setOnChange(newValue -> {
			permutation = new Permutation(newValue, permutationSize, 2);
		});
		seed.setValue(new Random().nextLong());
		
		AbstractParameter<?>[] p = {
				scale,
				probability,
				seed
				};
		return p;
	}
	
	@Override
	public Map<String, ModuleInputRequest> getInputRequests(ModuleOutputRequest outputRequest) {
		HashMap<String, ModuleInputRequest> inputRequests = new HashMap<String, ModuleInputRequest>();
		
		double scale = this.scale.getValue();
		
		DatatypeFeaturespace ord = (DatatypeFeaturespace) outputRequest.data;
		double x = ord.extent.getX();
		double y = ord.extent.getY();
		double z = ord.extent.getZ();
		double width = ord.extent.getWidth();
		double height = ord.extent.getHeight();
		double length = ord.extent.getLength();
		
		DatatypeValuespace valuespaceRequestData = new DatatypeValuespace(ord.extent, ord.resolution);
		inputRequests.put("probabilityspace", new ModuleInputRequest(getInput("Probability"), valuespaceRequestData));
		
		double xNegExp = Math.floor(Math.floor(x           / scale) * scale);
		double xPosExp = Math.ceil(Math.ceil((x + width)  / scale) * scale);
		double xWidExp = xPosExp - xNegExp;
		
		double yNegExp = Math.floor(Math.floor(y           / scale) * scale);
		double yPosExp = Math.ceil(Math.ceil((y + height) / scale) * scale);
		double yHeiExp = yPosExp - yNegExp;
		
		double zNegExp = Math.floor(Math.floor(z           / scale) * scale);
		double zPosExp = Math.ceil(Math.ceil((z + length) / scale) * scale);
		double zLenExp = zPosExp - zNegExp;
		
		DatatypeBlockspace blockspaceRequestData = new DatatypeBlockspace(new Extent(xNegExp, yNegExp, zNegExp, xWidExp, yHeiExp, zLenExp), ord.resolution);
		inputRequests.put("blockmask", new ModuleInputRequest(getInput("Block mask"), blockspaceRequestData));
		
		return inputRequests;
	}
	
	@Override
	public AbstractDatatype buildModule(Map<String, AbstractDatatype> inputs, ModuleOutputRequest request) {
		DatatypeFeaturespace requestData = (DatatypeFeaturespace) request.data;
		
		double x = requestData.extent.getX();
		double y = requestData.extent.getY();
		double z = requestData.extent.getZ();
		double width = requestData.extent.getWidth();
		double height = requestData.extent.getHeight();
		double length = requestData.extent.getLength();
		double resolution = requestData.resolution;
		
		//----------READ INPUTS----------//
		
		double scale = this.scale.getValue();
		double probability = this.probability.getValue();
		
		if (inputs.get("blockmask") == null) {
			return null;
		}
		DatatypeBlockspace blockMask = (DatatypeBlockspace) inputs.get("blockmask");
		
		DatatypeValuespace probabilitySpace = null;
		if (inputs.get("probabilityspace") != null) {
			probabilitySpace = (DatatypeValuespace) inputs.get("probabilityspace");
		}
		
		//----------BUILD----------//
		
		int minXPoint = (int) Math.floor(x/scale);
		int maxXPoint = (int) Math.ceil((x+width)/scale);
		int minYPoint = (int) Math.floor(y/scale);
		int maxYPoint = (int) Math.ceil((y+height)/scale);
		int minZPoint = (int) Math.floor(z/scale);
		int maxZPoint = (int) Math.ceil((z+length)/scale);
		
		int samples = (int) Math.ceil(scale / resolution);
		
		ArrayList<Featurepoint3D> generatedPoints = new ArrayList<Featurepoint3D>();
		
		for (int u = minXPoint; u < maxXPoint; u++) {
			for (int v = minYPoint; v < maxYPoint; v++) {
				for (int w = minZPoint; w < maxZPoint; w++) {
					if (probabilitySpace == null && permutation.gUnitHash(1, u, v, w) >= probability) {
						continue;
					}
					int seed = permutation.gHash(0, u, v, w);
					Featurepoint3D f = placePoint(u, v, w, seed, scale, samples, blockMask);
					if (f != null && requestData.isGlobalContained(f.getX(), f.getY(), f.getZ())) {
						if (probabilitySpace != null) {
							probability = probabilitySpace.getGlobalLerpValue(f.getX(), f.getY(), f.getZ());
						}
						if (permutation.gUnitHash(1, u, v, w) < probability) {
							generatedPoints.add(f);
						}
					}
				}
			}
		}
		
		Featurepoint3D[] points = new Featurepoint3D[generatedPoints.size()];
		generatedPoints.toArray(points);
		requestData.setFeaturepoints(points);
		
		return requestData;
	}
	
	private Featurepoint3D placePoint(int u, int v, int w, int seed, double scale, int samples, DatatypeBlockspace blockMask) {
		double samplePitch = scale / samples;
		double sampleOffset = samplePitch / 2;
		
		int validPlaces = 0;
		for (int uu = 0; uu < samples; uu++) {
			for (int vv = 0; vv < samples; vv++) {
				for (int ww = 0; ww < samples; ww++) {
					double gx = u * scale + uu * samplePitch + sampleOffset;
					double gy = v * scale + vv * samplePitch + sampleOffset;
					double gz = w * scale + ww * samplePitch + sampleOffset;
					if (!blockMask.getGlobalMaterial(gx, gy, gz).isAir()) {
						validPlaces++;
					}
				}
			}
		}
		if (validPlaces == 0) {
			return null;
		}
		
		int placementIndex = (int) (((double) seed / (double) repeat) * (double) validPlaces);
		for (int uu = 0; uu < samples; uu++) {
			for (int vv = 0; vv < samples; vv++) {
				for (int ww = 0; ww < samples; ww++) {
					double gx = u * scale + uu * samplePitch + sampleOffset;
					double gy = v * scale + vv * samplePitch + sampleOffset;
					double gz = w * scale + ww * samplePitch + sampleOffset;
					if (!blockMask.getGlobalMaterial(gx, gy, gz).isAir()) {
						if (placementIndex == 0) {
							return new Featurepoint3D(gx, gy, gz, seed);
						}
						placementIndex--;
					}
				}
			}
		}
		
		return null;
	}
	
	@Override
	public String getModuleName() {
		return "Distribute on blocks";
	}

	@Override
	public IModuleCategory getModuleCategory() {
		return ModuleCategory.GENERATOR;
	}

	@Override
	public ModuleInput[] registerInputs() {
		ModuleInput[] i = {
				new ModuleInput(new DatatypeBlockspace(), "Block mask"),
				new ModuleInput(new DatatypeValuespace(), "Probability")
				};
		return i;
	}

	@Override
	public ModuleOutput[] registerOutputs() {
		ModuleOutput[] o = {
				new ModuleOutput(new DatatypeFeaturespace(), "Output")
				};
		return o;
	}

	@Override
	public boolean isBypassable() {
		return false;
	}
}
