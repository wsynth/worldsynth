/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.module.fileio;

import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.layout.GridPane;
import javafx.stage.FileChooser.ExtensionFilter;
import net.worldsynth.WorldSynth;
import net.worldsynth.build.Build;
import net.worldsynth.datatype.AbstractDatatype;
import net.worldsynth.datatype.DatatypeHeightmap;
import net.worldsynth.extent.Extent;
import net.worldsynth.extent.WorldExtent;
import net.worldsynth.module.*;
import net.worldsynth.parameter.AbstractParameter;
import net.worldsynth.parameter.FileParameter;
import net.worldsynth.parameter.WorldExtentParameter;
import net.worldsynth.standalone.ui.event.ModuleApplyParametersEvent;
import net.worldsynth.standalone.ui.parameters.ParameterUiElement;
import net.worldsynth.util.math.MathHelperScalar;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class ModuleHeightmapExporter extends AbstractModule {

	private static final Logger logger = LogManager.getLogger(ModuleHeightmapExporter.class);

	private WorldExtentParameter exportExtent = new WorldExtentParameter("extent", "Export extent", null, this);
	private FileParameter exportFile = new FileParameter("file", "Save file", null, new File(System.getProperty("user.home") + "/heightmap_export.png"), true, false, new ExtensionFilter("PNG", "*.png"));
	
	@Override
	public AbstractParameter<?>[] registerParameters() {
		AbstractParameter<?>[] p = {
				exportExtent,
				exportFile
				};
		return p;
	}
	
	@Override
	public AbstractDatatype buildModule(Map<String, AbstractDatatype> inputs, ModuleOutputRequest request) {
		return inputs.get("heightmap");
	}
	
	@Override
	public Map<String, ModuleInputRequest> getInputRequests(ModuleOutputRequest outputRequest) {
		HashMap<String, ModuleInputRequest> inputRequests = new HashMap<String, ModuleInputRequest>();
		
		DatatypeHeightmap heightmapRequestData = (DatatypeHeightmap) outputRequest.data;
		
		inputRequests.put("heightmap", new ModuleInputRequest(getInput(0), heightmapRequestData));
		
		return inputRequests;
	}

	@Override
	public String getModuleName() {
		return "Heightmap exporter";
	}

	@Override
	public IModuleCategory getModuleCategory() {
		return ModuleCategory.EXPORTER;
	}

	@Override
	public ModuleInput[] registerInputs() {
		ModuleInput[] i = {
				new ModuleInput(new DatatypeHeightmap(), "Heightmap")
				};
		return i;
	}

	@Override
	public ModuleOutput[] registerOutputs() {
		ModuleOutput[] o = {
				new ModuleOutput(new DatatypeHeightmap(), "", false) // Throughput
				};
		return o;
	}

	@Override
	public boolean isBypassable() {
		return false;
	}

	@Override
	public EventHandler<ModuleApplyParametersEvent> moduleUI(GridPane pane) {
		ParameterUiElement<WorldExtent> parameterExtent = this.exportExtent.getUi();
		ParameterUiElement<File> parameterFile = this.exportFile.getUi();
		
		Button exportButton = new Button("Export heightmap");
		exportButton.setOnAction(e -> {
			if (parameterExtent.getValue() == null) {
				return;
			}
			
			float[][] heightmap = getHeightmap(parameterExtent.getValue().asExtent());
			
			float normalizedHeight = getSynthParameters().getNormalizedHeightmapHeight();
			writeImageToFile(heightmap, parameterFile.getValue(), normalizedHeight);
		});
		
		parameterExtent.addToGrid(pane, 0);
		parameterFile.addToGrid(pane, 1);
		pane.add(exportButton, 1, 2);
		
		EventHandler<ModuleApplyParametersEvent> applyHandler = e -> {
			parameterExtent.applyUiValue();
			parameterFile.applyUiValue();
		};
		
		return applyHandler;
	}
	
	private float[][] getHeightmap(Extent extent) {
		ModuleInputRequest request = new ModuleInputRequest(getInput(0), new DatatypeHeightmap(extent, 1.0));
		Build build = new Build(WorldSynth.getBuildCache(), e -> {
			logger.info(e.getStatus().toString());
		});
		DatatypeHeightmap hmd = (DatatypeHeightmap) buildInputData(request, build);
		return hmd.getHeightmap();
	}
	
	private void writeImageToFile(float[][] heightmap, File f, float normalizedHeight) {
		try {
			BufferedImage image = new BufferedImage(heightmap.length, heightmap[0].length, BufferedImage.TYPE_USHORT_GRAY);

			int lastPercentage = 0;
			for (int x = 0; x < heightmap.length; x++) {
				for (int y = 0; y < heightmap[0].length; y++) {
					int pixelValue = (int) (MathHelperScalar.clamp(heightmap[x][y]/normalizedHeight, 0.0f, 1.0f) * 65535.0f);
					image.getRaster().setPixel(x, y, new int[] {pixelValue});
				}

				int percentage = 100 * (x+1) / heightmap.length;
				if (percentage != lastPercentage) {
					logger.info("Converting to buffer: " + percentage + "%");
				}
				lastPercentage = percentage;
			}
			ImageIO.write(image, "png", f);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
