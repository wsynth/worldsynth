/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.module;

import net.worldsynth.addon.AddonLoader;
import net.worldsynth.module.biomemap.ModuleBiomemapConstant;
import net.worldsynth.module.biomemap.ModuleBiomemapParameterVoronoi;
import net.worldsynth.module.biomemap.ModuleBiomemapSelector;
import net.worldsynth.module.biomemap.ModuleBiomemapTranslate;
import net.worldsynth.module.biomespace.ModuleBiomespaceConstant;
import net.worldsynth.module.biomespace.ModuleBiomespaceExtrude;
import net.worldsynth.module.biomespace.ModuleBiomespaceSelector;
import net.worldsynth.module.biomespace.ModuleBiomespaceTranslate;
import net.worldsynth.module.blockspace.*;
import net.worldsynth.module.colormap.*;
import net.worldsynth.module.composition.ModuleCompositionFeaturemapLayer;
import net.worldsynth.module.composition.ModuleCompositionHeightmapLayer;
import net.worldsynth.module.composition.ModuleCompositionMaterialmapLayer;
import net.worldsynth.module.featuremap.ModuleFeaturemapSimpleDistribution;
import net.worldsynth.module.featurespace.ModuleFeaturespaceDistributeOnBlocks;
import net.worldsynth.module.featurespace.ModuleFeaturespaceSimpleDistribution;
import net.worldsynth.module.fileio.ModuleColormapExporter;
import net.worldsynth.module.fileio.ModuleColormapImporter;
import net.worldsynth.module.fileio.ModuleHeightmapExporter;
import net.worldsynth.module.fileio.ModuleHeightmapImporter;
import net.worldsynth.module.heightmap.*;
import net.worldsynth.module.heightmapoverlay.ModuleOverlayGenerator;
import net.worldsynth.module.materialmap.*;
import net.worldsynth.module.objects.*;
import net.worldsynth.module.scalar.ModuleScalarClamp;
import net.worldsynth.module.scalar.ModuleScalarCombiner;
import net.worldsynth.module.scalar.ModuleScalarConstat;
import net.worldsynth.module.valuespace.*;
import net.worldsynth.module.vectormap.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class NativeModuleRegister extends AbstractModuleRegister {
	private static final Logger logger = LogManager.getLogger(NativeModuleRegister.class);
	
	public NativeModuleRegister(AddonLoader addonLoader) {
		super();
		
		try {
			//Scalars
			registerModule(ModuleScalarConstat.class, "\\Scalar");
			
			registerModule(ModuleScalarClamp.class, "\\Scalar");
			
			registerModule(ModuleScalarCombiner.class, "\\Scalar");
			
			
			//Heightmap
			registerModule(ModuleHeightmapConstant.class, "\\Heightmap\\Generator");
			registerModule(ModuleHeightmapGradient.class, "\\Heightmap\\Generator");
			registerModule(ModuleHeightmapSimplePerlin.class, "\\Heightmap\\Generator");
			registerModule(ModuleHeightmapFractalPerlin.class, "\\Heightmap\\Generator");
			registerModule(ModuleHeightmapValueNoise.class, "\\Heightmap\\Generator");
			registerModule(ModuleHeightmapWorley.class, "\\Heightmap\\Generator");
			registerModule(ModuleHeightmapWorleyConical.class,  "\\Heightmap\\Generator");
			registerModule(ModuleHeightmapCells.class, "\\Heightmap\\Generator");
			registerModule(ModuleHeightmapFromBlockspace.class, "\\Heightmap\\Generator");
			registerModule(ModuleHeightmapExtentGradient.class, "\\Heightmap\\Generator");
			
			registerModule(ModuleHeightmapGain.class, "\\Heightmap\\Modifier");
			registerModule(ModuleHeightmapTerrace.class, "\\Heightmap\\Modifier");
			registerModule(ModuleHeightmapCellTerrace.class, "\\Heightmap\\Modifier");
			registerModule(ModuleHeightmapInverter.class, "\\Heightmap\\Modifier");
			registerModule(ModuleHeightmapRamp.class, "\\Heightmap\\Modifier");
			registerModule(ModuleHeightmapSmoothen.class, "\\Heightmap\\Modifier");
			registerModule(ModuleHeightmapCellularize.class, "\\Heightmap\\Modifier");
			registerModule(ModuleHeightmapDistort.class, "\\Heightmap\\Modifier");
			registerModule(ModuleHeightmapClamp.class, "\\Heightmap\\Modifier");
			registerModule(ModuleHeightmapRescale.class, "\\Heightmap\\Modifier");
			registerModule(ModuleHeightmapMacSearlasPseudoErosion.class, "\\Heightmap\\Modifier");
			registerModule(ModuleHeightmapExpander.class, "\\Heightmap\\Modifier");
			registerModule(ModuleHeightmapSlope.class, "\\Heightmap\\Modifier");
			registerModule(ModuleHeightmapConvexity.class, "\\Heightmap\\Modifier");
			registerModule(ModuleHeightmapTranslate.class, "\\Heightmap\\Modifier");
			registerModule(ModuleHeightmapAffineTransform.class, "\\Heightmap\\Modifier");
			registerModule(ModuleHeightmapResample.class, "\\Heightmap\\Modifier");
			registerModule(ModuleHeightmapDither.class, "\\Heightmap\\Modifier");
			
			registerModule(ModuleHeightmapHeightSelector.class, "\\Heightmap\\Selector");
			registerModule(ModuleHeightmapSlopeSelector.class, "\\Heightmap\\Selector");
			registerModule(ModuleHeightmapConvexitySelector.class, "\\Heightmap\\Selector");
			
			registerModule(ModuleHeightmapExponentiation.class, "\\Heightmap\\Math");
			registerModule(ModuleHeightmapTrigonometry.class, "\\Heightmap\\Math");
			registerModule(ModuleHeightmapAbsolute.class, "\\Heightmap\\Math");
			registerModule(ModuleHeightmapSignum.class, "\\Heightmap\\Math");
			
			registerModule(ModuleHeightmapCombiner.class, "\\Heightmap");
			registerModule(ModuleHeightmapSelector.class, "\\Heightmap");
			registerModule(ModuleHeightmapSmoothMinMax.class, "\\Heightmap");
			
			registerModule(ModuleHeightmapScatter.class, "\\Heightmap");
			
			registerModule(ModuleHeightmapExporter.class, "\\Heightmap");
			registerModule(ModuleHeightmapImporter.class, "\\Heightmap");
			
			
			//Biomemap
			registerModule(ModuleBiomemapConstant.class, "\\Biomemap\\Generator");
			registerModule(ModuleBiomemapParameterVoronoi.class, "\\Biomemap\\Generator");
			
			registerModule(ModuleBiomemapTranslate.class, "\\Biomemap\\Modifier");
			
			registerModule(ModuleBiomemapSelector.class, "\\Biomemap");
			
			
			//Biomespace
			registerModule(ModuleBiomespaceConstant.class, "\\Biomespace\\Generator");
			registerModule(ModuleBiomespaceExtrude.class, "\\Biomespace\\Generator");

			registerModule(ModuleBiomespaceTranslate.class, "\\Biomespace\\Modifier");
			
			registerModule(ModuleBiomespaceSelector.class, "\\Biomespace");
			
			
			//Valuespace
			registerModule(ModuleValuespaceConstant.class, "\\Valuespace\\Generator");
			registerModule(ModuleValuespaceSimplePerlin.class, "\\Valuespace\\Generator");
			registerModule(ModuleValuespaceWorley.class, "\\Valuespace\\Generator");
			registerModule(ModuleValuespacePointDistance.class, "\\Valuespace\\Generator");
			registerModule(ModuleValuespaceGradient.class, "\\Valuespace\\Generator");
			registerModule(ModuleValuespaceExtrude.class, "\\Valuespace\\Generator");
			registerModule(ModuleValuespaceTransition.class, "\\Valuespace\\Generator");
			
			registerModule(ModuleValuespaceGain.class, "\\Valuespace\\Modifier");
			registerModule(ModuleValuespaceCellularize.class, "\\Valuespace\\Modifier");
			registerModule(ModuleValuespaceDistort.class, "\\Valuespace\\Modifier");
			registerModule(ModuleValuespaceTranslate.class, "\\Valuespace\\Modifier");
			registerModule(ModuleValuespaceResample.class, "\\Valuespace\\Modifier");
			registerModule(ModuleValuespaceRescale.class, "\\Valuespace\\Modifier");

			registerModule(ModuleValuespaceAbsolute.class, "\\Valuespace\\Math");
			
			registerModule(ModuleValuespaceCombiner.class, "\\Valuespace");
			registerModule(ModuleValuespaceSmoothMinMax.class, "\\Valuespace");
			
			
			//Blockspace
			registerModule(ModuleBlockspaceFromValuespace.class, "\\Blockspace\\Generator");
			registerModule(ModuleBlockspaceStrataFromValuespace.class, "\\Blockspace\\Generator");
			registerModule(ModuleBlockspaceFromHeightmap.class, "\\Blockspace\\Generator");
			registerModule(ModuleBlockspacePerlinWorm.class, "\\Blockspace\\Generator");

			registerModule(ModuleBlockspaceSurfaceCover.class, "\\Blockspace\\Modifier");
			registerModule(ModuleBlockspaceFilter.class, "\\Blockspace\\Modifier");
			registerModule(ModuleBlockspaceIsolateSurface.class, "\\Blockspace\\Modifier");
			registerModule(ModuleBlockspaceCellularize.class, "\\Blockspace\\Modifier");
			registerModule(ModuleBlockspaceLayeredCellularize.class, "\\Blockspace\\Modifier");
			registerModule(ModuleBlockspaceDistort.class, "\\Blockspace\\Modifier");
			registerModule(ModuleBlockspaceHeightClamp.class, "\\Blockspace\\Modifier");
			registerModule(ModuleBlockspaceAffineTransform.class, "\\Blockspace\\Modifier");
			registerModule(ModuleBlockspaceTranslate.class, "\\Blockspace\\Modifier");
			
			registerModule(ModuleBlockspaceCombiner.class, "\\Blockspace");
			
			
			//Colormap
			registerModule(ModuleColormapConstant.class, "\\Colormap\\Generator");
			registerModule(ModuleColormapRGB.class, "\\Colormap\\Generator");
			registerModule(ModuleColormapHSV.class, "\\Colormap\\Generator");
			registerModule(ModuleColormapHSL.class, "\\Colormap\\Generator");
			registerModule(ModuleColormapGradient.class, "\\Colormap\\Generator");
			registerModule(ModuleColormapFromMaterialmap.class, "\\Colormap\\Generator");
			
			registerModule(ModuleColormapTranslate.class, "\\Colormap\\Modifier");
			
			registerModule(ModuleColormapCombiner.class, "\\Colormap");
			registerModule(ModuleColormapSelector.class, "\\Colormap");
			
			registerModule(ModuleColormapExporter.class, "\\Colormap");
			registerModule(ModuleColormapImporter.class, "\\Colormap");
			
			
			//Vectorfield
			registerModule(ModuleVectorfieldConstant.class, "\\Vectormap\\Generator");
			registerModule(ModuleVectorfieldHeightmapComponents.class, "\\Vectormap\\Generator");
			registerModule(ModuleVectorfieldHeightmapGradient.class, "\\Vectormap\\Generator");
			
			registerModule(ModuleVectorfieldNormalize.class, "\\Vectormap\\Modifier");
			registerModule(ModuleVectorfieldRotate.class, "\\Vectormap\\Modifier");
			registerModule(ModuleVectorfieldTranslate.class, "\\Vectormap\\Modifier");
			
			registerModule(ModuleVectorfieldDotProduct.class, "\\Vectormap\\Other");
			registerModule(ModuleVectorfieldAbs.class, "\\Vectormap\\Other");
			registerModule(ModuleVectorfieldAngle.class, "\\Vectormap\\Other");
			
			registerModule(ModuleVectorfieldCombiner.class, "\\Vectormap");
			
			
			//Heightmap overlay
			registerModule(ModuleOverlayGenerator.class, "\\Overlay");
			
			
			//Materialmap
			registerModule(ModuleMaterialmapConstant.class, "\\Materialmap\\Generator");
			registerModule(ModuleMaterialmapFromColormap.class, "\\Materialmap\\Generator");
			registerModule(ModuleMaterialmapFromBiomemap.class, "\\Materialmap\\Generator");

			registerModule(ModuleMaterialmapTranslate.class, "\\Materialmap\\Modifier");
			
			registerModule(ModuleMaterialmapSelector.class, "\\Materialmap");
			
			
			//Featuremap
			registerModule(ModuleFeaturemapSimpleDistribution.class, "\\Featuremap\\Generator");
			
			
			//Featurespace
			registerModule(ModuleFeaturespaceSimpleDistribution.class, "\\Featurespace\\Generator");
			registerModule(ModuleFeaturespaceDistributeOnBlocks.class, "\\Featurespace\\Generator");
			
			
			//Objects
			registerModule(ModuleObjectsCenter.class, "\\Objects\\Modifier");
			registerModule(ModuleObjectsMaterialReplace.class, "\\Objects\\Modifier");
			registerModule(ModuleObjectsOffset.class, "\\Objects\\Modifier");
			
			registerModule(ModuleObjectsPlacer.class, "\\Objects");
			registerModule(ModuleObjectsImporter.class, "\\Objects");
			registerModule(ModuleObjectsMixer.class, "\\Objects");
			registerModule(ModuleObjectsObjectFromBlockspace.class, "\\Objects");


			//Composition
			registerModule(ModuleCompositionHeightmapLayer.class, "\\Composition");
			registerModule(ModuleCompositionMaterialmapLayer.class, "\\Composition");
			registerModule(ModuleCompositionFeaturemapLayer.class, "\\Composition");


			//Load from addons
			logger.info("Loading modules from addons");
			for (AbstractModuleRegister moduleRegister: addonLoader.getAddonModuleRegisters()) {
				loadModuleRegister(moduleRegister);
			}
			
		} catch (ClassNotModuleExeption e) {
			throw new RuntimeException(e);
		}
	}
	
	private void loadModuleRegister(AbstractModuleRegister moduleRegister) {
		for (ModuleEntry moduleEntry: moduleRegister.getRegisteredModuleEntries()) {
			try {
				registerModule(moduleEntry);
			} catch (ClassNotModuleExeption e) {
				e.printStackTrace();
			}
		}
	}
}
