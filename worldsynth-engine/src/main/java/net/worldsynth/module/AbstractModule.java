/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.module;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import net.worldsynth.build.Build;
import net.worldsynth.datatype.AbstractDatatype;
import net.worldsynth.event.EventDispatcher;
import net.worldsynth.event.EventHandler;
import net.worldsynth.event.EventType;
import net.worldsynth.extent.WorldExtentManager;
import net.worldsynth.parameter.AbstractParameter;
import net.worldsynth.parameter.event.ParameterEvent;
import net.worldsynth.patch.ModuleWrapper;
import net.worldsynth.patch.event.ModuleEvent;
import net.worldsynth.standalone.ui.event.ModuleApplyParametersEvent;
import net.worldsynth.standalone.ui.event.ModuleParametersChangeEvent;
import net.worldsynth.standalone.ui.parameters.ParameterUiElement;
import net.worldsynth.synth.SynthParameters;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * This is the abstract class for a module. To register a module, it has to be an extension of this class.
 * <br><br>
 * AbstractModule has no defined constructor, and derived modules extending AbstractModule preferably doesn't have
 * constructors. Registering IO is done during the automatic {@link #init(ModuleWrapper) init} call after construction,
 * which calls the {@link #registerInputs() registerInputs()} and {@link #registerOutputs() registerOutputs()}
 * methods.<br>
 * Registering parameters are likewise also done at this time through the
 * {@link #registerParameters()} registerInputs()} For operations needed to be done before or after init, call
 * override the methods {@link #preInit() preInit()} and {@link #postInit() postInit()}.
 */
public abstract class AbstractModule {

	private AbstractParameter<?>[] parameters;

	/**
	 * The {@link ModuleWrapper} instance wrapping this module instance.
	 */
	ModuleWrapper wrapper = null;

	/**
	 * The array that holds the {@link ModuleInput} objects that specify the inputs to the module.
	 */
	private ModuleInput[] inputs;

	/**
	 * The array that holds the {@link ModuleOutput} objects that specify the inputs to the module.
	 */
	private ModuleOutput[] outputs;

	protected final EventDispatcher<ModuleEvent> eventDispatcher = new EventDispatcher<>();

	protected void preInit() {
	}

	protected void postInit() {
	}

	public final void init(ModuleWrapper wrapper) {
		preInit();
		this.wrapper = wrapper;

		parameters = registerParameters();
		for (AbstractParameter<?> p : parameters) {
			p.addEventHandler(ParameterEvent.PARAMETER_CHANGED, e -> {
				eventDispatcher.dispatchEvent(new ModuleEvent(this, ModuleEvent.MODULE_PARAMETERS_CHANGED, (ParameterEvent) e));
			});
		}

		inputs = registerInputs();
		outputs = registerOutputs();
		postInit();
	}

	public abstract AbstractParameter<?>[] registerParameters();

	protected void onParametersChange() {};

	/**
	 * Gets whether the module is bypassed at the wrapper level.
	 */
	public final boolean isBypassed() {
		return wrapper.isBypassed();
	}

	/**
	 * This method is dedicated to building the module based on the output request and the data input to the module. The
	 * Output request will specify what output the request is for, and the returned data should be the data that belongs
	 * to this output if there is multiple outputs from the module.<br>
	 * If a module has inputs that is not connected to anything else, the data could not be built earlier in the module
	 * chain, the input will contain a {@code null} value, so a check for null on all inputs with corresponding plans if
	 * it is null should be made. Preferably if the module could not be built it should return null.
	 */
	public abstract AbstractDatatype buildModule(Map<String, AbstractDatatype> inputs, ModuleOutputRequest request);

	/**
	 * Build method for use when the module is bypassed instead of the normal {@link #buildModule} method.
	 */
	private AbstractDatatype buildModuleBypassed(Map<String, AbstractDatatype> inputs) {
		return inputs.get("bypassInput");
	}

	private final Map<Thread, Build> buildThreads = Collections.synchronizedMap(new HashMap<>(1));

	/**
	 * Build method called by the build system. This will depend on the bypassed state of the module redirect
	 * to the appropriate build method to use.
	 */
	public final AbstractDatatype buildModuleSuper(Map<String, AbstractDatatype> inputs, ModuleOutputRequest request, Build build) {
		if (isBypassed()) {
			return buildModuleBypassed(inputs);
		}
		buildThreads.put(Thread.currentThread(), build);
		AbstractDatatype data = buildModule(inputs, request);
		buildThreads.remove(Thread.currentThread());
		return data;
	}

	/**
	 * This method is used to create all the inputrequests for the module, potentially according to an
	 * outputrequest.<br>
	 * An {@link ModuleInputRequest} holds is initiated using the {@link ModuleInput} and an instance of the datatype
	 * extending {@link AbstractDatatype} that is applicable to the given input. The information the datatype needs to
	 * contain for being used as request data depends on the dataype used, but the data contained will usually specify
	 * the required area/volume/or other bounds of the data that the  wants and that should be built by the proceeding
	 * module to that builds the wanted data.<br>
	 * All the {@link ModuleInputRequest} instances needs to be added to an ArrayList that is returned.
	 * <br><br>
	 * Example of an {@link ModuleInputRequest} for a heightmap:<br>
	 * {@code DatatypeHeightmap heightdata = new DatatypeHeightmap(x, y, width, height, resolution);}<br>
	 * {@code ModuleInputRequest ir0 = new ModuleInputRequest(inputs[0], heightdata);}<br><br>
	 * This will create an {@link ModuleInputRequest} to the {@link ModuleInput} of index 0, for a heightmap within the
	 * square described by starting in the coordinate (x, y), and having the width and height defined, and a distance
	 * between every datpoint in the heighmap equal to the resolution value.
	 * <br><br>
	 * The {@code ModuleOutputRequest} parameter of the method contains data with the same kind of structuring defining
	 * the bound of the data it wants. This can be used to define the necessary bounds of the data this module needs to
	 * request to be able to build the requested data. If the datatype is the same and there is not a need for changing
	 * the bounds of the data to be bigger(or smaller), the {@code data} field can often be extracted from the
	 * {@link ModuleOutputRequest} and be used directly in the {@link ModuleInputRequest}.
	 *
	 * @param outputRequest The output request containing the {@link ModuleOutput} there is requested data from, and the
	 *                      {@code data} that is requested @return An {@link ArrayList} containing elements of
	 *                      {@link ModuleInputRequest} for all the inputs to the module
	 */
	public abstract Map<String, ModuleInputRequest> getInputRequests(ModuleOutputRequest outputRequest);

	/**
	 * Method used for creating appropriate input requests when the module is bypassed instead of the normal
	 * {@link #getInputRequests} getInputRequests method.
	 */
	private Map<String, ModuleInputRequest> getInputRequestsBypassed(ModuleOutputRequest outputRequest) {
		HashMap<String, ModuleInputRequest> inputRequests = new HashMap<>();
		inputRequests.put("bypassInput", new ModuleInputRequest(getBypassInput(), outputRequest.data));
		return inputRequests;
	}

	/**
	 * Method called by the build system. This will depend on the bypassed state of the module redirect to the
	 * appropriate getInputRequests method to use.
	 */
	public final Map<String, ModuleInputRequest> getInputRequestsSuper(ModuleOutputRequest outputRequest, Build build) {
		if (isBypassed()) {
			return getInputRequestsBypassed(outputRequest);
		}
		buildThreads.put(Thread.currentThread(), build);
		Map<String, ModuleInputRequest> requests = getInputRequests(outputRequest);
		buildThreads.remove(Thread.currentThread());
		return requests;
	}

	/**
	 * The name of the module is a descriptive name that tells the user what this module does. It will be the name that
	 * it is shown by in the menu for adding modules, and the module will take on this name and show it over the top of
	 * the module in the patch editor. If the module is given a custom name, this will still be shown in parentheses to
	 * indicate the function of the module.
	 *
	 * @return The name of this module
	 */
	public abstract String getModuleName();

	/**
	 * <b>Optional to override for adding meta tag</b>
	 * <br>The meta tag of the module is a descriptive tag that can be used to tell the user additional information
	 * about the module instance if it for example has several operation modes, an example of a module with multiple
	 * operation modes could be a combiner. Without a meta tag it will show up as a combiner, but the user has no way of
	 * knowing what operation mode it is in; addition, subtraction, multiply..., unless the user makes sure to give it a
	 * descriptive name.<br>
	 * If a meta tag is used it will not be shown in the menu for adding modules, but the module will take on this meta
	 * tag and show it in square brackets "[]" as a part of the module name over the top of the module in the editor and
	 * the editor list view.<br>
	 * This method by default returns null. A null indicates that there is no meta tag applied to the module and there
	 * is not shown any meta tag together with the module name in this case.
	 *
	 * @return A string naming the meta tag of the module, null if no tag is to be applied.
	 */
	public String getModuleMetaTag() {
		return null;
	}

	/**
	 * @return The {@link IModuleCategory} category that this module belongs to
	 */
	public abstract IModuleCategory getModuleCategory();

	/**
	 * Used by the rendering of the module in the patch editor for coloring the module that wraps the module. By
	 * default, it returns the color set for the ModuleClass the module is defined as, which is the recommended way of
	 * setting module render colors, but this method can be overridden to set a unique color independently. @return The
	 * {@link Color} to render the module containing this module in when viewed in the patch editor.
	 */
	public Color getModuleColor() {
		return getModuleCategory().classColor();
	}

	/**
	 * Gets the name of the class representing the module, including package as given by
	 * {@link java.lang.Class#getName() getName()}.
	 *
	 * @return String representing the name of the class
	 */
	public final String getModuleClassString() {
		if (this instanceof ModuleUnknown) {
			return ((ModuleUnknown) this).moduleclass;
		}
		return getClass().getName();
	}

	/**
	 * @return An array of {@link ModuleInput}. A module input contains an empty instance of the {@code datatype}
	 * extending {@link AbstractDatatype} that the input takes, and the name of the input. The name should be unique
	 * among the inputs and outputs on a module. The order they are listed in the array, is the order they appear in on
	 * the wrapper that wraps the module.
	 */
	public abstract ModuleInput[] registerInputs();

	/**
	 * @return An array of {@link ModuleOutput}. A module output contains an empty instance of the {@code datatype}
	 * extending {@link AbstractDatatype} that the output gives, and the name of the output. The name should be unique
	 * among the inputs and outputs on a module. The order they are listed in the array, is the order they appear in on
	 * the wrapper that wraps the module.
	 */
	public abstract ModuleOutput[] registerOutputs();

	/**
	 * This method is used in the case of a need for changes to the IO of a module when it's been initiated. Calling
	 * this method will cause the module to register IO again by running the two methods {@link #registerInputs()} and
	 * {@link #registerOutputs()} again, and update the wrapper about a change in IO.
	 */
	protected final void reregisterIO() {
		inputs = registerInputs();
		outputs = registerOutputs();
		notifyIoChange();
	}

	public final ModuleInput[] getInputs() {
		return inputs;
	}

	public final ModuleInput getInput(int index) {
		return inputs[index];
	}

	public final ModuleInput getInput(String name) {
		for (ModuleInput input : inputs) {
			if (input.getName().equals(name)) {
				return input;
			}
		}
		return null;
	}

	public final ModuleOutput[] getOutputs() {
		return outputs;
	}

	public final ModuleOutput getOutput(int index) {
		return outputs[index];
	}

	public final ModuleOutput getOutput(String name) {
		for (ModuleOutput output : outputs) {
			if (output.getName().equals(name)) {
				return output;
			}
		}
		return null;
	}

	public final void addEventHandler(EventType<? extends ModuleEvent> eventType, final EventHandler<ModuleEvent> eventHandler) {
		eventDispatcher.addEventHandler(eventType, eventHandler);
	}

	public final void removeEventHandler(EventType<? extends ModuleEvent> eventType, final EventHandler<ModuleEvent> eventHandler) {
		eventDispatcher.removeEventHandler(eventType, eventHandler);
	}

	private void notifyIoChange() {
		eventDispatcher.dispatchEvent(new ModuleEvent(this, ModuleEvent.MODULE_IO_CHANGED));
	}

	/**
	 * Defines whether the wrapper of this module will be possible to bypass.
	 *
	 * @return {@code true} if the module is bypassable, and {@code false} if not.
	 */
	public abstract boolean isBypassable();

	/**
	 * Defines which input an {@link ModuleOutputRequest} should be forwarded through when a module is bypassed. It is
	 * important that the input is of the same type as the output.
	 *
	 * @return The {@link ModuleInput} used to forward a request when bypassed
	 */
	public ModuleInput getBypassInput() {
		return getInput(0);
	}

	/**
	 * <b>Only for internal use</b>
	 * <br>Wrapper for getting moduleUI and registering
	 */
	public final javafx.event.EventHandler<ModuleApplyParametersEvent> getModuleUI(GridPane pane, javafx.event.EventHandler<ModuleParametersChangeEvent> moduleParametersChangeHandler) {
		javafx.event.EventHandler<ModuleApplyParametersEvent> moduleApplyHandler = moduleUI(pane);

		javafx.event.EventHandler<ModuleApplyParametersEvent> wrapperApplyHandler = e -> {
			JsonNode oldParametersJson = toJson(null);
			moduleApplyHandler.handle(e);
			JsonNode newParametersJson = toJson(null);
			if (!oldParametersJson.equals(newParametersJson) && moduleParametersChangeHandler != null) {
				moduleParametersChangeHandler.handle(new ModuleParametersChangeEvent(this, oldParametersJson, newParametersJson));
			}
		};

		return wrapperApplyHandler;
	}

	/**
	 * This method is used for creating the module editor UI. This is used for adjust the module's parameters.<br>
	 * The UI is usually automatically generated from the registered parameters, unless this is overridden for a further
	 * customized UI.<br>
	 * Buttons for applying or canceling the actions taken is added automatically externally, and is not to be added in
	 * this method.<br>
	 * The function is expected to return a {@link javafx.event.EventHandler} that defines the behavior for applying
	 * the parameters when the user presses either the "Apply" or "OK" button.<br>
	 * The "Apply" button will only apply the parameters, while the "OK" button will also close the UI.
	 *
	 * @param pane The {@link GridPane} to add the module-specific UI elements within
	 * @return {@link javafx.event.EventHandler} for apply event handling internal in the module
	 */
	protected javafx.event.EventHandler<ModuleApplyParametersEvent> moduleUI(GridPane pane) {
		ParameterUiElement<?>[] parameterUis = new ParameterUiElement<?>[parameters.length];

		for (int i = 0; i < parameters.length; i++) {
			parameterUis[i] = parameters[i].getUi();
			parameterUis[i].addToGrid(pane, i);
		}

		//////////

		javafx.event.EventHandler<ModuleApplyParametersEvent> applyHandler = e -> {
			for (int i = 0; i < parameters.length; i++) {
				parameterUis[i].applyUiValue();
			}
			onParametersChange();
		};

		return applyHandler;
	}

	@Override
	public final String toString() {
		return getModuleName();
	}

	/**
	 * Create a {@link JsonNode} representation of the configured module state, containing the parameters and values
	 * needed to reproduce the module in the same state.
	 *
	 * @return A JSON node representing the configured module state
	 */
	public JsonNode toJson(File destination) {
		ObjectMapper objectMapper = new ObjectMapper();
		ObjectNode node = objectMapper.createObjectNode();

		for (AbstractParameter<?> p : parameters) {
			node.set(p.getName(), p.toJson(destination));
		}

		return node;
	}

	/**
	 * Parse a JSON node representing the configured module state.
	 *
	 * @param node is a JSON node representing the configured module state
	 */
	public void fromJson(JsonNode node, File source) {
		for (AbstractParameter<?> p : parameters) {
			if (node.has(p.getName())) {
				p.fromJson(node.get(p.getName()), source);
			}
		}

		onParametersChange();
	}

	/**
	 * This method is used by a module to get data built for an input to itself. The use of this method is to be avoided
	 * if it's reasonably possible. Its intended use is for modules where one or multiple inputs needs to be known
	 * before the correct or necessary inputrequests can be registered in the
	 * {@link #getInputRequests(ModuleOutputRequest) getInputRequests}.
	 * <br><br>
	 * If an input is needs to be built on demand from the module's UI, use the
	 * {@link #buildInputData(ModuleInputRequest, Build) buildInputData(request, build)} instead.
	 *
	 * @param request An {@link ModuleInputRequest} for the data to build
	 */
	public final AbstractDatatype buildInputData(ModuleInputRequest request) {
		Build build = buildThreads.get(Thread.currentThread());

		if (build == null) {
			throw new IllegalStateException("No related build instance found. Was this buildInputData() call not part of an ongoing build?");
		}

		return wrapper.buildInputData(request, build);
	}

	/**
	 * This method is used by a module to get data built for an input to itself. It is intended to be used when an input
	 * needs to be built on demand from the module's UI, like for instance in exporter modules.
	 * <br><br>
	 * If an input is needs to be built on in conjuction with an already ongoing build, use the
	 * {@link #buildInputData(ModuleInputRequest) buildInputData(request)} instead.
	 *
	 * @param request An {@link ModuleInputRequest} for the data to build.
	 * @param build   A {@link Build} instance to be used for the build
	 */
	public final AbstractDatatype buildInputData(ModuleInputRequest request, Build build) {
		return wrapper.buildInputData(request, build);
	}

	/**
	 * This method is used to get the {@link WorldExtentManager} associated with the Synth this module is a part of.<br>
	 * This manager has control over all the extents associated with the current Synth and should only be used to access
	 * extents in a read only way.
	 *
	 * @return The {@link WorldExtentManager} associated with Synth this module is a part of.
	 */
	public final WorldExtentManager getExtentManager() {
		if (wrapper == null) return null;
		return wrapper.getParentPatch().getParentSynth().getExtentManager();
	}

	/**
	 * This method is used to get the {@link SynthParameters} associated with the Synth this module is a part of.<br>
	 * This manager has control over all the properties associated with the current Synth and should only be used to
	 * access in a read only way.
	 *
	 * @return The {@link SynthParameters} associated with Synth this module is a part of.
	 */
	public final SynthParameters getSynthParameters() {
		if (wrapper == null) return null;
		return wrapper.getParentPatch().getParentSynth().getParameters();
	}
}
