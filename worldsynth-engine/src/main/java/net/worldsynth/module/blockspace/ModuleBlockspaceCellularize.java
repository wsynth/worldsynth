/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.module.blockspace;

import net.worldsynth.datatype.AbstractDatatype;
import net.worldsynth.datatype.DatatypeBlockspace;
import net.worldsynth.extent.Extent;
import net.worldsynth.material.MaterialState;
import net.worldsynth.module.*;
import net.worldsynth.parameter.*;
import net.worldsynth.util.Axis;
import net.worldsynth.util.Tuple;
import net.worldsynth.util.gen.Permutation;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

public class ModuleBlockspaceCellularize extends AbstractModule {
	
	private LongParameter seed = new LongParameter("seed", "Seed", null, 0, Long.MIN_VALUE, Long.MAX_VALUE);
	private DoubleTupleParameter scale = new DoubleTupleParameter("scale", "Scale", null, 0.0, Double.POSITIVE_INFINITY, 32.0, 32.0, 32.0);
	private EnumParameter<CellDistribution> cellDistribution = new EnumParameter<>("distribution", "Cell distribution", null, CellDistribution.class, CellDistribution.VORONOI);
	private EnumParameter<DistanceFunction> distanceFunction = new EnumParameter<>("distancefunction", "Distance function", null, DistanceFunction.class, DistanceFunction.EUCLIDEAN);
	private Axes3DParameter sampleAttraction = new Axes3DParameter("sampleattraction", "Sample attraction", null, true, true, true);

	private Permutation permutation;
	
	@Override
	public AbstractParameter<?>[] registerParameters() {
		
		seed.setOnChange(newValue -> {
			permutation = new Permutation(newValue, 256, 3);
		});
		seed.setValue(new Random().nextLong());
		
		AbstractParameter<?>[] p = {
				scale,
				cellDistribution,
				distanceFunction,
				sampleAttraction,
				seed
				};
		return p;
	}
	
	@Override
	public AbstractDatatype buildModule(Map<String, AbstractDatatype> inputs, ModuleOutputRequest request) {
		DatatypeBlockspace requestData = (DatatypeBlockspace) request.data;
		
		double x = requestData.extent.getX();
		double y = requestData.extent.getY();
		double z = requestData.extent.getZ();
		double res = requestData.resolution;
		int spw = requestData.spacePointsWidth;
		int sph = requestData.spacePointsHeight;
		int spl = requestData.spacePointsLength;
		
		//----------READ INPUTS----------//
		
		Tuple<Double> scale = this.scale.getValue();
		CellDistribution cellDistribution = this.cellDistribution.getValue();
		DistanceFunction distanceFunction = this.distanceFunction.getValue();
		boolean attractionX = this.sampleAttraction.getValue().get(Axis.X);
		boolean attractionY = this.sampleAttraction.getValue().get(Axis.Y);
		boolean attractionZ = this.sampleAttraction.getValue().get(Axis.Z);
		
		//Read in input
		if (inputs.get("input") == null) {
			//If the main input (index 0) is null, there is no input and then just return null
			return null;
		}
		DatatypeBlockspace input = (DatatypeBlockspace) inputs.get("input");
		
		//----------BUILD----------//
		
		MaterialState<?, ?>[][][] materials = new MaterialState<?, ?>[spw][sph][spl];
		
		for (int u = 0; u < spw; u++) {
			for (int v = 0; v < sph; v++) {
				for (int w = 0; w < spl; w++) {
					materials[u][v][w] = getMaterialAt(x+u*res, y+v*res, z+w*res, scale, input, cellDistribution, distanceFunction, attractionX, attractionY, attractionZ);
				}
			}
		}
		
		requestData.setBlockspace(materials);
		
		return requestData;
	}
	
	@Override
	public Map<String, ModuleInputRequest> getInputRequests(ModuleOutputRequest outputRequest) {
		HashMap<String, ModuleInputRequest> inputRequests = new HashMap<>();
		
		DatatypeBlockspace ord = (DatatypeBlockspace)outputRequest.data;
		
		double expandX = Math.ceil(Math.abs(scale.getValue().getValue(0) * 2.0) / ord.resolution) * ord.resolution;
		double expandY = Math.ceil(Math.abs(scale.getValue().getValue(1) * 2.0) / ord.resolution) * ord.resolution;
		double expandZ = Math.ceil(Math.abs(scale.getValue().getValue(2) * 2.0) / ord.resolution) * ord.resolution;
		DatatypeBlockspace inputRequestDatatype = new DatatypeBlockspace(Extent.expandedBuildExtent(ord.extent, expandX, expandY, expandZ), ord.resolution);
		inputRequests.put("input", new ModuleInputRequest(getInput("Input"), inputRequestDatatype));
		
		return inputRequests;
	}
	
	public MaterialState<?, ?> getMaterialAt(double x, double y, double z, Tuple<Double> scale, DatatypeBlockspace blockspace, CellDistribution cellDistribution, DistanceFunction distanceFunction, boolean ax, boolean ay, boolean az) {
		Point3D p = closestPoint(x/scale.getValue(0), y/scale.getValue(1), z/scale.getValue(2), cellDistribution, distanceFunction);

		double sx = ax ? p.x * scale.getValue(0) : x;
		double sy = ay ? p.y * scale.getValue(1) : y;
		double sz = az ? p.z * scale.getValue(2) : z;
		return blockspace.getGlobalMaterial(sx, sy, sz);
	}
	
	private Point3D closestPoint(final double x, final double y, final double z, CellDistribution cellDistribution, DistanceFunction distanceFunction) {
		// Calculate the coordinates for the unit square that the coordinates is inside
		int xi = (int) Math.floor(x);
		int yi = (int) Math.floor(y);
		int zi = (int) Math.floor(z);

		// Calculate the local coordinates inside the unit square
		double xf = x - xi;
		double yf = y - yi;
		double zf = z - zi;

		double minDist = Double.POSITIVE_INFINITY;
		double minDistX = 0, minDistY = 0, minDistZ = 0;
		for (int ix = -1; ix <= 1; ix++) {
			for (int iy = -1; iy <= 1; iy++) {
				for (int iz = -1; iz <= 1; iz++) {
					double xoffset = 0.5;
					double yoffset = 0.5;
					double zoffset = 0.5;

					if (cellDistribution == CellDistribution.VORONOI) {
						int cx = xi + ix;
						int cy = yi + iy;
						int cz = zi + iz;

						int[] offsets = permutation.lHashAll(cx & 255, cy & 255, cz & 255);
						xoffset = offsets[0] / 255.0;
						yoffset = offsets[1] / 255.0;
						zoffset = offsets[2] / 255.0;
					}

					double xDist = ix + xoffset - xf;
					double yDist = iy + yoffset - yf;
					double zDist = iz + zoffset - zf;

					//Distance from point
					double dist = 0;
					switch (distanceFunction) {
					case EUCLIDEAN:
						dist = xDist*xDist + yDist*yDist + zDist*zDist;
						break;
					case MANHATTAN:
						dist = Math.abs(xDist) + Math.abs(yDist) + Math.abs(zDist);
						break;
					case CHEBYSHEV:
						dist = Math.max(Math.max(Math.abs(xDist), Math.abs(yDist)), Math.abs(zDist));
						break;
					case MIN:
						dist = Math.min(Math.min(Math.abs(xDist), Math.abs(yDist)), Math.abs(zDist));
						break;
					}

					if (dist < minDist) {
						minDistX = xDist;
						minDistY = yDist;
						minDistZ = zDist;
						minDist = dist;
					}
				}
			}
		}

		return new Point3D(x + minDistX, y + minDistY, z + minDistZ);
	}

	@Override
	public String getModuleName() {
		return "Cellularize";
	}

	@Override
	public IModuleCategory getModuleCategory() {
		return ModuleCategory.MODIFIER;
	}

	@Override
	public ModuleInput[] registerInputs() {
		ModuleInput[] i = {
				new ModuleInput(new DatatypeBlockspace(), "Input")
				};
		return i;
	}

	@Override
	public ModuleOutput[] registerOutputs() {
		ModuleOutput[] o = {
				new ModuleOutput(new DatatypeBlockspace(), "Output")
				};
		return o;
	}

	@Override
	public boolean isBypassable() {
		return true;
	}
	
	private enum DistanceFunction {
		EUCLIDEAN,
		MANHATTAN,
		CHEBYSHEV,
		MIN;
	}
	
	private enum CellDistribution {
		VORONOI,
		SQUARE;
	}
	
	private class Point3D {
		public double x, y, z;
		public Point3D(double x, double y, double z) {
			this.x = x;
			this.y = y;
			this.z = z;
		}
	}
}
