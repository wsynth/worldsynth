/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.module.blockspace;

import net.worldsynth.datatype.AbstractDatatype;
import net.worldsynth.datatype.DatatypeBlockspace;
import net.worldsynth.extent.Extent;
import net.worldsynth.material.MaterialState;
import net.worldsynth.module.*;
import net.worldsynth.parameter.AbstractParameter;
import net.worldsynth.parameter.DoubleTupleParameter;
import net.worldsynth.util.Tuple;
import net.worldsynth.util.math.MathHelperScalar;
import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;
import org.apache.commons.math3.linear.MatrixUtils;
import org.apache.commons.math3.linear.RealMatrix;

import java.util.HashMap;
import java.util.Map;

public class ModuleBlockspaceAffineTransform extends AbstractModule {

	private final DoubleTupleParameter xTransform = new DoubleTupleParameter("x", "X", null, Double.NEGATIVE_INFINITY, Double.POSITIVE_INFINITY, 1.0, 0.0, 0.0, 0.0);
	private final DoubleTupleParameter yTransform = new DoubleTupleParameter("y", "Y", null, Double.NEGATIVE_INFINITY, Double.POSITIVE_INFINITY, 0.0, 1.0, 0.0, 0.0);
	private final DoubleTupleParameter zTransform = new DoubleTupleParameter("z", "Z", null, Double.NEGATIVE_INFINITY, Double.POSITIVE_INFINITY, 0.0, 0.0, 1.0, 0.0);

	@Override
	public AbstractParameter<?>[] registerParameters() {
		return new AbstractParameter<?>[]{
				xTransform,
				yTransform,
				zTransform
		};
	}

	@Override
	public Map<String, ModuleInputRequest> getInputRequests(ModuleOutputRequest outputRequest) {
		HashMap<String, ModuleInputRequest> inputRequests = new HashMap<>();

		DatatypeBlockspace ord = (DatatypeBlockspace) outputRequest.data;

		FastAffineMatrix affineMatrix = getAffineMatrix().invert();
		Extent transformExtent = transformBoundingExtent(affineMatrix, ord.extent);
		DatatypeBlockspace translatedRequestData = new DatatypeBlockspace(transformExtent, ord.resolution);

		inputRequests.put("input", new ModuleInputRequest(getInput(0), translatedRequestData));

		return inputRequests;
	}

	private FastAffineMatrix getAffineMatrix()  {
		Tuple<Double> xTransform = this.xTransform.getValue();
		Tuple<Double> yTransform = this.yTransform.getValue();
		Tuple<Double> zTransform = this.zTransform.getValue();

		return new FastAffineMatrix(xTransform, yTransform, zTransform);
	}

	private Vector3D transformCoordinate(FastAffineMatrix transform, double x, double y, double z) {
		return transform.applyAffine(new Vector3D(x, y, z));
	}

	private Extent transformBoundingExtent(FastAffineMatrix transform, Extent extent) {
		Vector3D x0y0z0 = transformCoordinate(transform, extent.getX(), extent.getY(), extent.getZ());
		Vector3D x0y0z1 = transformCoordinate(transform, extent.getX(), extent.getY(), extent.getZ() + extent.getLength());
		Vector3D x0y1z0 = transformCoordinate(transform, extent.getX(), extent.getY() + extent.getHeight(), extent.getZ());
		Vector3D x0y1z1 = transformCoordinate(transform, extent.getX(), extent.getY() + extent.getHeight(), extent.getZ() + extent.getLength());
		Vector3D x1y0z0 = transformCoordinate(transform, extent.getX() + extent.getWidth(), extent.getY() + extent.getHeight(), extent.getZ());
		Vector3D x1y0z1 = transformCoordinate(transform, extent.getX() + extent.getWidth(), extent.getY(), extent.getZ() + extent.getLength());
		Vector3D x1y1z0 = transformCoordinate(transform, extent.getX() + extent.getWidth(), extent.getY() + extent.getHeight(), extent.getZ());
		Vector3D x1y1z1 = transformCoordinate(transform, extent.getX() + extent.getWidth(), extent.getY() + extent.getHeight(), extent.getZ() + extent.getLength());

		double x0 = Math.floor(MathHelperScalar.min(x0y0z0.getX(), x0y0z1.getX(), x0y1z0.getX(), x0y1z1.getX(), x1y0z0.getX(), x1y0z1.getX(), x1y1z0.getX(), x1y1z1.getX()));
		double x1 = Math.ceil(MathHelperScalar.max(x0y0z0.getX(), x0y0z1.getX(), x0y1z0.getX(), x0y1z1.getX(), x1y0z0.getX(), x1y0z1.getX(), x1y1z0.getX(), x1y1z1.getX()));
		double y0 = Math.floor(MathHelperScalar.min(x0y0z0.getY(), x0y0z1.getY(), x0y1z0.getY(), x0y1z1.getY(), x1y0z0.getY(), x1y0z1.getY(), x1y1z0.getY(), x1y1z1.getY()));
		double y1 = Math.ceil(MathHelperScalar.max(x0y0z0.getY(), x0y0z1.getY(), x0y1z0.getY(), x0y1z1.getY(), x1y0z0.getY(), x1y0z1.getY(), x1y1z0.getY(), x1y1z1.getY()));
		double z0 = Math.floor(MathHelperScalar.min(x0y0z0.getZ(), x0y0z1.getZ(), x0y1z0.getZ(), x0y1z1.getZ(), x1y0z0.getZ(), x1y0z1.getZ(), x1y1z0.getZ(), x1y1z1.getZ()));
		double z1 = Math.ceil(MathHelperScalar.max(x0y0z0.getZ(), x0y0z1.getZ(), x0y1z0.getZ(), x0y1z1.getZ(), x1y0z0.getZ(), x1y0z1.getZ(), x1y1z0.getZ(), x1y1z1.getZ()));

		return new Extent(x0, y0, z0, x1 - x0, y1 - y0, z1 - z0);
	}

	@Override
	public AbstractDatatype buildModule(Map<String, AbstractDatatype> inputs, ModuleOutputRequest request) {
		DatatypeBlockspace requestData = (DatatypeBlockspace) request.data;

		double x = requestData.extent.getX();
		double y = requestData.extent.getY();
		double z = requestData.extent.getZ();
		double res = requestData.resolution;
		int mpw = requestData.spacePointsWidth;
		int mph = requestData.spacePointsHeight;
		int mpl = requestData.spacePointsLength;

		//----------READ INPUTS----------//

		DatatypeBlockspace inputData = (DatatypeBlockspace) inputs.get("input");

		if (inputData == null) {
			// Return null if there is no input
			return null;
		}

		//----------BUILD----------//

		MaterialState<?, ?>[][][] blocks = new MaterialState<?, ?>[mpw][mph][mpl];

		FastAffineMatrix affineMatrix = getAffineMatrix().invert();

		long t0 = System.currentTimeMillis();
		for (int u = 0; u < mpw; u++) {
			for (int v = 0; v < mph; v++) {
				for (int w = 0; w < mpl; w++) {
					Vector3D sampleCoordinate = transformCoordinate(affineMatrix, x + u * res, y + v * res, z + w * res);
					blocks[u][v][w] = inputData.getGlobalMaterial(sampleCoordinate.getX(), sampleCoordinate.getY(), sampleCoordinate.getZ());
				}
			}
		}
		System.out.println("Transform time: " + (System.currentTimeMillis() - t0));

		requestData.setBlockspace(blocks);
		return requestData;
	}

	@Override
	public String getModuleName() {
		return "Affine transform";
	}

	@Override
	public IModuleCategory getModuleCategory() {
		return ModuleCategory.MODIFIER;
	}

	@Override
	public ModuleInput[] registerInputs() {
		return new ModuleInput[]{
				new ModuleInput(new DatatypeBlockspace(), "Input")
		};
	}

	@Override
	public ModuleOutput[] registerOutputs() {
		return new ModuleOutput[]{
				new ModuleOutput(new DatatypeBlockspace(), "Output")
		};
	}

	@Override
	public boolean isBypassable() {
		return true;
	}

	/**
	 * Because apache matrix multiplication is slow.
	 */
	private static class FastAffineMatrix {
		private final double[][] matrix;

		public FastAffineMatrix(Tuple<Double> xTransform, Tuple<Double> yTransform, Tuple<Double> zTransform) {
			matrix = new double[][]{
					{xTransform.getValue(0), yTransform.getValue(0), zTransform.getValue(0), 0},
					{xTransform.getValue(1), yTransform.getValue(1), zTransform.getValue(1), 0},
					{xTransform.getValue(2), yTransform.getValue(2), zTransform.getValue(2), 0},
					{xTransform.getValue(3), yTransform.getValue(3), zTransform.getValue(3), 1}
			};
		}

		public FastAffineMatrix(RealMatrix matrix) {
			this.matrix = new double[4][4];

			for (int i = 0; i < 4; i++) {
				for (int j = 0; j < 4; j++) {
					this.matrix[i][j] = matrix.getEntry(i, j);
				}
			}
		}

		public FastAffineMatrix invert() {
			return new FastAffineMatrix(MatrixUtils.inverse(MatrixUtils.createRealMatrix(matrix)));
		}

		public Vector3D applyAffine(Vector3D v) {
			double x = v.getX() * matrix[0][0] + v.getY() * matrix[1][0] + v.getZ() * matrix[2][0] + matrix[3][0];
			double y = v.getX() * matrix[0][1] + v.getY() * matrix[1][1] + v.getZ() * matrix[2][1] + matrix[3][1];
			double z = v.getX() * matrix[0][2] + v.getY() * matrix[1][2] + v.getZ() * matrix[2][2] + matrix[3][2];

			return new Vector3D(x, y, z);
		}
	}
}
