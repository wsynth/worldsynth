/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.module.blockspace;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import net.worldsynth.datatype.AbstractDatatype;
import net.worldsynth.datatype.DatatypeBlockspace;
import net.worldsynth.material.MaterialRegistry;
import net.worldsynth.material.MaterialState;
import net.worldsynth.module.AbstractModule;
import net.worldsynth.module.IModuleCategory;
import net.worldsynth.module.ModuleCategory;
import net.worldsynth.module.ModuleInput;
import net.worldsynth.module.ModuleInputRequest;
import net.worldsynth.module.ModuleOutput;
import net.worldsynth.module.ModuleOutputRequest;
import net.worldsynth.parameter.AbstractParameter;
import net.worldsynth.parameter.Directions3DParameter;
import net.worldsynth.util.Direction;

public class ModuleBlockspaceIsolateSurface extends AbstractModule {
	
	private Directions3DParameter directionsMask = new Directions3DParameter("directions", "Directions mask", null, true, true, true, true, true, true);
	
	@Override
	public AbstractParameter<?>[] registerParameters() {
		AbstractParameter<?>[] p = {
				directionsMask
		};
		return p;
	}

	@Override
	public AbstractDatatype buildModule(Map<String, AbstractDatatype> inputs, ModuleOutputRequest request) {
		DatatypeBlockspace requestData = (DatatypeBlockspace) request.data;
		
		int spw = requestData.spacePointsWidth;
		int sph = requestData.spacePointsHeight;
		int spl = requestData.spacePointsLength;
		
		//----------READ INPUTS----------//
		
		// Direction masks
		ArrayList<Direction> directionsMask = new ArrayList<Direction>();
		for (Direction d: Direction.values()) {
			if (this.directionsMask.getValue().get(d)) {
				directionsMask.add(d);
			}
		}
		
		if (inputs.get("input") == null) {
			return null;
		}
		DatatypeBlockspace blockspace = (DatatypeBlockspace) inputs.get("input");
		
		//----------BUILD----------//
		
		MaterialState<?, ?> air = MaterialRegistry.getDefaultAir().getDefaultState();
		
		MaterialState<?, ?>[][][] outBlockspace = new MaterialState<?, ?>[spw][sph][spl];
		for (int u = 0; u < spw; u++) {
			for (int v = 0; v < sph; v++) {
				for (int w = 0; w < spl; w++) {
					if (blockWithAirNeighbour(blockspace, directionsMask, u, v, w)) {
						outBlockspace[u][v][w] = blockspace.getLocalMaterial(u, v, w);
					}
					else {
						outBlockspace[u][v][w] = air;
					}
				}
			}
		}
		requestData.setBlockspace(outBlockspace);
		
		return requestData;
	}
	
	private boolean blockWithAirNeighbour(DatatypeBlockspace blockspace, ArrayList<Direction> directionsMask, int x, int y, int z) {
		if (blockspace.getLocalMaterial(x, y, z).isAir()) {
			return false;
		}
		for (Direction d: directionsMask) {
			if (blockspace.isLocalContained(x+d.getX(), y+d.getY(), z+d.getZ())) {
				if (blockspace.getLocalMaterial(x+d.getX(), y+d.getY(), z+d.getZ()).isAir()) {
					return true;
				}
			}
		}
		return false;
	}

	@Override
	public Map<String, ModuleInputRequest> getInputRequests(ModuleOutputRequest outputRequest) {
		HashMap<String, ModuleInputRequest> inputRequests = new HashMap<String, ModuleInputRequest>();
		
		DatatypeBlockspace outputRequestData = (DatatypeBlockspace) outputRequest.data;
		
		inputRequests.put("input", new ModuleInputRequest(getInput("Input"), outputRequestData));
		
		return inputRequests;
	}

	@Override
	public String getModuleName() {
		return "Isolate surface";
	}

	@Override
	public IModuleCategory getModuleCategory() {
		return ModuleCategory.MODIFIER;
	}

	@Override
	public ModuleInput[] registerInputs() {
		ModuleInput[] i = {
				new ModuleInput(new DatatypeBlockspace(), "Input")
				};
		return i;
	}

	@Override
	public ModuleOutput[] registerOutputs() {
		ModuleOutput[] o = {
				new ModuleOutput(new DatatypeBlockspace(), "Output")
				};
		return o;
	}

	@Override
	public boolean isBypassable() {
		return true;
	}
}
