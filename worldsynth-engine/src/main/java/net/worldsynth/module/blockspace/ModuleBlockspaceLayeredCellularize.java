/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.module.blockspace;

import net.worldsynth.datatype.AbstractDatatype;
import net.worldsynth.datatype.DatatypeBlockspace;
import net.worldsynth.extent.Extent;
import net.worldsynth.material.MaterialState;
import net.worldsynth.module.*;
import net.worldsynth.parameter.AbstractParameter;
import net.worldsynth.parameter.DoubleTupleParameter;
import net.worldsynth.parameter.EnumParameter;
import net.worldsynth.parameter.LongParameter;
import net.worldsynth.util.Tuple;
import net.worldsynth.util.gen.Permutation;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

public class ModuleBlockspaceLayeredCellularize extends AbstractModule {
	
	private LongParameter seed = new LongParameter("seed", "Seed", null, 0, Long.MIN_VALUE, Long.MAX_VALUE);
	private DoubleTupleParameter scale = new DoubleTupleParameter("scale", "Scale", null, 0.0, Double.POSITIVE_INFINITY, 32.0, 32.0, 32.0);
	private EnumParameter<CellDistribution> cellDistribution = new EnumParameter<>("distribution", "Cell distribution", null, CellDistribution.class, CellDistribution.VORONOI);
	private EnumParameter<DistanceFunction> distanceFunction = new EnumParameter<>("distancefunction", "Distance function", null, DistanceFunction.class, DistanceFunction.EUCLIDEAN);
	
	private final double S60 = Math.sin(Math.toRadians(60));

	private Permutation permutation;
	
	@Override
	public AbstractParameter<?>[] registerParameters() {
		
		seed.setOnChange(newValue -> {
			permutation = new Permutation(newValue, 256, 5);
		});
		seed.setValue(new Random().nextLong());
		
		AbstractParameter<?>[] p = {
				scale,
				cellDistribution,
				distanceFunction,
				seed
				};
		return p;
	}
	
	@Override
	public AbstractDatatype buildModule(Map<String, AbstractDatatype> inputs, ModuleOutputRequest request) {
		DatatypeBlockspace requestData = (DatatypeBlockspace) request.data;
		
		double x = requestData.extent.getX();
		double y = requestData.extent.getY();
		double z = requestData.extent.getZ();
		double res = requestData.resolution;
		int spw = requestData.spacePointsWidth;
		int sph = requestData.spacePointsHeight;
		int spl = requestData.spacePointsLength;
		
		//----------READ INPUTS----------//
		
		Tuple<Double> scale = this.scale.getValue();
		CellDistribution cellDistribution = this.cellDistribution.getValue();
		DistanceFunction distanceFunction = this.distanceFunction.getValue();
		
		//Read in input
		if (inputs.get("input") == null) {
			//If the main input (index 0) is null, there is no input and then just return null
			return null;
		}
		DatatypeBlockspace input = (DatatypeBlockspace) inputs.get("input");
		
		//----------BUILD----------//
		
		MaterialState<?, ?>[][][] materials = new MaterialState<?, ?>[spw][sph][spl];
		
		for (int u = 0; u < spw; u++) {
			for (int v = 0; v < sph; v++) {
				for (int w = 0; w < spl; w++) {
					double gx = x+u*res;
					double gy = y+v*res;
					double gz = z+w*res;
					
					materials[u][v][w] = getMaterialAt(gx, gy, gz, scale, input, cellDistribution, distanceFunction);
				}
			}
		}
		
		requestData.setBlockspace(materials);
		
		return requestData;
	}
	
	@Override
	public Map<String, ModuleInputRequest> getInputRequests(ModuleOutputRequest outputRequest) {
		HashMap<String, ModuleInputRequest> inputRequests = new HashMap<>();
		
		DatatypeBlockspace ord = (DatatypeBlockspace)outputRequest.data;
		
		double expandX = Math.ceil(Math.abs(scale.getValue().getValue(0) * 2.0) / ord.resolution) * ord.resolution;
//		double expandY = Math.ceil(Math.abs(scale.getValue().getValue(1) * 2.0) / ord.resolution) * ord.resolution;
		double expandZ = Math.ceil(Math.abs(scale.getValue().getValue(2) * 2.0) / ord.resolution) * ord.resolution;
		DatatypeBlockspace inputRequestDatatype = new DatatypeBlockspace(Extent.expandedBuildExtent(ord.extent, expandX, 0.0, expandZ), ord.resolution);
		inputRequests.put("input", new ModuleInputRequest(getInput("Input"), inputRequestDatatype));
		
		return inputRequests;
	}
	
	public MaterialState<?, ?> getMaterialAt(double x, double y, double z, Tuple<Double> scale, DatatypeBlockspace blockspace, CellDistribution cellDistribution, DistanceFunction distanceFunction) {
		int layer = (int) Math.floor(y / scale.getValue(1));
		double xOffset = permutation.gUnitHash(3, layer) * 255.0;
		double zOffset = permutation.gUnitHash(4, layer) * 255.0;
		
		Point2D p = closestPoint(x/scale.getValue(0) + xOffset, z/scale.getValue(2) + zOffset, cellDistribution, distanceFunction);
		return blockspace.getGlobalMaterial((p.x-xOffset) * scale.getValue(0), y, (p.z-zOffset) * scale.getValue(2));
	}
	
	private Point2D closestPoint(double x, double y, CellDistribution cellDistribution, DistanceFunction distanceFunction) {
		double initX = x, initY = y;

		if (cellDistribution == CellDistribution.HEXAGON) {
			y *= 1.0/S60;
		}

		// Calculate the coordinates for the unit square that the coordinates is inside
		int xi = (int) Math.floor(x);
		int yi = (int) Math.floor(y);

		// Calculate the local coordinates inside the unit square
		double xf = x - xi;
		double yf = y - yi;

		double minDist = Double.POSITIVE_INFINITY;
		double minDistX = 0, minDistY = 0;
		for (int ix = -1; ix <= 1; ix++) {
			for (int iy = -1; iy <= 1; iy++) {
				double xoffset = 0.0;
				double yoffset = 0.0;

				if (cellDistribution == CellDistribution.VORONOI) {
					int cx = xi + ix;
					int cy = yi + iy;

					int[] offsets = permutation.lHashAll(cx & 255, cy & 255);
					xoffset = offsets[0] / 255.0;
					yoffset = offsets[1] / 255.0;
				}
				else if (cellDistribution == CellDistribution.HEXAGON) {
					if ((yi & 1) == 1) {
						double c = 0.5;
						double m = 0 + Math.abs(iy-1);
						xoffset =  c * m;
					}
					else {
						double c = 0.5;
						double m = 1 - Math.abs(iy-1);
						xoffset =  c * m;
					}
					yoffset = 0;
				}

				double xDist = ix + xoffset - xf;
				double yDist = iy + yoffset - yf;

				if (cellDistribution == CellDistribution.HEXAGON) {
					yDist *= S60;
				}

				// Distance from point
				double dist = 0;
				switch (distanceFunction) {
				case EUCLIDEAN:
					dist = xDist*xDist + yDist*yDist;
					break;
				case MANHATTAN:
					dist = Math.abs(xDist) + Math.abs(yDist);
					break;
				case CHEBYSHEV:
					dist = Math.max(Math.abs(xDist), Math.abs(yDist));
					break;
				case MIN:
					dist = Math.min(Math.abs(xDist), Math.abs(yDist));
					break;
				}

				if (dist < minDist) {
					minDistX = xDist;
					minDistY = yDist;
					minDist = dist;
				}
			}
		}

		return new Point2D(initX + minDistX, initY + minDistY);
	}

	@Override
	public String getModuleName() {
		return "Layered cellularize";
	}

	@Override
	public IModuleCategory getModuleCategory() {
		return ModuleCategory.MODIFIER;
	}

	@Override
	public ModuleInput[] registerInputs() {
		ModuleInput[] i = {
				new ModuleInput(new DatatypeBlockspace(), "Input")
				};
		return i;
	}

	@Override
	public ModuleOutput[] registerOutputs() {
		ModuleOutput[] o = {
				new ModuleOutput(new DatatypeBlockspace(), "Output")
				};
		return o;
	}

	@Override
	public boolean isBypassable() {
		return true;
	}
	
	private enum DistanceFunction {
		EUCLIDEAN,
		MANHATTAN,
		CHEBYSHEV,
		MIN;
	}
	
	private enum CellDistribution {
		VORONOI, HEXAGON, SQUARE;
	}
	
	private class Point2D {
		public double x, z;
		public Point2D(double x, double z) {
			this.x = x;
			this.z = z;
		}
	}
}
