/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.module;

import net.worldsynth.composition.Composition;
import net.worldsynth.composition.layer.Layer;
import net.worldsynth.parameter.AbstractParameter;
import net.worldsynth.parameter.CompositionLayerParameter;

import java.util.function.Supplier;

public abstract class ModuleCompositionLayer<T extends Layer> extends AbstractModule {

	private final CompositionLayerParameter<T> layer;

	public ModuleCompositionLayer(Supplier<T> layerConstructor) {
		layer = new CompositionLayerParameter<T>("layerid", "Layer", null, this, getLayerClass(), layerConstructor);
	}

	@Override
	public AbstractParameter<?>[] registerParameters() {
		AbstractParameter<?>[] p = {
				layer
		};
		return p;
	}

	public final Composition getComposition() {
		return wrapper.getParentPatch().getParentSynth().getComposition();
	}
	
	public final T getLayer() {
		return layer.getValue();
	}

	public abstract Class<T> getLayerClass();
}
