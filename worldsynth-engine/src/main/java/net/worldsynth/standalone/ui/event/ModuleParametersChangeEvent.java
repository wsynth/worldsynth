/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.standalone.ui.event;

import com.fasterxml.jackson.databind.JsonNode;

import javafx.event.Event;
import net.worldsynth.module.AbstractModule;

public class ModuleParametersChangeEvent extends Event {
	private static final long serialVersionUID = -7955269316224933178L;
	
	private final AbstractModule sourceModule;
	private final JsonNode oldParametersJson;
	private final JsonNode newParametersJson;
	
	public ModuleParametersChangeEvent(AbstractModule sourceModule, JsonNode oldParametersJson, JsonNode newParametersJson) {
		super(null);
		this.sourceModule = sourceModule;
		this.oldParametersJson = oldParametersJson;
		this.newParametersJson = newParametersJson;
	}
	
	public AbstractModule getSourceModule() {
		return sourceModule;
	}
	
	public JsonNode getOldParametersJson() {
		return oldParametersJson;
	}
	
	public JsonNode getNewParametersJson() {
		return newParametersJson;
	} 
}
