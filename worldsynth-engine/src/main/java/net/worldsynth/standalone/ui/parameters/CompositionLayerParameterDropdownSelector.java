/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.standalone.ui.parameters;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.Tooltip;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import net.worldsynth.composition.Composition;
import net.worldsynth.composition.layer.Layer;
import net.worldsynth.parameter.CompositionLayerParameter;
import net.worldsynth.composition.event.CompositionEvent;
import net.worldsynth.composition.event.CompositionLayerRemovalEvent;

import java.util.ArrayList;
import java.util.function.Supplier;

public class CompositionLayerParameterDropdownSelector<T extends Layer> extends ParameterUiElement<T> {

	private Label nameLabel;
	private ComboBox<T> parameterDropdownSelector;
	private Button newLayerButton, deleteLayerButton;

	private boolean ignoreSelectionChange = false;

	private final Class<T> layerClass;

	public CompositionLayerParameterDropdownSelector(CompositionLayerParameter<T> parameter, Class<T> layerClass, Supplier<T> layerConstructor, Composition composition) {
		super(parameter);
		this.layerClass = layerClass;

		composition.addEventHandler(CompositionEvent.COMPOSITION_ANY, e -> {
			if (e.getEventType() == CompositionLayerRemovalEvent.COMPOSITION_LAYER_REMOVED && ((CompositionLayerRemovalEvent) e).getLayer() == uiValue) {
				uiValue = null;
			}
			ignoreSelectionChange = true;
			setLayerDropdownItems(layersList(composition));
			parameterDropdownSelector.setValue(uiValue);
			ignoreSelectionChange = false;
		});
		
		// Label
		nameLabel = new Label(parameter.getDisplayName());
		if (parameter.getDescription() != null && parameter.getDescription().length() > 0) {
			nameLabel.setTooltip(new Tooltip(parameter.getDescription()));
		}
		
		// Dropdown selector
		parameterDropdownSelector = new ComboBox<>();
		parameterDropdownSelector.setMinWidth(300);
		setLayerDropdownItems(layersList(composition));
		parameterDropdownSelector.setMaxWidth(Double.MAX_VALUE);
		parameterDropdownSelector.getSelectionModel().select(uiValue);
		parameterDropdownSelector.valueProperty().addListener((observable, oldValue, newValue) -> {
			if (!ignoreSelectionChange) {
				uiValue = newValue;
				notifyChangeHandlers();
			}
		});

		// New layer button
		newLayerButton = new Button("New");
		newLayerButton.setOnAction(e -> {
			T newLayer = layerConstructor.get();
			composition.addLayer(newLayer);
			parameterDropdownSelector.getSelectionModel().select(newLayer);
			notifyChangeHandlers();
		});

		// Delete layer button
		deleteLayerButton = new Button("Delete");
		deleteLayerButton.setOnAction(e -> {
			if (uiValue != null) {
				composition.removeLayer(uiValue);
				parameterDropdownSelector.getSelectionModel().select(null);
				notifyChangeHandlers();
			}
		});
	}

	private ObservableList<T> layersList(Composition composition) {
		ArrayList<T> layers = new ArrayList<>();
		composition.getRootGroup().forEachLayer(true, l -> {
			if (l.getClass().isAssignableFrom(layerClass)) {
				layers.add((T) l);
			}
		});
		return FXCollections.observableArrayList(layers);
	}

	private void setLayerDropdownItems(ObservableList<T> items) {
		parameterDropdownSelector.getItems().clear();
		for (T par: items) {
			parameterDropdownSelector.getItems().add(par);
		}
	}
	
	@Override
	public void setDisable(boolean disable) {
		nameLabel.setDisable(disable);
		parameterDropdownSelector.setDisable(disable);
	}

	@Override
	public void addToGrid(GridPane pane, int row) {
		pane.add(nameLabel, 0, row);
		pane.add(new HBox(parameterDropdownSelector, newLayerButton, deleteLayerButton), 1, row, 2, 1);
	}
	
	public Label getLabel() {
		return nameLabel;
	}

	public ComboBox<T> getDropdown() {
		return parameterDropdownSelector;
	}
}
