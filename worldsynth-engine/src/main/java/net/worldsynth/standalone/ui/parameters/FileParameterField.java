/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.standalone.ui.parameters;

import java.io.File;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.layout.GridPane;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import net.worldsynth.parameter.FileParameter;

public class FileParameterField extends ParameterUiElement<File> {
	private Label nameLabel;
	private TextField parameterField;
	private Button directoryDialogButton;
	
	public FileParameterField(FileParameter parameter) {
		super(parameter);
		
		//Label
		nameLabel = new Label(parameter.getDisplayName());
		if (parameter.getDescription() != null && parameter.getDescription().length() > 0) {
			nameLabel.setTooltip(new Tooltip(parameter.getDescription()));
		}
		
		//Field
		parameterField = new TextField(String.valueOf(uiValue));
		parameterField.setPrefColumnCount(50);
		parameterField.textProperty().addListener(new ChangeListener<String>() {

			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				boolean validValue = false;
				File newParameterFile = new File(newValue);
				if (parameter.isDirectory()) {
					//Validate directory
					if (newParameterFile.isDirectory()) {
						validValue = true;
					}
				}
				else {
					//Validate file
					if (!newParameterFile.isDirectory()) {
						if (parameter.getFilter() != null) {
							for (String extension: parameter.getFilter().getExtensions()) {
								extension = extension.substring(1);
								if (newParameterFile.getAbsolutePath().endsWith(extension)) {
									validValue = true;
									break;
								}
							}
						}
					}
				}
				
				if (validValue) {
					parameterField.setStyle(null);
					uiValue = newParameterFile;
					notifyChangeHandlers();
				}
				else {
					parameterField.setStyle("-fx-background-color: RED;");
				}
			}
		});
		
		//Button
		directoryDialogButton = new Button("...");
		directoryDialogButton.setOnAction(e -> {
			if (parameter.isDirectory()) {
				DirectoryChooser directoryChooserDialog = new DirectoryChooser();
				if (uiValue != null) {
					if (uiValue.exists()) {
						directoryChooserDialog.setInitialDirectory(uiValue);
					}
				}
				
				File chosenDir = directoryChooserDialog.showDialog(parameterField.getScene().getWindow());
				if (chosenDir != null) {
					uiValue = chosenDir;
					parameterField.setText(uiValue.getAbsolutePath());
					notifyChangeHandlers();
				}
			}
			else {
				FileChooser fileChooserDialog = new FileChooser();
				if (uiValue != null) {
					if (uiValue.exists()) {
						fileChooserDialog.setInitialDirectory(uiValue.getParentFile());
						fileChooserDialog.setInitialFileName(uiValue.getName());
					}
				}
				
				if (parameter.getFilter() != null) {
					fileChooserDialog.getExtensionFilters().add(parameter.getFilter());
				}
				
				File chosenFile = null;
				if (parameter.isSave()) {
					// Show a file saving dialog
					chosenFile = fileChooserDialog.showSaveDialog(parameterField.getScene().getWindow());

					if (parameter.getFilter() != null) {
						// Check for valid file extension
						boolean hasValidExtension = false;
						for (String extension: parameter.getFilter().getExtensions()) {
							if (chosenFile.getAbsolutePath().endsWith(extension.substring(1))) {
								hasValidExtension = true;
								break;
							}
						}

						// Add valid file extension if it is missing
						if (!hasValidExtension) {
							String primaryExtension = parameter.getFilter().getExtensions().get(0);
							chosenFile = new File(chosenFile.toString() + primaryExtension.substring(1));
						}
					}
				}
				else {
					// Show a file opening dialog
					chosenFile = fileChooserDialog.showOpenDialog(parameterField.getScene().getWindow());
				}
				
				if (chosenFile != null) {
					uiValue = chosenFile;
					parameterField.setText(uiValue.getAbsolutePath());
					notifyChangeHandlers();
				}
			}
		});
	}
	
	@Override
	public void setDisable(boolean disable) {
		nameLabel.setDisable(disable);
		parameterField.setDisable(disable);
		directoryDialogButton.setDisable(disable);
	}
	
	@Override
	public void addToGrid(GridPane pane, int row) {
		pane.add(nameLabel, 0, row);
		pane.add(parameterField, 1, row);
		pane.add(directoryDialogButton, 2, row);
	}
	
	public Label getLabel() {
		return nameLabel;
	}

	public TextField getField() {
		return parameterField;
	}

	public Button getDialogButton() {
		return directoryDialogButton;
	}
}
