/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.standalone.ui.parameters;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;
import net.worldsynth.parameter.AbstractNumberTupleParameter;
import net.worldsynth.util.Tuple;

public class NumberTupleParameterField<T extends Number> extends ParameterUiElement<Tuple<T>> {
	
	private Label nameLabel;
	private TextField[] parameterFields;
	private HBox parameterFiledsHBox;
	
	public NumberTupleParameterField(AbstractNumberTupleParameter<T> parameter) {
		super(parameter);
		
		//Label
		nameLabel = new Label(parameter.getDisplayName());
		if (parameter.getDescription() != null && parameter.getDescription().length() > 0) {
			nameLabel.setTooltip(new Tooltip(parameter.getDescription()));
		}
		
		//Fields
		parameterFields = new TextField[uiValue.getTupleSize()];
		for (int i = 0; i < uiValue.getTupleSize(); i++) {
			TextField parameterField = new TextField(String.valueOf(uiValue.getValue(i)));
			parameterField.setPrefColumnCount(10);
			parameterField.setTooltip(new Tooltip("Range: " + parameter.getMin() + " - " + parameter.getMax()));
			parameterField.textProperty().addListener(new TextFieldChangeListener(parameter, i, parameterField));
			HBox.setHgrow(parameterField, Priority.ALWAYS);
			
			parameterFields[i] = parameterField;
			
		}
		parameterFiledsHBox = new HBox(parameterFields);
	}
	
	@Override
	public void setDisable(boolean disable) {
		nameLabel.setDisable(disable);
		for (TextField f: parameterFields) {
			f.setDisable(disable);
		}
	}
	
	@Override
	public void addToGrid(GridPane pane, int row) {
		pane.add(nameLabel, 0, row);
		pane.add(parameterFiledsHBox, 1, row);
	}
	
	public Label getLabel() {
		return nameLabel;
	}

	public Region getFields() {
		return parameterFiledsHBox;
	}
	
	private class TextFieldChangeListener implements ChangeListener<String> {
		
		private final AbstractNumberTupleParameter<T> parameter;
		private final int tupleIndex;
		private final TextField parameterField;
		
		public TextFieldChangeListener(AbstractNumberTupleParameter<T> parameter, int tupleIndex, TextField parameterField) {
			this.parameter = parameter;
			this.tupleIndex = tupleIndex;
			this.parameterField = parameterField;
		}
		
		@Override
		public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
			String text = parameterField.getText();
			try {
				T v = parameter.parseValue(text);
				if (!parameter.insideBounds(v)) throw new Exception("Value is out of range");
				Tuple<T> newUiValue = new Tuple<T>(uiValue.values.clone());
				newUiValue.setValue(tupleIndex, v);
				uiValue = newUiValue;
				parameterField.setStyle(null);
			} catch (Exception exception) {
				parameterField.setStyle("-fx-background-color: RED;");
			}
			notifyChangeHandlers();
		}
		
	}
}
