/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.extent;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import net.worldsynth.event.EventDispatcher;
import net.worldsynth.event.EventHandler;
import net.worldsynth.event.EventType;
import net.worldsynth.extent.event.ExtentEvent;

public class WorldExtent {

	private static long nextExtentId = 1;
	private long extentId;

	private final EventDispatcher<ExtentEvent> eventDispatcher = new EventDispatcher<>();

	private String name = "Unnamed extent";
	private boolean sizeRatioLock = true;
	private double sizeRatio = 1.0;

	private double x = 0.0;
	private double y = 0.0;
	private double z = 0.0;

	private double width = 256.0;
	private double height = 256.0;
	private double length = 256.0;

	public WorldExtent(String name) {
		this(name, 0.0, 0.0, 256.0);
	}

	public WorldExtent(String name, double x, double z, double size) {
		this(name, x, 0, z, size, 256.0, size, true);
	}

	public WorldExtent(String name, double x, double z, double width, double length) {
		this(name, x, 0, z, width, 256.0, length, true);
	}

	public WorldExtent(String name, double x, double y, double z, double width, double height, double length, boolean sizeRatioLock) {
		this.name = name;
		this.x = x;
		this.y = y;
		this.z = z;
		this.sizeRatioLock = sizeRatioLock;
		this.sizeRatio = width / length;
		this.width = width;
		this.height = height;
		this.length = length;

		extentId = nextExtentId++;
	}

	public WorldExtent(JsonNode jsonNode) {
		fromJson(jsonNode);
	}

	public long getId() {
		return extentId;
	}

	public void setName(String name) {
		if (!this.name.equals(name)) {
			JsonNode oldJson = toJson();
			this.name = name;
			fireExtentEditedEvent(oldJson, toJson());
		}
	}

	public String getName() {
		return name;
	}

	public void setSizeRatioLock(boolean sizeRatioLock) {
		if (this.sizeRatioLock != sizeRatioLock) {
			JsonNode oldJson = toJson();
			this.sizeRatioLock = sizeRatioLock;
			fireExtentEditedEvent(oldJson, toJson());
		}
	}

	public boolean getSizeRatioLock() {
		return sizeRatioLock;
	}

	public double getSizeRatio() {
		return sizeRatio;
	}

	public void setX(double x) {
		if (this.x != x) {
			JsonNode oldJson = toJson();
			this.x = x;
			fireExtentEditedEvent(oldJson, toJson());
		}
	}

	public double getX() {
		return x;
	}

	public void setY(double y) {
		if (this.y != y) {
			JsonNode oldJson = toJson();
			this.y = y;
			fireExtentEditedEvent(oldJson, toJson());
		}
	}

	public double getY() {
		return y;
	}

	public void setZ(double z) {
		if (this.z != z) {
			JsonNode oldJson = toJson();
			this.z = z;
			fireExtentEditedEvent(oldJson, toJson());
		}
	}

	public double getZ() {
		return z;
	}

	public void setWidth(double width) {
		if (this.width != width) {
			JsonNode oldJson = toJson();

			this.width = width;

			if (getSizeRatioLock()) {
				length = width / sizeRatio;
			} else {
				sizeRatio = width / getLength();
			}

			fireExtentEditedEvent(oldJson, toJson());
		}
	}

	public double getWidth() {
		return width;
	}

	public void setHeight(double height) {
		if (this.height != height) {
			JsonNode oldJJson = toJson();
			this.height = height;
			fireExtentEditedEvent(oldJJson, toJson());
		}
	}

	public double getHeight() {
		return height;
	}

	public void setLength(double length) {
		if (this.length != length) {
			JsonNode oldJson = toJson();

			this.length = length;

			if (getSizeRatioLock()) {
				width = length * sizeRatio;
			} else {
				sizeRatio = getWidth() / length;
			}

			fireExtentEditedEvent(oldJson, toJson());
		}
	}

	public double getLength() {
		return length;
	}

	public double getArea() {
		return getLength() * getLength();
	}

	public double getVolume() {
		return getArea() * getHeight();
	}

	public final void addEventHandler(EventType<? extends ExtentEvent> eventType, EventHandler<ExtentEvent> eventHandler) {
		eventDispatcher.addEventHandler(eventType, eventHandler);
	}

	public final void removeEventHandler(EventType<? extends ExtentEvent> eventType, EventHandler<ExtentEvent> eventHandler) {
		eventDispatcher.removeEventHandler(eventType, eventHandler);
	}

	void fireExtentAddedEvent() {
		eventDispatcher.dispatchEvent(new ExtentEvent(this, ExtentEvent.EXTENT_ADDED));
	}

	void fireExtentEditedEvent(JsonNode oldJson, JsonNode newJson) {
		eventDispatcher.dispatchEvent(new ExtentEvent(this, ExtentEvent.EXTENT_EDITED, oldJson, newJson));
	}

	void fireExtentRemovedEvent() {
		eventDispatcher.dispatchEvent(new ExtentEvent(this, ExtentEvent.EXTENT_REMOVED));
	}

	/**
	 * Check if the coordinates is contained inside this extent.
	 * 
	 * @param x
	 * @param z
	 * @return true if the coordinate is contained inside the extent, false
	 *         otherwise.
	 */
	public boolean containsCoordinate(double x, double z) {
		if (x < getX() || x >= getX() + getWidth()) {
			return false;
		}
		else if (z < getZ() || z >= getZ() + getLength()) {
			return false;
		}
		return true;
	}

	/**
	 * Check if the coordinates iscontained inside this extent.
	 * 
	 * @param x
	 * @param y
	 * @param z
	 * @return true if the coordinate is contained inside the extent, false
	 *         otherwise.
	 */
	public boolean containsCoordinate(double x, double y, double z) {
		if (x < getX() || x >= getX() + getWidth()) {
			return false;
		}
		else if (y < getY() || y >= getY() + getHeight()) {
			return false;
		}
		else if (z < getZ() || z >= getZ() + getLength()) {
			return false;
		}
		return true;
	}

	public double distanceToClosestConer(double x, double z) {
		// Dist NW
		double dx = x - getX();
		double dz = z - getZ();
		double dist = Math.sqrt(dx * dx + dz * dz);
		// Dist NE
		dx = x - (getX() + getWidth());
		dz = z - getZ();
		dist = Math.min(dist, Math.sqrt(dx * dx + dz * dz));
		// Dist SW
		dx = x - getX();
		dz = z - (getZ() + getLength());
		dist = Math.min(dist, Math.sqrt(dx * dx + dz * dz));
		// Dist SE
		dx = x - (getX() + getWidth());
		dz = z - (getZ() + getLength());
		dist = Math.min(dist, Math.sqrt(dx * dx + dz * dz));

		return dist;
	}

	public ExtentCorner closestCorner(double x, double z) {
		// NW
		double dx = x - getX();
		double dz = z - getZ();
		double d = Math.sqrt(dx * dx + dz * dz);
		double minDist = d;
		ExtentCorner closestCorner = ExtentCorner.NW;
		// NE
		dx = x - (getX() + getWidth());
		dz = z - getZ();
		d = Math.sqrt(dx * dx + dz * dz);
		if (d < minDist) {
			minDist = d;
			closestCorner = ExtentCorner.NE;
		}
		// SW
		dx = x - getX();
		dz = z - (getZ() + getLength());
		d = Math.sqrt(dx * dx + dz * dz);
		if (d < minDist) {
			minDist = d;
			closestCorner = ExtentCorner.SW;
		}
		// SE
		dx = x - (getX() + getWidth());
		dz = z - (getZ() + getLength());
		d = Math.sqrt(dx * dx + dz * dz);
		if (d < minDist) {
			minDist = d;
			closestCorner = ExtentCorner.SE;
		}

		return closestCorner;
	}

	public Extent asExtent() {
		return new Extent(x, y, z, width, height, length);
	}

	public String toString() {
		return getName();
	}

	@Override
	public WorldExtent clone() {
		return new WorldExtent(getName(), getX(), getY(), getZ(), getWidth(), getHeight(), getLength(), getSizeRatioLock());
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof WorldExtent) {
			WorldExtent comparableExtent = (WorldExtent) obj;
			if (comparableExtent.getName().equals(getName()) && comparableExtent.getX() == getX() && comparableExtent.getY() == getY() && comparableExtent.getZ() == getZ() && comparableExtent.getWidth() == getWidth() && comparableExtent.getHeight() == getHeight()
					&& comparableExtent.getLength() == getLength()) {
				return true;
			}
		}
		return false;
	}

	public void cloneValuesFrom(WorldExtent extent) {
		JsonNode oldJson = toJson();

		x = extent.getX();
		y = extent.getY();
		z = extent.getZ();
		width = extent.getWidth();
		height = extent.getHeight();
		length = extent.getLength();
		sizeRatio = width / length;
		sizeRatioLock = extent.getSizeRatioLock();

		fireExtentEditedEvent(oldJson, toJson());
	}

	public JsonNode toJson() {
		ObjectMapper objectMapper = new ObjectMapper();
		ObjectNode extentNode = objectMapper.createObjectNode();

		extentNode.put("id", extentId);
		extentNode.put("name", getName());
		extentNode.put("sizeratiolock", getSizeRatioLock());
		extentNode.put("x", getX());
		extentNode.put("y", getY());
		extentNode.put("z", getZ());
		extentNode.put("width", getWidth());
		extentNode.put("height", getHeight());
		extentNode.put("length", getLength());

		return extentNode;
	}

	public void fromJson(JsonNode extentNode) {
		extentId = extentNode.get("id").asLong();
		nextExtentId = Math.max(nextExtentId, extentId + 1);
		
		setName(extentNode.get("name").asText());
		setSizeRatioLock(extentNode.get("sizeratiolock").asBoolean());
		setX(extentNode.get("x").asDouble());
		setY(extentNode.get("y").asDouble());
		setZ(extentNode.get("z").asDouble());
		setWidth(extentNode.get("width").asDouble());
		setHeight(extentNode.get("height").asDouble());
		setLength(extentNode.get("length").asDouble());
	}

	public enum ExtentCorner {
		NE, NW, SE, SW;
	}
}
