/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.extent.event;

import net.worldsynth.event.Event;
import net.worldsynth.event.EventType;
import net.worldsynth.event.HistoricalEvent;
import net.worldsynth.extent.WorldExtent;
import net.worldsynth.extent.WorldExtentManager;

public class ExtentManagerEvent extends Event implements HistoricalEvent {

	public static final EventType<ExtentManagerEvent> EXTENT_ANY = new EventType<>(Event.ANY, "EXTENT_ANY");
	public static final EventType<ExtentManagerEvent> EXTENT_ADDED = new EventType<>(ExtentManagerEvent.EXTENT_ANY, "EXTENT_ADDED");
	public static final EventType<ExtentManagerEvent> EXTENT_EDITED = new EventType<>(ExtentManagerEvent.EXTENT_ANY, "EXTENT_EDITED");
	public static final EventType<ExtentManagerEvent> EXTENT_REMOVED = new EventType<>(ExtentManagerEvent.EXTENT_ANY, "EXTENT_REMOVED");

	private WorldExtent extent;
	private ExtentEvent extentEvent;

	public ExtentManagerEvent(WorldExtentManager source, EventType<ExtentManagerEvent> eventType, WorldExtent extent) {
		super(source, eventType);
		this.extent = extent;
	}

	public ExtentManagerEvent(WorldExtentManager source, EventType<ExtentManagerEvent> eventType, ExtentEvent extentEvent) {
		super(source, eventType);
		this.extentEvent = extentEvent;
	}

	public WorldExtent getExtent() {
		return extent;
	}

	public ExtentEvent getExtentEvent() {
		return extentEvent;
	}

	@Override
	public void undo() {
		WorldExtentManager extentManager = (WorldExtentManager) getSource();

		if (getEventType() == EXTENT_ADDED) {
			extentManager.removeWorldExtent(getExtent());
		}
		else if (getEventType() == EXTENT_EDITED) {
			getExtentEvent().undo();
		}
		else if (getEventType() == EXTENT_REMOVED) {
			extentManager.addWorldExtent(getExtent());
		}
	}

	@Override
	public void redo() {
		WorldExtentManager extentManager = (WorldExtentManager) getSource();

		if (getEventType() == EXTENT_ADDED) {
			extentManager.addWorldExtent(getExtent());
		}
		else if (getEventType() == EXTENT_EDITED) {
			getExtentEvent().redo();
		}
		else if (getEventType() == EXTENT_REMOVED) {
			extentManager.removeWorldExtent(getExtent());
		}
	}
}
