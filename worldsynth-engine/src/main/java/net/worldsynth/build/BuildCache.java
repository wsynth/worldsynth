/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.build;

import net.worldsynth.datatype.*;
import net.worldsynth.module.ModuleOutput;
import net.worldsynth.module.ModuleOutputRequest;
import net.worldsynth.patch.ModuleWrapper;
import net.worldsynth.patch.ModuleWrapperIO;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.lang.ref.SoftReference;
import java.lang.ref.WeakReference;
import java.util.*;

public class BuildCache {
	private static final Logger LOGGER = LogManager.getLogger(BuildCache.class);
	private HashMap<CacheKey, List<CacheEntry>> cache = new HashMap<>();
	private LinkedHashSet<CacheEntry> accessOrder = new LinkedHashSet<>();

	public void addCache(ModuleWrapper wrapper, ModuleOutputRequest request, AbstractDatatype data) {
		List<CacheEntry> cleanedEntries = cleanCache();
		int hard = 0, soft = 0, weak = 0;
		for (CacheEntry e: cleanedEntries) {
			if (e.isHard()) hard++;
			else if (e.isSoft()) soft++;
			else weak++;
		}
		if (cleanedEntries.size() > 0) {
			LOGGER.printf(Level.DEBUG, "Cleaned T" + cleanedEntries.size() + ":H" + hard + ":S" + soft + ":W" + weak + " cached entries");
		}

		CacheEntry cacheEntry = new CacheEntry(data, wrapper, request.output);
		cacheEntry.soften();

		List<CacheEntry> cached = cache.get(cacheEntry.getCacheKey());
		if (cached == null) {
			cached = new ArrayList<>();
			cache.put(cacheEntry.getCacheKey(), cached);
		}
		cached.add(cacheEntry);
		accessOrder.add(cacheEntry);

		// Weaken 20% of the cache to be garbage collectable if memory usage exceeds 70%
		if (getUsage(true) > 0.70) {
			// Count weak refs
			int weakRefs = 0;
			for (CacheEntry ce: accessOrder) {
				if (ce.isWeak()) weakRefs++;
			}

			int toWeaken = Math.max(accessOrder.size() / 5 - weakRefs, 0);
			if (toWeaken > 0) {
				LOGGER.printf(Level.DEBUG, "Weakening " + toWeaken + " cache entries. " + weakRefs + "+" + toWeaken + " weak entries in cache.");
				weaken(toWeaken);
			}
		}

		// Explicitly run the garbage collection if memory usage exceeds 90%
		if (getUsage(false) > 0.9) {
			LOGGER.printf(Level.DEBUG, "Running extra garbage collection");
			System.gc();

			cleanedEntries = cleanCache();
			hard = soft = weak = 0;
			for (CacheEntry e: cleanedEntries) {
				if (e.isHard()) hard++;
				else if (e.isSoft()) soft++;
				else weak++;
			}
			if (cleanedEntries.size() > 0) {
				LOGGER.printf(Level.DEBUG, "Cleaned T" + cleanedEntries.size() + ":H" + hard + ":S" + soft + ":W" + weak + " cached entries");
			}

			getUsage(true);
		}
	}

	private double getUsage(boolean log) {
		double heapSize = Runtime.getRuntime().totalMemory() / (double) (1024 * 1024 * 1024);
		double heapMaxSize = Runtime.getRuntime().maxMemory() / (double) (1024 * 1024 * 1024);
		double freeMemory = Runtime.getRuntime().freeMemory() / (double) (1024 * 1024 * 1024);
		double usage = (heapSize - freeMemory) / heapMaxSize;

		if (log) {
			LOGGER.printf(Level.DEBUG, "Cache size: %3d\tusage: %.4f\tfree: %.4f\t heap: %.4f\t used: %.4f\tmax: %.4f",
					accessOrder.size(), usage, freeMemory, heapSize, (heapSize - freeMemory), heapMaxSize);
		}

		return usage;
	}

	private List<CacheEntry> cleanCache() {
		ArrayList<CacheEntry> cleanedEntries = new ArrayList<>();

		Iterator<Map.Entry<CacheKey, List<CacheEntry>>> it = cache.entrySet().iterator();
		while (it.hasNext()) {
			Map.Entry<CacheKey, List<CacheEntry>> entry = it.next();

			Iterator<CacheEntry> eit = entry.getValue().iterator();
			while (eit.hasNext()) {
				CacheEntry cacheEntry = eit.next();

				if (cacheEntry.isCleared()) {
					eit.remove();
					accessOrder.remove(cacheEntry);
					cleanedEntries.add(cacheEntry);
				}
			}

			if (entry.getValue().size() == 0) {
				it.remove();
			}
		}

		return cleanedEntries;
	}

	public AbstractDatatype getCache(ModuleWrapper wrapper, ModuleOutputRequest request) {
		List<CacheEntry> cached = cache.get(new CacheKey(wrapper, request.output));
		if (cached != null) {
			Iterator<CacheEntry> it = cached.iterator();
			while (it.hasNext()) {
				CacheEntry cacheEntry = it.next();
				AbstractDatatype data = cacheEntry.get();
				if (data == null) {
					it.remove();
					accessOrder.remove(cacheEntry);
					continue;
				}

				if (data.compareRequestData(request.data)) {
					accessOrder.remove(cacheEntry);
					accessOrder.add(cacheEntry);
					return data;
				}
			}
		}

		return null;
	}
	
	public int hasCachedData(ModuleWrapper wrapper) {
		int cachedInstancesCount = 0;

		for (ModuleWrapperIO wrapperOutput: wrapper.wrapperOutputs.values()) {
			ModuleOutput moduleOutput = (ModuleOutput) wrapperOutput.getIO();
			List<CacheEntry> cachedEntries = cache.get(new CacheKey(wrapper, moduleOutput));

			if (cachedEntries != null) {
				Iterator<CacheEntry> it = cachedEntries.iterator();
				while (it.hasNext()) {
					CacheEntry cacheEntry = it.next();
					if (!cacheEntry.isCleared()) {
						cachedInstancesCount++;
					}
				}
			}
		}

		return cachedInstancesCount;
	}

	public int clearCachedData(ModuleWrapper wrapper) {
		int cachedInstancesRemoved = 0;

		for (ModuleWrapperIO wrapperOutput: wrapper.wrapperOutputs.values()) {
			ModuleOutput moduleOutput = (ModuleOutput) wrapperOutput.getIO();
			List<CacheEntry> cachedEntries = cache.get(new CacheKey(wrapper, moduleOutput));

			if (cachedEntries != null) {
				Iterator<CacheEntry> it = cachedEntries.iterator();
				while (it.hasNext()) {
					CacheEntry cacheEntry = it.next();
					it.remove();
					accessOrder.remove(cacheEntry);

					if (!cacheEntry.isCleared()) {
						cachedInstancesRemoved++;
					}
				}
			}

			cache.remove(new CacheKey(wrapper, moduleOutput));
		}

		return cachedInstancesRemoved;
	}

	private void weaken(int entries) {
		Iterator<CacheEntry> it = accessOrder.iterator();
		int i = 0;
		while (it.hasNext() && i < entries) {
			CacheEntry entry = it.next();
			if (entry.isWeak()) continue;
			entry.weaken();
			i++;
		}
	}

	private class CacheKey {

		private final ModuleWrapper wrapper;
		private final ModuleOutput output;

		public CacheKey(ModuleWrapper wrapper, ModuleOutput output) {
			this.wrapper = wrapper;
			this.output = output;
		}

		@Override
		public int hashCode() {
			return Objects.hash(wrapper, output);
		}

		@Override
		public boolean equals(Object obj) {
			if (!(obj instanceof CacheKey)) return false;

			CacheKey otherKey = (CacheKey) obj;
			return wrapper == otherKey.wrapper && output == otherKey.output;
		}
	}

	private class CacheEntry {
		private final ModuleWrapper wrapper;
		private final ModuleOutput output;
		private AbstractDatatype hardReferent;
		private SoftReference<AbstractDatatype> softReferent = null;
		private WeakReference<AbstractDatatype> weakReferent = null;

		public CacheEntry(AbstractDatatype data, ModuleWrapper wrapper, ModuleOutput output) {
			this.wrapper = wrapper;
			this.output = output;
			this.hardReferent = data;
		}

		public CacheKey getCacheKey() {
			return new CacheKey(wrapper, output);
		}

		public AbstractDatatype get() {
			if (hardReferent != null) {
				return hardReferent;
			}
			else if (softReferent != null) {
				return softReferent.get();
			}
			else if (weakReferent != null) {
				return weakReferent.get();
			}
			return null;
		}

		public boolean isCleared() {
			if (weakReferent != null) {
				return weakReferent.refersTo(null);
			}
			else if (softReferent != null) {
				return softReferent.refersTo(null);
			}
			else {
				return hardReferent != null;
			}
		}

		public boolean isHard() {
			return hardReferent != null;
		}

		public boolean isSoft() {
			return softReferent != null;
		}

		public boolean isWeak() {
			return weakReferent != null;
		}

		public void harden() {
			hardReferent = get();
			softReferent = null;
			weakReferent = null;
		}

		public void soften() {
			softReferent = new SoftReference<>(get());
			hardReferent = null;
			weakReferent = null;
		}

		public void weaken() {
			weakReferent = new WeakReference<>(get());
			hardReferent = null;
			softReferent = null;
		}

		public ModuleWrapper getWrapper() {
			return wrapper;
		}

		public ModuleOutput getOutput() {
			return output;
		}
	}
}
