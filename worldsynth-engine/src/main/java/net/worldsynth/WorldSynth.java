/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth;

import java.io.File;

import net.worldsynth.build.BuildCache;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import net.worldsynth.addon.AddonLoader;
import net.worldsynth.addon.IAddon;
import net.worldsynth.biome.BiomeRegistry;
import net.worldsynth.customobject.CustomObjectFormatRegistry;
import net.worldsynth.material.MaterialRegistry;
import net.worldsynth.module.NativeModuleRegister;

public class WorldSynth {
	
	private static final Logger logger = LogManager.getLogger(WorldSynth.class);

	private static WorldSynth instance;

	public final WorldSynthDirectoryConfig directoryConfig;
	public final NativeModuleRegister moduleRegister;
	public final BuildCache buildCache;

	public WorldSynth(WorldSynthDirectoryConfig worldSynthDirectoryConfig) {
		this(worldSynthDirectoryConfig, null);
	}
	
	public WorldSynth(WorldSynthDirectoryConfig directoryConfig, AddonLoader addonLoader) {
		if (instance != null) throw new IllegalStateException("WorldSynth is already initialized");
		instance = this;

		// Write the WorldSynth version to the log
		logger.info("WorldSynth - Version " + BuildInfo.getVersion());

		this.directoryConfig = directoryConfig;

		if (addonLoader == null) {
			addonLoader = new AddonLoader(directoryConfig.getAddonDirectory());
		}

		// Initialize loaded addons
		logger.info("Initializing loaded addons");
		addonLoader.initAddons(directoryConfig);
		
		// Initialize the biome registry
		logger.info("Initializing biome registry");
		new BiomeRegistry(directoryConfig.getBiomesDirectory(), addonLoader);

		// Initialize  the material registry
		logger.info("Initializing material registry");
		new MaterialRegistry(directoryConfig.getMaterialsDirectory(), addonLoader);

		// Initialize the custom object format registry
		logger.info("Initializing custom object format registry");
		new CustomObjectFormatRegistry(addonLoader);

		// Initialize the module registry
		logger.info("Initializing module registry");
		moduleRegister = new NativeModuleRegister(addonLoader);

		// Initialize build cache
		logger.info("Initializing build cache");
		buildCache = new BuildCache();
	}

	public static String getVersion() {
		return BuildInfo.getVersion();
	}

	public static WorldSynthDirectoryConfig getDirectoryConfig() {
		return instance.directoryConfig;
	}

	public static NativeModuleRegister getModuleRegister() {
		return instance.moduleRegister;
	}

	public static BuildCache getBuildCache() {
		return instance.buildCache;
	}
}
