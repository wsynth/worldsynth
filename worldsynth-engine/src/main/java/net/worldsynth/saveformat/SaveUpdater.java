/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.saveformat;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import net.worldsynth.synth.Synth;

import java.util.ArrayList;

public class SaveUpdater {

	private static final ArrayList<AbstractFormatUpdater> updaters = new ArrayList<>();

	static {
		updaters.add(new FormatUpdaterV2());
	}

	public static JsonNode updateSynth(JsonNode synthNode) {
		int formatVersion = 0;
		if (synthNode.has("version")) {
			formatVersion = synthNode.get("version").get("saveformat").asInt();
		}

		if (formatVersion >= Synth.SAVE_FORMAT_VERSION) {
			return synthNode;
		}

		ObjectNode synthObjectNode = (ObjectNode) synthNode;
		for (AbstractFormatUpdater updater: updaters) {
			if (formatVersion < updater.getTargetVersion()) {
				updater.updateSynth(synthObjectNode);
				formatVersion = updater.getTargetVersion();
			}
		}

		return synthObjectNode;
	}
}
