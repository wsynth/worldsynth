/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.saveformat;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

public abstract class AbstractFormatUpdater {

	private final int targetVersion;

	public AbstractFormatUpdater(int targetVersion) {
		this.targetVersion = targetVersion;
	}

	public int getTargetVersion() {
		return targetVersion;
	}

	public abstract JsonNode updateSynth(ObjectNode synthNode);
}
