/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.saveformat;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

public class FormatUpdaterV2 extends AbstractFormatUpdater {

	public FormatUpdaterV2() {
		super(2);
	}

	@Override
	public JsonNode updateSynth(ObjectNode synthNode) {
		ObjectMapper objectMapper = new ObjectMapper();

		// Format version 2 stores compositions in an array.
		// The array may be empty if no composition has been created.
		ArrayNode compositionsNode = objectMapper.createArrayNode();
		synthNode.set("compositions", compositionsNode);

		return synthNode;
	}
}
