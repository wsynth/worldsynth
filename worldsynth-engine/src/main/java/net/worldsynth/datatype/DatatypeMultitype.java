/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.datatype;

import javafx.scene.paint.Color;

public class DatatypeMultitype extends AbstractDatatype {
	
	AbstractDatatype[] types;
	
	
	public DatatypeMultitype(AbstractDatatype... types) {
		if (types.length == 0) {
			throw new IllegalArgumentException("Expected one or more datatypes");
		}
		else {
			this.types = types;
		}
	}
	
	@Override
	public Color getDatatypeColor() {
		return types[0].getDatatypeColor();
	}

	@Override
	public String getDatatypeName() {
		String name = "";
		for (int i = 0; i < types.length; i++) {
			if (i != 0) {
				name += ", ";
			}
			name += types[i].getDatatypeName();
		}
		name += "";
		return name;
	}

	@Override
	public AbstractDatatype clone() {
		AbstractDatatype[] types = new AbstractDatatype[this.types.length];
		for (int i = 0; i < types.length; i++) {
			types[i] = this.types[i].clone();
		}
		return new DatatypeMultitype(types);
	}

	@Override
	public boolean compareRequestData(AbstractDatatype requestData) {
		return false;
	}

	@Override
	public AbstractDatatype getPreviewDatatype(double x, double y, double z, double width, double height, double length, double resolution) {
		//This datatype is not intended for use as output type and needs not this implemented
		return null;
	}
	
	public AbstractDatatype[] getDatatypes() {
		return types.clone();
	}
}
