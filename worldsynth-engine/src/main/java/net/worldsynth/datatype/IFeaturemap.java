/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.datatype;

import net.worldsynth.featurepoint.Featurepoint2D;

public interface IFeaturemap {
	
	public void setFeaturepoints(Featurepoint2D[] points);
	
	public Featurepoint2D[] getFeaturepoints();

	public boolean isGlobalContained(double x, double z);
}
