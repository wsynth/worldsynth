/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.datatype;

import javafx.scene.paint.Color;
import net.worldsynth.extent.Extent;
import net.worldsynth.material.MaterialRegistry;
import net.worldsynth.material.MaterialState;

public class DatatypeBlockspace extends AbstractDatatype implements IBlockspace {
	
	/**
	 * Internal material data
	 */
	private MaterialState<?, ?>[][][] blockspace;
	
	/**
	 * The extent of the data
	 */
	public final Extent extent;
	
	/**
	 * The resolutions of units per blockspace point
	 */
	public final double resolution;
	
	/**
	 * The number of points in the blockspace
	 */
	public final int spacePointsWidth, spacePointsHeight, spacePointsLength;
	
	/**
	 * Only intended for use defining device IO
	 */
	public DatatypeBlockspace() {
		this(new Extent(0, 0, 0, 0, 0, 0), 1);
	}
	
	/**
	 * @param extent
	 * @param resolution The unit-distance between the points in the blockspace. Mainly used for preview in the
	 * standalone editor, but also usable in other applications where the resolution of the terrain is different than 1 unit.
	 */
	public DatatypeBlockspace(Extent extent, double resolution) {
		this.extent = extent;
		this.resolution = resolution;
		
		spacePointsWidth = (int) Math.ceil(extent.getWidth() / resolution);
		spacePointsHeight = (int) Math.ceil(extent.getHeight() / resolution);
		spacePointsLength = (int) Math.ceil(extent.getLength() / resolution);
	}
	
	@Override
	public void setBlockspace(MaterialState<?, ?>[][][] blockspace) {
		int paramWidth = blockspace.length;
		int paramHeight = blockspace[0].length;
		int paramLength = blockspace[0][0].length;
		if (paramWidth != spacePointsWidth || paramHeight != spacePointsHeight || paramLength != spacePointsLength) {
			throw new IllegalArgumentException("Blockspace data has wrong size. Expected a " + spacePointsWidth + "x" + spacePointsHeight + "x" + spacePointsLength + " map, got a " + paramWidth + "x" + paramHeight + "x" + paramLength  + " map.");
		}
		
		//Replace all null entries with default air
		MaterialState<?, ?> air = MaterialRegistry.getDefaultAir().getDefaultState();
		for (int u = 0; u < spacePointsWidth; u++) {
			for (int v = 0; v < spacePointsHeight; v++) {
				for (int w = 0; w < spacePointsLength; w++) {
					if (blockspace[u][v][w] == null) blockspace[u][v][w] = air;
				}
			}
		}
		
		this.blockspace = blockspace;
	}
	
	@Override
	public MaterialState<?, ?>[][][] getBlockspace() {
		return blockspace;
	}
	
	@Override
	public boolean isLocalContained(int x, int y, int z) {
		if (x < 0 || x >= spacePointsWidth || y < 0 || y >= spacePointsHeight || z < 0 || z >= spacePointsLength) {
			return false;
		}
		return true;
	}

	@Override
	public MaterialState<?, ?> getLocalMaterial(int x, int y, int z) {
		return blockspace[x][y][z];
	}

	@Override
	public boolean isGlobalContained(double x, double y, double z) {
		return extent.isContained(x, y, z);
	}

	@Override
	public MaterialState<?, ?> getGlobalMaterial(double x, double y, double z) {
		x = (x - extent.getX()) / resolution;
		y = (y - extent.getY()) / resolution;
		z = (z - extent.getZ()) / resolution;
		
		return blockspace[(int) x][(int) y][(int) z];
	}
	
	@Override
	public Color getDatatypeColor() {
		return Color.rgb(85, 85, 85);
	}

	@Override
	public String getDatatypeName() {
		return "Blockspace";
	}

	@Override
	public AbstractDatatype clone() {
		DatatypeBlockspace dbs = new DatatypeBlockspace(extent, resolution);
		if (blockspace != null) {
			MaterialState<?, ?>[][][] newSpace = new MaterialState[spacePointsWidth][spacePointsHeight][spacePointsLength];
			for (int u = 0; u < spacePointsWidth; u++) {
				for (int v = 0; v < spacePointsHeight; v++) {
					newSpace[u][v] = blockspace[u][v].clone();
				}
			}
			dbs.blockspace = newSpace;
		}
		return dbs;
	}

	@Override
	public boolean compareRequestData(AbstractDatatype requestData) {
		if (!(requestData instanceof DatatypeBlockspace)) return false;

		DatatypeBlockspace castRequestData = (DatatypeBlockspace) requestData;
		return castRequestData.extent.equals(extent) && castRequestData.resolution == resolution;
	}
	
	@Override
	public AbstractDatatype getPreviewDatatype(double x, double y, double z, double width, double height, double length, double resolution) {
		return new DatatypeBlockspace(new Extent(x, y, z, width, height, length), resolution);
	}
}
