/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.datatype;

import javafx.scene.paint.Color;
import net.worldsynth.biome.Biome;
import net.worldsynth.extent.Extent;

public class DatatypeBiomespace extends AbstractDatatype implements IBiomespace {
	
	/**
	 * Internal biome data
	 */
	private Biome[][][] biomespace;
	
	/**
	 * The extent of the data
	 */
	public final Extent extent;
	
	/**
	 * The resolutions of units per biomespace point
	 */
	public final double resolution;
	
	/**
	 * The number of points in the biomespace
	 */
	public final int spacePointsWidth, spacePointsHeight, spacePointsLength;
	
	/**
	 * Only intended for use defining device IO
	 */
	public DatatypeBiomespace() {
		this(new Extent(0, 0, 0, 0, 0, 0), 1);
	}
	
	/**
	 * @param extent
	 * @param resolution The unit-distance between the points in the biomespace. Mainly used for preview in the
	 * standalone editor, but also usable in other applications where the resolution of the terrain is different than 1 unit.
	 */
	public DatatypeBiomespace(Extent extent, double resolution) {
		this.extent = extent;
		this.resolution = resolution;
		
		spacePointsWidth = (int) Math.ceil(extent.getWidth() / resolution);
		spacePointsHeight = (int) Math.ceil(extent.getHeight() / resolution);
		spacePointsLength = (int) Math.ceil(extent.getLength() / resolution);
	}
	
	@Override
	public void setBiomespace(Biome[][][] biomespace) {
		int paramWidth = biomespace.length;
		int paramHeight = biomespace[0].length;
		int paramLength = biomespace[0][0].length;
		if (paramWidth != spacePointsWidth || paramHeight != spacePointsHeight || paramLength != spacePointsLength) {
			throw new IllegalArgumentException("Biomespace data has wrong size. Expected a " + spacePointsWidth + "x" + spacePointsHeight + "x" + spacePointsLength + " map, got a " + paramWidth + "x" + paramHeight + "x" + paramLength  + " map.");
		}
		
		//Check all entries for null value
		for (int u = 0; u < spacePointsWidth; u++) {
			for (int v = 0; v < spacePointsHeight; v++) {
				for (int w = 0; w < spacePointsLength; w++) {
					if (biomespace[u][v][w] == null) {
						throw new IllegalArgumentException("Biomespace cannot contain null as a value");
					};
				}
			}
		}
		
		this.biomespace = biomespace;
	}
	
	@Override
	public Biome[][][] getBiomespace() {
		return biomespace;
	}
	
	@Override
	public boolean isLocalContained(int x, int y, int z) {
		if (x < 0 || x >= spacePointsWidth || y < 0 || y >= spacePointsHeight || z < 0 || z >= spacePointsLength) {
			return false;
		}
		return true;
	}

	@Override
	public Biome getLocalBiome(int x, int y, int z) {
		return biomespace[x][y][z];
	}

	@Override
	public boolean isGlobalContained(double x, double y, double z) {
		return extent.isContained(x, y, z);
	}

	@Override
	public Biome getGlobalBiome(double x, double y, double z) {
		x = (x - extent.getX()) / resolution;
		y = (y - extent.getY()) / resolution;
		z = (z - extent.getZ()) / resolution;
		
		return biomespace[(int) x][(int) y][(int) z];
	}
	
	@Override
	public Color getDatatypeColor() {
		return Color.rgb(140, 140, 255);
	}
	
	@Override
	public String getDatatypeName() {
		return "Biomespace";
	}
	
	@Override
	public AbstractDatatype clone() {
		DatatypeBiomespace dbs = new DatatypeBiomespace(extent, resolution);
		if (biomespace != null) {
			Biome[][][] newSpace = new Biome[spacePointsWidth][spacePointsHeight][spacePointsLength];
			for (int u = 0; u < spacePointsWidth; u++) {
				for (int v = 0; v < spacePointsHeight; v++) {
					newSpace[u][v] = biomespace[u][v].clone();
				}
			}
			dbs.biomespace = newSpace;
		}
		return dbs;
	}

	@Override
	public boolean compareRequestData(AbstractDatatype requestData) {
		if (!(requestData instanceof DatatypeBiomespace)) return false;

		DatatypeBiomespace castRequestData = (DatatypeBiomespace) requestData;
		return castRequestData.extent.equals(extent) && castRequestData.resolution == resolution;
	}
	
	@Override
	public AbstractDatatype getPreviewDatatype(double x, double y, double z, double width, double height, double length, double resolution) {
		return new DatatypeBiomespace(new Extent(x, y, z, width, height, length), resolution);
	}
}
