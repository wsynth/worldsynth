/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.datatype;

import net.worldsynth.biome.Biome;

public interface IBiomespace {
	
	public void setBiomespace(Biome[][][] biomespace);
	
	public Biome[][][] getBiomespace();
	
	public boolean isLocalContained(int x, int y, int z);
	
	public Biome getLocalBiome(int x, int y, int z);

	public boolean isGlobalContained(double x, double y, double z);
	
	public Biome getGlobalBiome(double x, double y, double z);
}
