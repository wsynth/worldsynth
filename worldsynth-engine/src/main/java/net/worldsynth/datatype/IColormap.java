/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.datatype;

public interface IColormap {
	
	public void setColormap(float[][][] colormap);
	
	public float[][][] getColormap();
	
	public boolean isLocalContained(int x, int z);
	
	public float[] getLocalColor(int x, int z);	
	
	public float[] getLocalLerpColor(double x, double z);

	public boolean isGlobalContained(double x, double z);
	
	public float[] getGlobalColor(double x, double z);
	
	public float[] getGlobalLerpColor(double x, double z);
	
}
