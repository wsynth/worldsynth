/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.datatype;

import javafx.scene.paint.Color;

public abstract class AbstractDatatype {

	public AbstractDatatype() {

	}

	/**
	 * @return The {@link Color} that represents the datatype at the IO rendering in the UI
	 */
	public abstract Color getDatatypeColor();

	/**
	 * @return The name of the datattype
	 */
	public abstract String getDatatypeName();

	public abstract AbstractDatatype clone();

	/**
	 * Compares this data with another instance representing a request. If the request should be expected to produce the
	 * same resulting data, given that the request is for the same module and output, this method returns {@code true},
	 * otherwise returns {@code false}.
	 *
	 * @param requestData request to compare
	 * @return {@code true} if resulting data from the request should be similar, otherwise returns {@code false}.
	 */
	public abstract boolean compareRequestData(AbstractDatatype requestData);

	/**
	 * This method is to be implemented by the implementor of a datatype to define the way a
	 * the datatype in question is mapped to a preview extent in the patcher when requesting
	 * a preview of the datatype. The method has parameters corresponding to the preview extent
	 * parameters that is intended to be used to construct a request instance of the datatype
	 * to be used with the outputrequest for a module with this datatype as the preview output.
	 * @param x
	 * @param y
	 * @param z
	 * @param width
	 * @param height
	 * @param length
	 * @param resolution
	 * @return a request instance of the datatype this is implemented for
	 */
	public abstract AbstractDatatype getPreviewDatatype(double x, double y, double z, double width, double height, double length, double resolution);
}
