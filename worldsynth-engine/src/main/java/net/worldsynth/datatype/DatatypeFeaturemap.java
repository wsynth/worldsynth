/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.datatype;

import javafx.scene.paint.Color;
import net.worldsynth.extent.Extent;
import net.worldsynth.featurepoint.Featurepoint2D;

public class DatatypeFeaturemap extends AbstractDatatype implements IFeaturemap {
	
	private Featurepoint2D[] points;
	
	/**
	 * The extent of the data
	 */
	public final Extent extent;
	
	/**
	 * Resolution is not mainly used while generating or performing modifications to featuremaps, but is used when
	 * requesting other datatypes as inputs for the various operations.
	 */
	public final double resolution;
	
	/**
	 * Only intended for use defining device IO
	 */
	public DatatypeFeaturemap() {
		this(new Extent(0, 0, 0, 0, 0, 0), 1);
	}
	
	/**
	 * @param extent
	 * @param resolution
	 */
	public DatatypeFeaturemap(Extent extent, double resolution) {
		this.extent = extent;
		this.resolution = resolution;
	}
	
	@Override
	public void setFeaturepoints(Featurepoint2D[] points) {
		this.points = points;
	}
	
	@Override
	public Featurepoint2D[] getFeaturepoints() {
		return points;
	}
	
	@Override
	public boolean isGlobalContained(double x, double z) {
		return extent.isContained(x, z);
	}
	
	@Override
	public Color getDatatypeColor() {
		return Color.rgb(255, 85, 255);
	}

	@Override
	public String getDatatypeName() {
		return "Featuremap";
	}

	@Override
	public AbstractDatatype clone() {
		DatatypeFeaturemap dvb = new DatatypeFeaturemap(extent, resolution);
		if (points != null) {
			dvb.points = points.clone();
		}
		return dvb;
	}

	@Override
	public boolean compareRequestData(AbstractDatatype requestData) {
		if (!(requestData instanceof DatatypeFeaturemap)) return false;

		DatatypeFeaturemap castRequestData = (DatatypeFeaturemap) requestData;
		return castRequestData.extent.equals(extent) && castRequestData.resolution == resolution;
	}
	
	@Override
	public AbstractDatatype getPreviewDatatype(double x, double y, double z, double width, double height, double length, double resolution) {
		return new DatatypeFeaturemap(new Extent(x, y, z, width, height, length), resolution);
	}
}
