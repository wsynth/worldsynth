/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.datatype;

import javafx.scene.paint.Color;
import net.worldsynth.extent.Extent;

public class DatatypeHeightmapOverlay extends AbstractDatatype {
	
	public float[][] heightMap;
	public float[][][] colorMap;
	
	/**
	 * The extent of the data
	 */
	public final Extent extent;
	
	/**
	 * The resolutions of units per heightmap point
	 */
	public final double resolution;
	
	/**
	 * The number of points in the heightmap
	 */
	public final int mapPointsWidth, mapPointsLength;
	
	/**
	 * Only intended for use defining device IO
	 */
	public DatatypeHeightmapOverlay() {
		this(new Extent(0, 0, 0, 0, 0, 0), 1);
	}
	
	/**
	 * @param extent
	 * @param resolution The unit-distance between the points in the heightmap. Mainly used for preview in the
	 * standalone editor, but also usable in other applications where the resolution of the terrain is lower than 1 unit.
	 */
	public DatatypeHeightmapOverlay(Extent extent, double resolution) {
		this.extent = extent;
		this.resolution = resolution;
		
		mapPointsWidth = (int) Math.ceil(extent.getWidth() / resolution);
		mapPointsLength = (int) Math.ceil(extent.getLength() / resolution);
	}
	
	@Override
	public Color getDatatypeColor() {
		return Color.rgb(0, 0, 0);
	}

	@Override
	public String getDatatypeName() {
		return "Heightmap overlay";
	}

	@Override
	public AbstractDatatype clone() {
		DatatypeHeightmapOverlay dhmo = new DatatypeHeightmapOverlay(extent, resolution);
		if (heightMap != null) {
			float[][] newMap = new float[mapPointsWidth][mapPointsLength];
			for (int u = 0; u < mapPointsWidth; u++) {
				newMap[u] = heightMap[u].clone();
			}
			dhmo.heightMap = newMap;
		}
		if (colorMap != null) {
			float[][][] newMap = new float[mapPointsWidth][mapPointsLength][];
			for (int u = 0; u < mapPointsWidth; u++) {
				for (int v = 0; v < mapPointsLength; v++) {
					newMap[u][v] = colorMap[u][v].clone();
				}
			}
			dhmo.colorMap = newMap;
		}
		return dhmo;
	}

	@Override
	public boolean compareRequestData(AbstractDatatype requestData) {
		if (!(requestData instanceof DatatypeHeightmapOverlay)) return false;

		DatatypeHeightmapOverlay castRequestData = (DatatypeHeightmapOverlay) requestData;
		return castRequestData.extent.equals(extent) && castRequestData.resolution == resolution;
	}
	
	@Override
	public AbstractDatatype getPreviewDatatype(double x, double y, double z, double width, double height, double length, double resolution) {
		return new DatatypeHeightmapOverlay(new Extent(x, y, z, width, height, length), resolution);
	}
}
