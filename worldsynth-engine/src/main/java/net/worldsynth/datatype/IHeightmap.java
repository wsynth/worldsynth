/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.datatype;

public interface IHeightmap {
	
	public void setHeightmap(float[][] heightmap);
	
	public float[][] getHeightmap();
	
	public boolean isLocalContained(int x, int z);
	
	public float getLocalHeight(int x, int z);	
	
	public float getLocalLerpHeight(double x, double z);

	public boolean isGlobalContained(double x, double z);
	
	public float getGlobalHeight(double x, double z);
	
	public float getGlobalLerpHeight(double x, double z);
	
}
