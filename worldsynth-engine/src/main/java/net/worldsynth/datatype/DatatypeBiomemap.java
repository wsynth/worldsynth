/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.datatype;

import java.util.HashSet;
import java.util.Set;

import javafx.scene.paint.Color;
import net.worldsynth.biome.Biome;
import net.worldsynth.extent.Extent;

public class DatatypeBiomemap extends AbstractDatatype implements IBiomemap {
	
	private Biome[][] biomemap;
	
	/**
	 * The extent of the data
	 */
	public final Extent extent;
	
	/**
	 * The resolutions of units per biomemap point
	 */
	public final double resolution;
	
	/**
	 * The number of points in the biomemap
	 */
	public final int mapPointsWidth, mapPointsLength;
	
	/**
	 * Only intended for use defining device IO
	 */
	public DatatypeBiomemap() {
		this(new Extent(0, 0, 0, 0, 0, 0), 1);
	}
	
	/**
	 * @param extent
	 * @param resolution The unit-distance between the points in the biomemap. Mainly used for preview in the
	 * standalone editor, but also usable in other applications where the resolution of the terrain is lower than 1 unit.
	 */
	public DatatypeBiomemap(Extent extent, double resolution) {
		this.extent = extent;
		this.resolution = resolution;
		
		mapPointsWidth = (int) Math.ceil(extent.getWidth() / resolution);
		mapPointsLength = (int) Math.ceil(extent.getLength() / resolution);
	}
	
	@Override
	public void setBiomemap(Biome[][] biomemap) {
		int paramWidth = biomemap.length;
		int paramLength = biomemap[0].length;
		if (paramWidth != mapPointsWidth || paramLength != mapPointsLength) {
			throw new IllegalArgumentException("Biomemap data has wrong size. Expected a " + mapPointsWidth + "x" + mapPointsLength + " map, got a " + paramWidth + "x" + paramLength  + " map.");
		}
		
		//Check all entries for null value
		for (int u = 0; u < mapPointsWidth; u++) {
			for (int v = 0; v < mapPointsLength; v++) {
				if (biomemap[u][v] == null) {
					throw new IllegalArgumentException("Biomemap cannot contain null as a value");
				};
			}
		}
		
		this.biomemap = biomemap;
	}
	
	@Override
	public Biome[][] getBiomemap() {
		return biomemap;
	}
	
	@Override
	public boolean isLocalContained(int x, int z) {
		if (x < 0 || x >= mapPointsWidth || z < 0 || z >= mapPointsLength) {
			return false;
		}
		return true;
	}
	
	@Override
	public Biome getLocalBiome(int x, int z) {
		return biomemap[x][z];
	}
	
	@Override
	public boolean isGlobalContained(double x, double z) {
		return extent.isContained(x, z);
	}
	
	@Override
	public Biome getGlobalBiome(double x, double z) {
		x = (x - extent.getX()) / resolution;
		z = (z - extent.getZ()) / resolution;
		
		return biomemap[(int) x][(int) z];
	}
	
	public Set<Biome> getBiomeset() {
		HashSet<Biome> biomeset = new HashSet<Biome>();

		for (int u = 0; u < mapPointsWidth; u++) {
			for (int v = 0; v < mapPointsLength; v++) {
				biomeset.add(biomemap[u][v]);
			}
		}
		
		return biomeset;
	}
	
	@Override
	public Color getDatatypeColor() {
		return Color.rgb(85, 85, 255);
	}

	@Override
	public String getDatatypeName() {
		return "Biomemap";
	}

	@Override
	public AbstractDatatype clone() {
		DatatypeBiomemap dbm = new DatatypeBiomemap(extent, resolution);
		if (biomemap != null) {
			Biome[][] newMap = new Biome[mapPointsWidth][mapPointsLength];
			for (int u = 0; u < mapPointsWidth; u++) {
				newMap[u] = biomemap[u].clone();
			}
			dbm.biomemap = newMap;
		}
		return dbm;
	}

	@Override
	public boolean compareRequestData(AbstractDatatype requestData) {
		if (!(requestData instanceof DatatypeBiomemap)) return false;

		DatatypeBiomemap castRequestData = (DatatypeBiomemap) requestData;
		return castRequestData.extent.equals(extent) && castRequestData.resolution == resolution;
	}
	
	@Override
	public AbstractDatatype getPreviewDatatype(double x, double y, double z, double width, double height, double length, double resolution) {
		return new DatatypeBiomemap(new Extent(x, y, z, width, height, length), resolution);
	}
}
