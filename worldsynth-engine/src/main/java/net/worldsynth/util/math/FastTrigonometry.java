/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.util.math;

import java.util.function.Function;

public class FastTrigonometry {
	
	private static final double PERIOD = 2.0*Math.PI;
	private static final int DEFAULT_SAMPLES = 1024; // Sample count must be a power of 2
	
	/**
	 * Fast sine instance with 1024 samples
	 */
	public static final FastTrigonometry SIN = sin(DEFAULT_SAMPLES);
	
	/**
	 * Fast cosine instance with 1024 samples
	 */
	public static final FastTrigonometry COS = cos(DEFAULT_SAMPLES);
	
	/**
	 * Fast tangent instance with 1024 samples
	 */
	public static final FastTrigonometry TAN = tan(DEFAULT_SAMPLES);
	
	
	private final int samples;
	private final double[] table;
	
	public final double period;
	public final double dx;
	
	/**
	 * Constructs a fast sine instance with the given number of samples
	 * 
	 * @param samples number of samples used by the instance
	 * @return the constructed fast sine instance
	 */
	public static FastTrigonometry sin(int samples) {
		if (!MathHelperScalar.isPositivePowerOfTwo(samples)) {
			throw new IllegalArgumentException("Samples must be a positive power of two");
		}
		return new FastTrigonometry(x -> Math.sin(x), PERIOD, samples);
	}
	
	/**
	 * Constructs a fast cosine instance with the given number of samples
	 * 
	 * @param samples number of samples used by the instance
	 * @return the constructed fast cosine instance
	 */
	public static FastTrigonometry cos(int samples) {
		if (!MathHelperScalar.isPositivePowerOfTwo(samples)) {
			throw new IllegalArgumentException("Samples must be a positive power of two");
		}
		return new FastTrigonometry(x -> Math.cos(x), PERIOD, samples);
	}
	
	/**
	 * Constructs a fast tangent instance with the given number of samples
	 * 
	 * @param samples number of samples used by the instance
	 * @return the constructed fast tangent instance
	 */
	public static FastTrigonometry tan(int samples) {
		if (!MathHelperScalar.isPositivePowerOfTwo(samples)) {
			throw new IllegalArgumentException("Samples must be a positive power of two");
		}
		return new FastTrigonometry(x -> Math.tan(x), PERIOD, samples);
	}
	
	private FastTrigonometry(Function<Double, Double> function, double period, int samples) {
		this.samples = samples;
		table = new double[samples+1];
		
		this.period = period;
		dx = period / (samples);
		
		for (int i = 0; i <= samples; i++) {
			table[i] = function.apply(i * dx);
		}
	}
	
	/**
	 * @param x
	 * @return the value of the closest discrete sample for an input smaller than or
	 *         equal to x
	 */
	public final double applyDiscrete(double x) {
		double xx = x / dx;
		int ix = (int) (xx >= 0 ? xx : xx - 1);
		ix = ix & (samples - 1);
		
		return table[ix];
	}
	
	/**
	 * @param x
	 * @return the linearly interpolation of the discrete samples x lays between
	 */
	public final double applyLerp(double x) {
		double xx = x / dx;
		int ix = (int) (xx >= 0 ? xx : xx - 1);
		double jx = xx - (double) ix;
		ix = ix & (samples - 1);
		
		return table[ix] + jx * (table[ix+1] - table[ix]);
	}
}
