/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.util;

import java.io.File;

public class ExecutionInfo {
	
	public static File getWorldSynthDirectory() {
		File wsDir = new File(System.getProperty("user.dir"));

		if (wsDir.getName().matches("bin")) {
			// Use the parent folder when the execution path is a bin folder
			// Expected case when using the run script from a gradle build
			wsDir = wsDir.getParentFile();
		}

		return wsDir;
	}
	
	public static String getSystemOsName() {
		return System.getProperty("os.name");
	}
	
	public static boolean systemIsWindows() {
		String OS = getSystemOsName().toLowerCase();
		return OS.contains("windows");
	}

	public static boolean systemIsMac() {
		String OS = getSystemOsName().toLowerCase();
		return OS.contains("mac");
	}

	public static boolean systemIsUnix() {
		String OS = getSystemOsName().toLowerCase();
		return (OS.contains("nix") || OS.contains("nux") || OS.contains("aix"));
	}

	public static boolean systemIsSolaris() {
		String OS = getSystemOsName().toLowerCase();
		return OS.contains("sunos");
	}
}
