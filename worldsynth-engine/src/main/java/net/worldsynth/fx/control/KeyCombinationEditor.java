/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.fx.control;

import javafx.beans.property.SimpleObjectProperty;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.BorderPane;

import java.util.ArrayList;

public class KeyCombinationEditor extends BorderPane {

	private SimpleObjectProperty<KeyCodeCombination> keyCombination;

	private final TextField editorField;

	public KeyCombinationEditor() {
		this.keyCombination = new SimpleObjectProperty<>(new KeyCodeCombination(KeyCode.A));

		editorField = new TextField(this.keyCombination.get().toString());
		editorField.setEditable(false);

		keyCombination.addListener((observable, oldValue, newValue) -> {
			editorField.setText(newValue.getDisplayText());
		});

		setCenter(editorField);

		editorField.addEventFilter(KeyEvent.ANY, e -> {
			e.consume();

			if (e.getEventType() != KeyEvent.KEY_PRESSED
					|| e.getCode() == KeyCode.CONTROL
					|| e.getCode() == KeyCode.SHIFT
					|| e.getCode() == KeyCode.CONTROL
					|| e.getCode() == KeyCode.ALT
					|| e.getCode() == KeyCode.META
					|| e.getCode() == KeyCode.SHORTCUT
					|| e.getCode() == KeyCode.UNDEFINED) {
				return;
			}

			ArrayList<KeyCombination.Modifier> modifiers = new ArrayList<>();
			if (e.isShiftDown()) modifiers.add(KeyCombination.SHIFT_DOWN);
			if (e.isShortcutDown()) modifiers.add(KeyCombination.SHORTCUT_DOWN);
			if (e.isAltDown()) modifiers.add(KeyCombination.ALT_DOWN);
			keyCombination.set(new KeyCodeCombination(e.getCode(), modifiers.toArray(new KeyCombination.Modifier[0])));
		});
	}

	public KeyCodeCombination getKeyCombination() {
		return keyCombination.get();
	}

	public SimpleObjectProperty<KeyCodeCombination> keyCombinationProperty() {
		return keyCombination;
	}

	public void setKeyCombination(KeyCodeCombination keyCombination) {
		this.keyCombination.set(keyCombination);
	}
}
