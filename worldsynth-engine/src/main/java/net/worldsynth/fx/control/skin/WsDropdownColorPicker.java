/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.fx.control.skin;

import net.worldsynth.fx.HueBoxColorPicker;
import net.worldsynth.fx.control.WsColorPicker;

public class WsDropdownColorPicker extends HueBoxColorPicker {

	public WsDropdownColorPicker(WsColorPicker colorPicker) {
		super(colorPicker.getValue());
		getStyleClass().add("dropdown-hue-box-color-picker");
		colorPicker.valueProperty().bindBidirectional(selectedColorProperty());
	}

	@Override
	public String getUserAgentStylesheet() {
		return getClass().getClassLoader().getResource("worldsynth-color-picker-dropdown.css").toExternalForm();
	}
}
