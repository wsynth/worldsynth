/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.fx.control;

import com.booleanbyte.fx.popupboxbase.control.PopupBoxBase;
import javafx.scene.control.Skin;
import javafx.scene.paint.Color;
import net.worldsynth.color.WsColor;
import net.worldsynth.fx.control.skin.WsColorPickerSkin;

public class WsColorPicker extends PopupBoxBase<Color> {

	public WsColorPicker(WsColor color) {
		this(color.getFxColor());
	}

	public WsColorPicker() {
		this(Color.WHITE);
	}

	public WsColorPicker(Color color) {
		setValue(color);
		getStyleClass().add("worldsynth-color-picker");
	}

	@Override
	protected Skin<?> createDefaultSkin() {
		return new WsColorPickerSkin(this);
	}
}
