/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.composition.event;

import net.worldsynth.composition.layer.Layer;
import net.worldsynth.event.Event;
import net.worldsynth.event.EventType;
import net.worldsynth.event.HistoricalEvent;

public abstract class LayerEvent<T extends Layer> extends Event implements HistoricalEvent {

	public static final EventType<LayerEvent> LAYER_ANY = new EventType<>(Event.ANY, "LAYER_ANY");
	public static final EventType<LayerEvent> LAYER_MODIFIED = new EventType<>(LayerEvent.LAYER_ANY, "LAYER_MODIFIED");

	public LayerEvent(Layer source, EventType<LayerEvent> eventType) {
		super(source, eventType);
	}
}
