/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.composition.event;

import net.worldsynth.composition.layer.regioned.LayerRegion;
import net.worldsynth.composition.layer.regioned.RegionMap;
import net.worldsynth.composition.layer.regioned.RegionedLayer;
import net.worldsynth.event.EventType;

public class RegionedLayerModifcationEvent<T extends LayerRegion> extends LayerEvent<RegionedLayer<T>> {

	private RegionMap<T> oldRegions;
	private RegionMap<T> newRegions;

	public RegionedLayerModifcationEvent(RegionedLayer<T> source, EventType<LayerEvent> eventType, RegionMap<T> oldRegions, RegionMap<T> newRegions) {
		super(source, eventType);
		this.oldRegions = oldRegions;
		this.newRegions = newRegions;
	}

	public RegionMap<T> getOldRegions() {
		return oldRegions;
	}

	public RegionMap<T> getNewRegions() {
		return newRegions;
	}

	@Override
	public void undo() {
		RegionedLayer<T> sourceLayer = (RegionedLayer<T>) getSource();

		oldRegions.forEach((coord, region) -> {
			try {
				sourceLayer.putRegion((T) region.clone());
			} catch (CloneNotSupportedException e) {
				throw new RuntimeException(e);
			}
		});

		sourceLayer.fireEditEvent(new RegionedLayerModifcationEvent<T>(sourceLayer, LAYER_MODIFIED, newRegions, oldRegions));
	}

	@Override
	public void redo() {
		RegionedLayer<T> sourceLayer = (RegionedLayer<T>) getSource();

		newRegions.forEach((coord, region) -> {
			try {
				sourceLayer.putRegion((T) region.clone());
			} catch (CloneNotSupportedException e) {
				throw new RuntimeException(e);
			}
		});

		sourceLayer.fireEditEvent(new RegionedLayerModifcationEvent<T>(sourceLayer, LAYER_MODIFIED, oldRegions, newRegions));
	}
}
