/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.composition.event;

import net.worldsynth.composition.Composition;
import net.worldsynth.event.Event;
import net.worldsynth.event.EventType;
import net.worldsynth.event.HistoricalEvent;

public abstract class CompositionEvent extends Event implements HistoricalEvent {

	public static final EventType<CompositionEvent> COMPOSITION_ANY = new EventType<>(Event.ANY, "COMPOSITION_ANY");
	public CompositionEvent(Composition source, EventType<CompositionEvent> eventType) {
		super(source, eventType);
	}
}
