/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.composition.event;

import net.worldsynth.composition.Composition;
import net.worldsynth.event.EventType;
import net.worldsynth.event.HistoricalEvent;

public class CompositionLayerModifiedEvent extends CompositionEvent implements HistoricalEvent {

	public static final EventType<CompositionEvent> COMPOSITION_LAYER_MODIFIED = new EventType<>(CompositionEvent.COMPOSITION_ANY, "COMPOSITION_LAYER_MODIFIED");

	private LayerEvent layerEvent;

	public CompositionLayerModifiedEvent(Composition source, LayerEvent layerEvent) {
		super(source, COMPOSITION_LAYER_MODIFIED);
		this.layerEvent = layerEvent;
	}

	public LayerEvent getLayerEvent() {
		return layerEvent;
	}

	@Override
	public void undo() {
		getLayerEvent().undo();
	}

	@Override
	public void redo() {;
		getLayerEvent().redo();
	}
}
