/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.composition.event;

import net.worldsynth.composition.layer.Layer;
import net.worldsynth.event.EventType;

public class LayerPropertyChangeEvent<T extends Layer> extends LayerEvent<T> {

	public static final EventType<LayerEvent> LAYER_PROPERTY_CHANGED = new EventType<>(LayerEvent.LAYER_ANY, "LAYER_PROPERTY_CHANGED");

	private String propertyKey;
	private String oldValue, newValue;

	public LayerPropertyChangeEvent(T source, String propertyKey, String oldValue, String newValue) {
		super(source, LAYER_PROPERTY_CHANGED);
		this.propertyKey = propertyKey;
		this.oldValue = oldValue;
		this.newValue = newValue;
	}

	public String getPropertyKey() {
		return propertyKey;
	}

	public String getOldValue() {
		return oldValue;
	}

	public String getNewValue() {
		return newValue;
	}

	@Override
	public void undo() {
		T sourceLayer = (T) getSource();
		sourceLayer.setProperty(propertyKey, oldValue);
	}

	@Override
	public void redo() {
		T sourceLayer = (T) getSource();
		sourceLayer.setProperty(propertyKey, newValue);
	}
}
