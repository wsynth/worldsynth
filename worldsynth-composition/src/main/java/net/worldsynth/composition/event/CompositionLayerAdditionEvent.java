/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.composition.event;

import net.worldsynth.composition.Composition;
import net.worldsynth.composition.layer.Layer;
import net.worldsynth.composition.layer.LayerGroup;
import net.worldsynth.event.EventType;
import net.worldsynth.event.HistoricalEvent;

public class CompositionLayerAdditionEvent extends CompositionEvent implements HistoricalEvent {

	public static final EventType<CompositionEvent> COMPOSITION_LAYER_ADDED = new EventType<>(CompositionEvent.COMPOSITION_ANY, "COMPOSITION_LAYER_ADDED");

	private final Layer layer;
	private final LayerGroup group;
	private final int index;

	public CompositionLayerAdditionEvent(Composition source, Layer layer, LayerGroup group, int index) {
		super(source, COMPOSITION_LAYER_ADDED);
		this.layer = layer;
		this.group = group;
		this.index = index;
	}

	public Layer getLayer() {
		return layer;
	}

	public LayerGroup getGroup() {
		return group;
	}

	public int getIndex() {
		return index;
	}

	@Override
	public void undo() {
		Composition sourceComposition = (Composition) getSource();
		sourceComposition.removeLayer(getLayer());
	}

	@Override
	public void redo() {
		Composition sourceComposition = (Composition) getSource();
		sourceComposition.addLayer(getLayer(), getGroup(), getIndex());
	}
}
