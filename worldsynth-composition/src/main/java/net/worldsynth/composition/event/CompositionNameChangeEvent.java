/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.composition.event;

import net.worldsynth.composition.Composition;
import net.worldsynth.event.EventType;

public class CompositionNameChangeEvent extends CompositionEvent {
	public static final EventType<CompositionEvent> COMPOSITION_NAME_CHANGED = new EventType<>(CompositionEvent.COMPOSITION_ANY, "COMPOSITION_NAME_CHANGED");

	private final String oldName, newName;

	public CompositionNameChangeEvent(Composition source, String oldName, String newName) {
		super(source, COMPOSITION_NAME_CHANGED);
		this.oldName = oldName;
		this.newName = newName;
	}

	public String getOldName() {
		return oldName;
	}

	public String getNewName() {
		return newName;
	}

	@Override
	public void undo() {
		Composition sourceComposition = (Composition) getSource();
		sourceComposition.setName(oldName);
	}

	@Override
	public void redo() {
		Composition sourceComposition = (Composition) getSource();
		sourceComposition.setName(newName);
	}
}
