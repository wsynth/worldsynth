/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.composition.event;

import net.worldsynth.composition.Composition;
import net.worldsynth.composition.layer.Layer;
import net.worldsynth.composition.layer.LayerGroup;
import net.worldsynth.event.EventType;
import net.worldsynth.event.HistoricalEvent;

public class CompositionLayerMoveEvent extends CompositionEvent implements HistoricalEvent {

	public static final EventType<CompositionEvent> COMPOSITION_LAYER_MOVED = new EventType<>(CompositionEvent.COMPOSITION_ANY, "COMPOSITION_LAYER_MOVED");

	private final Layer layer;
	private final LayerGroup oldGroup, newGroup;
	private final int oldIndex, newIndex;

	public CompositionLayerMoveEvent(Composition source, Layer layer, LayerGroup oldGroup, int oldIndex, LayerGroup newGroup, int newIndex) {
		super(source, COMPOSITION_LAYER_MOVED);
		this.layer = layer;
		this.oldGroup = oldGroup;
		this.oldIndex = oldIndex;
		this.newGroup = newGroup;
		this.newIndex = newIndex;
	}

	public Layer getLayer() {
		return layer;
	}

	public LayerGroup getOldGroup() {
		return oldGroup;
	}

	public int getOldIndex() {
		return oldIndex;
	}

	public LayerGroup getNewGroup() {
		return newGroup;
	}

	public int getNewIndex() {
		return newIndex;
	}

	@Override
	public void undo() {
		Composition sourceComposition = (Composition) getSource();
		sourceComposition.moveLayer(getLayer(), getOldGroup(), getOldIndex());
	}

	@Override
	public void redo() {
		Composition sourceComposition = (Composition) getSource();
		sourceComposition.moveLayer(getLayer(), getNewGroup(), getNewIndex());
	}
}
