/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.composition.layer.colormap;

import net.worldsynth.composition.layer.regioned.LayerRegion;

public class ColormapRegion extends LayerRegion implements Cloneable {
	private static final long serialVersionUID = 1l;
	
	public float[][][] regionValues = new float[256][256][3];
	
	public ColormapRegion(int regionX, int regionZ) {
		super(regionX, regionZ);
	}
	
	public final float[] getColorAt(int localX, int localZ) {
		return regionValues[localX][localZ];
	}

	@Override
	public ColormapRegion clone() throws CloneNotSupportedException {
		ColormapRegion clonedRegion = new ColormapRegion(getRegionX(), getRegionZ());
		for (int i = 0; i < regionValues.length; i++) {
			for (int j = 0; j < regionValues[0].length; j++) {
				clonedRegion.regionValues[i][j] = regionValues[i][j].clone();
			}
		}
		return clonedRegion;
	}
}
