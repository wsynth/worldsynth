/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.composition.layer.regioned;

import java.util.HashMap;

public class RegionMap<T extends LayerRegion> extends HashMap<RegionCoordinate, T> {

    public RegionMap() {
        super();
    }

    public RegionMap(int initialCapacity) {
        super(initialCapacity);
    }

    public T get(int x, int z) {
        return super.get(new RegionCoordinate(x, z));
    }

    public T put(T value) {
        return super.put(value.getRegionCoordinate(), value);
    }

    public boolean containsRegion(int x, int z) {
        return super.containsKey(new RegionCoordinate(x, z));
    }
}
