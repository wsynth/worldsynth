/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.composition.layer.regioned;

import java.io.Serializable;

public abstract class LayerRegion implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private final RegionCoordinate regionCoordinate;
	
	public LayerRegion(int regionX, int regionZ) {
		regionCoordinate = new RegionCoordinate(regionX, regionZ);
	}
	
	public final RegionCoordinate getRegionCoordinate() {
		return regionCoordinate;
	}
	
	public final int getRegionX() {
		return regionCoordinate.x();
	}
	
	public final int getRegionZ() {
		return regionCoordinate.z();
	}

	@Override
	public abstract LayerRegion clone() throws CloneNotSupportedException;
}
