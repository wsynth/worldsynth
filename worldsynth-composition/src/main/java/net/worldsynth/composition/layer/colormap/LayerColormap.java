/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.composition.layer.colormap;

import net.worldsynth.composition.layer.regioned.RegionedLayer;

public class LayerColormap extends RegionedLayer<ColormapRegion> {

	public LayerColormap(String name) {
		super(name);
	}

	@Override
	protected Class<ColormapRegion> getRegionClass() {
		return ColormapRegion.class;
	}

	public float[] getColorAt(int x, int z) {
		int regionX = Math.floorDiv(x, 256);
		int regionZ = Math.floorDiv(z, 256);
		
		ColormapRegion region = getRegion(regionX, regionZ);
		if (region == null) {
			return new float[]{0.0f, 0.0f, 0.0f};
		}
		
		int rx = x - regionX*256;
		int rz = z - regionZ*256;
		
		return region.regionValues[rx][rz];
	}
}
