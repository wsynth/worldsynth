/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.composition.layer.regioned;

import java.io.Serializable;

public record RegionCoordinate(int x, int z) implements Serializable {
	private static final long serialVersionUID = 2867819566239579928L;

	@Override
	public String toString() {
		return "RegionCoordinate{x=" + x + ", z=" + z + "}";
	}
}
