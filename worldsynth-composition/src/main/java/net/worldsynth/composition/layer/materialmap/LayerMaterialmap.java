/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.composition.layer.materialmap;

import net.worldsynth.composition.layer.regioned.RegionedLayer;
import net.worldsynth.material.MaterialState;

public class LayerMaterialmap extends RegionedLayer<MaterialmapRegion> {
	
	public LayerMaterialmap() {
		super();
	}
	
	public LayerMaterialmap(String name) {
		super(name);
	}
	
	@Override
	protected Class<MaterialmapRegion> getRegionClass() {
		return MaterialmapRegion.class;
	}
	
	public MaterialState<?, ?> getValueAt(int x, int z) {
		int regionX = Math.floorDiv(x, 256);
		int regionZ = Math.floorDiv(z, 256);
		
		MaterialmapRegion region = getRegion(regionX, regionZ);
		if (region == null) {
			return null;
		}
		
		int rx = x - regionX*256;
		int rz = z - regionZ*256;
		
		return region.regionValues[rx][rz];
	}
}
