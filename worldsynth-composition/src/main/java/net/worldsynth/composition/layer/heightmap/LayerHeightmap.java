/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.composition.layer.heightmap;

import net.worldsynth.composition.layer.regioned.RegionedLayer;

public class LayerHeightmap extends RegionedLayer<HeightmapRegion> {

	public LayerHeightmap() {
		super();
	}
	
	public LayerHeightmap(String name) {
		super(name);
	}

	@Override
	protected Class<HeightmapRegion> getRegionClass() {
		return HeightmapRegion.class;
	}

	public float getValueAt(int x, int z) {
		int regionX = Math.floorDiv(x, 256);
		int regionZ = Math.floorDiv(z, 256);
		HeightmapRegion region = getRegion(regionX, regionZ);
		if (region == null) {
			return 0.0f;
		}

		int regionLocalX = x - regionX*256;
		int regionLocalZ = z - regionZ*256;

		return region.getHeightAt(regionLocalX, regionLocalZ);
	}
}
