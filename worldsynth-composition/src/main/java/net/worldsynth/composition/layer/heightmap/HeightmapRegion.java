/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.composition.layer.heightmap;

import net.worldsynth.composition.layer.regioned.LayerRegion;

public class HeightmapRegion extends LayerRegion implements Cloneable {
	private static final long serialVersionUID = 1L;
	
	public float[][] regionValues = new float[256][256];
	
	public HeightmapRegion(int regionX, int regionZ) {
		super(regionX, regionZ);
	}
	
	public final float getHeightAt(int localX, int localZ) {
		return regionValues[localX][localZ];
	}

	@Override
	public HeightmapRegion clone() throws CloneNotSupportedException {
		HeightmapRegion clonedRegion = new HeightmapRegion(getRegionX(), getRegionZ());
		for (int i = 0; i < regionValues.length; i++) {
			clonedRegion.regionValues[i] = regionValues[i].clone();
		}
		return clonedRegion;
	}
}
