/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.composition.layer.materialmap;

import net.worldsynth.composition.layer.regioned.LayerRegion;
import net.worldsynth.material.MaterialRegistry;
import net.worldsynth.material.MaterialState;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.HashMap;
import java.util.Map.Entry;

public class MaterialmapRegion extends LayerRegion implements Cloneable {
	private static final long serialVersionUID = 1L;
	
	// To avoid serializing material state instances, the region values are translated to and from numeric references with a material id dictionary when serialized and deserialized.
	public transient MaterialState<?, ?>[][] regionValues = new MaterialState<?, ?>[256][256];
	
	private HashMap<Integer, String> dictionary;
	private int[][] numericRegionValues;
	
	public MaterialmapRegion(int regionX, int regionZ) {
		super(regionX, regionZ);
	}

	public final MaterialState<?, ?> getMaterialAt(int localX, int localZ) {
		return regionValues[localX][localZ];
	}
	
	private void writeObject(ObjectOutputStream out) throws IOException {
		preRegionSerialize();
		out.defaultWriteObject();
		postRegionSerialize();
	}
	
	private void readObject(ObjectInputStream in) throws IOException, ClassNotFoundException {
		in.defaultReadObject();
		postRegionDeserialize();
	}
	
	// Translate material references to a numeric numeric values with a dictionary
	private void preRegionSerialize() {
		HashMap<MaterialState<?, ?>, Integer> mmap = new HashMap<>();
		mmap.put(null, 0);
		int i = 1;
		
		numericRegionValues = new int[256][256];
		for (int u = 0; u < 256; u++) {
			for (int v = 0; v < 256; v++) {
				if (!mmap.containsKey(regionValues[u][v])) {
					mmap.put(regionValues[u][v], i++);
				}
				numericRegionValues[u][v] = mmap.get(regionValues[u][v]);
			}
		}
		
		dictionary = new HashMap<>();
		for (Entry<MaterialState<?, ?>, Integer> e: mmap.entrySet()) {
			dictionary.put(e.getValue(), e.getKey() == null ? null : e.getKey().getIdName());
		}
	}
	
	// Free up memory after the region is serialized
	private void postRegionSerialize() {
		dictionary = null;
		numericRegionValues = null;
	}
	
	// Translate material references numeric numeric values to material references
	private void postRegionDeserialize() {
		HashMap<Integer, MaterialState<?, ?>> mmap = new HashMap<>();
		mmap.put(0, null);
		
		regionValues = new MaterialState<?, ?>[256][256];
		for (int u = 0; u < 256; u++) {
			for (int v = 0; v < 256; v++) {
				int materialNumeric = numericRegionValues[u][v];
				if (!mmap.containsKey(materialNumeric)) {
					String materialString = dictionary.get(materialNumeric);
					MaterialState<?, ?> material = MaterialRegistry.getMaterialState(materialString);
					mmap.put(materialNumeric, material);
				}
				regionValues[u][v] = mmap.get(materialNumeric);
			}
		}
		
		dictionary = null;
		numericRegionValues = null;
	}

	@Override
	public MaterialmapRegion clone() throws CloneNotSupportedException {
		MaterialmapRegion clonedRegion = new MaterialmapRegion(getRegionX(), getRegionZ());
		for (int i = 0; i < regionValues.length; i++) {
			clonedRegion.regionValues[i] = regionValues[i].clone();
		}
		return clonedRegion;
	}
}
