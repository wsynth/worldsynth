/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.composition.layer;

import com.fasterxml.jackson.annotation.JsonTypeInfo;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;

@JsonTypeInfo(use = JsonTypeInfo.Id.CLASS, property = "layerclass")
public class LayerGroup extends Layer {

	private final LayerList layersList = new LayerList();

	public LayerGroup() {
		super();
	}

	public LayerGroup(String name) {
		super(name);
	}

	@Override
	public void setDirectory(File directory) {
		super.setDirectory(directory);

		layersList.forEach(l -> {
			File layerDirectory = new File(directory, "layer_" + l.getLayerId());
			l.setDirectory(layerDirectory);
		});
	}

	/**
	 * Interface for layer list change events
	 */
	public interface OnChange {
		void action();
	}

	private OnChange onChange;
	public void setOnListChange(OnChange onChange) {
		this.onChange = onChange;
	}

	private void fireOnChange() {
		if (onChange != null) {
			onChange.action();
		}
	}

	/**
	 * Used for json serialization
	 */
	public List<Layer> getLayers() {
		return layersList;
	}

	public boolean containsLayer(Layer layer, boolean recursive) {
		int[] index = getRecursiveLayerIndex(layer);
		if (index != null) {
			if (index.length == 1) {
				return true;
			}
			else {
				return recursive;
			}
		}
		return false;
	}

	public Layer getLayer(int layerId) {
		for (Layer l : layersList) {
			if (l.getLayerId() == layerId) {
				return l;
			}
			else if (l instanceof LayerGroup) {
				Layer gl = ((LayerGroup) l).getLayer(layerId);
				if (gl != null) {
					return gl;
				}
			}
		}
		return null;
	}

	public Layer getLayer(int[] index) {
		if (index.length == 1) {
			return layersList.get(index[0]);
		}
		else {
			LayerGroup childGroup = (LayerGroup) layersList.get(index[0]);
			return childGroup.getLayer(Arrays.copyOfRange(index, 1, index.length));
		}
	}

	public void addLayer(Layer layer) {
		layersList.add(layer);
		fireOnChange();
	}

	public void addLayer(Layer layer, int index) {
		layersList.add(index, layer);
		fireOnChange();
	}

	public void removeLayer(Layer layer) {
		layersList.remove(layer);
		fireOnChange();
	}

	public int[] getRecursiveLayerIndex(Layer layer) {
		for (int i = 0; i < layersList.size(); i++) {
			Layer l = layersList.get(i);
			if (l == layer) {
				return new int[]{i};
			}
			else if (l instanceof LayerGroup lg) {
				int[] lgIndex = lg.getRecursiveLayerIndex(layer);
				if (lgIndex != null) {
					return concatIndex(i, lgIndex);
				}
			}
		}
		return null;
	}

	public int getLayerIndex(Layer layer) {
		for (int i = 0; i < layersList.size(); i++) {
			Layer l = layersList.get(i);
			if (l == layer) {
				return i;
			}
		}
		return -1;
	}

	private int[] concatIndex(int a, int[] b) {
		int[] c = new int[b.length + 1];
		c[0] = a;
		for (int i = 0; i < b.length; i++) {
			c[i + 1] = b[i];
		}
		return c;
	}

	public void setLayerIndex(Layer layer, int index) {
		if (!layersList.contains(layer)) {
			throw new IllegalArgumentException("Can not change the index of a layer that is not added to the group");
		}
		layersList.add(index, layer);
		fireOnChange();
	}

	public void forEachLayer(boolean recursive, Consumer<Layer> action) {
		layersList.forEach(l -> {
			action.accept(l);
			if (l instanceof LayerGroup && recursive) {
				((LayerGroup) l).forEachLayer(true, action);
			}
		});
	}

	@Override
	public void save() throws IOException {
		for (Layer layer : layersList) {
			layer.getDirectory().mkdir();
			layer.save();
		}
	}

	public static class LayerList extends ArrayList<Layer> {

		@Override
		public void add(int index, Layer element) {
			remove(element);
			super.add(index, element);
		}

		@Override
		public Layer set(int index, Layer element) {
			throw new UnsupportedOperationException("Can only add and remove from layer list");
		}
	}
}
