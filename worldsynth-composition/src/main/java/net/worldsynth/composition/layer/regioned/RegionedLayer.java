/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.composition.layer.regioned;

import com.fasterxml.jackson.annotation.JsonIgnore;
import net.worldsynth.composition.layer.Layer;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;
import java.util.Collection;
import java.util.HashSet;

public abstract class RegionedLayer<T extends LayerRegion> extends Layer implements Cloneable {
	private static final Logger logger = LogManager.getLogger(RegionedLayer.class);

	private transient RegionMap<T> regionmap = new RegionMap<>(256);
	private transient HashSet<RegionCoordinate> nonexistRegions = new HashSet<RegionCoordinate>();
	private transient HashSet<RegionCoordinate> corruptedRegions = new HashSet<RegionCoordinate>();

	public RegionedLayer() {
		super();
	}
	
	public RegionedLayer(String name) {
		super(name);
	}
	
	protected abstract Class<T> getRegionClass();
	
	@JsonIgnore
	public final Collection<T> getRegions() {
		return regionmap.values();
	}
	
	public final T getRegion(int regionX, int regionZ) {
		T region = regionmap.get(regionX, regionZ);
		if (region == null) {
			RegionCoordinate coord = new RegionCoordinate(regionX, regionZ);
			if (!(nonexistRegions.contains(coord) || corruptedRegions.contains(coord)) && getDirectory() != null) {
				try {
					region = readRegionFromFile(regionX, regionZ);
					if (region == null) {
						nonexistRegions.add(coord);
					}
					else {
						regionmap.put(region);
					}
				} catch (Exception e) {
					corruptedRegions.add(coord);
					logger.error("Problem reading layer " + getLayerId() + " \"" + getName() + "\" region (" + regionX + ", " + regionZ + ") from file", e);
				}
			}
		}
		return region;
	}
	
	public final T putRegion(T region) {
		int regionX = region.getRegionX();
		int regionZ = region.getRegionZ();
		RegionCoordinate coord = new RegionCoordinate(regionX, regionZ);
		
		nonexistRegions.remove(coord);
		corruptedRegions.remove(coord);
		return regionmap.put(region);
	}
	
	@Override
	public void save() throws IOException {
		for (T region : regionmap.values()) {
			writeRegionToFile(region);
		}
	}
	
	private void writeRegionToFile(T region) throws IOException {
		File regionFile = new File(getDirectory(), "r." + region.getRegionX() + "." + region.getRegionZ() + ".wcomplayer");
		regionFile.createNewFile();
		
		ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(regionFile));
		oos.writeObject(region);
		oos.close();
	}
	
	private T readRegionFromFile(int regionX, int regionZ) throws IOException, ClassNotFoundException, InvalidRegionException, ClassCastException {
		File regionFile = new File(getDirectory(), "r." + regionX + "." + regionZ + ".wcomplayer");
		if (!regionFile.exists()) {
			return null;
		}
		
		ObjectInputStream ois = new ObjectInputStream(new FileInputStream(regionFile));
		Object regionObject = ois.readObject();
		ois.close();
		
		Class<T> regionClass = getRegionClass();
		if (regionObject != null && !regionClass.isAssignableFrom(regionObject.getClass())) {
			throw new InvalidRegionException("Deserialized object of type " + regionObject.getClass().getName() + " is not a valid region for type " + regionClass.getName());
		}
		
		@SuppressWarnings("unchecked")
		T region = (T) regionObject;
		
		return region;
	}
	
	private void readObject(ObjectInputStream in) throws IOException, ClassNotFoundException {
		in.defaultReadObject();
		regionmap = new RegionMap<>(256);
		nonexistRegions = new HashSet<RegionCoordinate>();
		corruptedRegions = new HashSet<RegionCoordinate>();
	}
}
