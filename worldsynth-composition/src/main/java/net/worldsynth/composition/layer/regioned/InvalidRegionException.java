/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.composition.layer.regioned;

/**
 * Thrown to indicate an object that should represent a region is not a valid
 * object for the intended region.
 */
public class InvalidRegionException extends Exception {
	private static final long serialVersionUID = -6346969638589920862L;

	/**
	 * Construct a {@code InvalidRegionException} with the specific detail message.
	 * 
	 * @param message the detail message
	 */
	public InvalidRegionException(String message) {
		super(message);
	}
}
