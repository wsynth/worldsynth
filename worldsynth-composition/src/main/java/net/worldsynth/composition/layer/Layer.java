/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.composition.layer;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import net.worldsynth.event.EventDispatcher;
import net.worldsynth.event.EventHandler;
import net.worldsynth.event.EventType;
import net.worldsynth.composition.event.LayerEvent;
import net.worldsynth.composition.event.LayerPropertyChangeEvent;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;

@JsonTypeInfo(use = JsonTypeInfo.Id.CLASS, include = JsonTypeInfo.As.PROPERTY, property = "layerclass")
public abstract class Layer {

	protected final EventDispatcher<LayerEvent> eventDispatcher = new EventDispatcher<>();

	@JsonIgnore
	private File directory;
	
	private Integer layerId;
	private String name = "Unnamed layer";
	
	/**
	 * A map to store custom data used by the editor but not by the engine
	 */
	private final HashMap<String, String> editorProperties = new HashMap<>();
	
	public Layer() {
	}
	
	public Layer(String name) {
		setName(name);
	}
	
	/**
	 * Used for json serialization
	 */
	public final String getLayerclass() {
		return this.getClass().getName();
	}
	
	public void setDirectory(File directory) {
		this.directory = directory;
	}
	
	public File getDirectory() {
		return directory;
	}
	
	public void setLayerId(int layerId) {
		if (this.layerId != null) throw new IllegalStateException("Layer already has a layerId assigned");
		this.layerId = layerId;
	}
	
	public int getLayerId() {
		return layerId;
	}
	
	public boolean hasLayerId() {
		return layerId != null;
	}
	
	public void setName(String name) {
		String oldName = this.name;
		this.name = name;

		if (!name.equals(oldName)) {
			eventDispatcher.dispatchEvent(new LayerPropertyChangeEvent(this, "name", oldName, name));
		}
	}
	
	public String getName() {
		return name;
	}

	/**
	 * Used for json serialization
	 */
	public final HashMap<String, String> getEditorProperties() {
		return editorProperties;
	}

	public boolean hasProperty(String key) {
		if (key.equals("name")) {
			return true;
		}
		return editorProperties.containsKey(key);
	}

	public String getProperty(String key) {
		if (key.equals("name")) {
			return name;
		}
		return editorProperties.get(key);
	}

	public String getProperty(String key, String defaultValue) {
		if (!hasProperty(key)) {
			editorProperties.put(key, defaultValue);
		}
		return getProperty(key);
	}

	public String setProperty(String key, String value) {
		String oldValue;
		if (key.equals("name")) {
			oldValue = name;
			name = value;
		}
		else {
			oldValue = editorProperties.put(key, value);
		}

		if (!value.equals(oldValue)) {
			eventDispatcher.dispatchEvent(new LayerPropertyChangeEvent(this, key, oldValue, value));
		}

		return oldValue;
	}

	public void addEventHandler(EventType<? extends LayerEvent> eventType, EventHandler<LayerEvent> eventHandler) {
		eventDispatcher.addEventHandler(eventType, eventHandler);
	}

	public void addEventHandler(EventType<? extends LayerEvent> eventType, EventHandler<LayerEvent> eventHandler, int priority) {
		eventDispatcher.addEventHandler(eventType, eventHandler, priority);
	}

	public void removeEventHandler(EventType<? extends LayerEvent> eventType, EventHandler<LayerEvent> eventHandler) {
		eventDispatcher.removeEventHandler(eventType, eventHandler);
	}

	public void removeEventHandler(EventType<? extends LayerEvent> eventType, EventHandler<LayerEvent> eventHandler, int priority) {
		eventDispatcher.removeEventHandler(eventType, eventHandler, priority);
	}

	public void fireEditEvent(LayerEvent event) {
		eventDispatcher.dispatchEvent(event);
	}
	
	public abstract void save() throws IOException;

	@Override
	public String toString() {
		return getName();
	}
}
