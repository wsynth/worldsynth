/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.composition.layer;

import java.io.IOException;

public class LayerNull extends Layer {

	public LayerNull(String name) {
		super(name);
	}

	@Override
	public void save() throws IOException {}
}
