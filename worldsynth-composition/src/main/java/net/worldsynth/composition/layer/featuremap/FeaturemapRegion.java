/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.composition.layer.featuremap;

import net.worldsynth.composition.layer.regioned.LayerRegion;
import net.worldsynth.extent.Extent;
import net.worldsynth.featurepoint.Featurepoint2D;

import java.util.ArrayList;
import java.util.List;

public class FeaturemapRegion extends LayerRegion implements Cloneable {
	private static final long serialVersionUID = 1L;
	
	public ArrayList<Featurepoint2D> features = new ArrayList<>();
	
	public FeaturemapRegion(int regionX, int regionZ) {
		super(regionX, regionZ);
	}
	
	public final List<Featurepoint2D> getFeatures(Extent extent) {
		ArrayList<Featurepoint2D> ff = new ArrayList<>();
		for (Featurepoint2D f: features) {
			if (extent.isContained(f.getX(), f.getZ())) {
				ff.add(f);
			}
		}
		return ff;
	}

	@Override
	public FeaturemapRegion clone() throws CloneNotSupportedException {
		FeaturemapRegion clonedRegion = new FeaturemapRegion(getRegionX(), getRegionZ());
		for (Featurepoint2D f: features) {
			clonedRegion.features.add(new Featurepoint2D(f.getX(), f.getZ(), f.getSeed()));
		}
		return clonedRegion;
	}
}
