/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.composition.layer.featuremap;

import net.worldsynth.composition.layer.regioned.RegionedLayer;
import net.worldsynth.extent.Extent;
import net.worldsynth.featurepoint.Featurepoint2D;

import java.util.ArrayList;
import java.util.List;

public class LayerFeaturemap extends RegionedLayer<FeaturemapRegion> {

	public LayerFeaturemap() {
		super();
	}
	
	public LayerFeaturemap(String name) {
		super(name);
	}
	
	@Override
	protected Class<FeaturemapRegion> getRegionClass() {
		return FeaturemapRegion.class;
	}

	public List<Featurepoint2D> getFeatures(Extent extent) {
		ArrayList<Featurepoint2D> features = new ArrayList<Featurepoint2D>();
		
		int minRegionX = (int) Math.floor(extent.getX() / 256);
		int minRegionZ = (int) Math.floor(extent.getZ() / 256);
		int maxRegionX = (int) Math.floor((extent.getX() + extent.getWidth()) / 256);
		int maxRegionZ = (int) Math.floor((extent.getZ() + extent.getLength()) / 256);
		
		for (int u = minRegionX; u <= maxRegionX; u++) {
			for (int v = minRegionZ; v <= maxRegionZ; v++) {
				FeaturemapRegion region = getRegion(u, v);
				if (region != null) {
					features.addAll(region.getFeatures(extent));
				}
			}
		}
		
		return features;
	}
}
