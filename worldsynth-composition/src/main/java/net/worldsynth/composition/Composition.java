/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.composition;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import net.worldsynth.composition.event.*;
import net.worldsynth.composition.layer.Layer;
import net.worldsynth.composition.layer.LayerGroup;
import net.worldsynth.event.EventDispatcher;
import net.worldsynth.event.EventHandler;
import net.worldsynth.event.EventType;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Composition {

	private final EventDispatcher<CompositionEvent> eventDispatcher = new EventDispatcher<>();
	private final EventHandler<LayerEvent> layerEventPropagator = e -> {
		eventDispatcher.dispatchEvent(new CompositionLayerModifiedEvent(this, e));
	};

	@JsonIgnore
	private File directory;

	private final Object parent;
	private String name;

	private final LayerGroup rootGroup;
	protected int nextLayerId = 0;

	public Composition(Object parent) {
		this.parent = parent;
		rootGroup = new LayerGroup();

		name = "Composition";
	}

	public Composition(JsonNode jsonNode, Object parent) throws JsonProcessingException, IllegalArgumentException {
		this.parent = parent;

		rootGroup = new LayerGroup();

		fromJson(jsonNode);
	}

	public void setDirectory(File directory) {
		this.directory = directory;
		rootGroup.setDirectory(directory);
	}

	public File getDirectory() {
		return directory;
	}

	protected Object getParent() {
		return parent;
	}

	public void setName(String name) {
		if (!this.name.equals(name)) {
			String oldName = this.name;
			this.name = name;
			eventDispatcher.dispatchEvent(new CompositionNameChangeEvent(this, oldName, name));
		}
	}

	public String getName() {
		return name;
	}

	public LayerGroup getRootGroup() {
		return rootGroup;
	}

	public Layer getLayer(int layerId) {
		return rootGroup.getLayer(layerId);
	}

	public void addLayer(Layer layer) {
		if (!layer.hasLayerId()) {
			layer.setLayerId(nextLayerId++);
		}

		File layerDirectory = new File(directory, "layer_" + layer.getLayerId());
		layer.setDirectory(layerDirectory);

		rootGroup.addLayer(layer);
		int index = rootGroup.getLayerIndex(layer);

		// Add the event propagation handler to the layer
		layer.addEventHandler(LayerEvent.LAYER_ANY, layerEventPropagator);

		// Notify listeners of the layer addition
		eventDispatcher.dispatchEvent(new CompositionLayerAdditionEvent(this, layer, rootGroup, index));
	}

	public void addLayer(Layer layer, LayerGroup group, int index) {
		if (!layer.hasLayerId()) {
			layer.setLayerId(nextLayerId++);
		}

		File layerDirectory = new File(directory, "layer_" + layer.getLayerId());
		layer.setDirectory(layerDirectory);

		rootGroup.addLayer(layer, index);

		// Add the event propagation handler to the layer
		layer.addEventHandler(LayerEvent.LAYER_ANY, layerEventPropagator);

		// Notify listeners of the layer addition
		eventDispatcher.dispatchEvent(new CompositionLayerAdditionEvent(this, layer, group, index));
	}

	public void removeLayer(Layer layer) {
		LayerGroup group = getLayerParent(layer);
		int index = group.getLayerIndex(layer);
		rootGroup.removeLayer(layer);

		// Remove the propagation handler from the layer
		layer.removeEventHandler(LayerEvent.LAYER_ANY, layerEventPropagator);

		// Notify listeners of the layer removal
		eventDispatcher.dispatchEvent(new CompositionLayerRemovalEvent(this, layer, group, index));
	}

	public LayerGroup getLayerParent(Layer layer) {
		if (layer == rootGroup) return null;
		int[] layerIndex = getRecursiveLayerIndex(layer);
		int[] groupIndex = Arrays.copyOfRange(layerIndex, 0, layerIndex.length - 1);
		if (groupIndex.length == 0) {
			return rootGroup;
		}
		else {
			return (LayerGroup) rootGroup.getLayer(groupIndex);
		}
	}

	public int[] getRecursiveLayerIndex(Layer layer) {
		return rootGroup.getRecursiveLayerIndex(layer);
	}

	public void moveLayer(Layer layer, LayerGroup group, int index) {
		LayerGroup oldGroup = getLayerParent(layer);
		int oldIndex = oldGroup.getLayerIndex(layer);
		oldGroup.removeLayer(layer);

		group.addLayer(layer, index);

		// Notify listeners of the layer removal
		eventDispatcher.dispatchEvent(new CompositionLayerMoveEvent(this, layer, oldGroup, oldIndex, group, index));
	}

	public void dissolveGroup(LayerGroup group) {
		if (group == rootGroup) {
			throw new IllegalArgumentException("Can not dissolve the root group of a composition");
		}

		LayerGroup parentGroup = getLayerParent(group);
		int groupIndex = parentGroup.getLayerIndex(group);


		ArrayList<Layer> movingLayers = new ArrayList<>(group.getLayers());
		for (int i = movingLayers.size()-1; i >= 0; i--) {
			moveLayer(movingLayers.get(i), parentGroup, groupIndex);
		}
		removeLayer(group);
	}

	public void addEventHandler(EventType<? extends CompositionEvent> eventType, EventHandler<CompositionEvent> eventHandler) {
		eventDispatcher.addEventHandler(eventType, eventHandler);
	}

	public void removeEventHandler(EventType<? extends CompositionEvent> eventType, EventHandler<CompositionEvent> eventHandler) {
		eventDispatcher.removeEventHandler(eventType, eventHandler);
	}

	public JsonNode toJson() {
		ObjectMapper objectMapper = new ObjectMapper();
		ObjectNode objectNode = objectMapper.createObjectNode();

		objectNode.put("name", name);
		objectNode.put("nextlayerid", nextLayerId);
		objectNode.set("layers", objectMapper.valueToTree(rootGroup.getLayers()));

		return objectNode;
	}

	private void fromJson(JsonNode node) throws JsonProcessingException, IllegalArgumentException {
		name = node.get("name").asText("");
		nextLayerId = node.get("nextlayerid").asInt();

		ObjectMapper objectMapper = new ObjectMapper();
		List<Layer> layers = objectMapper.treeToValue(node.get("layers"), LayerList.class);
		layers.forEach(l -> {
			rootGroup.addLayer(l);
		});

		// Add the event propagation handler to all the layers
		rootGroup.forEachLayer(true, l -> {
			l.addEventHandler(LayerEvent.LAYER_ANY, layerEventPropagator);
		});
	}

	public void save() throws IOException {
		if (!directory.exists()) {
			directory.mkdir();
		}

		rootGroup.save();
	}

	// Custom list
	private static class LayerList extends ArrayList<Layer> {
		private static final long serialVersionUID = 1L;
	}
}
