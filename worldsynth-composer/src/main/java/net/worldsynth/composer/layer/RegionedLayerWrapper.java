/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.composer.layer;

import net.worldsynth.composer.ComposerSession;
import net.worldsynth.composition.event.RegionedLayerModifcationEvent;
import net.worldsynth.composition.layer.regioned.LayerRegion;
import net.worldsynth.composition.layer.regioned.RegionMap;
import net.worldsynth.composition.layer.regioned.RegionedLayer;

public abstract class RegionedLayerWrapper<T extends RegionedLayer<R>, R extends LayerRegion> extends LayerWrapper<T> {

	protected RegionMap<R> tempOldRegions = new RegionMap<>();

	public RegionedLayerWrapper(T layer, ComposerSession editorSession) {
		super(layer, editorSession);
	}

	@Override
	public void fireEditEvent() {
		RegionMap<R> tempNewRegions = new RegionMap<>();
		tempOldRegions.forEach((coord, region) -> {
			try {
				R newRegion = getLayer().getRegion(coord.x(), coord.z());
				tempNewRegions.put((R) newRegion.clone());
			} catch (CloneNotSupportedException e) {
				throw new RuntimeException(e);
			}
		});

		internallyFiredEvent = true;
		getLayer().fireEditEvent(new RegionedLayerModifcationEvent<>(getLayer(), RegionedLayerModifcationEvent.LAYER_MODIFIED, tempOldRegions, tempNewRegions));
		internallyFiredEvent = false;

		tempOldRegions = new RegionMap<>();
	}
	protected boolean internallyFiredEvent = false;
}
