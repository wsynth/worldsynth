/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.composer.layer.property;

import javafx.beans.property.DoublePropertyBase;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.WeakChangeListener;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import net.worldsynth.composition.layer.Layer;
import net.worldsynth.composition.event.LayerPropertyChangeEvent;

import java.util.function.Consumer;

public class DoubleLayerProperty extends DoublePropertyBase {
	
	private final String key;
	
	public DoubleLayerProperty(Layer layer, String key, double defaultValue) {
		super(defaultValue);
		this.key = key;

		// Configure initial value
		set(Double.parseDouble(layer.getProperty(key, String.valueOf(get()))));

		// Propagate the value down to the layer when the value changes
		addListener((observable, oldValue, newValue) -> {
			if (newValue.equals(oldValue)) return;
			layer.setProperty(key, String.valueOf(get()));
		});

		// Update the property value when the layer value changes
		layer.addEventHandler(LayerPropertyChangeEvent.LAYER_PROPERTY_CHANGED, e -> {
			LayerPropertyChangeEvent event = (LayerPropertyChangeEvent) e;
			String k = event.getPropertyKey();
			if (k.equals(key)) {
				double oldValue = Double.parseDouble(event.getOldValue());
				double newValue = Double.parseDouble(event.getNewValue());
				if (newValue == oldValue) return;
				set(newValue);
			}
		}, -1);
	}

	public TextField getNumberField(Consumer<Double> onChanged, double rangeMin, double rangeMax) {
		return new NumberField(onChanged, rangeMin, rangeMax);
	}
	
	@Override
	public Object getBean() {
		return null;
	}

	@Override
	public String getName() {
		return key;
	}

	private class NumberField extends TextField {

		private final ChangeListener<Number> propertyChangeListener;

		public NumberField(Consumer<Double> onChanged, double rangeMin, double rangeMax) {
			super(String.valueOf(get()));

			// Update the background coloring according to the validity of the input
			textProperty().addListener((ob, ov, nv) -> {
				if (validValueString(getText(), rangeMin, rangeMax)) {
					setStyle(null);
				}
				else {
					setStyle("-fx-background-color: RED;");
				}
			});

			// Apply change on ENTER
			setOnKeyReleased(e -> {
				if (e.getCode() == KeyCode.ENTER) {
					if (!validValueString(getText(), rangeMin, rangeMax)) return;

					double oldValue = get();
					double newValue = Double.parseDouble(getText().replace(',', '.'));

					if (newValue != oldValue) {
						set(newValue);
						onChanged.accept(get());
					}
				}
			});

			// Apply change on changing focus
			focusedProperty().addListener((ob, ov, nv) -> {
				if (!nv) {
					if (!validValueString(getText(), rangeMin, rangeMax)) {
						setText(String.valueOf(get()));
						return;
					}

					double oldValue = get();
					double newValue = Double.parseDouble(getText().replace(',', '.'));

					if (newValue != oldValue) {
						set(newValue);
						onChanged.accept(get());
					}
				}
			});

			propertyChangeListener = (observable, oldValue, newValue) -> {
				setText(String.valueOf(newValue));
			};
			addListener(new WeakChangeListener<>(propertyChangeListener));
		}

		private boolean validValueString(String valueString, double rangeMin, double rangeMax) {
			valueString = valueString.replace(',', '.');
			try {
				double v = Double.parseDouble(valueString);
				if (v < rangeMin || v > rangeMax) throw new Exception("Value is out of range");
				return true;
			} catch (Exception exception) {
				return false;
			}
		}
	}
}
