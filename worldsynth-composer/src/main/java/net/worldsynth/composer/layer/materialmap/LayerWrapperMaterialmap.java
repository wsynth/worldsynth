/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.composer.layer.materialmap;

import javafx.scene.image.Image;
import javafx.scene.input.DataFormat;
import javafx.scene.layout.Pane;
import net.worldsynth.composer.ComposerSession;
import net.worldsynth.composer.layer.LayerRender;
import net.worldsynth.composer.layer.RegionedLayerWrapper;
import net.worldsynth.composer.tool.masking.LayerMask;
import net.worldsynth.composer.tool.masking.MaterialmapMask;
import net.worldsynth.composition.layer.materialmap.LayerMaterialmap;
import net.worldsynth.composition.layer.materialmap.MaterialmapRegion;
import net.worldsynth.material.MaterialState;

public class LayerWrapperMaterialmap extends RegionedLayerWrapper<LayerMaterialmap, MaterialmapRegion> {
	
	public static final DataFormat DATAFORMAT_LAYER_MATERIALMAP = new DataFormat("application/ws-comp-layer-materialmap");
	
	private final LayerRenderMaterialmap render = new LayerRenderMaterialmap(this);
	
	public LayerWrapperMaterialmap(LayerMaterialmap layer, ComposerSession editorSession) {
		super(layer, editorSession);
	}
	
	@Override
	public DataFormat getLayerDataFormat() {
		return DATAFORMAT_LAYER_MATERIALMAP;
	}
	
	@Override
	public LayerRender<?> getLayerRender() {
		return render;
	}
	
	@Override
	public Image getDefaultLayerTumbnail() {
		return new Image(getClass().getClassLoader().getResourceAsStream("icons/layers/materialmap.png"));
	}
	
	public MaterialState<?, ?> getValueAt(int x, int z) {
		return layer.getValueAt(x, z);
	}
	
	public void applyValues(int x, int z, MaterialState<?, ?>[][] values) {
		int changeWidth = values.length;
		int changeLength = values[0].length;
		
		for (int u = 0; u < changeWidth; u++) {
			for (int v = 0; v < changeLength; v++) {
				applyValueAt(x+u, z+v, values[u][v]);
			}
		}
	}
	
	public void applyValueAt(int x, int z, MaterialState<?, ?> value) {
		int regionX = Math.floorDiv(x, 256);
		int regionZ = Math.floorDiv(z, 256);
		
		MaterialmapRegion region = layer.getRegion(regionX, regionZ);
		
		if (region == null) {
			region = new MaterialmapRegion(regionX, regionZ);
			layer.putRegion(region);
		}

		if (!tempOldRegions.containsRegion(regionX, regionZ)) {
			try {
				tempOldRegions.put(region.clone());
			} catch (CloneNotSupportedException e) {
				throw new RuntimeException(e);
			}
		}
		
		int rx = x - regionX*256;
		int rz = z - regionZ*256;
		
		
		region.regionValues[rx][rz] = value;
		render.queueRegion(region);
	}

	@Override
	public Pane layerPropertiesPane() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getCoordinateTooltip(double x, double z) {
		MaterialState value = getValueAt((int) x, (int) z);
		return getLayerName() + ": " + (value == null ? "null" : value.getDisplayName());
	}

	@Override
	public LayerMask createMask() {
		return new MaterialmapMask(this);
	}
}
