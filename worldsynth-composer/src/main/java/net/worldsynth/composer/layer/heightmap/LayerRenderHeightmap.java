/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.composer.layer.heightmap;

import javafx.beans.value.ObservableValue;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import net.worldsynth.composer.ComposerSession;
import net.worldsynth.composer.layer.RegionedLayerRender;
import net.worldsynth.composer.layer.property.ObjectLayerProperty;
import net.worldsynth.composition.layer.heightmap.HeightmapRegion;
import net.worldsynth.composition.layer.regioned.LayerRegion;

import java.util.Arrays;

public class LayerRenderHeightmap extends RegionedLayerRender<LayerWrapperHeightmap> {
	
	private final ObjectLayerProperty<HeightmapRenderStyle> propertyRenderStyle;
	
	private final RenderPropertiesPane renderPropertiesPane;
	
	public LayerRenderHeightmap(LayerWrapperHeightmap layer, ComposerSession editorSession) {
		super(layer);
		
		propertyRenderStyle = new ObjectLayerProperty<>(layer.getLayer(), "renderstyle", Arrays.asList(
				new HeightmapRenderStyleGrayscale(),
				new HeightmapRenderStyleShaded(),
				new HeightmapRenderStyleShadedSteps(),
				new HeightmapRenderStyleColored(this, editorSession)
		));

		propertyRenderStyle.addListener(o -> {
			clearRegions();
		});
		
		renderPropertiesPane = new RenderPropertiesPane(editorSession);
	}
	
	public ObjectLayerProperty<HeightmapRenderStyle> renderStyleProperty() {
		return propertyRenderStyle;
	}
	
	public void setRenderStyle(HeightmapRenderStyle style) {
		propertyRenderStyle.set(style);
	}
	
	public HeightmapRenderStyle getRenderStyle() {
		return propertyRenderStyle.get();
	}
	
	@Override
	protected Image renderRegion(LayerWrapperHeightmap layer, LayerRegion region) {
		return renderStyleProperty().get().renderRegion(layer, (HeightmapRegion) region);
	}
	
	@Override
	protected int getRegionSize() {
		return 256;
	}
	
	@Override
	protected Color defaultRenderColor() {
		return getRenderStyle().defaultColor();
	}
	
	protected Pane getPropertiesPane() {
		return renderPropertiesPane;
	}
	
	private class RenderPropertiesPane extends GridPane {

		public RenderPropertiesPane(ComposerSession editorSession) {
			setHgap(2.0);
			setVgap(5.0);
			
			// Render style
			Label styleLabel = new Label("Render style: ");
			add(styleLabel, 0, 0);
			
			ComboBox<HeightmapRenderStyle> renderStyleComboBox = propertyRenderStyle.getComboBox(e -> {
				editorSession.commitHistory();
			});
			renderStyleComboBox.setMaxWidth(500);
			renderStyleComboBox.setMinHeight(USE_PREF_SIZE);
			add(renderStyleComboBox, 1, 0);
			
			// Style properties
			BorderPane stylePropertiesWrapperPane = new BorderPane();
			if (propertyRenderStyle.get().getPropertiesPane() != null) {
				stylePropertiesWrapperPane.setRight(propertyRenderStyle.get().getPropertiesPane());
				add(stylePropertiesWrapperPane, 0, 1, 2, 1);
			}

			propertyRenderStyle.addListener((ObservableValue<? extends HeightmapRenderStyle> observable, HeightmapRenderStyle oldValue, HeightmapRenderStyle newValue) -> {
				stylePropertiesWrapperPane.setRight(newValue.getPropertiesPane());

				if (oldValue.getPropertiesPane() != null) {
					getChildren().remove(stylePropertiesWrapperPane);
				}
				if (newValue.getPropertiesPane() != null) {
					add(stylePropertiesWrapperPane, 0, 1, 2, 1);
				}
			});
		}
	}
}
