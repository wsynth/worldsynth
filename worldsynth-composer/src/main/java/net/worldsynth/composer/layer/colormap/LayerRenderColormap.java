/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.composer.layer.colormap;

import javafx.scene.image.Image;
import javafx.scene.image.PixelWriter;
import javafx.scene.image.WritableImage;
import javafx.scene.paint.Color;
import net.worldsynth.composer.layer.RegionedLayerRender;
import net.worldsynth.composition.layer.colormap.ColormapRegion;
import net.worldsynth.composition.layer.regioned.LayerRegion;

public class LayerRenderColormap extends RegionedLayerRender<LayerWrapperColormap> {
	
	public LayerRenderColormap(LayerWrapperColormap layer) {
		super(layer);
	}
	
	@Override
	protected Image renderRegion(LayerWrapperColormap layer, LayerRegion region) {
		ColormapRegion colorRegion = (ColormapRegion) region;
		
		WritableImage regionRenderImage = new WritableImage(256, 256);
		PixelWriter pw = regionRenderImage.getPixelWriter();
		
		for (int u = 0; u < 256; u++) {
			for (int v = 0; v < 256; v++) {
				pw.setColor(u, v, Color.color(colorRegion.regionValues[u][v][0], colorRegion.regionValues[u][v][1], colorRegion.regionValues[u][v][2]));
			}
		}
		
		return regionRenderImage;
	}

	@Override
	protected int getRegionSize() {
		return 256;
	}

	@Override
	protected Color defaultRenderColor() {
		return null;
	}
}
