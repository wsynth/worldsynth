/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.composer.layer;

import javafx.scene.canvas.GraphicsContext;
import net.worldsynth.composer.ui.navcanvas.NavigationalCanvas;

public class LayerRenderNull extends LayerRender<LayerWrapperNull> {

	public LayerRenderNull(LayerWrapperNull layer) {
		super(layer);
	}

	@Override
	protected void updateRender(LayerWrapperNull layer, GraphicsContext g, NavigationalCanvas navCanvas) {
		g.clearRect(0, 0, 10000, 10000);
	}
}
