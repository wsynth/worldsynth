/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.composer.layer;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.image.Image;
import javafx.scene.input.DataFormat;
import javafx.scene.layout.Pane;
import net.worldsynth.composer.ComposerSession;
import net.worldsynth.composer.layer.featuremap.LayerWrapperFeaturemap;
import net.worldsynth.composer.layer.heightmap.LayerWrapperHeightmap;
import net.worldsynth.composer.layer.materialmap.LayerWrapperMaterialmap;
import net.worldsynth.composer.tool.masking.LayerMask;
import net.worldsynth.composition.layer.Layer;
import net.worldsynth.composition.layer.LayerGroup;
import net.worldsynth.composition.layer.featuremap.LayerFeaturemap;
import net.worldsynth.composition.layer.heightmap.LayerHeightmap;
import net.worldsynth.composition.layer.materialmap.LayerMaterialmap;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.ListIterator;
import java.util.function.Consumer;

public class LayerGroupWrapper extends LayerWrapper<LayerGroup> {

	public static final DataFormat DATAFORMAT_LAYER_GROUP = new DataFormat("application/ws-comp-layer-group");

	private final ObservableList<LayerWrapper<?>> layerWrappers = FXCollections.observableArrayList();

	public LayerGroupWrapper(LayerGroup layer, ComposerSession editorSession) {
		super(layer, editorSession);

		for (Layer l : layer.getLayers()) {
			layerWrappers.add(wrapLayer(l));
		}

		layer.setOnListChange(() -> {
			ArrayList<Layer> wrapperLayers = new ArrayList<>();

			// Check for removed layers and remove wrappers accordingly
			Iterator<LayerWrapper<?>> lwit = layerWrappers.iterator();
			while (lwit.hasNext()) {
				LayerWrapper<?> lw = lwit.next();
				if (!layer.getLayers().contains(lw.getLayer())) {
					lwit.remove();
					continue;
				}
				// If the layer was not removed, remember it for later, to check if it was added.
				wrapperLayers.add(lw.getLayer());
			}

			// Checked for added layers and add wrapper accordingly
			for (Layer l : layer.getLayers()) {
				if (!wrapperLayers.contains(l)) {
					layerWrappers.add(wrapLayer(l));
				}
			}

			// Sort wrappers list according to layer order
			layerWrappers.sort((LayerWrapper<?> o1, LayerWrapper<?> o2) -> {
				int i1 = layer.getLayerIndex(o1.getLayer());
				int i2 = layer.getLayerIndex(o2.getLayer());
				return i1 - i2;
			});
		});
	}

	private LayerWrapper<?> wrapLayer(Layer layer) {
		if (layer instanceof LayerGroup) {
			return new LayerGroupWrapper((LayerGroup) layer, editorSession);
		}
		else if (layer instanceof LayerHeightmap) {
			return new LayerWrapperHeightmap((LayerHeightmap) layer, editorSession);
		}
		else if (layer instanceof LayerMaterialmap) {
			return new LayerWrapperMaterialmap((LayerMaterialmap) layer, editorSession);
		}
		else if (layer instanceof LayerFeaturemap) {
			return new LayerWrapperFeaturemap((LayerFeaturemap) layer, editorSession);
		}
		throw new IllegalArgumentException("Layer type has no layer wrapper");
	}

	public LayerWrapper<?> getLayerWrapper(Layer layer) {
		return getLayerWrapper(layer, false);
	}

	public LayerWrapper<?> getLayerWrapper(Layer layer, boolean recursive) {
		for (LayerWrapper<?> lw : layerWrappers) {
			if (lw.getLayer() == layer) {
				return lw;
			}
			else if (recursive && lw instanceof LayerGroupWrapper) {
				LayerGroupWrapper gw = (LayerGroupWrapper) lw;
				lw = gw.getLayerWrapper(layer, true);
				if (lw != null) {
					return lw;
				}
			}
		}
		return null;
	}

	public ObservableList<LayerWrapper<?>> getObservableLayersList() {
		return layerWrappers;
	}

	public void forEachLayer(boolean recursive, Consumer<LayerWrapper> action) {
		layerWrappers.forEach(l -> {
			action.accept(l);
			if (l instanceof LayerGroupWrapper && recursive) {
				((LayerGroupWrapper) l).forEachLayer(true, action);
			}
		});
	}

	public Iterator<LayerWrapper<?>> layersIterator() {
		return layerWrappers.listIterator();
	}

	public Iterator<LayerWrapper<?>> layersReverseIterator() {
		return new Iterator<>() {
			ListIterator<LayerWrapper<?>> layerIterator = layerWrappers.listIterator(layerWrappers.size());

			@Override
			public boolean hasNext() {
				return layerIterator.hasPrevious();
			}

			@Override
			public LayerWrapper<?> next() {
				return layerIterator.previous();
			}
		};
	}

	@Override
	public DataFormat getLayerDataFormat() {
		return DATAFORMAT_LAYER_GROUP;
	}

	@Override
	public LayerRender<?> getLayerRender() {
		return null;
	}

	@Override
	public Image getDefaultLayerTumbnail() {
		return new Image(getClass().getClassLoader().getResourceAsStream("icons/layers/group.png"));
	}

	@Override
	public Pane layerPropertiesPane() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void fireEditEvent() {

	}

	@Override
	public String getCoordinateTooltip(double x, double z) {
		StringBuilder sb = new StringBuilder();
		for (LayerWrapper lw: layerWrappers) {
			String tooltip = lw.getCoordinateTooltip(x, z);
			if (tooltip != null) {
				sb.append(tooltip);
				sb.append("        ");
			}
		}

		if (sb.length() == 0) {
			return null;
		}

		sb.setLength(sb.length() - 8);
		return sb.toString();
	}

	@Override
	public LayerMask createMask() {
		return null;
	}
}
