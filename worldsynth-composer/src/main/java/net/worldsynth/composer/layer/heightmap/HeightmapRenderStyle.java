/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.composer.layer.heightmap;

import javafx.scene.image.Image;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import net.worldsynth.composition.layer.heightmap.HeightmapRegion;

public abstract class HeightmapRenderStyle {
	
	public abstract Color defaultColor();
	
	public abstract Image renderRegion(LayerWrapperHeightmap layer, HeightmapRegion region);
	
	public abstract Pane getPropertiesPane();
}
