/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.composer.layer.heightmap;

import javafx.scene.image.Image;
import javafx.scene.image.PixelFormat;
import javafx.scene.image.PixelWriter;
import javafx.scene.image.WritableImage;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import net.worldsynth.composition.layer.heightmap.HeightmapRegion;

public class HeightmapRenderStyleGrayscale extends HeightmapRenderStyle {
	
	@Override
	public Color defaultColor() {
		return Color.BLACK;
	}
	
	@Override
	public Image renderRegion(LayerWrapperHeightmap layer, HeightmapRegion region) {
		WritableImage regionRenderImage = new WritableImage(256, 256);
		PixelWriter pw = regionRenderImage.getPixelWriter();
		
		// TODO Get normalized height from synth
		float normalizedHeight = 256.0f;
		
		int[] buffer = new int[256*256];
		for (int u = 0; u < 256; u++) {
			for (int v = 0; v < 256; v++) {
				float s = region.regionValues[u][v] / normalizedHeight;
				int gray = (int) (Math.max(Math.min(s, 1.0f), 0.0f) * 255.0f);
				buffer[u+v*256] = (255 << 24) | (gray << 16) | (gray << 8) | gray;
			}
		}
		
		pw.setPixels(0, 0, 256, 256, PixelFormat.getIntArgbPreInstance(), buffer, 0, 256);
		
		return regionRenderImage;
	}
	
	@Override
	public String toString() {
		return "Grayscale";
	}

	@Override
	public Pane getPropertiesPane() {
		return null;
	}
}
