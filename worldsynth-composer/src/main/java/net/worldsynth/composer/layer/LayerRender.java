/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.composer.layer;

import javafx.scene.canvas.GraphicsContext;
import net.worldsynth.composer.ui.navcanvas.NavigationalCanvas;

public abstract class LayerRender<T extends LayerWrapper<?>> {
	private final T layer;
	
	public LayerRender(T layer) {
		this.layer = layer;
	}
	
	public T getLayer() {
		return layer;
	}
	
	public final void updateRender(GraphicsContext g, NavigationalCanvas navCanvas) {
		updateRender(layer, g, navCanvas);
	}
	
	public final void clearRender(GraphicsContext g, NavigationalCanvas navCanvas) {
		g.clearRect(0, 0, navCanvas.getWidth(), navCanvas.getHeight());
	}
	
	protected abstract void updateRender(T layer, GraphicsContext g, NavigationalCanvas navCanvas);
}
