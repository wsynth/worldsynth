/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.composer.layer.featuremap;

import javafx.scene.image.Image;
import javafx.scene.input.DataFormat;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import net.worldsynth.composer.ComposerSession;
import net.worldsynth.composer.layer.LayerRender;
import net.worldsynth.composer.layer.RegionedLayerWrapper;
import net.worldsynth.composer.tool.masking.LayerMask;
import net.worldsynth.composition.layer.featuremap.FeaturemapRegion;
import net.worldsynth.composition.layer.featuremap.LayerFeaturemap;
import net.worldsynth.extent.Extent;
import net.worldsynth.featurepoint.Featurepoint2D;

import java.util.List;

public class LayerWrapperFeaturemap extends RegionedLayerWrapper<LayerFeaturemap, FeaturemapRegion> {
	
	public static final DataFormat DATAFORMAT_LAYER_FEATUREMAP = new DataFormat("application/ws-comp-layer-featuremap");
	
	private final LayerRenderFeaturemap layerRender;
	
	private final LayerPropertiesPane propertiesPane;
	
	public LayerWrapperFeaturemap(LayerFeaturemap layer, ComposerSession editorSession) {
		super(layer, editorSession);
		
		layerRender = new LayerRenderFeaturemap(this);
		propertiesPane = new LayerPropertiesPane();
	}
	
	@Override
	public DataFormat getLayerDataFormat() {
		return DATAFORMAT_LAYER_FEATUREMAP;
	}
	
	@Override
	public LayerRender<?> getLayerRender() {
		return layerRender;
	}
	
	@Override
	public Image getDefaultLayerTumbnail() {
		return new Image(getClass().getClassLoader().getResourceAsStream("icons/layers/featuremap.png"));
	}
	
	public List<Featurepoint2D> getFeatures(Extent extent) {
		return layer.getFeatures(extent);
	}
	
	public void addFeatures(List<Featurepoint2D> features) {
		for (Featurepoint2D f: features) {
			addFeature(f);
		}
	}
	
	public void addFeature(Featurepoint2D feature) {
		int regionX = (int) Math.floor(feature.getX() / 256);
		int regionZ = (int) Math.floor(feature.getZ() / 256);
		
		FeaturemapRegion region = layer.getRegion(regionX, regionZ);
		
		if (region == null) {
			region = new FeaturemapRegion(regionX, regionZ);
			layer.putRegion(region);
		}

		if (!tempOldRegions.containsRegion(regionX, regionZ)) {
			try {
				tempOldRegions.put(region.clone());
			} catch (CloneNotSupportedException e) {
				throw new RuntimeException(e);
			}
		}
		
		region.features.add(feature);
	}
	
	public void removeFeatures(List<Featurepoint2D> features) {
		for (Featurepoint2D f: features) {
			removeFeature(f);
		}
	}
	
	public void removeFeature(Featurepoint2D feature) {
		int regionX = (int) Math.floor(feature.getX() / 256);
		int regionZ = (int) Math.floor(feature.getZ() / 256);
		
		FeaturemapRegion region = layer.getRegion(regionX, regionZ);
		
		if (region == null) {
			region = new FeaturemapRegion(regionX, regionZ);
			layer.putRegion(region);
		}

		if (!tempOldRegions.containsRegion(regionX, regionZ)) {
			try {
				tempOldRegions.put(region.clone());
			} catch (CloneNotSupportedException e) {
				throw new RuntimeException(e);
			}
		}
		
		region.features.remove(feature);
	}

	@Override
	public Pane layerPropertiesPane() {
		return propertiesPane;
	}
	
	private class LayerPropertiesPane extends GridPane {
		
		public LayerPropertiesPane() {
//			add(layerRender.getPropertiesPane(), 0, 0);
		}
	}

	@Override
	public String getCoordinateTooltip(double x, double z) {
		return null;
	}

	@Override
	public LayerMask createMask() {
		return null;
	}
}
