/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.composer.layer.featuremap;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import net.worldsynth.composer.layer.LayerRender;
import net.worldsynth.composer.ui.navcanvas.Coordinate;
import net.worldsynth.composer.ui.navcanvas.NavigationalCanvas;
import net.worldsynth.composer.ui.navcanvas.Pixel;
import net.worldsynth.extent.Extent;
import net.worldsynth.featurepoint.Featurepoint2D;

public class LayerRenderFeaturemap extends LayerRender<LayerWrapperFeaturemap> {
	
	public LayerRenderFeaturemap(LayerWrapperFeaturemap layer) {
		super(layer);
	}

	@Override
	protected void updateRender(LayerWrapperFeaturemap layer, GraphicsContext g, NavigationalCanvas navCanvas) {
		g.clearRect(0, 0, navCanvas.getWidth(), navCanvas.getHeight());
		
		Coordinate c = new Coordinate(new Pixel(0, 0), navCanvas);
		double zoomedCanvasWidth = navCanvas.getWidth() / navCanvas.getZoom();
		double zoomedCanvasLength = navCanvas.getHeight()/ navCanvas.getZoom();
		
		Extent extent = new Extent(c.x, 0, c.y, zoomedCanvasWidth, 0, zoomedCanvasLength);
		
		for (Featurepoint2D f: layer.getFeatures(extent)) {
			drawFeature(layer, f, g, navCanvas);
		}
	}
	
	private void drawFeature(LayerWrapperFeaturemap layer, Featurepoint2D feature, GraphicsContext g, NavigationalCanvas navCanvas) {
		Pixel f = new Pixel(new Coordinate(feature.getX(), feature.getZ()), navCanvas);
		g.setStroke(Color.WHITE);
		g.strokeOval(f.x-5*navCanvas.getZoom(), f.y-5*navCanvas.getZoom(), 10*navCanvas.getZoom(), 10*navCanvas.getZoom());
		g.strokeLine(f.x, f.y-7*navCanvas.getZoom(), f.x, f.y+7*navCanvas.getZoom());
		g.strokeLine(f.x-7*navCanvas.getZoom(), f.y, f.x+7*navCanvas.getZoom(), f.y);
	}
}
