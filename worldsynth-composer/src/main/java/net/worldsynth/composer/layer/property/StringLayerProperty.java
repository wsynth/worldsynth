/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.composer.layer.property;

import javafx.beans.property.StringPropertyBase;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.WeakChangeListener;
import javafx.scene.input.KeyCode;
import net.worldsynth.composition.layer.Layer;
import net.worldsynth.composition.event.LayerPropertyChangeEvent;

import java.util.function.Consumer;

public class StringLayerProperty extends StringPropertyBase {

	private final String key;
	
	public StringLayerProperty(Layer layer, String key, String defaultValue) {
		super(defaultValue);
		this.key = key;

		// Configure initial value
		set(layer.getProperty(key, get()));

		// Propagate the value down to the layer when the value changes
		addListener((observable, oldValue, newValue) -> {
			if (newValue.equals(oldValue)) return;
			layer.setProperty(key, get());
		});

		// Update the property value when the layer value changes
		layer.addEventHandler(LayerPropertyChangeEvent.LAYER_PROPERTY_CHANGED, e -> {
			LayerPropertyChangeEvent event = (LayerPropertyChangeEvent) e;
			String k = event.getPropertyKey();
			if (k.equals(key)) {
				if (event.getNewValue().equals(event.getOldValue())) return;
				set(event.getNewValue());
			}
		}, -1);
	}

	public javafx.scene.control.TextField getTextField(Consumer<String> onChanged) {
		return new TextField(onChanged);
	}
	
	@Override
	public Object getBean() {
		return null;
	}

	@Override
	public String getName() {
		return key;
	}

	private class TextField extends javafx.scene.control.TextField {

		private final ChangeListener<String> propertyChangeListener;

		public TextField(Consumer<String> onChanged) {
			super(get());

			// Apply change on ENTER
			setOnKeyReleased(e -> {
				if (e.getCode() == KeyCode.ENTER) {
					String oldValue = get();
					String newValue = getText();

					if (!newValue.equals(oldValue)) {
						set(newValue);
						onChanged.accept(get());
					}
				}
			});

			// Apply change on changing focus
			focusedProperty().addListener((ob, ov, nv) -> {
				if (!nv) {
					String oldValue = get();
					String newValue = getText();

					if (!newValue.equals(oldValue)) {
						set(newValue);
						onChanged.accept(get());
					}
				}
			});

			propertyChangeListener = (observable, oldValue, newValue) -> {
				setText(newValue);
			};
			addListener(new WeakChangeListener<>(propertyChangeListener));
		}
	}
}
