/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.composer.layer.property;

import javafx.beans.property.ObjectPropertyBase;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.WeakChangeListener;
import javafx.collections.FXCollections;
import javafx.scene.control.ComboBox;
import net.worldsynth.composition.layer.Layer;
import net.worldsynth.composition.event.LayerPropertyChangeEvent;

import java.util.function.Consumer;

public class EnumLayerProperty<E extends Enum<E>> extends ObjectPropertyBase<E> {
	
	private final String key;
	private final Class<E> enumClass;
	
	public EnumLayerProperty(Layer layer, String key, E defaultValue, Class<E> enumClass) {
		super(defaultValue);
		this.key = key;
		this.enumClass = enumClass;

		// Configure initial value
		String stringified = layer.getProperty(key, get().name());
		set(parseFromStringified(stringified, defaultValue));

		// Propagate the value down to the layer when the value changes
		addListener((observable, oldValue, newValue) -> {
			if (newValue.equals(oldValue)) return;
			layer.setProperty(key, get().name());
		});

		// Update the property value when the layer value changes
		layer.addEventHandler(LayerPropertyChangeEvent.LAYER_PROPERTY_CHANGED, e -> {
			LayerPropertyChangeEvent event = (LayerPropertyChangeEvent) e;
			String k = event.getPropertyKey();
			if (k.equals(key)) {
				E oldValue = parseFromStringified(event.getOldValue(), null);
				E newValue = parseFromStringified(event.getNewValue(), null);
				if (newValue.equals(oldValue)) return;
				set(newValue);
			}
		}, -1);
	}

	private E parseFromStringified(String stringified, E defaultValue) {
		for (E type: enumClass.getEnumConstants()) {
			if (stringified.equals(type.name())) {
				return type;
			}
		}
		return defaultValue;
	}

	public ComboBox<E> getComboBox(Consumer<E> onChanged) {
		return new EnumSelector(onChanged);
	}
	
	@Override
	public Object getBean() {
		return null;
	}

	@Override
	public String getName() {
		return key;
	}

	private class EnumSelector extends ComboBox<E> {

		private final ChangeListener<E> propertyChangeListener;

		public EnumSelector(Consumer<E> onChanged) {
			super(FXCollections.observableArrayList(enumClass.getEnumConstants()));
			setValue(get());

			// Apply change
			setOnAction(e -> {
				E oldValue = get();
				E newValue = getValue();

				if (!newValue.equals(oldValue)) {
					set(newValue);
					onChanged.accept(get());
				}
			});

			propertyChangeListener = (observable, oldValue, newValue) -> {
				setValue(newValue);
			};
			addListener(new WeakChangeListener<>(propertyChangeListener));
		}
	}
}
