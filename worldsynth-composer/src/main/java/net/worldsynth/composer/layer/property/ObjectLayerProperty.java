/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.composer.layer.property;

import java.util.List;
import java.util.function.Consumer;

import javafx.beans.property.ObjectPropertyBase;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.WeakChangeListener;
import javafx.collections.FXCollections;
import javafx.scene.control.ComboBox;
import net.worldsynth.composition.layer.Layer;
import net.worldsynth.composition.event.LayerPropertyChangeEvent;

public class ObjectLayerProperty<E> extends ObjectPropertyBase<E> {
	
	private final String key;
	private final List<E> selectables;
	
	public ObjectLayerProperty(Layer layer, String key, List<E> selectables) {
		this(layer, key, selectables.get(0), selectables);
	}
	
	public ObjectLayerProperty(Layer layer, String key, E defaultValue, List<E> selectables) {
		super(defaultValue);
		this.key = key;
		this.selectables = selectables;

		// Configure initial value
		String stringified = layer.getProperty(key, get().toString());
		super.set(parseFromStringified(stringified, defaultValue));

		// Propagate the value down to the layer when the value changes
		addListener((observable, oldValue, newValue) -> {
			if (newValue.equals(oldValue)) return;
			layer.setProperty(key, get().toString());
		});

		// Update the property value when the layer value changes
		layer.addEventHandler(LayerPropertyChangeEvent.LAYER_PROPERTY_CHANGED, e -> {
			LayerPropertyChangeEvent event = (LayerPropertyChangeEvent) e;
			String k = event.getPropertyKey();
			if (k.equals(this.key)) {
				E oldValue = parseFromStringified(event.getOldValue(), null);
				E newValue = parseFromStringified(event.getNewValue(), null);
				if (newValue.equals(oldValue)) return;
				set(newValue);
			}
		}, -1);
	}

	private E parseFromStringified(String stringified, E defaultValue) {
		for (E type: selectables) {
			if (stringified.equals(type.toString())) {
				return type;
			}
		}
		return defaultValue;
	}

	public ComboBox<E> getComboBox(Consumer<E> onChanged) {
		return new ObjectSelector(onChanged);
	}
	
	@Override
	public Object getBean() {
		return null;
	}

	@Override
	public String getName() {
		return key;
	}
	
	@Override
	public void set(E newValue) {
		for (E validValue: selectables) {
			if (newValue == validValue) {
				super.set(newValue);
				return;
			}
		}
		throw new IllegalArgumentException("Value must be set to a value among valid values for the property");
	}
	
	@Override
	public void setValue(E v) {
		for (E validValue: selectables) {
			if (v == validValue) {
				super.setValue(v);
				return;
			}
		}
		throw new IllegalArgumentException("Value must be set to a value among valid values for the property");
	}

	private class ObjectSelector extends ComboBox<E> {

		private final ChangeListener<E> propertyChangeListener;

		public ObjectSelector(Consumer<E> onChanged) {
			super(FXCollections.observableArrayList(selectables));
			setValue(get());

			// Apply change
			setOnAction(e -> {
				E oldValue = get();
				E newValue = getValue();

				if (!newValue.equals(oldValue)) {
					set(newValue);
					onChanged.accept(get());
				}
			});

			propertyChangeListener = (observable, oldValue, newValue) -> {
				setValue(newValue);
			};
			addListener(new WeakChangeListener<>(propertyChangeListener));
		}
	}
}
