/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.composer.layer.colormap;

import javafx.scene.image.Image;
import javafx.scene.input.DataFormat;
import javafx.scene.layout.Pane;
import net.worldsynth.composer.ComposerSession;
import net.worldsynth.composer.layer.LayerRender;
import net.worldsynth.composer.layer.RegionedLayerWrapper;
import net.worldsynth.composer.tool.masking.LayerMask;
import net.worldsynth.composition.layer.colormap.ColormapRegion;
import net.worldsynth.composition.layer.colormap.LayerColormap;

public class LayerWrapperColormap extends RegionedLayerWrapper<LayerColormap, ColormapRegion> {

	public static final DataFormat DATAFORMAT_LAYER_COLORMAP = new DataFormat("application/ws-comp-layer-colormap");
	
	private final LayerRenderColormap render = new LayerRenderColormap(this);
	
	public LayerWrapperColormap(LayerColormap layer, ComposerSession editorSession) {
		super(layer, editorSession);
	}
	
	@Override
	public DataFormat getLayerDataFormat() {
		return DATAFORMAT_LAYER_COLORMAP;
	}
	
	@Override
	public LayerRender<?> getLayerRender() {
		return render;
	}
	
	@Override
	public Image getDefaultLayerTumbnail() {
		return new Image(getClass().getClassLoader().getResourceAsStream("icons/layers/colormap.png"));
	}
	
	public float[] getColorAt(int x, int z) {
		return layer.getColorAt(x, z);
	}
	
	public void applyColors(int x, int z, float[][][] values) {
		int changeWidth = values.length;
		int changeLength = values[0].length;
		
		for (int u = 0; u < changeWidth; u++) {
			for (int v = 0; v < changeLength; v++) {
				applyColorAt(x+u, z+v, values[u][v]);
			}
		}
	}
	
	public void applyColorAt(int x, int z, float[] value) {
		int regionX = Math.floorDiv(x, 256);
		int regionZ = Math.floorDiv(z, 256);
		
		ColormapRegion region = layer.getRegion(regionX, regionZ);
		
		if (region == null) {
			region = new ColormapRegion(regionX, regionZ);
			layer.putRegion(region);
		}

		if (!tempOldRegions.containsRegion(regionX, regionZ)) {
			try {
				tempOldRegions.put(region.clone());
			} catch (CloneNotSupportedException e) {
				throw new RuntimeException(e);
			}
		}
		
		int rx = x - regionX*256;
		int rz = z - regionZ*256;
		
		
		region.regionValues[rx][rz] = value;
		render.queueRegion(region);
	}
	
	public void applyColorChanges(int x, int z, float[][][] changes, float factor) {
		int changeWidth = changes.length;
		int changeLength = changes[0].length;
		
		for (int u = 0; u < changeWidth; u++) {
			for (int v = 0; v < changeLength; v++) {
				applyColorChangeAt(x+u, z+v, changes[u][v], factor);
			}
		}
	}
	
	public void applyColorChangeAt(int x, int z, float[] change, float factor) {
		int regionX = Math.floorDiv(x, 256);
		int regionZ = Math.floorDiv(z, 256);
		
		ColormapRegion region = layer.getRegion(regionX, regionZ);
		
		if (region == null) {
			region = new ColormapRegion(regionX, regionZ);
			layer.putRegion(region);
		}

		if (!tempOldRegions.containsRegion(regionX, regionZ)) {
			try {
				tempOldRegions.put(region.clone());
			} catch (CloneNotSupportedException e) {
				throw new RuntimeException(e);
			}
		}
		
		int rx = x - regionX*256;
		int rz = z - regionZ*256;
		
		for (int i = 0; i < 3; i++) {
			region.regionValues[rx][rz][i] = Math.min(1.0f, Math.max(0.0f, region.regionValues[rx][rz][i] + change[i] * factor));
		}
		render.queueRegion(region);
	}

	@Override
	public Pane layerPropertiesPane() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getCoordinateTooltip(double x, double z) {
		float[] value = getColorAt((int) x, (int) z);
		return "Color: " + (int) (value[0] * 255) + ", " + (int) (value[1] * 255) + ", " + (int) (value[2] * 255);
	}

	@Override
	public LayerMask createMask() {
		return null;
	}
}
