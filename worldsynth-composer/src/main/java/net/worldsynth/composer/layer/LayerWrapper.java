/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.composer.layer;

import javafx.beans.property.SimpleObjectProperty;
import javafx.scene.effect.BlendMode;
import javafx.scene.image.Image;
import javafx.scene.input.DataFormat;
import javafx.scene.layout.Pane;
import net.worldsynth.composer.ComposerSession;
import net.worldsynth.composer.layer.property.BooleanLayerProperty;
import net.worldsynth.composer.layer.property.DoubleLayerProperty;
import net.worldsynth.composer.layer.property.EnumLayerProperty;
import net.worldsynth.composer.layer.property.StringLayerProperty;
import net.worldsynth.composer.tool.masking.LayerMask;
import net.worldsynth.composition.layer.Layer;

public abstract class LayerWrapper<T extends Layer> {

	public static final DataFormat DATAFORMAT_LAYER = new DataFormat("application/ws-comp-layer");
	
	protected final T layer;
	protected final ComposerSession editorSession;
	
	private final StringLayerProperty propertyLayerName;

	private final EnumLayerProperty<BlendMode> propertyLayerPreviewBlendMode;
	private final DoubleLayerProperty propertyLayerPreviewOpacity;
	private final BooleanLayerProperty propertyLayerLocked;
	private final BooleanLayerProperty propertyLayerActive;
	
	private final SimpleObjectProperty<Image> propertyLayerTumbnail;
	
	public LayerWrapper(T layer, ComposerSession editorSession) {
		this.layer = layer;
		this.editorSession = editorSession;
		
		propertyLayerName = new StringLayerProperty(layer, "name", "");
		propertyLayerPreviewBlendMode = new EnumLayerProperty<>(layer, "previewblendmode", BlendMode.ADD, BlendMode.class);
		propertyLayerPreviewOpacity = new DoubleLayerProperty(layer, "opacity", 1.0);
		propertyLayerLocked = new BooleanLayerProperty(layer, "locked", false);
		propertyLayerActive = new BooleanLayerProperty(layer, "active", true);
		
		propertyLayerTumbnail = new SimpleObjectProperty<>(getDefaultLayerTumbnail());
	}
	
	public T getLayer() {
		return layer;
	}

	/**
	 * Get the data format identifier for the layer. This is used for identifying
	 * the layer on a clipboard or dragboard.
	 * <p>
	 * All implementations should define a data format with a custom mime type for
	 * the implemented layer type. "application/ws-comp-layer-[type]".
	 * <p>
	 * Fore a general data format to be associated with any type of layer, see
	 * {@link LayerWrapper#DATAFORMAT_LAYER}.
	 * <p>
	 * The value to be associated with the data format on clipboard or dragboard is
	 * the layer id referencing the given layer.
	 * 
	 * @return Data format for the layer type.
	 */
	public abstract DataFormat getLayerDataFormat();
	
	public abstract LayerRender<?> getLayerRender();

	public StringLayerProperty layerNameProperty() {
		return propertyLayerName;
	}
	
	public void setLayerName(String layerName) {
		layerNameProperty().set(layerName);
	}
	
	public String getLayerName() {
		return layerNameProperty().get();
	}

	public EnumLayerProperty<BlendMode> layerPreviewBlendmodeProperty() {
		return propertyLayerPreviewBlendMode;
	}
	
	public void setLayerPreviewBlendMode(BlendMode blendMode) {
		layerPreviewBlendmodeProperty().set(blendMode);
	}
	
	public BlendMode getLayerPreviewBlendMode() {
		return layerPreviewBlendmodeProperty().get();
	}
	
	public DoubleLayerProperty layerPreviewOpacityProperty() {
		return propertyLayerPreviewOpacity;
	}
	
	public void setLayerPreviewOpacity(double opacity) {
		layerPreviewOpacityProperty().set(opacity);
	}
	
	public double getLayerPreviewOpacity() {
		return layerPreviewOpacityProperty().get();
	}
	
	public BooleanLayerProperty layerLockedProperty() {
		return propertyLayerLocked;
	}
	
	public void setLayerLocked(boolean layerLocked) {
		layerLockedProperty().set(layerLocked);
	}
	
	public boolean isLayerLocked() {
		return layerLockedProperty().get();
	}
	
	public BooleanLayerProperty layerActiveProperty() {
		return propertyLayerActive;
	}
	
	public void setLayerActive(boolean layerActive) {
		layerActiveProperty().set(layerActive);;
	}
	
	public boolean isLayerActive() {
		return layerActiveProperty().get();
	}
	
	public SimpleObjectProperty<Image> layerTumbnailProperty() {
		return propertyLayerTumbnail;
	}
	
	public void setLayerTumbnail(Image layerTumbnail) {
		layerTumbnailProperty().set(layerTumbnail);
	}
	
	public Image getLayerTumbnail() {
		return layerTumbnailProperty().get();
	}
	
	public abstract Image getDefaultLayerTumbnail();
	
	public abstract Pane layerPropertiesPane();

	public abstract void fireEditEvent();

	public abstract String getCoordinateTooltip(double x, double z);

	public abstract LayerMask createMask();
}
