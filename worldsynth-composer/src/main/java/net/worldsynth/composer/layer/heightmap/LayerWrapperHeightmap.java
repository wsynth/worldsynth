/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.composer.layer.heightmap;

import javafx.scene.image.Image;
import javafx.scene.input.DataFormat;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import net.worldsynth.composer.ComposerSession;
import net.worldsynth.composer.layer.LayerRender;
import net.worldsynth.composer.layer.RegionedLayerWrapper;
import net.worldsynth.composer.tool.masking.HeightmapMask;
import net.worldsynth.composer.tool.masking.LayerMask;
import net.worldsynth.composition.layer.heightmap.HeightmapRegion;
import net.worldsynth.composition.layer.heightmap.LayerHeightmap;
import net.worldsynth.composition.event.RegionedLayerModifcationEvent;

import java.text.DecimalFormat;

public class LayerWrapperHeightmap extends RegionedLayerWrapper<LayerHeightmap, HeightmapRegion> {
	
	public static final DataFormat DATAFORMAT_LAYER_HEIGHTMAP = new DataFormat("application/ws-comp-layer-heightmap");
	
	private final LayerRenderHeightmap layerRender;
	
	private final LayerPropertiesPane propertiesPane;
	
	public LayerWrapperHeightmap(LayerHeightmap layer, ComposerSession editorSession) {
		super(layer, editorSession);
		
		layerRender = new LayerRenderHeightmap(this, editorSession);
		propertiesPane = new LayerPropertiesPane();

		layer.addEventHandler(RegionedLayerModifcationEvent.LAYER_MODIFIED, e -> {
			if (internallyFiredEvent) return;
			RegionedLayerModifcationEvent<HeightmapRegion> rle = (RegionedLayerModifcationEvent) e;
			rle.getNewRegions().forEach((coord, region) -> {
				layerRender.queueRegion(region);
			});
		});
	}
	
	@Override
	public DataFormat getLayerDataFormat() {
		return DATAFORMAT_LAYER_HEIGHTMAP;
	}
	
	@Override
	public LayerRender<?> getLayerRender() {
		return layerRender;
	}
	
	@Override
	public Image getDefaultLayerTumbnail() {
		return new Image(getClass().getClassLoader().getResourceAsStream("icons/layers/heightmap.png"));
	}
	
	public float getValueAt(int x, int z) {
		return layer.getValueAt(x, z);
	}
	
	public void applyValues(int x, int z, float[][] values) {
		int changeWidth = values.length;
		int changeLength = values[0].length;
		
		for (int u = 0; u < changeWidth; u++) {
			for (int v = 0; v < changeLength; v++) {
				applyValueAt(x+u, z+v, values[u][v]);
			}
		}
	}
	
	public void applyValueAt(int x, int z, float value) {
		int regionX = Math.floorDiv(x, 256);
		int regionZ = Math.floorDiv(z, 256);
		
		HeightmapRegion region = layer.getRegion(regionX, regionZ);
		
		if (region == null) {
			region = new HeightmapRegion(regionX, regionZ);
			layer.putRegion(region);
		}

		if (!tempOldRegions.containsRegion(regionX, regionZ)) {
			try {
				tempOldRegions.put(region.clone());
			} catch (CloneNotSupportedException e) {
				throw new RuntimeException(e);
			}
		}
		
		int rx = x - regionX*256;
		int rz = z - regionZ*256;
		
		
		region.regionValues[rx][rz] = value;
		layerRender.queueRegion(region);
	}
	
	public void applyChanges(int x, int z, float[][] changes, float factor, float normalizedHeight, boolean clamp) {
		int changeWidth = changes.length;
		int changeLength = changes[0].length;
		
		for (int u = 0; u < changeWidth; u++) {
			for (int v = 0; v < changeLength; v++) {
				applyChangeAt(x+u, z+v, changes[u][v], factor, normalizedHeight, clamp);
			}
		}
	}
	
	public void applyChangeAt(int x, int z, float change, float factor, float normalizedHeight, boolean clamp) {
		int regionX = Math.floorDiv(x, 256);
		int regionZ = Math.floorDiv(z, 256);
		
		HeightmapRegion region = layer.getRegion(regionX, regionZ);
		
		if (region == null) {
			region = new HeightmapRegion(regionX, regionZ);
			layer.putRegion(region);
		}

		if (!tempOldRegions.containsRegion(regionX, regionZ)) {
			try {
				tempOldRegions.put(region.clone());
			} catch (CloneNotSupportedException e) {
				throw new RuntimeException(e);
			}
		}

		int rx = x - regionX*256;
		int rz = z - regionZ*256;
		
		region.regionValues[rx][rz] = region.regionValues[rx][rz] + change * factor;
		if (clamp) {
			region.regionValues[rx][rz] = Math.min(Math.max(region.regionValues[rx][rz], 0.0f), normalizedHeight);
		}
		
		layerRender.queueRegion(region);
	}

	@Override
	public Pane layerPropertiesPane() {
		return propertiesPane;
	}
	
	private class LayerPropertiesPane extends BorderPane {
		
		public LayerPropertiesPane() {
			setRight(layerRender.getPropertiesPane());
		}
	}

	@Override
	public String getCoordinateTooltip(double x, double z) {
		float value = getValueAt((int) x, (int) z);
		DecimalFormat df = new DecimalFormat("#.##");
		return getLayerName() + ": " + df.format(value);
	}

	@Override
	public LayerMask createMask() {
		return new HeightmapMask(this);
	}
}
