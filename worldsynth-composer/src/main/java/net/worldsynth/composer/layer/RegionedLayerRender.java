/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.composer.layer;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;
import net.worldsynth.composer.ui.navcanvas.Coordinate;
import net.worldsynth.composer.ui.navcanvas.NavigationalCanvas;
import net.worldsynth.composer.ui.navcanvas.Pixel;
import net.worldsynth.composition.layer.regioned.LayerRegion;

public abstract class RegionedLayerRender<T extends RegionedLayerWrapper<?, ?>> extends LayerRender<T> {

	private final HashMap<LayerRegion, Image> regionRenders = new HashMap<LayerRegion, Image>();
	private final HashSet<LayerRegion> queuedRegions = new HashSet<LayerRegion>();

	public RegionedLayerRender(T layer) {
		super(layer);
	}
	
	public final void clearRegions() {
		regionRenders.clear();
	}
	
	public final void queueRegion(LayerRegion region) {
		queuedRegions.add(region);
	}
	
	@Override
	protected void updateRender(T layer, GraphicsContext g, NavigationalCanvas navCanvas) {
		Color defaultColor = defaultRenderColor();
		if (defaultColor == null) {
			g.clearRect(0, 0, navCanvas.getWidth(), navCanvas.getHeight());
		}
		else {
			g.setFill(defaultColor);
			g.fillRect(0, 0, navCanvas.getWidth(), navCanvas.getHeight());
		}
		
		int regionSize = getRegionSize();
		double canvasWidth = navCanvas.getWidth();
		double canvasHeight = navCanvas.getHeight();
		double canvasZoom = navCanvas.getZoom();
		double canvasCenterX = navCanvas.getCenterCoordinateX();
		double canvasCenterY = navCanvas.getCenterCoordinateY();
		
		double zoomedCanvasWidth = canvasWidth / canvasZoom;
		double zoomedCanvasHeight = canvasHeight / canvasZoom;
		
		int minRegionX = (int) Math.floor((canvasCenterX - zoomedCanvasWidth * 0.5) / regionSize);
		int maxRegionX = (int) Math.floor((canvasCenterX + zoomedCanvasWidth * 0.5) / regionSize);
		int minRegionZ = (int) Math.floor((canvasCenterY - zoomedCanvasHeight * 0.5) / regionSize);
		int maxRegionZ = (int) Math.floor((canvasCenterY + zoomedCanvasHeight * 0.5) / regionSize);
		
		// Update render for queued regions
		Iterator<LayerRegion> i = queuedRegions.iterator();
		while (i.hasNext()) {
			LayerRegion region = i.next();
			regionRenders.put(region, renderRegion(layer, region));
			i.remove();
		}
		
		// Draw render for regions within the canvas
		for (int x = minRegionX; x <= maxRegionX; x++) {
			for (int z = minRegionZ; z <= maxRegionZ; z++) {
				LayerRegion region = layer.getLayer().getRegion(x, z);
				if (region != null) {
					drawRegion(layer, region, g, navCanvas);
				}
			}
		}
	}
	
	private void drawRegion(T layer, LayerRegion region, GraphicsContext g, NavigationalCanvas navCanvas) {
		Image regionImage = regionRenders.get(region);
		if (regionImage == null) {
			regionImage = renderRegion(layer, region);
			regionRenders.put(region, regionImage);
		}
		
		Pixel corner = new Pixel(new Coordinate(region.getRegionX()*256, region.getRegionZ()*256), navCanvas);
		g.drawImage(regionImage, corner.x, corner.y, 256.0*navCanvas.getZoom(), 256.0*navCanvas.getZoom());
//		g.setStroke(Color.DEEPPINK);
//		g.strokeRect(corner.x, corner.y, 256.0*navCanvas.getZoom(), 256.0*navCanvas.getZoom());
	}
	
	protected abstract Image renderRegion(T layer, LayerRegion region);
	
	protected abstract int getRegionSize();
	
	protected abstract Color defaultRenderColor();
}
