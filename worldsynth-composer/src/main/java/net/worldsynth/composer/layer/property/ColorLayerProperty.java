/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.composer.layer.property;

import javafx.beans.property.ObjectPropertyBase;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.WeakChangeListener;
import javafx.scene.input.KeyCode;
import javafx.scene.paint.Color;
import net.worldsynth.composition.layer.Layer;
import net.worldsynth.fx.control.WsColorPicker;
import net.worldsynth.composition.event.LayerPropertyChangeEvent;

import java.util.function.Consumer;

public class ColorLayerProperty extends ObjectPropertyBase<Color> {

	private final String key;
	
	public ColorLayerProperty(Layer layer, String key, Color defaultValue) {
		super(defaultValue);
		this.key = key;

		// Configure initial value
		set(Color.web(layer.getProperty(key, get().toString())));

		// Propagate the value down to the layer when the value changes
		addListener((observable, oldValue, newValue) -> {
			if (newValue.equals(oldValue)) return;
			layer.setProperty(key, get().toString());
		});

		// Update the property value when the layer value changes
		layer.addEventHandler(LayerPropertyChangeEvent.LAYER_PROPERTY_CHANGED, e -> {
			LayerPropertyChangeEvent event = (LayerPropertyChangeEvent) e;
			String k = event.getPropertyKey();
			if (k.equals(key)) {
				Color oldValue = Color.web(event.getOldValue());
				Color newValue = Color.web(event.getNewValue());
				if (newValue.equals(oldValue)) return;
				set(newValue);
			}
		}, -1);
	}

	public WsColorPicker getColorPicker(Consumer<Color> onChanged) {
		return new ColorPicker(onChanged);
	}
	
	@Override
	public Object getBean() {
		return null;
	}

	@Override
	public String getName() {
		return key;
	}

	private class ColorPicker extends WsColorPicker {

		private final ChangeListener<Color> propertyChangeListener;

		public ColorPicker(Consumer<Color> onChanged) {
			super(get());

			// Apply change on ENTER
			setOnKeyReleased(e -> {
				if (e.getCode() == KeyCode.ENTER) {
					Color oldValue = get();
					Color newValue = getValue();

					if (!newValue.equals(oldValue)) {
						set(newValue);
						onChanged.accept(get());
					}
				}
			});

			// Apply change on changing focus
			focusedProperty().addListener((ob, ov, nv) -> {
				if (!nv) {
					Color oldValue = get();
					Color newValue = getValue();

					if (!newValue.equals(oldValue)) {
						set(newValue);
						onChanged.accept(get());
					}
				}
			});

			// Apply change on color picker dropdown closing
			showingProperty().addListener((ob, ov, nv) -> {
				if (!nv) {
					Color oldValue = get();
					Color newValue = getValue();

					if (!newValue.equals(oldValue)) {
						set(newValue);
						onChanged.accept(get());
					}
				}
			});

			propertyChangeListener = (observable, oldValue, newValue) -> {
				setValue(newValue);
			};
			addListener(new WeakChangeListener<>(propertyChangeListener));
		}
	}
}
