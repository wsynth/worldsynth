/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.composer.layer.property;

import javafx.beans.property.BooleanPropertyBase;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.WeakChangeListener;
import net.worldsynth.composition.layer.Layer;
import net.worldsynth.composition.event.LayerPropertyChangeEvent;

import java.util.function.Consumer;

public class BooleanLayerProperty extends BooleanPropertyBase {
	
	private final String key;
	
	public BooleanLayerProperty(Layer layer, String key, boolean defaultValue) {
		super(defaultValue);
		this.key = key;

		// Configure initial value
		set(Boolean.parseBoolean(layer.getProperty(key, String.valueOf(get()))));

		// Propagate the value down to the layer when the value changes
		addListener((observable, oldValue, newValue) -> {
			if (newValue.equals(oldValue)) return;
			layer.setProperty(key, String.valueOf(get()));
		});

		// Update the property value when the layer value changes
		layer.addEventHandler(LayerPropertyChangeEvent.LAYER_PROPERTY_CHANGED, e -> {
			LayerPropertyChangeEvent event = (LayerPropertyChangeEvent) e;
			String k = event.getPropertyKey();
			if (k.equals(key)) {
				boolean oldValue = Boolean.parseBoolean(event.getOldValue());
				boolean newValue = Boolean.parseBoolean(event.getNewValue());
				if (newValue == oldValue) return;
				set(newValue);
			}
		}, -1);
	}

	public javafx.scene.control.CheckBox getCheckBox(Consumer<Boolean> onChanged) {
		return new CheckBox(onChanged);
	}

	public javafx.scene.control.ToggleButton getToggleButton(String text, Consumer<Boolean> onChanged) {
		return new ToggleButton(text, onChanged);
	}
	
	@Override
	public Object getBean() {
		return null;
	}

	@Override
	public String getName() {
		return key;
	}

	private class CheckBox extends javafx.scene.control.CheckBox {

		private final ChangeListener<Boolean> propertyChangeListener;

		public CheckBox(Consumer<Boolean> onChanged) {
			setSelected(get());

			// Apply change
			setOnAction(e -> {
				boolean oldValue = get();
				boolean newValue = isSelected();

				if (newValue != oldValue) {
					set(newValue);
					onChanged.accept(get());
				}
			});

			propertyChangeListener = (observable, oldValue, newValue) -> {
				setSelected(newValue);
			};
			addListener(new WeakChangeListener<>(propertyChangeListener));
		}
	}

	private class ToggleButton extends javafx.scene.control.ToggleButton {

		private final ChangeListener<Boolean> propertyChangeListener;

		public ToggleButton(String text, Consumer<Boolean> onChanged) {
			super(text);
			setSelected(get());

			// Apply change
			setOnAction(e -> {
				boolean oldValue = get();
				boolean newValue = isSelected();

				if (newValue != oldValue) {
					set(newValue);
					onChanged.accept(get());
				}
			});

			propertyChangeListener = (observable, oldValue, newValue) -> {
				setSelected(newValue);
			};
			addListener(new WeakChangeListener<>(propertyChangeListener));
		}
	}
}
