/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.composer.layer;

import javafx.scene.image.Image;
import javafx.scene.input.DataFormat;
import javafx.scene.layout.Pane;
import net.worldsynth.composer.ComposerSession;
import net.worldsynth.composer.tool.masking.LayerMask;
import net.worldsynth.composition.layer.LayerNull;

public class LayerWrapperNull extends LayerWrapper<LayerNull> {
	
	public static final DataFormat DATAFORMAT_LAYER_NULL = new DataFormat("application/ws-comp-layer-null");
	
	private final LayerRenderNull render = new LayerRenderNull(this);
	
	public LayerWrapperNull(LayerNull layer, ComposerSession editorSession) {
		super(layer, editorSession);
	}
	
	@Override
	public DataFormat getLayerDataFormat() {
		return DATAFORMAT_LAYER_NULL;
	}

	@Override
	public Image getDefaultLayerTumbnail() {
		return null;
	}

	@Override
	public LayerRender<?> getLayerRender() {
		return render;
	}

	@Override
	public Pane layerPropertiesPane() {
		return null;
	}

	@Override
	public void fireEditEvent() {}

	@Override
	public String getCoordinateTooltip(double x, double z) {
		return null;
	}

	@Override
	public LayerMask createMask() {
		return null;
	}
}
