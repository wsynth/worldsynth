/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.composer.layer.materialmap;

import javafx.scene.image.Image;
import javafx.scene.image.PixelFormat;
import javafx.scene.image.PixelWriter;
import javafx.scene.image.WritableImage;
import javafx.scene.paint.Color;
import net.worldsynth.composer.layer.RegionedLayerRender;
import net.worldsynth.composition.layer.materialmap.MaterialmapRegion;
import net.worldsynth.composition.layer.regioned.LayerRegion;
import net.worldsynth.material.MaterialState;

public class LayerRenderMaterialmap extends RegionedLayerRender<LayerWrapperMaterialmap> {
	
	public LayerRenderMaterialmap(LayerWrapperMaterialmap layer) {
		super(layer);
	}
	
	@Override
	protected Image renderRegion(LayerWrapperMaterialmap layer, LayerRegion region) {
		MaterialmapRegion materialRegion = (MaterialmapRegion) region;
		
		WritableImage regionRenderImage = new WritableImage(256, 256);
		PixelWriter pw = regionRenderImage.getPixelWriter();

		int[] buffer = new int[256*256];
		for (int u = 0; u < 256; u++) {
			for (int v = 0; v < 256; v++) {
				MaterialState<?, ?> ms = materialRegion.regionValues[u][v];
				if (ms == null) continue;
				int red = (int) (ms.getWsColor().getRed() * 255.0f);
				int green = (int) (ms.getWsColor().getGreen() * 255.0f);
				int blue = (int) (ms.getWsColor().getBlue() * 255.0f);
				buffer[u+v*256] = (255 << 24) | (red << 16) | (green << 8) | blue;
			}
		}
		pw.setPixels(0, 0, 256, 256, PixelFormat.getIntArgbPreInstance(), buffer, 0, 256);
		
		return regionRenderImage;
	}
	
	@Override
	protected int getRegionSize() {
		return 256;
	}
	
	@Override
	protected Color defaultRenderColor() {
		return null;
	}
}
