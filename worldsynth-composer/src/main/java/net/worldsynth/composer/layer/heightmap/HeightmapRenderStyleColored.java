/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.composer.layer.heightmap;

import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.PixelFormat;
import javafx.scene.image.PixelWriter;
import javafx.scene.image.WritableImage;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import net.worldsynth.composer.ComposerSession;
import net.worldsynth.composer.layer.property.ColorLayerProperty;
import net.worldsynth.composition.layer.heightmap.HeightmapRegion;
import net.worldsynth.fx.control.WsColorPicker;

import java.util.Random;

public class HeightmapRenderStyleColored extends HeightmapRenderStyle {
	
	private final ColorLayerProperty propertyColor;
	
	private final StylePropertiesPane stylePropertiesPane;
	
	public HeightmapRenderStyleColored(LayerRenderHeightmap render, ComposerSession editorSession) {
		Random rand = new Random();
		
		int defaultRed = rand.nextInt(256);
		int defaultGreen = rand.nextInt(256);
		int defaultBlue = rand.nextInt(256);
		Color color = Color.rgb(defaultRed, defaultGreen, defaultBlue);
		
		propertyColor = new ColorLayerProperty(render.getLayer().getLayer(), "renderstyle.colored.color", color);
		propertyColor.addListener(o -> {
			render.clearRegions();
		});
		
		stylePropertiesPane = new StylePropertiesPane(editorSession);
	}
	
	@Override
	public Color defaultColor() {
		return null;
	}
	
	@Override
	public Image renderRegion(LayerWrapperHeightmap layer, HeightmapRegion region) {
		WritableImage regionRenderImage = new WritableImage(256, 256);
		PixelWriter pw = regionRenderImage.getPixelWriter();
		
		// TODO Get normalized height from synth
		float normalizedHeight = 256.0f;
		
		// Get style color
		Color color = propertyColor.get();
		int r = (int) (color.getRed() * 255);
		int g = (int) (color.getGreen() * 255);
		int b = (int) (color.getBlue() * 255);
		
		int[] buffer = new int[256*256];
		for (int u = 0; u < 256; u++) {
			for (int v = 0; v < 256; v++) {
				float s = region.regionValues[u][v] / normalizedHeight;
				
				int r_ = (int) (r * s);
				int g_ = (int) (g * s);
				int b_ = (int) (b * s);
				int a_ = (int) (Math.max(Math.min(s, 1.0f), 0.0f) * 255.0f);
				
				buffer[u+v*256] = (a_ << 24) | (r_ << 16) | (g_ << 8) | b_;
			}
		}
		
		pw.setPixels(0, 0, 256, 256, PixelFormat.getIntArgbPreInstance(), buffer, 0, 256);
		
		return regionRenderImage;
	}
	
	@Override
	public String toString() {
		return "Colored";
	}

	@Override
	public Pane getPropertiesPane() {
		return stylePropertiesPane;
	}
	
	private class StylePropertiesPane extends GridPane {

		public StylePropertiesPane(ComposerSession editorSession) {
			setHgap(2.0);
			setVgap(5.0);
			
			// Color
			add(new Label("Color: "), 0, 0);
			
			WsColorPicker styleColorPicker = propertyColor.getColorPicker(e -> {
				editorSession.commitHistory();
			});
			styleColorPicker.setPrefHeight(25.6);
			styleColorPicker.setMinHeight(USE_PREF_SIZE);
			add(styleColorPicker, 1, 0);
		}
	}
}
