/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.composer.fx;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.DataFormat;
import javafx.scene.input.Dragboard;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;
import net.worldsynth.composer.layer.LayerWrapper;
import net.worldsynth.composer.ui.layerbrowser.LayerView;
import net.worldsynth.composer.ui.resources.Resources;

public class LayerDropField<T extends LayerWrapper> extends HBox {

	private final Label layerName;
	private final ImageView layerIcon;

	private final SimpleObjectProperty<T> layer = new SimpleObjectProperty<>(null);

	public LayerDropField(DataFormat layerDataFormat) {
		this(layerDataFormat, null, "Drag and drop layer here");
	}

	public LayerDropField(DataFormat layerDataFormat, T defaultValue, String defaultText) {
		getStyleClass().add("layer-drop-field");

		Image dropIcon = new Image(Resources.getResourceStream("icons/drop.png"));

		layerIcon = new ImageView(dropIcon);
		layerName = new Label(defaultText, layerIcon);

		layer.addListener((observable, oldValue, newValue) -> {
			layerName.textProperty().unbind();
			layerIcon.imageProperty().unbind();

			if (newValue == null) {
				layerName.textProperty().set(defaultText);
				layerIcon.setImage(dropIcon);
			}
			else {
				layerName.textProperty().bind(newValue.layerNameProperty());
				layerIcon.imageProperty().bind(newValue.layerTumbnailProperty());
			}
		});

		layer.set(defaultValue);

		setOnDragOver(e -> {
			if (e.getDragboard().hasContent(layerDataFormat)) {
				e.acceptTransferModes(TransferMode.LINK);
			}

			e.consume();
		});

		setOnDragDropped(e -> {
			Dragboard db = e.getDragboard();
			boolean success = false;
			if (db.hasContent(layerDataFormat)) {
				LayerWrapper layerWrapper = ((LayerView) e.getGestureSource()).getLayerWrapper();
				layer.set((T) layerWrapper);
				success = true;
			}
			e.setDropCompleted(success);

			e.consume();
		});

		Region spacerRegion1 = new Region();
		HBox.setHgrow(spacerRegion1, Priority.ALWAYS);
		Region spacerRegion2 = new Region();
		HBox.setHgrow(spacerRegion2, Priority.ALWAYS);

		getChildren().addAll(spacerRegion1, layerName, spacerRegion2);
	}

	public ObjectProperty layerProperty() {
		return layer;
	}

	public T getLayer() {
		return layer.get();
	}

	public void setLayer(T layer) {
		this.layer.set(layer);
	}
}
