/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.composer.brush;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import net.worldsynth.composer.brush.heightmap.HeightbrushImage;

public class BrushRegistry {
	
	public static ArrayList<Brush> REGISTER = new ArrayList<Brush>();
	
	public BrushRegistry(File brushesDirectory) {
		registerBrushes(brushesDirectory);
	}
	
	private void registerBrushes(File brushesDirectory) {
		//Load brushes from files
		if (!brushesDirectory.exists()) {
			brushesDirectory.mkdir();
		}
		loadeBrushLib(brushesDirectory);
	}
	
	private void loadeBrushLib(File brushesDirectory) {
		
		if (!brushesDirectory.isDirectory()) {
			System.err.println("Lib directory \"" + brushesDirectory.getAbsolutePath() + "\" does not exist");
			return;
		}
		
		for (File sub: brushesDirectory.listFiles()) {
			if (sub.isDirectory()) {
				loadeBrushLib(sub);
			}
			else if (sub.getName().endsWith(".png")) {
				try {
					REGISTER.add(loadBrushFromFile(sub));
				} catch (IOException e) {
					System.err.println("Problem occoured while trying to read brush file: " + sub.getName());
					e.printStackTrace();
				}
			}
		}
	}
	
	private Brush loadBrushFromFile(File brushFile) throws IOException {
		System.out.println("Loading brush from file: " + brushFile.getName());
		return new HeightbrushImage(brushFile);
	}
	
	public static Brush[] getBrushes() {
		//Sort brushes alphabetically by name
		Brush[] brushSort = new Brush[REGISTER.size()];
		REGISTER.toArray(brushSort);
		
		return brushSort;
	}
}
