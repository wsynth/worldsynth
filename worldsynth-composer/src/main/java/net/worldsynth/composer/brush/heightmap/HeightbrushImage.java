/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.composer.brush.heightmap;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import javafx.scene.image.Image;
import javafx.scene.image.PixelWriter;
import javafx.scene.image.WritableImage;
import javafx.scene.paint.Color;

public class HeightbrushImage extends Heightbrush {
	
	private final float[][] brushValues;
	
	public HeightbrushImage(File brushFile) {
		super(brushFile.getName());
		brushValues = readeImageFromFile(brushFile);
		updateBrushTumbnail();
		updateBrushImage();
	}
	
	@Override
	public float[][] getBrushValues() {
		return brushValues;
	}

	@Override
	protected Image buildBrushTumbnail() {
		int tumbSize = 42;
		WritableImage tumb = new WritableImage(tumbSize, tumbSize);
		
		int w = brushValues.length;
		int h = brushValues[0].length;
		
		PixelWriter pw = tumb.getPixelWriter();
		for (int u = 0; u < tumbSize; u++) {
			for (int v = 0; v < tumbSize; v++) {
				pw.setColor(u, v, Color.gray(brushValues[u*w/tumbSize][v*h/tumbSize]));
			}
		}
		
		return tumb;
	}

	@Override
	protected Image buildBrushImage() {
		WritableImage brushImage = new WritableImage(getBrushWidth(), getBrushHeight());
		PixelWriter pw = brushImage.getPixelWriter();
		
		for (int u = 0; u < getBrushWidth(); u++) {
			for (int v = 0; v < getBrushHeight(); v++) {
				pw.setColor(u, v, Color.gray(getBrushValues()[u][v]));
			}
		}
		
		return brushImage;
	}
	
	private float[][] readeImageFromFile(File f) {
		float[][] heightmap = null;
		
		try {
			BufferedImage image = ImageIO.read(f);
			double divident = 256.0;
			
			switch (image.getType()) {
			case BufferedImage.TYPE_USHORT_GRAY:
				divident = 65536.0;
				break;
			case BufferedImage.TYPE_USHORT_565_RGB:
				divident = 32.0;
				break;
			case BufferedImage.TYPE_USHORT_555_RGB:
				divident = 32.0;
				break;
			default:
				divident = 256.0;
				break;
			}
			
			heightmap = new float[image.getWidth()][image.getHeight()];
			for (int x = 0; x < heightmap.length; x++) {
				for (int y = 0; y < heightmap[0].length; y++) {
					double[] height = image.getRaster().getPixel(x, y, new double[4]);
					heightmap[x][y] = (float) (height[0] / divident);
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return heightmap;
	}

	@Override
	public int getBrushWidth() {
		return brushValues.length;
	}

	@Override
	public int getBrushHeight() {
		return brushValues[0].length;
	}
}
