/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.composer.composition;

import javafx.beans.property.SimpleObjectProperty;
import net.worldsynth.composer.ComposerSession;
import net.worldsynth.composer.layer.LayerGroupWrapper;
import net.worldsynth.composer.layer.LayerWrapper;
import net.worldsynth.composition.Composition;
import net.worldsynth.composition.layer.Layer;
import net.worldsynth.composition.event.CompositionLayerMoveEvent;

public class CompositionWrapper {
	
	private final Composition composition;
	private final ComposerSession editorSession;

	private final LayerGroupWrapper rootGroupWrapper;
	private final SimpleObjectProperty<LayerWrapper<?>> selectedLayer = new SimpleObjectProperty<>(null);
	
	public CompositionWrapper(Composition composition, ComposerSession editorSession) {
		this.composition = composition;
		this.editorSession = editorSession;

		rootGroupWrapper = new LayerGroupWrapper(composition.getRootGroup(), editorSession);

		composition.addEventHandler(CompositionLayerMoveEvent.COMPOSITION_LAYER_MOVED, e -> {
			Layer movedLayer = ((CompositionLayerMoveEvent) e).getLayer();
			if (movedLayer.getLayerId() == selectedLayer.get().getLayer().getLayerId()) {
				// Moving a layer creates a new layer wrapper.
				// Therefore, update the wrapper selection when a selected layer is moved.
				LayerWrapper wrapper = rootGroupWrapper.getLayerWrapper(movedLayer, true);
				setSelectedLayer(wrapper);
			}
		});
	}
	
	public Composition getWrappedComposition() {
		return composition;
	}

	public LayerGroupWrapper getRootGroupWrapper() {
		return rootGroupWrapper;
	}

	public LayerWrapper getLayerWrapper(Layer layer) {
		return rootGroupWrapper.getLayerWrapper(layer);
	}

	public ComposerSession getEditorSession() {
		return editorSession;
	}
	
	public SimpleObjectProperty<LayerWrapper<?>> selectedLayerProperty() {
		return selectedLayer;
	}
	
	public LayerWrapper<?> getSelectedLayer() {
		return selectedLayer.get();
	}
	
	public void setSelectedLayer(LayerWrapper<?> selectedLayer) {
		this.selectedLayer.set(selectedLayer);
	}
	
	public void setSelectedLayer(Layer layer) {
		selectedLayer.set(rootGroupWrapper.getLayerWrapper(layer, true));
	}
}
