/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.composer;

import net.worldsynth.WorldSynth;
import net.worldsynth.composer.addon.IComposerAddon;
import net.worldsynth.composer.brush.BrushRegistry;
import net.worldsynth.composer.tool.ToolRegistry;

public class WorldSynthComposer {
	private static WorldSynthComposer instance;
	
	public WorldSynthComposer(IComposerAddon addonLoader) {
		if (instance != null) throw new IllegalStateException("Composer is already initialized");
		instance = this;
		
		new BrushRegistry(WorldSynth.getDirectoryConfig().getDirectory("brushes"));
		new ToolRegistry(addonLoader);
	}
}
