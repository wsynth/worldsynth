/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.composer.ui.toolbar;

import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.geometry.Orientation;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.Tooltip;
import javafx.scene.image.ImageView;
import net.worldsynth.composer.layer.LayerWrapper;
import net.worldsynth.composer.tool.Tool;
import net.worldsynth.composer.tool.ToolCursor;
import net.worldsynth.composer.tool.ToolRegistry;

public class ToolBar extends javafx.scene.control.ToolBar {
	
	private Class<? extends LayerWrapper<?>> currentLayerType = null;
	
	private ObjectProperty<Tool<?>> selectedTool = new SimpleObjectProperty<>();
	private LinkedHashMap<Tool<?>, ToggleButton> toolButtons = new LinkedHashMap<>();
	
	public ToolBar() {
		setOrientation(Orientation.VERTICAL);

		selectedTool.addListener((observable, oldValue, newValue) -> {
			// Ensure previous tool is deselected
			if (toolButtons.containsKey(oldValue)) {
				toolButtons.get(oldValue).setSelected(false);
			}

			if (newValue != null) {
				// Set the tool's tool button as selected
				toolButtons.get(newValue).setSelected(true);
			}
		});

		setToolsForLayer(null);
	}
	
	@SuppressWarnings("unchecked")
	public void setToolsForLayer(LayerWrapper<?> layer) {
		if (layer == null) {
			currentLayerType = null;
			setTools(new ToolCursor());
			return;
		}
		// Don't change the toolbar if the new layer is same type
		else if (currentLayerType == layer.getClass()) return;
		
		currentLayerType = (Class<? extends LayerWrapper<?>>) layer.getClass();
		setTools(ToolRegistry.getToolsForLayer(layer));
	}
	
	private void setTools(Tool<?> ... tools) {
		setTools(Arrays.asList(tools));
	}
	
	private void setTools(List<Tool<?>> tools) {
		getItems().clear();
		toolButtons.clear();
		selectedTool.set(null);
		
		for (Tool<?> t: tools) {
			ToggleButton toolButton = new ToggleButton();
			toolButton.getStyleClass().add("tool-button");
			toolButton.setGraphic(new ImageView(t.getToolIconImage()));
			toolButton.setTooltip(new Tooltip(t.getName()));
			
			toolButton.setOnAction(e -> {
				if (!toolButton.isSelected()) {
					// Don't allow deselection of the selected tool
					toolButton.setSelected(true);
					return;
				}
				selectedTool.set(t);
			});
			
			getItems().add(toolButton);
			toolButtons.put(t, toolButton);
		}
		
		// Select first tool
		selectedTool.set(tools.get(0));
	}
	
	public ObjectProperty<Tool<?>> selectedToolProperty() {
		return selectedTool;
	}
	
	public Tool<?> getSelectedTool() {
		return selectedToolProperty().get();
	}
	
	public void setSelectedTool(Tool<?> tool) {
		selectedToolProperty().set(tool);
	}
}
