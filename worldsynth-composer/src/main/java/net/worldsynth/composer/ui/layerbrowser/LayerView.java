/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.composer.ui.layerbrowser;

import net.worldsynth.composer.ComposerSession;
import net.worldsynth.composer.layer.LayerWrapper;

public class LayerView extends AbstractLayerView {
	
	public LayerView(LayerWrapper<?> layerWrapper, LayerBrowser parentLayerBrowser, ComposerSession editorSession) {
		super(layerWrapper, parentLayerBrowser, editorSession);
		
		// Content
		setContent(layerWrapper.layerPropertiesPane());
	}
}
