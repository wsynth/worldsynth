/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.composer.ui.layerbrowser;

import javafx.scene.control.MenuItem;
import net.worldsynth.composer.ComposerSession;
import net.worldsynth.composer.layer.LayerGroupWrapper;

public class GroupView extends AbstractLayerView {

	protected final LayerListView layerListView;

	public GroupView(LayerGroupWrapper groupWrapper, LayerBrowser parentLayerBrowser, ComposerSession editorSession) {
		super(groupWrapper, parentLayerBrowser, editorSession);

		getStyleClass().add("layer-group-view");

		// Setup layer list
		layerListView = new LayerListView(groupWrapper, parentLayerBrowser, editorSession);

		setContent(layerListView);

		// Context menu
		MenuItem menuItemDissolveGroup = new MenuItem("Dissolve group");
		menuItemDissolveGroup.setOnAction(e -> {
			parentLayerBrowser.getComposition().dissolveGroup(groupWrapper.getLayer());
			editorSession.commitHistory();
		});

		contextMenu.getItems().add(menuItemDissolveGroup);
	}
}
