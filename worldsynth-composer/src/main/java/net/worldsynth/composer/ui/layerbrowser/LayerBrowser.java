/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.composer.ui.layerbrowser;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ChangeListener;
import javafx.collections.FXCollections;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.ScrollPane.ScrollBarPolicy;
import javafx.scene.control.TextField;
import javafx.scene.effect.BlendMode;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import net.worldsynth.composer.ComposerSession;
import net.worldsynth.composer.composition.CompositionWrapper;
import net.worldsynth.composer.layer.LayerGroupWrapper;
import net.worldsynth.composer.layer.LayerWrapper;
import net.worldsynth.composition.Composition;

public class LayerBrowser extends BorderPane {
	
	private final CompositionWrapper compositionWrapper;
	private final ComposerSession editorSession;
	
	private final LayerPreviewBlending layerPreviewBlending;
	private final ScrollPane layersListScrollPane;
	private final LayerListView layerListView;

	private final SimpleObjectProperty<LayerWrapper<?>> selectedLayer = new SimpleObjectProperty<>(null);
		
	public LayerBrowser(CompositionWrapper compositionWrapper, ComposerSession editorSession) {
		this.compositionWrapper = compositionWrapper;
		this.editorSession = editorSession;

		getStyleClass().setAll("layer-browser");
		
		// Setup layer preview blending pane
		layerPreviewBlending = new LayerPreviewBlending();
		setTop(layerPreviewBlending);
		
		// Setup layer list
		layerListView = new LayerListView(compositionWrapper.getRootGroupWrapper(), this, editorSession);

		layersListScrollPane = new ScrollPane(layerListView);
		layersListScrollPane.setFitToWidth(true);
		layersListScrollPane.setVbarPolicy(ScrollBarPolicy.ALWAYS);
		layersListScrollPane.setHbarPolicy(ScrollBarPolicy.NEVER);
		layersListScrollPane.setFocusTraversable(false);

		layerListView.prefHeightProperty().bind(layersListScrollPane.heightProperty());

		// Observe layer selection
		selectedLayer.addListener((observable, oldValue, newValue) -> {
			if (oldValue != null) {
				layerListView.setLayerViewSelection(oldValue, false);
			}

			if (newValue != null) {
				layerListView.setLayerViewSelection(newValue, true);
			}

			layerPreviewBlending.bindLayer(newValue);
		});

		compositionWrapper.selectedLayerProperty().bindBidirectional(selectedLayer);

		setCenter(layersListScrollPane);
		setFocusTraversable(false);
	}

	public CompositionWrapper getCompositionWrapper() {
		return compositionWrapper;
	}

	public Composition getComposition() {
		return compositionWrapper.getWrappedComposition();
	}

	public LayerWrapper<?> getSelectedLayer() {
		return selectedLayer.get();
	}

	void selectLayerByView(AbstractLayerView layerView) {
		selectedLayer.set(layerView.getLayerWrapper());
	}
	
	private class LayerPreviewBlending extends HBox {
		
		private final Label previewBlendLabel;

		private final ComboBox<BlendMode> previewBlendModeSelector;
		private final ChangeListener<BlendMode> blendModePropertyListener;

		private final PercentageField previewBlendOpacityField;
		private final ChangeListener<Number> opacityPropertyListener;
		
		private LayerWrapper<?> layer;
		
		public LayerPreviewBlending() {
			previewBlendLabel = new Label("Preview blend:");
			previewBlendLabel.setAlignment(Pos.CENTER);
			previewBlendLabel.setMaxHeight(Double.MAX_VALUE);

			previewBlendModeSelector = new ComboBox<>(FXCollections.observableArrayList(BlendMode.values()));
			previewBlendModeSelector.setMaxWidth(Double.MAX_VALUE);
			HBox.setHgrow(previewBlendModeSelector, Priority.ALWAYS);

			blendModePropertyListener = (ob, ov, nv) -> {
				previewBlendModeSelector.setValue(nv);
			};

			// Apply change
			previewBlendModeSelector.setOnAction(e -> {
				BlendMode oldValue = layer.layerPreviewBlendmodeProperty().get();
				BlendMode newValue = previewBlendModeSelector.getValue();

				if (!newValue.equals(oldValue)) {
					layer.layerPreviewBlendmodeProperty().set(newValue);
					editorSession.commitHistory();
				}
			});

			previewBlendOpacityField = new PercentageField(1.0);
			previewBlendOpacityField.setMaxWidth(80);
			previewBlendOpacityField.setDisable(true);

			opacityPropertyListener = (ob, ov, nv) -> {
				previewBlendOpacityField.setValue((Double) nv);
			};

			// Apply change on ENTER
			previewBlendOpacityField.setOnKeyReleased(e -> {
				if (e.getCode() == KeyCode.ENTER) {
					double oldValue = layer.layerPreviewOpacityProperty().get();
					double newValue = previewBlendOpacityField.getValue();

					if (newValue != oldValue) {
						layer.layerPreviewOpacityProperty().set(newValue);
						editorSession.commitHistory();
					}
				}
			});

			// Apply change on changing focus
			previewBlendOpacityField.focusedProperty().addListener((ob, ov, nv) -> {
				if (!nv) {
					double oldValue = layer.layerPreviewOpacityProperty().get();
					double newValue = previewBlendOpacityField.getValue();

					if (newValue != oldValue) {
						layer.layerPreviewOpacityProperty().set(newValue);
						editorSession.commitHistory();
					}
				}
			});
			
			setPadding(new Insets(4));
			setSpacing(4);
			getChildren().addAll(previewBlendLabel, previewBlendModeSelector, previewBlendOpacityField);
		}

		public void bindLayer(LayerWrapper<?> layer) {
			if (this.layer != null) {
				this.layer.layerPreviewOpacityProperty().removeListener(opacityPropertyListener);
				this.layer.layerPreviewBlendmodeProperty().removeListener(blendModePropertyListener);
			}
			
			this.layer = layer;
			
			if (layer != null) {
				previewBlendOpacityField.setValue(layer.getLayerPreviewOpacity());
				layer.layerPreviewOpacityProperty().addListener(opacityPropertyListener);
				previewBlendOpacityField.setDisable(layer instanceof LayerGroupWrapper);

				previewBlendModeSelector.setValue(layer.getLayerPreviewBlendMode());
				layer.layerPreviewBlendmodeProperty().addListener(blendModePropertyListener);
				previewBlendModeSelector.setDisable(layer instanceof LayerGroupWrapper);
			}
			else {
				previewBlendOpacityField.setValue(0);
				previewBlendOpacityField.setDisable(true);

				previewBlendModeSelector.setValue(null);
				previewBlendModeSelector.setDisable(true);
			}
		}
	}
	
	/**
	 * A custom little percentage field.
	 * <p>
	 * The value is stored as a normalized 0 to 1 double value, and scaled from 0 to
	 * 100 for the text field.
	 * <p>
	 * Allows values between 0 to 100 to be entered in the text field. Values beyond
	 * this range are not applied, and the field indicates invalid input by becoming
	 * red.
	 */
	private class PercentageField extends TextField {
		
		private final DoubleProperty valueProperty;
		
		public PercentageField(double value) {
			valueProperty = new SimpleDoubleProperty(value);
			
			// Update text when value changes
			valueProperty.addListener((ob, ov, nv) -> {
				if (isFocused()) return;
				setText(String.valueOf((double) nv * 100.0) + " %");
			});
			
			// Update value when text is entered
			setText(String.valueOf(value * 100.0) + " %");
			textProperty().addListener((ob, ov, nv) -> {
				String text = nv.replace(",", ".").replace(" ", "").replace("%", "");
				try {
					double v = Double.parseDouble(text);
					if (v < 0.0 || v > 100.0) throw new Exception("Value is out of range");
					valueProperty.set(v / 100.0);
					setStyle(null);
				} catch (Exception exception) {
					setStyle("-fx-background-color: RED;");
				}
			});
			
			// Update the text when loosing focus
			focusedProperty().addListener((ob, ov, nv) -> {
				if (!nv) {
					setText(String.valueOf(getValue() * 100.0) + " %");
				}
			});
		}
		
		public DoubleProperty valueProperty() {
			return valueProperty;
		}
		
		public double getValue() {
			return valueProperty().getValue();
		}
		
		public void setValue(double value) {
			valueProperty().setValue(value);
		}
	}
}
