/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.composer.ui;

import javafx.geometry.Insets;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;
import net.worldsynth.composer.ui.composition.CompositionViewer;

public class TopBar extends HBox {

	private final ToggleButton gridButton;
	private final ToggleButton rulersButton;

	public TopBar(CompositionViewer compositionViewer) {
		gridButton = new ToggleButton();
		Image gridIcon = new Image((getClass().getClassLoader().getResourceAsStream("icons/settings/grid.png")));
		gridButton.setGraphic(new ImageView(gridIcon));
		gridButton.setTooltip(new Tooltip("Show grid"));
		gridButton.selectedProperty().bindBidirectional(compositionViewer.showGridProperty());

		rulersButton = new ToggleButton();
		Image rulersIcon = new Image((getClass().getClassLoader().getResourceAsStream("icons/settings/rulers.png")));
		rulersButton.setGraphic(new ImageView(rulersIcon));
		rulersButton.setTooltip(new Tooltip("Show rulers"));
		rulersButton.selectedProperty().bindBidirectional(compositionViewer.showRulersProperty());

		Region spacerRegion1 = new Region();
		HBox.setHgrow(spacerRegion1, Priority.ALWAYS);
		Region spacerRegion2 = new Region();
		HBox.setHgrow(spacerRegion2, Priority.ALWAYS);

		getChildren().addAll(spacerRegion1, gridButton, rulersButton, spacerRegion2);
		BorderPane.setMargin(this, new Insets(5));
	}
}
