/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.composer.ui;

import javafx.geometry.Orientation;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import net.worldsynth.composer.ComposerSession;
import net.worldsynth.composer.composition.CompositionWrapper;
import net.worldsynth.composer.ui.composition.CompositionViewer;
import net.worldsynth.composer.ui.layerbrowser.LayerBrowser;
import net.worldsynth.composer.ui.resources.Resources;
import net.worldsynth.composer.ui.toolbar.ToolBar;
import net.worldsynth.composition.Composition;
import net.worldsynth.composition.layer.Layer;

public class CompositionEditor extends BorderPane {

	private final ComposerSession editorSession;

	private final Composition composition;
	private final CompositionWrapper compositionWrapper;

	private final TopBar topBar;
	private final ToolBar toolbar;
	private final CompositionViewer compositionViewer;
	private final LayerBrowser layerBrowser;

	public final ScrollPane toolSettingsScrollPane;
	
	public CompositionEditor(Composition composition, ComposerSession editorSession) {
		this.composition = composition;
		this.editorSession = editorSession;

		String stylesheet = Resources.getResourceURL("Composer.css").toExternalForm();
		getStylesheets().add(stylesheet);

		compositionWrapper = new CompositionWrapper(composition, editorSession);

		// Construct the composition viewer
		compositionViewer = new CompositionViewer(compositionWrapper, this);

		// Construct the top bar
		topBar = new TopBar(compositionViewer);

		// Construct the tool settings scroll pane
		toolSettingsScrollPane = new ScrollPane();
		toolSettingsScrollPane.setFitToWidth(true);
		toolSettingsScrollPane.setVbarPolicy(ScrollPane.ScrollBarPolicy.ALWAYS);
		toolSettingsScrollPane.setHbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);
		toolSettingsScrollPane.setFocusTraversable(false);

		// Construct the toolbar
		toolbar = new ToolBar();
		compositionViewer.setActiveTool(toolbar.getSelectedTool());
		toolbar.selectedToolProperty().addListener((observable, oldValue, newValue) -> {
			compositionViewer.setActiveTool(newValue);

			if (newValue != null) {
				// Display tool settings pane
				toolSettingsScrollPane.setContent(newValue.getToolSettingsPane());
			}
			else {
				toolSettingsScrollPane.setContent(null);
			}
		});
		compositionWrapper.selectedLayerProperty().addListener((observable, oldValue, newValue) -> {
			toolbar.setToolsForLayer(newValue);
		});

		if (toolbar.getSelectedTool() != null) {
			toolSettingsScrollPane.setContent(toolbar.getSelectedTool().getToolSettingsPane());
		}

		// Construct the layer browser
		layerBrowser = new LayerBrowser(compositionWrapper, editorSession);

		// Combine the tool settings pane and layer browser
		VBox.setVgrow(layerBrowser, Priority.ALWAYS);
		SplitPane toolAndLayersPane = new SplitPane(toolSettingsScrollPane, layerBrowser);
		toolAndLayersPane.setOrientation(Orientation.VERTICAL);
		toolAndLayersPane.setMinWidth(310);

		// Do layout
		setTop(topBar);
		setLeft(toolbar);

		SplitPane projectSplit = new SplitPane(compositionViewer, toolAndLayersPane);
		SplitPane.setResizableWithParent(toolAndLayersPane, Boolean.FALSE);
		projectSplit.setDividerPositions(1, 0);
		
		setCenter(projectSplit);
	}

	public ComposerSession getEditorSession() {
		return editorSession;
	}

	public void selectLayer(Layer layer) {
		compositionWrapper.setSelectedLayer(layer);
	}
}
