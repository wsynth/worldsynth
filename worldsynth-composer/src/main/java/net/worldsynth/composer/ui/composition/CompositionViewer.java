/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.composer.ui.composition;

import java.util.HashMap;
import java.util.Iterator;
import java.util.concurrent.atomic.AtomicBoolean;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.effect.BlendMode;
import javafx.scene.input.MouseButton;
import javafx.scene.layout.BorderPane;
import javafx.scene.paint.Color;
import javafx.util.Duration;
import net.worldsynth.composer.composition.CompositionWrapper;
import net.worldsynth.composer.layer.LayerGroupWrapper;
import net.worldsynth.composer.layer.LayerWrapper;
import net.worldsynth.composer.tool.Tool;
import net.worldsynth.composer.ui.CompositionEditor;
import net.worldsynth.composer.ui.navcanvas.Coordinate;
import net.worldsynth.composer.ui.navcanvas.NavigationalCanvas;
import net.worldsynth.composer.ui.navcanvas.Pixel;
import net.worldsynth.composition.event.CompositionLayerAdditionEvent;
import net.worldsynth.composition.event.CompositionLayerModifiedEvent;
import net.worldsynth.composition.event.CompositionLayerMoveEvent;
import net.worldsynth.composition.event.CompositionLayerRemovalEvent;
import net.worldsynth.composition.layer.Layer;

public class CompositionViewer extends BorderPane implements NavigationalCanvas {
	
	private final CompositionWrapper compositionWrapper;
	
	private final Canvas backgroundCanvas = new Canvas();
	private final GridCanvas gridCanvas = new GridCanvas();
	private final Canvas toolCanvas = new Canvas();
	private final RulersCanvas rulerCanvas = new RulersCanvas();
	private final Group canvasGroup = new Group();
	private final HashMap<Layer, Canvas> layerCanvasMap = new HashMap<>();
	
	private double centerCoordX = 0.0;
	private double centerCoordY = 0.0;
	private double zoom = 1.0;
	
	private double lastMouseX = 0;
	private double lastMouseY = 0;
	
	private boolean primaryMouseButton = false;
	private boolean secondaryMouseButton = false;
	private boolean midleMouseButton = false;
	
	// Periodic apply
	private boolean periodicApplyOn = false;
	private final Timeline periodicApply;
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public CompositionViewer(CompositionWrapper compositionWrapper, CompositionEditor parentEditor) {
		this.compositionWrapper = compositionWrapper;

		// Handle composition changes
		compositionWrapper.getWrappedComposition().addEventHandler(CompositionLayerModifiedEvent.COMPOSITION_LAYER_MODIFIED, e -> {
			updatePreview();
		});
		compositionWrapper.getWrappedComposition().addEventHandler(CompositionLayerMoveEvent.COMPOSITION_LAYER_MOVED, e -> {
			buildLayersCanvasStack();
			updatePreview();
		});
		compositionWrapper.getWrappedComposition().addEventHandler(CompositionLayerAdditionEvent.COMPOSITION_LAYER_ADDED, e -> {
			Layer layer = ((CompositionLayerAdditionEvent) e).getLayer();
			LayerWrapper wrapper = compositionWrapper.getLayerWrapper(layer);

			if (!(wrapper instanceof LayerGroupWrapper)) {
				layerCanvasMap.put(wrapper.getLayer(), new Canvas());
			}

			buildLayersCanvasStack();
			updatePreview();
		});
		compositionWrapper.getWrappedComposition().addEventHandler(CompositionLayerRemovalEvent.COMPOSITION_LAYER_REMOVED, e -> {
			Layer layer = ((CompositionLayerRemovalEvent) e).getLayer();
			LayerWrapper wrapper = compositionWrapper.getLayerWrapper(layer);

			if (!(wrapper instanceof LayerGroupWrapper)) {
				layerCanvasMap.remove(wrapper);
			}

			buildLayersCanvasStack();
			updatePreview();
		});

		// Map all non-group layers to a new canvas
		compositionWrapper.getRootGroupWrapper().forEachLayer(true, layer -> {
			if (!(layer instanceof LayerGroupWrapper)) {
				layerCanvasMap.put(layer.getLayer(), new Canvas());
			}
		});

		buildLayersCanvasStack();
		
		getChildren().add(backgroundCanvas);
		getChildren().add(canvasGroup);
		getChildren().add(gridCanvas);
		getChildren().add(toolCanvas);
		getChildren().add(rulerCanvas);
		toolCanvas.setBlendMode(BlendMode.ADD);

		final AtomicBoolean editPerformed = new AtomicBoolean(false);
		
		periodicApply = new Timeline(new KeyFrame(Duration.seconds(0.05), new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				boolean toolUsed = false;
				
				if (!compositionWrapper.getSelectedLayer().isLayerLocked() && periodicApplyOn) {
					double x = getMouseCoordinateX();
					double z = getMouseCoordinateZ();
					
					if (primaryMouseButton) {
						toolUsed = activeTool.onPrimaryDown(x, z, 1.0, compositionWrapper.getSelectedLayer());
					}
					else if (secondaryMouseButton) {
						toolUsed = activeTool.onSecondaryDown(x, z, 1.0, compositionWrapper.getSelectedLayer());
					}
				}
				
				if (toolUsed) {
					editPerformed.set(true);
					updatePreview();
				}
				
				periodicApply.stop();
				if (periodicApplyOn) {
					periodicApply.play();
				}
			}
		}));
		periodicApply.setCycleCount(1);
		
		setOnMousePressed(e -> {
			if (e.isSynthesized()) return;
			
			lastMouseX = e.getX();
			lastMouseY = e.getY();
			
			if (e.getButton() == MouseButton.PRIMARY) primaryMouseButton = true;
			else if (e.getButton() == MouseButton.SECONDARY) secondaryMouseButton = true;
			else if (e.getButton() == MouseButton.MIDDLE) midleMouseButton = true;

			boolean toolUsed = false;

			if (!compositionWrapper.getSelectedLayer().isLayerLocked()) {
				double x = getMouseCoordinateX();
				double z = getMouseCoordinateZ();
				
				if (e.getButton() == MouseButton.PRIMARY) {
					toolUsed = activeTool.onPrimaryPressed(x, z, 1.0, compositionWrapper.getSelectedLayer());
					startPeriodicApply();
				}
				else if (e.getButton() == MouseButton.SECONDARY) {
					toolUsed = activeTool.onSecondaryPressed(x, z, 1.0, compositionWrapper.getSelectedLayer());
					startPeriodicApply();
				}
			}
			
			if (toolUsed) {
				editPerformed.set(true);
				updatePreview();
			}
		});
		
		setOnMouseDragged(e -> {
			if (e.isSynthesized()) return;
			
			double diffX = lastMouseX - e.getX();
			double diffY = lastMouseY - e.getY();
			lastMouseX = e.getX();
			lastMouseY = e.getY();

			boolean toolUsed = false;
			boolean updatePreview = false;

			if (midleMouseButton) {
				centerCoordX += diffX / zoom;
				centerCoordY += diffY / zoom;
				updatePreview = true;
			}
			else if (!compositionWrapper.getSelectedLayer().isLayerLocked()) {
				double x = getMouseCoordinateX();
				double z = getMouseCoordinateZ();
				
				if (primaryMouseButton) {
					toolUsed = activeTool.onPrimaryDragged(x, z, 1.0, compositionWrapper.getSelectedLayer());
				}
				else if (secondaryMouseButton) {
					toolUsed = activeTool.onSecondaryDragged(x, z, 1.0, compositionWrapper.getSelectedLayer());
				}
			}

			if (toolUsed) {
				editPerformed.set(true);
				updatePreview = true;
			}
			
			if (updatePreview) {
				updatePreview();
			}
		});
		
		setOnMouseReleased(e -> {
			if (e.isSynthesized()) return;
			
			lastMouseX = e.getX();
			lastMouseY = e.getY();

			boolean toolUsed = false;
			
			if (!compositionWrapper.getSelectedLayer().isLayerLocked()) {
				double x = getMouseCoordinateX();
				double z = getMouseCoordinateZ();
				
				if (e.getButton() == MouseButton.PRIMARY) {
					toolUsed = activeTool.onPrimaryReleased(x, z, 1.0, compositionWrapper.getSelectedLayer());
				}
				else if (e.getButton() == MouseButton.SECONDARY) {
					toolUsed = activeTool.onSecondaryReleased(x, z, 1.0, compositionWrapper.getSelectedLayer());
				}
			}

			if (toolUsed) {
				editPerformed.set(true);
			}
			
			if (e.getButton() == MouseButton.PRIMARY) primaryMouseButton = false;
			else if (e.getButton() == MouseButton.SECONDARY) secondaryMouseButton = false;
			else if (e.getButton() == MouseButton.MIDDLE) midleMouseButton = false;
			
			if (!primaryMouseButton && !secondaryMouseButton && e.getButton() != MouseButton.MIDDLE) {
				stopPeriodicApply();
			}

			if (editPerformed.get()) {
				editPerformed.set(false);
				updatePreview();

				compositionWrapper.getSelectedLayer().fireEditEvent();
				compositionWrapper.getEditorSession().commitHistory();
				compositionWrapper.getEditorSession().updatePreview();
			}
		});
		
		setOnTouchPressed(e -> {
			lastMouseX = e.getTouchPoint().getX();
			lastMouseY = e.getTouchPoint().getY();
		});
		
		setOnTouchMoved(e -> {
			double diffX = lastMouseX - e.getTouchPoint().getX();
			double diffY = lastMouseY - e.getTouchPoint().getY();
			lastMouseX = e.getTouchPoint().getX();
			lastMouseY = e.getTouchPoint().getY();
			
			centerCoordX += diffX / zoom;
			centerCoordY += diffY / zoom;
			
			updatePreview();
		});
		
		setOnScroll(e -> {
			if (e.isDirect()) return;
			
			if (e.isControlDown()) {
				double maxZoom = 16.0;
				double minZoom = 0.2;

				Pixel lmpx = new Pixel(lastMouseX ,lastMouseY);
				Coordinate lmco = new Coordinate(lmpx, this);

				double lastZoom = zoom;
				zoom += e.getDeltaY() / e.getMultiplierY() * zoom / 10;
				if (zoom < minZoom) zoom = minZoom;
				else if (zoom > maxZoom) zoom = maxZoom;

				Coordinate nmco = new Coordinate(lmpx, this);
				centerCoordX -= nmco.x - lmco.x;
				centerCoordY -= nmco.y - lmco.y;

				if (lastZoom != zoom) {
					updatePreview();
				}
			}
			else {
				boolean updatePreview = activeTool.onScroll(
						e.getDeltaX() / e.getMultiplierX(),
						e.getDeltaY() / e.getMultiplierY(),
						compositionWrapper.getSelectedLayer());
				if (updatePreview) {
					updatePreview();
				}
			}
		});
		
		setOnMouseEntered(e -> {
			if (e.isSynthesized()) return;
			lastMouseX = e.getX();
			lastMouseY = e.getY();
			
			updateRulers();
		});
		
		setOnMouseMoved(e -> {
			if (e.isSynthesized()) return;
			lastMouseX = e.getX();
			lastMouseY = e.getY();
			
			toolCanvas.getGraphicsContext2D().clearRect(0, 0, 10000, 10000);
			activeTool.renderTool(getMouseCoordinateX(), getMouseCoordinateZ(), compositionWrapper.getSelectedLayer(), toolCanvas.getGraphicsContext2D(), this);
			
			updateRulers();
		});

		showRulers.addListener((observable, oldValue, newValue) -> {
			updatePreview();
		});

		showGrid.addListener((observable, oldValue, newValue) -> {
			updatePreview();
		});
	}
	
	private void startPeriodicApply() {
		periodicApplyOn = true;
		periodicApply.play();
	}
	
	private void stopPeriodicApply() {
		periodicApplyOn = false;
		periodicApply.stop();
	}
	
	@Override
	protected final void layoutChildren() {
		super.layoutChildren();
		final double x = snappedLeftInset();
		final double y = snappedTopInset();
		// Java 9 - snapSize is depricated used snapSizeX() and snapSizeY() accordingly
		final double w = snapSize(getWidth()) - x - snappedRightInset();
		final double h = snapSize(getHeight()) - y - snappedBottomInset();
		
		layoutCanvas(backgroundCanvas, x, y, w, h);
		layoutCanvas(gridCanvas, x, y, w, h);
		layoutCanvas(toolCanvas, x, y, w, h);
		layoutCanvas(rulerCanvas, x, y, w, h);

		layerCanvasMap.entrySet().forEach(t -> {
			layoutCanvas(t.getValue(), x, y, w, h);
		});

		updatePreview();
	}
	
	private void layoutCanvas(Canvas canvas, double x, double y, double w, double h) {
		canvas.setLayoutX(x);
		canvas.setLayoutY(y);
		canvas.setWidth(w);
		canvas.setHeight(h);
	}
	
	@Override
	public final double getCenterCoordinateX() {
		return centerCoordX;
	}

	@Override
	public final double getCenterCoordinateY() {
		return centerCoordY;
	}

	@Override
	public final double getZoom() {
		return zoom;
	}
	
	public final double getMousePixelX() {
		return lastMouseX;
	}
	
	public final double getMousePixelY() {
		return lastMouseY;
	}
	
	public final double getMouseCoordinateX() {
		return new Coordinate(new Pixel(lastMouseX, lastMouseY), this).x;
	}
	
	public final double getMouseCoordinateZ() {
		return new Coordinate(new Pixel(lastMouseX, lastMouseY), this).y;
	}
	
	public void buildLayersCanvasStack() {
		canvasGroup.getChildren().clear();
		populateRecursiveCanvasStack(compositionWrapper.getRootGroupWrapper(), canvasGroup);
		layoutChildren();
	}

	private void populateRecursiveCanvasStack(LayerGroupWrapper group, Group canvasGroup) {
		Iterator<LayerWrapper<?>> iter = group.layersIterator();
		while (iter.hasNext()) {
			LayerWrapper<?> layer = iter.next();
			if (layer instanceof LayerGroupWrapper) {
				populateRecursiveCanvasStack((LayerGroupWrapper) layer, canvasGroup);
			}
			else {
				Canvas layerCanvas = layerCanvasMap.get(layer.getLayer());
				canvasGroup.getChildren().add(layerCanvas);
			}
		}
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void updatePreview() {
		backgroundCanvas.getGraphicsContext2D().setFill(Color.BLACK);
		backgroundCanvas.getGraphicsContext2D().fillRect(0, 0, getWidth(), getHeight());

		recursiveLayersPreview(compositionWrapper.getRootGroupWrapper(), true, true);

		updateGrid();
		updateRulers();

		toolCanvas.getGraphicsContext2D().clearRect(0, 0, 10000, 10000);
		if (compositionWrapper.getSelectedLayer() != null) {
			activeTool.renderTool(getMouseCoordinateX(), getMouseCoordinateZ(), compositionWrapper.getSelectedLayer(), toolCanvas.getGraphicsContext2D(), this);
		}
	}

	private boolean recursiveLayersPreview(LayerGroupWrapper group, boolean recursiveVisibility, boolean firstLayer) {
		for (LayerWrapper layer : group.getObservableLayersList()) {
			if (layer instanceof LayerGroupWrapper) {
				firstLayer = recursiveLayersPreview((LayerGroupWrapper) layer, recursiveVisibility && layer.isLayerActive(), firstLayer);
			}
			else {
				Canvas layerCanvas = layerCanvasMap.get(layer.getLayer());
				if (firstLayer && layer.isLayerActive()) {
					layerCanvas.setBlendMode(null);
					firstLayer = false;
				}
				else {
					layerCanvas.setBlendMode(layer.getLayerPreviewBlendMode());
					layerCanvas.setOpacity(layer.getLayerPreviewOpacity());
				}

				GraphicsContext g = layerCanvas.getGraphicsContext2D();
				if (recursiveVisibility && layer.isLayerActive()) {
					layer.getLayerRender().updateRender(g, this);
				}
				else {
					layer.getLayerRender().clearRender(g, this);
				}
			}
		}
		return firstLayer;
	}

	private void updateGrid() {
		if (showGrid.get()) {
			gridCanvas.drawGrid(this);
		}
		else {
			gridCanvas.clearGrid();
		}
	}

	private void updateRulers() {
		if (showRulers.get()) {
			rulerCanvas.drawCoordinateRulers(this, compositionWrapper.getSelectedLayer());
		}
		else {
			rulerCanvas.clearRulers();
		}
	}

	private Tool activeTool;
	public void setActiveTool(Tool<?> tool) {
		activeTool = tool;
	}

	private SimpleBooleanProperty showGrid = new SimpleBooleanProperty(true);
	public BooleanProperty showGridProperty() {
		return showGrid;
	}

	public void setShowGrid(boolean showGrid) {
		this.showGrid.set(showGrid);
	}

	public boolean getShowGrid() {
		return showGrid.get();
	}

	private SimpleBooleanProperty showRulers = new SimpleBooleanProperty(true);
	public BooleanProperty showRulersProperty() {
		return showRulers;
	}
	public void setShowRulers(boolean showRulers) {
		this.showRulers.set(showRulers);
	}

	public boolean getShowRulers() {
		return showRulers.get();
	}
}
