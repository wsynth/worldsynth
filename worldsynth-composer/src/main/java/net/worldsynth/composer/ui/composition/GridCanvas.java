/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.composer.ui.composition;

import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import net.worldsynth.composer.ui.navcanvas.Coordinate;
import net.worldsynth.composer.ui.navcanvas.Pixel;

public class GridCanvas extends Canvas {
	
	public void drawGrid(CompositionViewer compViewer) {
		GraphicsContext g = getGraphicsContext2D();
		g.clearRect(0, 0, getWidth(), getHeight());
		
		Coordinate c0 = new Coordinate(new Pixel(0, 0), compViewer);
		Coordinate c1 = new Coordinate(new Pixel(getWidth(), getHeight()), compViewer);
		
		double gridIncrement = 16.0;
		double xmin = roundDownToMultiple(c0.x, gridIncrement);
		double xmax = roundUpToMultiple(c1.x, gridIncrement);
		double zmin = roundDownToMultiple(c0.y, gridIncrement);
		double zmax = roundUpToMultiple(c1.y, gridIncrement);
		
		//Draw the grid
		g.setStroke(Color.web("#363636", Math.max(0.0, Math.min(0.5, compViewer.getZoom()*2-0.5))));
		g.setLineWidth(1.0);
		
		for (double x = xmin; x <= xmax; x += gridIncrement) {
			Pixel p = new Pixel(new Coordinate(x, 0), compViewer);
			g.strokeLine(p.x, 0, p.x, getHeight());
		}
		
		for (double z = zmin; z <= zmax; z += gridIncrement) {
			Pixel p = new Pixel(new Coordinate(0, z), compViewer);
			g.strokeLine(0, p.y, getWidth(), p.y);
		}
	}

	public void clearGrid() {
		GraphicsContext g = getGraphicsContext2D();
		g.clearRect(0, 0, getWidth(), getHeight());
	}
	
	private double roundDownToMultiple(double value, double multiple) {
		return multiple * Math.floor(value / multiple);
	}
	
	private double roundUpToMultiple(double value, double multiple) {
		return multiple * Math.ceil(value / multiple);
	}
}
