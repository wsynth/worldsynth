/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.composer.ui.layerbrowser;

import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.css.PseudoClass;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseButton;
import javafx.scene.layout.*;
import net.worldsynth.composer.ComposerSession;
import net.worldsynth.composer.layer.LayerWrapper;

public abstract class AbstractLayerView extends BorderPane {

	protected final LayerWrapper<?> layerWrapper;
	protected final ContextMenu contextMenu;

	public AbstractLayerView(LayerWrapper<?> layerWrapper, LayerBrowser parentLayerBrowser, ComposerSession editorSession) {
		this.layerWrapper = layerWrapper;

		getStyleClass().setAll("layer-view");

		// Title
		TitleGraphics titleGraphics = new TitleGraphics(layerWrapper, editorSession);
		setTop(titleGraphics);

		// Content
		StackPane contentContainer = new StackPane();
		contentContainer.getStyleClass().setAll("content");

		content.addListener((observable, oldValue, newValue) -> {
			if (newValue == null) {
				contentContainer.getChildren().clear();
			}
			else {
				contentContainer.getChildren().setAll(newValue);
			}
		});

		// Expansion
		expanded.addListener((observable, oldValue, newValue) -> {
			if (newValue) {
				setCenter(contentContainer);
			}
			else {
				setCenter(null);
			}
		});

		// Context menu
		MenuItem menuItemRenameLayer = new MenuItem("Rename");
		menuItemRenameLayer.setOnAction(e -> {
			titleGraphics.showNameField();
		});

		contextMenu = new ContextMenu(menuItemRenameLayer);

		// Mouse clicked
		setOnMouseClicked(e -> {
			if (e.getButton() == MouseButton.PRIMARY) {
				parentLayerBrowser.selectLayerByView(this);
				e.consume();
			}
			else if (e.getButton() == MouseButton.SECONDARY) {
				contextMenu.show(this, e.getScreenX(), e.getScreenY());
				e.consume();
			}
		});
	}

	private final SimpleObjectProperty<Node> content = new SimpleObjectProperty<>();
	public SimpleObjectProperty<Node> contentProperty() {
		return content;
	}

	public void setContent(Node content) {
		this.content.set(content);
	}

	public Node getContent() {
		return content.get();
	}

	private final SimpleBooleanProperty expanded = new SimpleBooleanProperty();
	public SimpleBooleanProperty expandedProperty() {
		return expanded;
	}

	public void setExpanded(boolean expanded) {
		this.expanded.set(expanded);
	}

	public boolean isExpanded() {
		return expanded.get();
	}

	public LayerWrapper<?> getLayerWrapper() {
		return layerWrapper;
	}

	void setSelected(boolean selected) {
		if (selected) {
			pseudoClassStateChanged(PseudoClass.getPseudoClass("selected"), true);
		}
		else {
			pseudoClassStateChanged(PseudoClass.getPseudoClass("selected"), false);
		}
	}

	private class TitleGraphics extends HBox {

		private final ToggleButton expandedButton;
		private final ImageView layerThumbnailView;
		private final Label nameLabel;
		private final Pane fillPane;
		private final TextField layerNameField;
		private final ToggleButton lockButton;
		private final ToggleButton visibilityButton;

		public TitleGraphics(LayerWrapper<?> layerWrapper, ComposerSession editorSession) {
			getStyleClass().setAll("title");

			expandedButton = new ToggleButton();
			expandedButton.getStyleClass().add("group-expand-button");
			expandedButton.selectedProperty().bindBidirectional(expandedProperty());

			// Layer thumbnail
			layerThumbnailView = new ImageView();
			layerThumbnailView.imageProperty().bindBidirectional(layerWrapper.layerTumbnailProperty());

			// Layer name
			nameLabel = new Label();
			nameLabel.textProperty().bindBidirectional(layerWrapper.layerNameProperty());

			fillPane = new Pane();
			fillPane.setMaxWidth(Double.POSITIVE_INFINITY);
			HBox.setHgrow(fillPane, Priority.ALWAYS);

			// Layer name editing field
			layerNameField = layerWrapper.layerNameProperty().getTextField(e -> {
				editorSession.commitHistory();
			});
			layerNameField.setMaxWidth(Double.POSITIVE_INFINITY);
			HBox.setHgrow(layerNameField, Priority.ALWAYS);

			// Layer edit lock
			lockButton = layerWrapper.layerLockedProperty().getToggleButton("", e -> {
				editorSession.commitHistory();
			});
			lockButton.getStyleClass().add("layer-locked-button");
			Tooltip.install(lockButton, new Tooltip("Lock layer"));

			// Layer visibility
			visibilityButton = layerWrapper.layerActiveProperty().getToggleButton("", e -> {
				editorSession.commitHistory();
			});
			visibilityButton.getStyleClass().add("layer-visible-button");
			Tooltip.install(visibilityButton, new Tooltip("Show layer"));


			// On name edit field lost focus
			layerNameField.focusedProperty().addListener((observable, oldValue, newValue) -> {
				if (newValue == false) {
					getChildren().clear();
					getChildren().setAll(expandedButton, layerThumbnailView, nameLabel, fillPane, lockButton, visibilityButton);
				}
			});

			getChildren().setAll(expandedButton ,layerThumbnailView, nameLabel, fillPane, lockButton, visibilityButton);

			setAlignment(Pos.CENTER);
			setSpacing(5.0);
		}

		public void showNameField() {
			getChildren().clear();
			getChildren().setAll(expandedButton, layerThumbnailView, layerNameField, lockButton, visibilityButton);
			layerNameField.requestFocus();
		}
	}
}
