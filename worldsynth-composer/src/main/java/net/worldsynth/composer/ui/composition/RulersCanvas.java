/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.composer.ui.composition;

import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.TextAlignment;
import net.worldsynth.composer.layer.LayerWrapper;
import net.worldsynth.composer.ui.navcanvas.Coordinate;
import net.worldsynth.composer.ui.navcanvas.Pixel;

public class RulersCanvas extends Canvas {
	
	public void drawCoordinateRulers(CompositionViewer compViewer, LayerWrapper<?> currentLayer) {
		GraphicsContext g = getGraphicsContext2D();
		g.clearRect(0, 0, getWidth(), getHeight());
		
		g.setFill(Color.grayRgb(64, 0.75));
		g.fillRect(0, 0, getWidth(), 20);
		g.fillRect(0, 20, 20, getHeight());
		
		Coordinate c0 = new Coordinate(new Pixel(20, 20), compViewer);
		Coordinate c1 = new Coordinate(new Pixel(getWidth(), getHeight()), compViewer);
		
		double multiple = 5.0;
		if (compViewer.getZoom() < 16) multiple = 10.0;
		if (compViewer.getZoom() < 4) multiple = 50.0;
		if (compViewer.getZoom() < 1) multiple = 100.0;
		if (compViewer.getZoom() < 0.4) multiple = 500.0;
		
		double xmin = roundUpToMultiple(c0.x, multiple);
		double xmax = roundUpToMultiple(c1.x, multiple);
		double zmin = roundUpToMultiple(c0.y, multiple);
		double zmax = roundUpToMultiple(c1.y, multiple);
		
		g.setStroke(Color.WHITE);
		g.setLineWidth(1.0);
		g.setTextAlign(TextAlignment.CENTER);
		g.setFont(new Font(10));
		g.setFill(Color.WHITE);
		
		for (double x = xmin; x <= xmax; x += multiple) {
			Pixel p = new Pixel(new Coordinate(x, 0), compViewer);
			g.strokeLine(p.x, 15, p.x, 19);
			g.fillText(String.valueOf((int) x), p.x, 10);
		}

		g.save();
		g.rotate(-90);
		g.setStroke(Color.WHITE);
		g.setLineWidth(1.0);
		g.setTextAlign(TextAlignment.CENTER);
		g.setFont(new Font(10));
		g.setFill(Color.WHITE);
		for (double z = zmin; z <= zmax; z += multiple) {
			Pixel p = new Pixel(new Coordinate(0, z), compViewer);
			g.strokeLine(-p.y, 15, -p.y, 19);
			g.fillText(String.valueOf((int) z), -p.y, 10);
		}
		g.restore();
		
		
		g.strokeLine(compViewer.getMousePixelX(), 0, compViewer.getMousePixelX(), 19);
		g.strokeLine(0, compViewer.getMousePixelY(), 19, compViewer.getMousePixelY());

		// Layer coordinate tooltip
		g.setFill(Color.grayRgb(64, 0.75));
		g.fillRect(20, getHeight() - 20, getWidth(), getHeight());

		g.setTextAlign(TextAlignment.LEFT);
		g.setFont(new Font(16));
		g.setFill(Color.WHITE);

		int x = (int) compViewer.getMouseCoordinateX();
		int z = (int) compViewer.getMouseCoordinateZ();

		g.fillText("Location: " + x + ", " + z, 40, getHeight()-4);
		if (currentLayer != null) {
			g.fillText(currentLayer.getCoordinateTooltip(x, z), 240, getHeight()-4);
		}
	}

	public void clearRulers() {
		GraphicsContext g = getGraphicsContext2D();
		g.clearRect(0, 0, getWidth(), getHeight());
	}
	
	private double roundUpToMultiple(double value, double multiple) {
		return multiple * Math.ceil(value / multiple);
	}
}
