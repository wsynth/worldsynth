/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.composer.ui.brushbrowser;

import javafx.beans.property.SimpleObjectProperty;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.FlowPane;
import net.worldsynth.composer.brush.Brush;
import net.worldsynth.composer.brush.BrushRegistry;

public class BrushBrowser extends ScrollPane {
	
	private final SimpleObjectProperty<BrushView> selectedBrushView = new SimpleObjectProperty<BrushView>();
	private final SimpleObjectProperty<Brush> selectedBrush = new SimpleObjectProperty<Brush>();
	private final FlowPane brushCatalogueFlowPane = new FlowPane();
	
	public BrushBrowser() {
		brushCatalogueFlowPane.setPadding(new Insets(2.0));
		brushCatalogueFlowPane.setVgap(2.0);
		brushCatalogueFlowPane.setHgap(2.0);
		brushCatalogueFlowPane.setAlignment(Pos.CENTER);
		
		setFitToWidth(true);
		setVbarPolicy(ScrollBarPolicy.ALWAYS);
		setHbarPolicy(ScrollBarPolicy.NEVER);
		
		setContent(brushCatalogueFlowPane);
		
		selectedBrushView.addListener((observable, oldValue, newValue) -> {
			if (oldValue != null) {
				oldValue.setSelected(false);
			}
			if (newValue != null) {
				newValue.setSelected(true);
			}
		});
		
		for (Brush b: BrushRegistry.getBrushes()) {
			addBrush(b);
		}
	}
	
	public void addBrush(Brush brush) {
		brushCatalogueFlowPane.getChildren().add(new BrushView(brush, this));
	}
	
	void setBrushSelection(BrushView newBrushView) {
		selectedBrushView.set(newBrushView);
		selectedBrush.set(newBrushView.getBrush());
	}
	
	public SimpleObjectProperty<Brush> selectedBrushProperty() {
		return selectedBrush;
	}
	
	public Brush getSelectedBrush() {
		return selectedBrush.get();
	}
}
