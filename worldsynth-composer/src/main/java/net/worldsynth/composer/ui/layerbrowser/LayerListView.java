/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.composer.ui.layerbrowser;

import javafx.collections.ListChangeListener;
import javafx.css.PseudoClass;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.MenuItem;
import javafx.scene.input.*;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import net.worldsynth.composer.ComposerSession;
import net.worldsynth.composer.layer.LayerGroupWrapper;
import net.worldsynth.composer.layer.LayerWrapper;
import net.worldsynth.composition.layer.Layer;
import net.worldsynth.composition.layer.LayerGroup;

import java.util.HashMap;
import java.util.Iterator;

public class LayerListView extends VBox {

	private final ComposerSession editorSession;
	private final LayerBrowser parentLayerBrowser;
	private final LayerGroupWrapper groupWrapper;

	private final HashMap<LayerWrapper<?>, AbstractLayerView> layersMap = new HashMap<>();

	public LayerListView(LayerGroupWrapper groupWrapper, LayerBrowser parentLayerBrowser, ComposerSession editorSession) {
		this.groupWrapper = groupWrapper;
		this.parentLayerBrowser = parentLayerBrowser;
		this.editorSession = editorSession;

		getStyleClass().setAll("layer-list-view");
		setFocusTraversable(false);

		// Add layers to list
		for (LayerWrapper<?> layerWrapper : groupWrapper.getObservableLayersList()) {
			AbstractLayerView view = makeLayerView(layerWrapper);
			layersMap.put(layerWrapper, view);
		}
		updateLayerDisplay();

		// Observe layer additions, removals and permutations
		groupWrapper.getObservableLayersList().addListener((ListChangeListener.Change<? extends LayerWrapper<?>> c) -> {
			while (c.next()) {
				for (LayerWrapper<?> l : c.getRemoved()) {
					layersMap.remove(l);
				}
				for (LayerWrapper<?> l : c.getAddedSubList()) {
					AbstractLayerView view = makeLayerView(l);
					layersMap.put(l, view);
				}
			}
			updateLayerDisplay();
		});

		// Context menu
		MenuItem menuItemAddGroup = new MenuItem("Add group");
		menuItemAddGroup.setOnAction(e -> {
			LayerGroup layerGroup = new LayerGroup("new group");
			parentLayerBrowser.getComposition().addLayer(layerGroup);
			editorSession.commitHistory();
		});

		ContextMenu contextMenu = new ContextMenu(menuItemAddGroup);

		addEventHandler(MouseEvent.MOUSE_CLICKED, e -> {
			if (e.getButton() == MouseButton.SECONDARY) {
				contextMenu.show(this, e.getScreenX(), e.getScreenY());
			}
		});
	}

	private AbstractLayerView makeLayerView(LayerWrapper<?> layer) {
		// Construct layer view
		AbstractLayerView view;
		if (layer instanceof LayerGroupWrapper) {
			view = new GroupView((LayerGroupWrapper) layer, parentLayerBrowser, editorSession);
		}
		else {
			view = new LayerView(layer, parentLayerBrowser, editorSession);
		}

		view.setOnDragDropped(e -> {
			try {
				Dragboard db = e.getDragboard();
				boolean success = false;
				if (db.hasContent(LayerWrapper.DATAFORMAT_LAYER)) {
					LayerGroup group = groupWrapper.getLayer();
					Layer targetLayer = view.getLayerWrapper().getLayer();
					int targetIndex = group.getLayerIndex(targetLayer);

					AbstractLayerView sourceView = (AbstractLayerView) e.getGestureSource();
					Layer sourceLayer = sourceView.getLayerWrapper().getLayer();
					LayerGroup sourceGroup = parentLayerBrowser.getComposition().getLayerParent(sourceLayer);
					int sourceIndex = sourceGroup.getLayerIndex(sourceLayer);

					double heightFraction = e.getY() / view.getHeight();
					if (heightFraction < 0.5) {
						if (group != sourceGroup) {
							parentLayerBrowser.getComposition().moveLayer(sourceLayer, group, targetIndex + 1);
						}
						else if (targetIndex < sourceIndex) {
							parentLayerBrowser.getComposition().moveLayer(sourceLayer, group, targetIndex + 1);
						}
						else {
							parentLayerBrowser.getComposition().moveLayer(sourceLayer, group, targetIndex);
						}
					}
					else {
						if (group != sourceGroup) {
							parentLayerBrowser.getComposition().moveLayer(sourceLayer, group, targetIndex);
						}
						else if (targetIndex < sourceIndex) {
							parentLayerBrowser.getComposition().moveLayer(sourceLayer, group, targetIndex);
						}
						else {
							parentLayerBrowser.getComposition().moveLayer(sourceLayer, group, targetIndex - 1);
						}
					}

					e.setDropCompleted(success);
					editorSession.commitHistory();
				}

				e.consume();
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		});

		// Drag and drop handling
		view.setOnDragDetected(e -> {
			Dragboard db = view.startDragAndDrop(TransferMode.LINK, TransferMode.MOVE);
			ClipboardContent content = new ClipboardContent();
			content.put(LayerWrapper.DATAFORMAT_LAYER, layer.getLayer().getLayerId());
			content.put(layer.getLayerDataFormat(), layer.getLayer().getLayerId());
			db.setContent(content);

			e.consume();
		});

		view.setOnDragOver(e -> {
			try {
				if (e.getGestureSource() != view && e.getDragboard().hasContent(LayerWrapper.DATAFORMAT_LAYER)) {
					LayerGroup group = groupWrapper.getLayer();
					AbstractLayerView sourceView = (AbstractLayerView) e.getGestureSource();
					Layer sourceLayer = sourceView.getLayerWrapper().getLayer();

					boolean recursive = false;
					if (sourceLayer instanceof LayerGroup) {
						if (group == sourceLayer || ((LayerGroup) sourceLayer).containsLayer(group, true)) {
							recursive = true;
						}
					}

					if (!recursive) {
						int sourceIndex = getChildren().indexOf(sourceView);
						int targetIndex = getChildren().indexOf(view);

						double heightFraction = e.getY() / view.getHeight();
						if (heightFraction < 0.5 && (sourceIndex == -1 || sourceIndex != targetIndex - 2)) {
							((LayerSeparator) getChildren().get(targetIndex - 1)).indicateDnd(true);
							((LayerSeparator) getChildren().get(targetIndex + 1)).indicateDnd(false);

							e.acceptTransferModes(TransferMode.MOVE);
						}
						else if (heightFraction > 0.5 && (sourceIndex == -1 || sourceIndex != targetIndex + 2)) {
							((LayerSeparator) getChildren().get(targetIndex - 1)).indicateDnd(false);
							((LayerSeparator) getChildren().get(targetIndex + 1)).indicateDnd(true);

							e.acceptTransferModes(TransferMode.MOVE);
						}
						else {
							((LayerSeparator) getChildren().get(targetIndex - 1)).indicateDnd(false);
							((LayerSeparator) getChildren().get(targetIndex + 1)).indicateDnd(false);
						}
					}
				}

				e.consume();
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		});

		view.setOnDragExited(e -> {
			if (e.getGestureSource() != view && e.getDragboard().hasContent(LayerWrapper.DATAFORMAT_LAYER)) {
				e.acceptTransferModes(TransferMode.MOVE);

				int i = getChildren().indexOf(view);
				((LayerSeparator) getChildren().get(i - 1)).indicateDnd(false);
				((LayerSeparator) getChildren().get(i + 1)).indicateDnd(false);
			}

			e.consume();
		});

		return view;
	}

	private void updateLayerDisplay() {
		getChildren().clear();

		Iterator<LayerWrapper<?>> iter = groupWrapper.layersReverseIterator();
		if (iter.hasNext()) {
			getChildren().add(new LayerSeparator());
		}
		else {
			getChildren().add(new LayerSeparator(20.0));
		}

		while (iter.hasNext()) {
			LayerWrapper<?> l = iter.next();
			AbstractLayerView view = layersMap.get(l);

			if (view != null) {
				getChildren().add(view);
				getChildren().add(new LayerSeparator());
			}
		}
	}

	public void setLayerViewSelection(LayerWrapper layer, boolean selection) {
		layersMap.values().forEach(e -> {
			if (e.getLayerWrapper() == layer) {
				e.setSelected(selection);
			}
			else if (e instanceof GroupView) {
				((GroupView) e).layerListView.setLayerViewSelection(layer, selection);
			}
		});
	}

	private class LayerSeparator extends Pane {

		public LayerSeparator() {
			this(2.0);
		}

		public LayerSeparator(double size) {
			getStyleClass().add("layer-separator");
			setMinHeight(size);

			setOnDragOver(e -> {
				if (e.getDragboard().hasContent(LayerWrapper.DATAFORMAT_LAYER)) {
					LayerGroup group = groupWrapper.getLayer();
					AbstractLayerView sourceView = (AbstractLayerView) e.getGestureSource();
					Layer sourceLayer = sourceView.getLayerWrapper().getLayer();

					boolean recursive = false;
					if (sourceLayer instanceof LayerGroup) {
						if (group == sourceLayer || ((LayerGroup) sourceLayer).containsLayer(group, true)) {
							recursive = true;
						}
					}

					if (!recursive) {
						int sourceIndex = LayerListView.this.getChildren().indexOf(sourceView);
						int targetIndex = LayerListView.this.getChildren().indexOf(this);

						if (sourceIndex == -1 || (sourceIndex != targetIndex - 1 && sourceIndex != targetIndex + 1)) {
							indicateDnd(true);

							e.acceptTransferModes(TransferMode.MOVE);
						}
					}
				}

				e.consume();
			});

			setOnDragDropped(e -> {
				try {
					Dragboard db = e.getDragboard();
					boolean success = false;
					if (db.hasContent(LayerWrapper.DATAFORMAT_LAYER)) {
						LayerGroup group = groupWrapper.getLayer();
						int separatorIndex = (LayerListView.this.getChildren().size() - LayerListView.this.getChildren().indexOf(this) - 1) / 2;
						int targetIndex = separatorIndex;

						AbstractLayerView sourceView = (AbstractLayerView) e.getGestureSource();
						Layer sourceLayer = sourceView.getLayerWrapper().getLayer();
						LayerGroup sourceGroup = parentLayerBrowser.getComposition().getLayerParent(sourceLayer);
						int sourceIndex = sourceGroup.getLayerIndex(sourceLayer);

						if (group == sourceGroup && targetIndex > sourceIndex) {
							parentLayerBrowser.getComposition().moveLayer(sourceLayer, group, targetIndex - 1);
						}
						else {
							parentLayerBrowser.getComposition().moveLayer(sourceLayer, group, targetIndex);
						}

						e.setDropCompleted(success);
						editorSession.commitHistory();
					}

					e.consume();
				} catch (Exception ex) {
					ex.printStackTrace();
				}
			});

			setOnDragExited(e -> {
				indicateDnd(false);
				e.consume();
			});
		}

		public void indicateDnd(boolean indicate) {
			pseudoClassStateChanged(PseudoClass.getPseudoClass("dnd-hover"), indicate);
		}
	}
}
