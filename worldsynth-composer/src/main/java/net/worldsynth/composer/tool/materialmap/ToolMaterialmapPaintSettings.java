/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.composer.tool.materialmap;

import javafx.beans.property.*;
import javafx.geometry.Insets;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.PixelFormat;
import javafx.scene.image.PixelWriter;
import javafx.scene.image.WritableImage;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import net.worldsynth.composer.brush.heightmap.Heightbrush;
import net.worldsynth.composer.tool.masking.MaskList;
import net.worldsynth.composer.ui.brushbrowser.BrushBrowser;
import net.worldsynth.fx.control.MaterialStateSelector;
import net.worldsynth.material.MaterialRegistry;
import net.worldsynth.material.MaterialState;

public class ToolMaterialmapPaintSettings extends VBox {

	private final ObjectProperty<Heightbrush> brush = new SimpleObjectProperty<>(null);
	private final ObjectProperty<MaterialState<?, ?>> material = new SimpleObjectProperty<>(MaterialRegistry.getDefaultMaterial().getDefaultState());
	private final DoubleProperty brushThreshold = new SimpleDoubleProperty(0.5);
	private final DoubleProperty brushSize = new SimpleDoubleProperty(1.0);
	private final DoubleProperty angle = new SimpleDoubleProperty(0.0);
	private final BooleanProperty randomizeRotation = new SimpleBooleanProperty(false);
	private final MaskList layerMasks = new MaskList();

	private Image brushPreviewImage;

	public ToolMaterialmapPaintSettings() {
		// Brush browser
		BrushBrowser brushBrowser = new BrushBrowser();
		brushBrowser.setPrefHeight(300);
		brushBrowser.setMinHeight(300);
		brushBrowser.selectedBrushProperty().addListener((observable, oldValue, newValue) -> {
			brush.set((Heightbrush) newValue);
		});

		TitledPane brushPane = new TitledPane("Brushes", brushBrowser);
		brushPane.setAnimated(false);
		brushPane.setExpanded(true);

		// Tool parameters
		GridPane settingsGrid = new GridPane();
		settingsGrid.setPadding(new Insets(10.0));
		settingsGrid.setHgap(5.0);
		settingsGrid.setVgap(2.0);
		settingsGrid.prefWidthProperty().bind(widthProperty());
		ColumnConstraints col1 = new ColumnConstraints(0, Control.USE_COMPUTED_SIZE, Control.USE_COMPUTED_SIZE);
		ColumnConstraints col2 = new ColumnConstraints(0, Control.USE_COMPUTED_SIZE, Double.MAX_VALUE);
		col2.setHgrow(Priority.ALWAYS);
		settingsGrid.getColumnConstraints().addAll(col1, col2);

		// Brush size
		Slider sizeSlider = new Slider(0.0, 2.0, brushSize.get());
		sizeSlider.valueProperty().bindBidirectional(brushSize);
		Label sizeLabel = new Label(String.format("Size: %.1f %%", brushSize.get()*100.0));
		brushSize.addListener((observable, oldValue, newValue) -> {
			sizeLabel.setText(String.format("Size: %.1f %%", (double) newValue*100.0));
		});
		GridPane.setColumnSpan(sizeLabel, GridPane.REMAINING);
		settingsGrid.add(sizeLabel, 0, 0);
		GridPane.setColumnSpan(sizeSlider, GridPane.REMAINING);
		settingsGrid.add(sizeSlider, 0, 1);

		// Brush threshold
		Slider thresholdSlider = new Slider(0.0, 1.0, brushThreshold.get());
		thresholdSlider.valueProperty().bindBidirectional(brushThreshold);
		Label thresholdLabel = new Label(String.format("Threshold: %.1f %%", brushThreshold.get()*100.0));
		brushThreshold.addListener((observable, oldValue, newValue) -> {
			thresholdLabel.setText(String.format("Threshold: %.1f %%", (double) newValue*100.0));
		});
		GridPane.setColumnSpan(thresholdLabel, GridPane.REMAINING);
		settingsGrid.add(thresholdLabel, 0, 2);
		GridPane.setColumnSpan(thresholdSlider, GridPane.REMAINING);
		settingsGrid.add(thresholdSlider, 0, 3);

		// Angle
		Slider angleSlider = new Slider(-180.0, 180.0, angle.get());
		angleSlider.valueProperty().bindBidirectional(angle);
		Label angleLabel = new Label(String.format("Angle: %.0f \u00b0", angle.get()));
		angle.addListener((observable, oldValue, newValue) -> {
			angleLabel.setText(String.format("Angle: %.0f \u00b0", newValue));
		});
		GridPane.setColumnSpan(angleLabel, GridPane.REMAINING);
		settingsGrid.add(angleLabel, 0, 4);
		GridPane.setColumnSpan(angleSlider, GridPane.REMAINING);
		settingsGrid.add(angleSlider, 0, 5);

		// Randomize rotation
		CheckBox randomizeRotationCheckBox = new CheckBox();
		randomizeRotationCheckBox.selectedProperty().bindBidirectional(randomizeRotation);
		settingsGrid.add(randomizeRotationCheckBox, 0, 6);
		Label randomizeRotationLabel = new Label("Random rotation");
		settingsGrid.add(randomizeRotationLabel, 1, 6);

		// Brush material
		MaterialStateSelector materialSelector = new MaterialStateSelector(material.get());
		materialSelector.setMaxWidth(Double.POSITIVE_INFINITY);
		GridPane.setHgrow(materialSelector, Priority.ALWAYS);

		materialSelector.valueProperty().bindBidirectional(material);
		settingsGrid.add(new Label("Material"), 0, 7);
		GridPane.setColumnSpan(materialSelector, 2);
		settingsGrid.add(materialSelector, 0, 8);

		TitledPane settingsPane = new TitledPane("Tool settings", settingsGrid);
		settingsPane.setAnimated(false);
		settingsPane.setExpanded(true);


		// Masking
		TitledPane maskPane = new TitledPane("Masking", layerMasks);
		layerMasks.setPadding(new Insets(10.0));
		maskPane.setAnimated(false);
		maskPane.setExpanded(false);


		// Add everything to the tool settings pane
		getChildren().setAll(brushPane, settingsPane, maskPane);


		// Bush preview image
		brush.addListener((observable, oldValue, newValue) -> {
			updateBrushPreview(newValue, (float) getBrushThreshold());
		});

		brushThreshold.addListener((observable, oldValue, newValue) -> {
			updateBrushPreview(getBrush(), (float) ((double) newValue));
		});

		material.addListener((observable, oldValue, newValue) -> {
			updateBrushPreview(getBrush(), (float) getBrushThreshold());
		});
	}

	private void updateBrushPreview(Heightbrush brush, float threshold) {
		int brushWidth = brush.getBrushWidth();
		int brushHeight = brush.getBrushHeight();

		WritableImage brushPreviewImage = new WritableImage(brushWidth, brushHeight);
		PixelWriter pw = brushPreviewImage.getPixelWriter();

		// Get material color
		Color color = material.get().getFxColor();
		float r = (float) color.getRed();
		float g = (float) color.getGreen();
		float b = (float) color.getBlue();

		int[] buffer = new int[brushWidth*brushHeight];
		for (int u = 0; u < brushWidth; u++) {
			for (int v = 0; v < brushHeight; v++) {
				float s = brush.getLocalHeight(u, v) >= threshold ? 255.0f : 0.0f;

				int r_ = (int) (r * s);
				int g_ = (int) (g * s);
				int b_ = (int) (b * s);
				int a_ = (int) s;

				buffer[u+v*brushWidth] = (a_ << 24) | (r_ << 16) | (g_ << 8) | b_;
			}
		}

		pw.setPixels(0, 0, brushWidth, brushHeight, PixelFormat.getIntArgbPreInstance(), buffer, 0, brushWidth);

		this.brushPreviewImage = brushPreviewImage;
	}

	public Heightbrush getBrush() {
		return brush.get();
	}

	public Image getBrushPreview() {
		return brushPreviewImage;
	}

	public ObjectProperty<MaterialState<?, ?>> materialProperty() {
		return material;
	}

	public MaterialState<?, ?> getMaterial() {
		return material.get();
	}

	public void setMaterial(MaterialState<?, ?> material) {
		this.material.set(material);
	}

	public DoubleProperty brushThresholdProperty() {
		return brushThreshold;
	}

	public double getBrushThreshold() {
		return brushThreshold.get();
	}

	public void setBrushThreshold(double brushThreshold) {
		this.brushThreshold.set(brushThreshold);
	}

	public DoubleProperty brushSizeProperty() {
		return brushSize;
	}

	public double getBrushSize() {
		return brushSize.get();
	}

	public void setBrushSize(double brushSize) {
		this.brushSize.set(brushSize);
	}

	public DoubleProperty angleProperty() {
		return angle;
	}

	public double getAngle() {
		return angle.get();
	}

	public void setAngle(double angle) {
		this.angle.set(angle);
	}

	public BooleanProperty randomizeRotationProperty() {
		return randomizeRotation;
	}

	public boolean getRandomizeRotation() {
		return randomizeRotation.get();
	}

	public void setRandomizeRotation(boolean randomizeRotation) {
		this.randomizeRotation.set(randomizeRotation);
	}

	public MaskList getLayerMasks() {
		return layerMasks;
	}
}
