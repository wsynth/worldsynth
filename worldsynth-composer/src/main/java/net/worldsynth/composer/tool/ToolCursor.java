/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.composer.tool;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.layout.Pane;
import net.worldsynth.composer.layer.LayerWrapper;
import net.worldsynth.composer.ui.navcanvas.NavigationalCanvas;
import net.worldsynth.composition.layer.Layer;

public class ToolCursor extends Tool<LayerWrapper<?>> {
	
	@Override
	public boolean isApplicableToLayer(Layer layer) {
		return true;
	}
	
	@Override
	public String getName() {
		return "Cursor";
	}
	
	@Override
	protected Image toolIconImage() {
		return new Image((getClass().getClassLoader().getResourceAsStream("icons/tools/cursor.png")));
	}

	@Override
	public Pane getToolSettingsPane() {
		return new Pane();
	}

	@Override
	public boolean onPrimaryPressed(double x, double z, double pressure, LayerWrapper<?> layer) {
		return false;
	}

	@Override
	public boolean onSecondaryPressed(double x, double z, double pressure, LayerWrapper<?> layer) {
		return false;
	}

	@Override
	public boolean onPrimaryDown(double x, double z, double pressure, LayerWrapper<?> layer) {
		return false;
	}

	@Override
	public boolean onSecondaryDown(double x, double z, double pressure, LayerWrapper<?> layer) {
		return false;
	}

	@Override
	public boolean onPrimaryDragged(double x, double z, double pressure, LayerWrapper<?> layer) {
		return false;
	}

	@Override
	public boolean onSecondaryDragged(double x, double z, double pressure, LayerWrapper<?> layer) {
		return false;
	}

	@Override
	public boolean onPrimaryReleased(double x, double z, double pressure, LayerWrapper<?> layer) {
		return false;
	}

	@Override
	public boolean onSecondaryReleased(double x, double z, double pressure, LayerWrapper<?> layer) {
		return false;
	}
	
	@Override
	public boolean onScroll(double deltaX, double deltaY, LayerWrapper<?> layer) {
		return false;
	}

	@Override
	public void renderTool(double x, double z, LayerWrapper<?> layer, GraphicsContext g, NavigationalCanvas navCanvas) {
	}
}
