/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.composer.tool;

import java.util.ArrayList;

import net.worldsynth.composer.layer.LayerWrapper;
import net.worldsynth.composer.layer.LayerWrapperNull;
import net.worldsynth.composer.tool.addon.IToolAddon;
import net.worldsynth.composer.tool.featuremap.ToolFeaturemapMovePoint;
import net.worldsynth.composer.tool.featuremap.ToolFeaturemapPlacePoint;
import net.worldsynth.composer.tool.featuremap.ToolFeaturemapRemovePoint;
import net.worldsynth.composer.tool.featuremap.ToolFeaturemapRemovePoints;
import net.worldsynth.composer.tool.featuremap.ToolFeaturemapScatterPoints;
import net.worldsynth.composer.tool.heightmap.ToolHeightmapElevation;
import net.worldsynth.composer.tool.heightmap.ToolHeightmapFlatten;
import net.worldsynth.composer.tool.heightmap.ToolHeightmapSmoothen;
import net.worldsynth.composer.tool.heightmap.ToolHeightmapStamp;
import net.worldsynth.composer.tool.materialmap.ToolMaterialmapPaint;
import net.worldsynth.composition.layer.LayerNull;

public class ToolRegistry {
	
	public static ArrayList<Tool<?>> REGISTER = new ArrayList<>();
	
	public ToolRegistry(IToolAddon addonLoader) {
		registerNativeTools();

		// Register tools form addons
		REGISTER.addAll(addonLoader.getAddonTools());
	}
	
	private void registerNativeTools() {
		// Null tools
		REGISTER.add(new ToolCursor());
		
		// Heightmap tools
		REGISTER.add(new ToolHeightmapElevation());
		REGISTER.add(new ToolHeightmapFlatten());
		REGISTER.add(new ToolHeightmapSmoothen());
		REGISTER.add(new ToolHeightmapStamp());
		
		// Materialmap tools
		REGISTER.add(new ToolMaterialmapPaint());
		
		// Featuremap tools
		REGISTER.add(new ToolFeaturemapPlacePoint());
		REGISTER.add(new ToolFeaturemapRemovePoint());
		REGISTER.add(new ToolFeaturemapMovePoint());
		REGISTER.add(new ToolFeaturemapScatterPoints());
		REGISTER.add(new ToolFeaturemapRemovePoints());
	}
	
	public static ArrayList<Tool<?>> getToolsForLayer(LayerWrapper<?> layer) {
		if (layer == null) {
			layer = new LayerWrapperNull(new LayerNull("null"), null);
		}
		
		ArrayList<Tool<?>> layerTools = new ArrayList<>();
		for (Tool<?> t: REGISTER) {
			if (t.isApplicableToLayer(layer.getLayer())) {
				layerTools.add(t);
			}
		}
		
		return layerTools;
	}
}
