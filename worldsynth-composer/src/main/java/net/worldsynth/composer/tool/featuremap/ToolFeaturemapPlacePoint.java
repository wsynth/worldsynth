/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.composer.tool.featuremap;

import java.util.Random;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.StrokeLineCap;
import net.worldsynth.composer.layer.featuremap.LayerWrapperFeaturemap;
import net.worldsynth.composer.ui.navcanvas.Coordinate;
import net.worldsynth.composer.ui.navcanvas.NavigationalCanvas;
import net.worldsynth.composer.ui.navcanvas.Pixel;
import net.worldsynth.featurepoint.Featurepoint2D;
import net.worldsynth.util.math.MathHelperScalar;

public class ToolFeaturemapPlacePoint extends ToolFeaturemap {
	
	private Random r = new Random();
	
	private boolean primaryActive = false;
	
	@Override
	public String getName() {
		return "Place point";
	}
	
	@Override
	protected Image toolIconImage() {
		return new Image((getClass().getClassLoader().getResourceAsStream("icons/tools/Point place tool.png")));
	}

	@Override
	public Pane getToolSettingsPane() {
		return new Pane();
	}

	@Override
	public boolean onPrimaryPressed(double x, double z, double pressure, LayerWrapperFeaturemap layer) {
		primaryActive = true;
		apply(x, z, layer);
		return true;
	}

	@Override
	public boolean onSecondaryPressed(double x, double z, double pressure, LayerWrapperFeaturemap layer) {
		return false;
	}

	@Override
	public boolean onPrimaryDown(double x, double z, double pressure, LayerWrapperFeaturemap layer) {
		return false;
	}

	@Override
	public boolean onSecondaryDown(double x, double z, double pressure, LayerWrapperFeaturemap layer) {
		return false;
	}

	@Override
	public boolean onPrimaryDragged(double x, double z, double pressure, LayerWrapperFeaturemap layer) {
		return false;
	}

	@Override
	public boolean onSecondaryDragged(double x, double z, double pressure, LayerWrapperFeaturemap layer) {
		return false;
	}

	@Override
	public boolean onPrimaryReleased(double x, double z, double pressure, LayerWrapperFeaturemap layer) {
		primaryActive = false;
		return false;
	}

	@Override
	public boolean onSecondaryReleased(double x, double z, double pressure, LayerWrapperFeaturemap layer) {
		return false;
	}
	
	@Override
	public boolean onScroll(double deltaX, double deltaY, LayerWrapperFeaturemap layer) {
		return false;
	}
	
	private void apply(double x, double z, LayerWrapperFeaturemap layer) {
		layer.addFeature(new Featurepoint2D(x, z, r.nextLong()));
	}
	
	@Override
	public void renderTool(double x, double z, LayerWrapperFeaturemap layer, GraphicsContext g, NavigationalCanvas navCanvas) {
		if (primaryActive) return;
		
		g.setStroke(Color.GREEN);
		g.setLineWidth(MathHelperScalar.clamp(1.0 * navCanvas.getZoom(), 2.0, 4.0));
		g.setLineCap(StrokeLineCap.ROUND);
		
		Pixel pc  = new Pixel(new Coordinate(x, z), navCanvas);
		g.strokeOval(pc.x-5*navCanvas.getZoom(), pc.y-5*navCanvas.getZoom(), 10*navCanvas.getZoom(), 10*navCanvas.getZoom());
		g.strokeLine(pc.x, pc.y-10*navCanvas.getZoom(), pc.x, pc.y+10*navCanvas.getZoom());
		g.strokeLine(pc.x-10*navCanvas.getZoom(), pc.y, pc.x+10*navCanvas.getZoom(), pc.y);
	}
}
