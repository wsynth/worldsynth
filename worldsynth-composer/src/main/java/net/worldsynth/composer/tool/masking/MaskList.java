/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.composer.tool.masking;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.layout.VBox;
import net.worldsynth.composer.fx.LayerDropField;
import net.worldsynth.composer.layer.LayerWrapper;

public class MaskList extends VBox {

	private final ObservableList<LayerMask> layerMasks = FXCollections.observableArrayList();

	public MaskList() {
		getStyleClass().add("mask-list");

		setSpacing(4.0);

		LayerDropField<?> addLayerMaskField = new LayerDropField<>(LayerWrapper.DATAFORMAT_LAYER, null, "Drag and drop layer here to add mask");
		addLayerMaskField.layerProperty().addListener((observable, oldValue, newValue) -> {
			if (newValue == null) return;
			addLayerMaskField.setLayer(null);

			LayerMask newMask = ((LayerWrapper) newValue).createMask();
			if (newMask == null) return;

			newMask.setOnRemove(event -> removeMask(newMask));
			addMask(newMask);
		});
		getChildren().add(addLayerMaskField);
	}

	public boolean maskedAt(int x, int z) {
		for (LayerMask mask: layerMasks) {
			if (mask.isMaskEnabled() && !mask.maskAt(x, z)) return false;
		}

		return true;
	}

	public void addMask(LayerMask mask) {
		layerMasks.add(mask);
		getChildren().add(getChildren().size()-1, mask.getSettingsPane());
	}

	public void removeMask(LayerMask mask) {
		layerMasks.remove(mask);
		getChildren().remove(mask);
	}
}
