/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.composer.tool.masking;

import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;
import net.worldsynth.composer.layer.materialmap.LayerWrapperMaterialmap;
import net.worldsynth.fx.control.MaterialStateSelector;
import net.worldsynth.material.MaterialRegistry;
import net.worldsynth.material.MaterialState;

public class MaterialmapMask extends LayerMask<LayerWrapperMaterialmap> {

	private final MaterialStateSelector onlyOnMaterial = new MaterialStateSelector(MaterialRegistry.getDefaultMaterial().getDefaultState());
	private final CheckBox onlyOnMaterialEnable = new CheckBox();

	private final MaterialStateSelector exceptOnMaterial = new MaterialStateSelector(MaterialRegistry.getDefaultMaterial().getDefaultState());
	private final CheckBox exceptOnMaterialEnable = new CheckBox();

	public MaterialmapMask(LayerWrapperMaterialmap layer) {
		super(layer);

		GridPane maskSettings = new GridPane();
		maskSettings.setHgap(5.0);
		maskSettings.setVgap(2.0);

		Label elevationLabel = new Label("Only on: ");
		elevationLabel.setMinWidth(Region.USE_PREF_SIZE);
		maskSettings.add(elevationLabel, 0, 0, 2, 1);

		onlyOnMaterial.setMaxWidth(Double.POSITIVE_INFINITY);
		GridPane.setHgrow(onlyOnMaterial, Priority.ALWAYS);
		maskSettings.add(onlyOnMaterial, 1, 0);
		maskSettings.add(onlyOnMaterialEnable, 2, 0);

		Label angleLabel = new Label("Except on: ");
		angleLabel.setMinWidth(Region.USE_PREF_SIZE);
		maskSettings.add(angleLabel, 0, 1);

		exceptOnMaterial.setMaxWidth(Double.POSITIVE_INFINITY);
		GridPane.setHgrow(exceptOnMaterial, Priority.ALWAYS);
		maskSettings.add(exceptOnMaterial, 1, 1);
		maskSettings.add(exceptOnMaterialEnable, 2, 1);

		setCenter(maskSettings);
	}

	public boolean maskAt(int x, int z) {
		MaterialState<?, ?> material = layer.getValueAt(x, z);

		if (onlyOnMaterialEnable.isSelected()) {
			MaterialState<?, ?> m = onlyOnMaterial.getValue();
			if (material != m) return false;
		}

		if (exceptOnMaterialEnable.isSelected()) {
			MaterialState<?, ?> m = exceptOnMaterial.getValue();
			if (material == m) return false;
		}

		return true;
	}
}
