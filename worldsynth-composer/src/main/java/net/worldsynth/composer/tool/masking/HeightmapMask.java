/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.composer.tool.masking;

import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.Spinner;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;
import net.worldsynth.composer.layer.heightmap.LayerWrapperHeightmap;

public class HeightmapMask extends LayerMask<LayerWrapperHeightmap> {

	private final Spinner<Double> aboveElevationSpinner = new Spinner(Double.MIN_VALUE, Double.MAX_VALUE, 0.0, 1.0);
	private final CheckBox aboveElevationEnable = new CheckBox();

	private final Spinner<Double> belowElevationSpinner = new Spinner(Double.MIN_VALUE, Double.MAX_VALUE, 0.0, 1.0);
	private final CheckBox belowElevationEnable = new CheckBox();

	private final Spinner<Double> aboveAngleSpinner = new Spinner(0.0, 90.0, 0.0, 1.0);
	private final CheckBox aboveAngleEnable = new CheckBox();

	private final Spinner<Double> belowAngleSpinner = new Spinner(0.0, 90.0, 0.0, 1.0);
	private final CheckBox belowAngleEnable = new CheckBox();

	public HeightmapMask(LayerWrapperHeightmap layer) {
		super(layer);

		belowElevationSpinner.setEditable(true);
		aboveElevationSpinner.setEditable(true);
		belowAngleSpinner.setEditable(true);
		aboveAngleSpinner.setEditable(true);

		GridPane maskSettings = new GridPane();
		maskSettings.setHgap(5.0);
		maskSettings.setVgap(2.0);

		maskSettings.add(new Label("Above"), 1, 0, 2, 1);
		maskSettings.add(new Label("Below"), 3, 0, 2, 1);

		Label elevationLabel = new Label("Elevation: ");
		elevationLabel.setMinWidth(Region.USE_PREF_SIZE);
		elevationLabel.setPrefWidth(54);
		maskSettings.add(elevationLabel, 0, 1, 2, 1);

		aboveElevationSpinner.setMaxWidth(Double.POSITIVE_INFINITY);
		GridPane.setHgrow(aboveElevationSpinner, Priority.ALWAYS);
		maskSettings.add(aboveElevationSpinner, 1, 1);
		maskSettings.add(aboveElevationEnable, 2, 1);

		belowElevationSpinner.setMaxWidth(Double.POSITIVE_INFINITY);
		GridPane.setHgrow(belowElevationSpinner, Priority.ALWAYS);
		maskSettings.add(belowElevationSpinner, 3, 1);
		maskSettings.add(belowElevationEnable, 4, 1);

		Label angleLabel = new Label("Angle: ");
		angleLabel.setMinWidth(Region.USE_PREF_SIZE);
		angleLabel.setPrefWidth(54);
		maskSettings.add(angleLabel, 0, 2);

		aboveAngleSpinner.setMaxWidth(Double.POSITIVE_INFINITY);
		GridPane.setHgrow(aboveAngleSpinner, Priority.ALWAYS);
		maskSettings.add(aboveAngleSpinner, 1, 2);
		maskSettings.add(aboveAngleEnable, 2, 2);

		belowAngleSpinner.setMaxWidth(Double.POSITIVE_INFINITY);
		GridPane.setHgrow(belowAngleSpinner, Priority.ALWAYS);
		maskSettings.add(belowAngleSpinner, 3, 2);
		maskSettings.add(belowAngleEnable, 4, 2);

		setCenter(maskSettings);
	}

	public boolean maskAt(int x, int z) {
		float elevation = layer.getValueAt(x, z);
		float dx = ((elevation - layer.getValueAt(x-1, z)) + (layer.getValueAt(x+1, z) - elevation)) / 2.0f;
		float dy = ((elevation - layer.getValueAt(x, z-1)) + (layer.getValueAt(x, z+1) - elevation)) / 2.0f;
		double slope = Math.sqrt(dx*dx + dy*dy);
		slope = Math.atan(slope);
		slope = slope * 180 / Math.PI;

		if (aboveElevationEnable.isSelected()) {
			double threshold = aboveElevationSpinner.getValue();
			if (elevation < threshold) return false;
		}

		if (belowElevationEnable.isSelected()) {
			double threshold = belowElevationSpinner.getValue();
			if (elevation > threshold) return false;
		}

		if (aboveAngleEnable.isSelected()) {
			double threshold = aboveAngleSpinner.getValue();
			if (slope < threshold) return false;
		}

		if (belowAngleEnable.isSelected()) {
			double threshold = belowAngleSpinner.getValue();
			if (slope > threshold) return false;
		}

		return true;
	}
}
