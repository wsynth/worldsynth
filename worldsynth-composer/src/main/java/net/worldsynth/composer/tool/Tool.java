/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.composer.tool;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.layout.Pane;
import net.worldsynth.composer.layer.LayerWrapper;
import net.worldsynth.composer.ui.navcanvas.NavigationalCanvas;
import net.worldsynth.composition.layer.Layer;

public abstract class Tool<T extends LayerWrapper<? extends Layer>> {
	private Image toolIconImage;
	
	public Tool() {
		toolIconImage = toolIconImage();
	}
	
	public abstract String getName();
	protected abstract Image toolIconImage();
	
	public Image getToolIconImage() {
		return toolIconImage;
	}
	
	public abstract Pane getToolSettingsPane();
	
	public abstract boolean isApplicableToLayer(Layer layer);
	
	public abstract boolean onPrimaryPressed(double x, double z, double pressure, T layer);
	public abstract boolean onSecondaryPressed(double x, double z, double pressure, T layer);
	public abstract boolean onPrimaryDown(double x, double z, double pressure, T layer);
	public abstract boolean onSecondaryDown(double x, double z, double pressure, T layer);
	public abstract boolean onPrimaryDragged(double x, double z, double pressure, T layer);
	public abstract boolean onSecondaryDragged(double x, double z, double pressure, T layer);
	public abstract boolean onPrimaryReleased(double x, double z, double pressure, T layer);
	public abstract boolean onSecondaryReleased(double x, double z, double pressure, T layer);
	
	public abstract boolean onScroll(double deltaX, double deltaY, T layer);
	
	public abstract void renderTool(double x, double z, T layer, GraphicsContext g, NavigationalCanvas navCanvas);
}
