/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.composer.tool.heightmap;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.geometry.Insets;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import net.worldsynth.composer.brush.heightmap.Heightbrush;
import net.worldsynth.composer.ui.brushbrowser.BrushBrowser;

public class ToolHeightmapElevationSettings extends VBox {
	
	private final ObjectProperty<Heightbrush> brush = new SimpleObjectProperty<Heightbrush>(null);
	private final DoubleProperty strength = new SimpleDoubleProperty(1.0);
	private final DoubleProperty size = new SimpleDoubleProperty(1.0);
	private final DoubleProperty angle = new SimpleDoubleProperty(0.0);
	private final BooleanProperty randomizeRotatation = new SimpleBooleanProperty(false);
	private final BooleanProperty clamp = new SimpleBooleanProperty(true);
	
	public ToolHeightmapElevationSettings() {
		// Brush browser
		BrushBrowser brushBrowser = new BrushBrowser();
		brushBrowser.setPrefHeight(300);
		brushBrowser.setMinHeight(300);
		brushBrowser.selectedBrushProperty().addListener((observable, oldValue, newValue) -> {
			brush.set((Heightbrush) newValue);
		});

		TitledPane brushPane = new TitledPane("Brushes", brushBrowser);
		brushPane.setAnimated(false);
		brushPane.setExpanded(true);


		// Tool settings
		GridPane settingsGrid = new GridPane();
		settingsGrid.prefWidthProperty().bind(widthProperty());
		settingsGrid.setPadding(new Insets(10.0));
		settingsGrid.setHgap(5.0);
		settingsGrid.setVgap(2.0);
		ColumnConstraints col1 = new ColumnConstraints(0, Control.USE_COMPUTED_SIZE, Control.USE_COMPUTED_SIZE);
		ColumnConstraints col2 = new ColumnConstraints(0, Control.USE_COMPUTED_SIZE, Double.MAX_VALUE);
		col2.setHgrow(Priority.ALWAYS);
		settingsGrid.getColumnConstraints().addAll(col1, col2);
		
		// Strength
		Slider strengthSlider = new Slider(0.0, 1.0, strength.get());
		strengthSlider.valueProperty().bindBidirectional(strength);
		Label strengthLabel = new Label(String.format("Strength: %.1f %%", strength.get()*100.0));
		strength.addListener((observable, oldValue, newValue) -> {
			strengthLabel.setText(String.format("Strength: %.1f %%", (double) newValue*100.0));
		});
		GridPane.setColumnSpan(strengthLabel, GridPane.REMAINING);
		settingsGrid.add(strengthLabel, 0, 0);
		GridPane.setColumnSpan(strengthSlider, GridPane.REMAINING);
		settingsGrid.add(strengthSlider, 0, 1);
		
		// Size
		Slider sizeSlider = new Slider(0.0, 2.0, size.get());
		sizeSlider.valueProperty().bindBidirectional(size);
		Label sizeLabel = new Label(String.format("Size: %.1f %%", size.get()*100.0));
		size.addListener((observable, oldValue, newValue) -> {
			sizeLabel.setText(String.format("Size: %.1f %%", (double) newValue*100.0));
		});
		GridPane.setColumnSpan(sizeLabel, GridPane.REMAINING);
		settingsGrid.add(sizeLabel, 0, 2);
		GridPane.setColumnSpan(sizeSlider, GridPane.REMAINING);
		settingsGrid.add(sizeSlider, 0, 3);
		
		// Angle
		Slider angleSlider = new Slider(-180.0, 180.0, angle.get());
		angleSlider.valueProperty().bindBidirectional(angle);
		Label angleLabel = new Label(String.format("Angle: %.0f \u00b0", angle.get()));
		angle.addListener((observable, oldValue, newValue) -> {
			angleLabel.setText(String.format("Angle: %.0f \u00b0", newValue));
		});
		GridPane.setColumnSpan(angleLabel, GridPane.REMAINING);
		settingsGrid.add(angleLabel, 0, 4);
		GridPane.setColumnSpan(angleSlider, GridPane.REMAINING);
		settingsGrid.add(angleSlider, 0, 5);
		
		// Randomize rotation
		CheckBox randomizeRotationCheckboBox = new CheckBox();
		randomizeRotationCheckboBox.selectedProperty().bindBidirectional(randomizeRotatation);
		settingsGrid.add(randomizeRotationCheckboBox, 0, 6);
		Label randomizeRotationLabel = new Label("Random rotation");
		settingsGrid.add(randomizeRotationLabel, 1, 6);
		
		// Clamp
		CheckBox clampCheckBox = new CheckBox();
		clampCheckBox.selectedProperty().bindBidirectional(clamp);
		settingsGrid.add(clampCheckBox, 0, 7);
		Label clampLabel = new Label("Clamp");
		settingsGrid.add(clampLabel, 1, 7);

		TitledPane settingsPane = new TitledPane("Tool settings", settingsGrid);
		settingsPane.setAnimated(false);
		settingsPane.setExpanded(true);


		// Add everything to the tool settings pane
		getChildren().setAll(brushPane, settingsPane);
	}
	
	public Heightbrush getBrush() {
		return brush.get();
	}
	
	public DoubleProperty strengthProperty() {
		return strength;
	}
	
	public double getStrength() {
		return strength.get();
	}
	
	public void setStrength(double strength) {
		this.strength.set(strength);
	}
	
	public DoubleProperty sizeProperty() {
		return size;
	}
	
	public double getSize() {
		return size.get();
	}
	
	public void setSize(double size) {
		this.size.set(size);
	}
	
	public DoubleProperty angleProperty() {
		return angle;
	}
	
	public double getAngle() {
		return angle.get();
	}
	
	public void setAngle(double angle) {
		this.angle.set(angle);
	}
	
	public BooleanProperty randomizeRotatationProperty() {
		return randomizeRotatation;
	}
	
	public boolean getRandomizeRotatation() {
		return randomizeRotatation.get();
	}
	
	public void setRandomizeRotatation(boolean randomizeRotatation) {
		this.randomizeRotatation.set(randomizeRotatation);
	}
	
	public BooleanProperty clampProperty() {
		return clamp;
	}
	
	public boolean getClamp() {
		return clamp.get();
	}
	
	public void setClamp(boolean clamp) {
		this.clamp.set(clamp);
	}
}
