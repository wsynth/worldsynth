/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.composer.tool.heightmap;

import net.worldsynth.composer.layer.heightmap.LayerWrapperHeightmap;
import net.worldsynth.composer.tool.Tool;
import net.worldsynth.composition.layer.Layer;
import net.worldsynth.composition.layer.heightmap.LayerHeightmap;

public abstract class ToolHeightmap extends Tool<LayerWrapperHeightmap> {
	
	@Override
	public boolean isApplicableToLayer(Layer layer) {
		return layer instanceof LayerHeightmap;
	}
}
