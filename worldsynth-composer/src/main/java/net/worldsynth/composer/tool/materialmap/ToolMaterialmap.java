/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.composer.tool.materialmap;

import net.worldsynth.composer.layer.materialmap.LayerWrapperMaterialmap;
import net.worldsynth.composer.tool.Tool;
import net.worldsynth.composition.layer.Layer;
import net.worldsynth.composition.layer.materialmap.LayerMaterialmap;

public abstract class ToolMaterialmap extends Tool<LayerWrapperMaterialmap> {
	
	@Override
	public boolean isApplicableToLayer(Layer layer) {
		return layer instanceof LayerMaterialmap;
	}
}
