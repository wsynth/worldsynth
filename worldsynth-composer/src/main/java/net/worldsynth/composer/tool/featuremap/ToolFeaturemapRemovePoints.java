/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.composer.tool.featuremap;

import java.util.ArrayList;
import java.util.List;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.geometry.Insets;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.image.Image;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Priority;
import javafx.scene.paint.Color;
import javafx.scene.shape.StrokeLineCap;
import net.worldsynth.composer.layer.featuremap.LayerWrapperFeaturemap;
import net.worldsynth.composer.ui.navcanvas.Coordinate;
import net.worldsynth.composer.ui.navcanvas.NavigationalCanvas;
import net.worldsynth.composer.ui.navcanvas.Pixel;
import net.worldsynth.extent.Extent;
import net.worldsynth.featurepoint.Featurepoint2D;
import net.worldsynth.util.math.MathHelperScalar;

public class ToolFeaturemapRemovePoints extends ToolFeaturemap {
	
	private final DoubleProperty brushRadius = new SimpleDoubleProperty(10.0);
	
	@Override
	public String getName() {
		return "Remove points";
	}
	
	@Override
	protected Image toolIconImage() {
		return new Image((getClass().getClassLoader().getResourceAsStream("icons/tools/Points remove tool.png")));
	}

	@Override
	public Pane getToolSettingsPane() {
		GridPane parametersPane = new GridPane();
		parametersPane.setPadding(new Insets(10.0));
		parametersPane.setHgap(5.0);
		parametersPane.setVgap(2.0);
		
		Slider sizeSlider = new Slider(0.0, 200.0, brushRadius.get());
		sizeSlider.valueProperty().bindBidirectional(brushRadius);
		Label sizeLabel = new Label(String.format("Radius: %.0f", brushRadius.get()));
		brushRadius.addListener((observable, oldValue, newValue) -> {
			sizeLabel.setText(String.format("Radius: %.0f", newValue));
		});
		parametersPane.add(sizeLabel, 0, 0);
		GridPane.setHgrow(sizeSlider, Priority.ALWAYS);
		GridPane.setColumnSpan(sizeSlider, 2);
		parametersPane.add(sizeSlider, 0, 1);
		
		return parametersPane;
	}

	@Override
	public boolean onPrimaryPressed(double x, double z, double pressure, LayerWrapperFeaturemap layer) {
		apply(x, z, layer);
		return true;
	}

	@Override
	public boolean onSecondaryPressed(double x, double z, double pressure, LayerWrapperFeaturemap layer) {
		return false;
	}

	@Override
	public boolean onPrimaryDown(double x, double z, double pressure, LayerWrapperFeaturemap layer) {
		return false;
	}

	@Override
	public boolean onSecondaryDown(double x, double z, double pressure, LayerWrapperFeaturemap layer) {
		return false;
	}

	@Override
	public boolean onPrimaryDragged(double x, double z, double pressure, LayerWrapperFeaturemap layer) {
		apply(x, z, layer);
		return true;
	}

	@Override
	public boolean onSecondaryDragged(double x, double z, double pressure, LayerWrapperFeaturemap layer) {
		return false;
	}

	@Override
	public boolean onPrimaryReleased(double x, double z, double pressure, LayerWrapperFeaturemap layer) {
		return false;
	}

	@Override
	public boolean onSecondaryReleased(double x, double z, double pressure, LayerWrapperFeaturemap layer) {
		return false;
	}
	
	@Override
	public boolean onScroll(double deltaX, double deltaY, LayerWrapperFeaturemap layer) {
		brushRadius.set(Math.max(1.0, brushRadius.get() - deltaY));
		return true;
	}
	
	private void apply(double x, double z, LayerWrapperFeaturemap layer) {
		List<Featurepoint2D> f = getClose(x, z, brushRadius.get(), layer);
		if (f != null) {
			layer.removeFeatures(f);
		}
	}
	
	private List<Featurepoint2D> getClose(double x, double z, double maxDist, LayerWrapperFeaturemap layer) {
		Extent extent = new Extent(x-maxDist, 0, z-maxDist, maxDist*2, 0, maxDist*2);
		
		ArrayList<Featurepoint2D> close = new ArrayList<Featurepoint2D>();
		
		List<Featurepoint2D> fs = layer.getFeatures(extent);
		for (Featurepoint2D f: fs) {
			if (dist(x, z, f) < maxDist) {
				close.add(f);
			}
		}
		
		return close;
	}
	
	private double dist(double x, double z, Featurepoint2D f) {
		return Math.sqrt((x - f.getX()) * (x - f.getX()) + (z - f.getZ()) * (z - f.getZ()));
	}

	@Override
	public void renderTool(double x, double z, LayerWrapperFeaturemap layer, GraphicsContext g, NavigationalCanvas navCanvas) {
		g.setStroke(Color.RED);
		g.setLineWidth(MathHelperScalar.clamp(1.0 * navCanvas.getZoom(), 2.0, 4.0));
		g.setLineCap(StrokeLineCap.ROUND);
		
		Pixel pc  = new Pixel(new Coordinate(x, z), navCanvas);
		g.strokeOval(pc.x-brushRadius.get()*navCanvas.getZoom(), pc.y-brushRadius.get()*navCanvas.getZoom(), brushRadius.get()*navCanvas.getZoom()*2, brushRadius.get()*navCanvas.getZoom()*2);
		
		List<Featurepoint2D> fs = getClose(x, z, brushRadius.get(), layer);
		
		fs.forEach(f -> {
			renderPoint(f, g, navCanvas);
		});
	}
	
	private void renderPoint(Featurepoint2D feature, GraphicsContext g, NavigationalCanvas navCanvas) {
		Pixel pc  = new Pixel(new Coordinate(feature.getX(), feature.getZ()), navCanvas);
		g.strokeOval(pc.x-5*navCanvas.getZoom(), pc.y-5*navCanvas.getZoom(), 10*navCanvas.getZoom(), 10*navCanvas.getZoom());
		g.strokeLine(pc.x-7*navCanvas.getZoom(), pc.y-7*navCanvas.getZoom(), pc.x+7*navCanvas.getZoom(), pc.y+7*navCanvas.getZoom());
		g.strokeLine(pc.x-7*navCanvas.getZoom(), pc.y+7*navCanvas.getZoom(), pc.x+7*navCanvas.getZoom(), pc.y-7*navCanvas.getZoom());
	}
}
