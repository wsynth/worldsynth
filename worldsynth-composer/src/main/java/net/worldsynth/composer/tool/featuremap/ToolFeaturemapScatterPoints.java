/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.composer.tool.featuremap;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.geometry.Insets;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.image.Image;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Priority;
import javafx.scene.paint.Color;
import javafx.scene.shape.StrokeLineCap;
import net.worldsynth.composer.layer.featuremap.LayerWrapperFeaturemap;
import net.worldsynth.composer.ui.navcanvas.Coordinate;
import net.worldsynth.composer.ui.navcanvas.NavigationalCanvas;
import net.worldsynth.composer.ui.navcanvas.Pixel;
import net.worldsynth.extent.Extent;
import net.worldsynth.featurepoint.Featurepoint2D;
import net.worldsynth.util.math.MathHelperScalar;

public class ToolFeaturemapScatterPoints extends ToolFeaturemap {
	
	private final DoubleProperty brushRadius = new SimpleDoubleProperty(50.0);
	private final DoubleProperty density = new SimpleDoubleProperty(10.0);
	
	private Random r = new Random();
	
	@Override
	public String getName() {
		return "Scatter points";
	}
	
	@Override
	protected Image toolIconImage() {
		return new Image((getClass().getClassLoader().getResourceAsStream("icons/tools/Points scatter tool.png")));
	}

	@Override
	public Pane getToolSettingsPane() {
		GridPane parametersPane = new GridPane();
		parametersPane.setPadding(new Insets(10.0));
		parametersPane.setHgap(5.0);
		parametersPane.setVgap(2.0);
		
		// Brush radius
		Slider sizeSlider = new Slider(1.0, 200.0, brushRadius.get());
		sizeSlider.valueProperty().bindBidirectional(brushRadius);
		Label sizeLabel = new Label(String.format("Radius: %.0f", brushRadius.get()));
		brushRadius.addListener((observable, oldValue, newValue) -> {
			sizeLabel.setText(String.format("Radius: %.0f", newValue));
		});
		parametersPane.add(sizeLabel, 0, 0);
		GridPane.setHgrow(sizeSlider, Priority.ALWAYS);
		GridPane.setColumnSpan(sizeSlider, 2);
		parametersPane.add(sizeSlider, 0, 1);
		
		// Density
		Slider densitySlider = new Slider(1.0, 200.0, density.get());
		densitySlider.valueProperty().bindBidirectional(density);
		Label desityLabel = new Label(String.format("Density: %.0f", density.get()));
		density.addListener((observable, oldValue, newValue) -> {
			desityLabel.setText(String.format("Density: %.0f", newValue));
		});
		parametersPane.add(desityLabel, 0, 2);
		GridPane.setHgrow(densitySlider, Priority.ALWAYS);
		GridPane.setColumnSpan(densitySlider, 2);
		parametersPane.add(densitySlider, 0, 3);
		
		return parametersPane;
	}

	@Override
	public boolean onPrimaryPressed(double x, double z, double pressure, LayerWrapperFeaturemap layer) {
		List<Featurepoint2D> bound = getClose(x, z, brushRadius.get()+density.get()*2, layer);
		List<Featurepoint2D> seeds = new ArrayList<Featurepoint2D>(bound);
		
		if (seeds.isEmpty()) {
			Featurepoint2D f = new Featurepoint2D(x, z, r.nextLong());
			layer.addFeature(f);
			bound.add(f);
			seeds.add(f);
		}
		
		apply(x, z, seeds, bound, layer);
		return true;
	}

	@Override
	public boolean onSecondaryPressed(double x, double z, double pressure, LayerWrapperFeaturemap layer) {
		return false;
	}

	@Override
	public boolean onPrimaryDown(double x, double z, double pressure, LayerWrapperFeaturemap layer) {
		return false;
	}

	@Override
	public boolean onSecondaryDown(double x, double z, double pressure, LayerWrapperFeaturemap layer) {
		return false;
	}

	@Override
	public boolean onPrimaryDragged(double x, double z, double pressure, LayerWrapperFeaturemap layer) {
		List<Featurepoint2D> bound = getClose(x, z, brushRadius.get()+density.get()*2, layer);
		List<Featurepoint2D> seeds = new ArrayList<Featurepoint2D>(bound);
		
		apply(x, z, seeds, bound, layer);
		return true;
	}

	@Override
	public boolean onSecondaryDragged(double x, double z, double pressure, LayerWrapperFeaturemap layer) {
		return false;
	}

	@Override
	public boolean onPrimaryReleased(double x, double z, double pressure, LayerWrapperFeaturemap layer) {
		return false;
	}

	@Override
	public boolean onSecondaryReleased(double x, double z, double pressure, LayerWrapperFeaturemap layer) {
		return false;
	}
	
	@Override
	public boolean onScroll(double deltaX, double deltaY, LayerWrapperFeaturemap layer) {
		brushRadius.set(Math.max(1.0, brushRadius.get() - deltaY));
		density.set(Math.max(1.0, density.get() - deltaX));
		return true;
	}
	
	private void apply(double x, double z, List<Featurepoint2D> seeds, List<Featurepoint2D> bound, LayerWrapperFeaturemap layer) {
		int maxSamples = 30;
		double densityFactor = 2.0;
		
		while (seeds.size() > 0) {
			Featurepoint2D seed = seeds.remove(r.nextInt(seeds.size()));
			
			for (int i = 0; i < maxSamples; i++) {
				// Create a new feature at a random distance and direction from the seed
				double rdist = density.get() + density.get() * densityFactor * r.nextDouble();
				double rdir = Math.PI * (r.nextDouble() * 2.0 - 1.0);
				double dx = rdist * Math.cos(rdir);
				double dz = rdist * Math.sin(rdir);
				Featurepoint2D newFeature = new Featurepoint2D(seed.getX() + dx, seed.getZ() + dz, r.nextLong());
				
				// Check if the new feature is a valid placement
				if (dist(x, z, newFeature) < brushRadius.get() && validatePlacement(newFeature, bound, density.get())) {
					layer.addFeature(newFeature);
					bound.add(newFeature);
					seeds.add(newFeature);
					break;
				}
			}
		}
	}
	
	private List<Featurepoint2D> getClose(double x, double z, double maxDist, LayerWrapperFeaturemap layer) {
		Extent extent = new Extent(x-maxDist, 0, z-maxDist, maxDist*2, 0, maxDist*2);
		
		ArrayList<Featurepoint2D> close = new ArrayList<Featurepoint2D>();
		
		List<Featurepoint2D> fs = layer.getFeatures(extent);
		for (Featurepoint2D f: fs) {
			if (dist(x, z, f) < maxDist) {
				close.add(f);
			}
		}
		
		return close;
	}
	
	private boolean validatePlacement(Featurepoint2D p, List<Featurepoint2D> existing, double density) {
		for (Featurepoint2D n: existing) {
			if (dist(n, p) <= density) return false;
		}
		
		return true;
	}
	
	private double dist(Featurepoint2D f1, Featurepoint2D f2) {
		return dist(f1.getX(), f1.getZ(), f2);
	}
	
	private double dist(double x, double z, Featurepoint2D f) {
		return Math.sqrt((x - f.getX()) * (x - f.getX()) + (z - f.getZ()) * (z - f.getZ()));
	}
	
	@Override
	public void renderTool(double x, double z, LayerWrapperFeaturemap layer, GraphicsContext g, NavigationalCanvas navCanvas) {
		g.setStroke(Color.GREEN);
		g.setLineWidth(MathHelperScalar.clamp(1.0 * navCanvas.getZoom(), 2.0, 4.0));
		g.setLineCap(StrokeLineCap.ROUND);
		
		Pixel pc  = new Pixel(new Coordinate(x, z), navCanvas);
		g.strokeOval(pc.x-brushRadius.get()*navCanvas.getZoom(), pc.y-brushRadius.get()*navCanvas.getZoom(), brushRadius.get()*navCanvas.getZoom()*2, brushRadius.get()*navCanvas.getZoom()*2);
	}
}
