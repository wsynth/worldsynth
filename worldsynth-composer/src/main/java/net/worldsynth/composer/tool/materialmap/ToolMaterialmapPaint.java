/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.composer.tool.materialmap;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.transform.Affine;
import net.worldsynth.composer.brush.heightmap.Heightbrush;
import net.worldsynth.composer.layer.materialmap.LayerWrapperMaterialmap;
import net.worldsynth.composer.ui.navcanvas.Coordinate;
import net.worldsynth.composer.ui.navcanvas.NavigationalCanvas;
import net.worldsynth.composer.ui.navcanvas.Pixel;
import net.worldsynth.material.MaterialState;
import net.worldsynth.util.gen.Permutation;
import net.worldsynth.util.math.MathHelperScalar;
import net.worldsynth.util.math.Vector2d;

public class ToolMaterialmapPaint extends ToolMaterialmap {

	private final ToolMaterialmapPaintSettings settings = new ToolMaterialmapPaintSettings();

	private final Permutation rotationHashPermutation = new Permutation(256, 1);

	private boolean primaryActive = false;
	private boolean secondaryActive = false;

	@Override
	public String getName() {
		return "Material paint";
	}

	@Override
	protected Image toolIconImage() {
		return new Image((getClass().getClassLoader().getResourceAsStream("icons/tools/paint_material.png")));
	}

	@Override
	public Pane getToolSettingsPane() {
		return settings;
	}

	@Override
	public boolean onPrimaryPressed(double x, double z, double pressure, LayerWrapperMaterialmap layer) {
		primaryActive = true;
		apply(x, z, settings.getBrushSize() / 2.0, settings.getMaterial(), layer);
		return true;
	}

	@Override
	public boolean onSecondaryPressed(double x, double z, double pressure, LayerWrapperMaterialmap layer) {
		secondaryActive = true;
		return false;
	}

	@Override
	public boolean onPrimaryDown(double x, double z, double pressure, LayerWrapperMaterialmap layer) {
		apply(x, z, settings.getBrushSize() / 2.0, settings.getMaterial(), layer);
		return true;
	}

	@Override
	public boolean onSecondaryDown(double x, double z, double pressure, LayerWrapperMaterialmap layer) {
		return false;
	}

	@Override
	public boolean onPrimaryDragged(double x, double z, double pressure, LayerWrapperMaterialmap layer) {
		return false;
	}

	@Override
	public boolean onSecondaryDragged(double x, double z, double pressure, LayerWrapperMaterialmap layer) {
		return false;
	}

	@Override
	public boolean onPrimaryReleased(double x, double z, double pressure, LayerWrapperMaterialmap layer) {
		primaryActive = false;
		return false;
	}

	@Override
	public boolean onSecondaryReleased(double x, double z, double pressure, LayerWrapperMaterialmap layer) {
		secondaryActive = false;
		return false;
	}

	@Override
	public boolean onScroll(double deltaX, double deltaY, LayerWrapperMaterialmap layer) {
		settings.setBrushSize(MathHelperScalar.clamp(settings.getBrushSize() - deltaY / 20.0, 0.0, 2.0));
		settings.setAngle(settings.getAngle() + deltaX * 5.0);
		return true;
	}

	private boolean apply(double x, double z, double brushRadius, MaterialState<?, ?> material, LayerWrapperMaterialmap layer) {
		Heightbrush brush = settings.getBrush();
		if (brush == null) {
			return false;
		}

		double brushThreshold = settings.getBrushThreshold();
		double scale = settings.getBrushSize();
		double rotationDeg = settings.getAngle();
		if (settings.getRandomizeRotation()) {
			rotationDeg = rotationHashPermutation.gUnitHash(0, (int) x, (int) z)*360.0;
		}
		// Convert rotation angle from deg to rad
		double rotationRad = Math.toRadians(rotationDeg);
		// Create the transform matrix for mapping points in the scatter heightmap to points in the feature heightmap
		double[][] inverse_transform = {
				{Math.cos(-rotationRad)/scale, -Math.sin(-rotationRad)/scale, (-x*Math.cos(-rotationRad)+z*Math.sin(-rotationRad))/scale + (double)brush.getBrushWidth()/2.0},
				{Math.sin(-rotationRad)/scale,  Math.cos(-rotationRad)/scale, (-x*Math.sin(-rotationRad)-z*Math.cos(-rotationRad))/scale + (double)brush.getBrushHeight()/2.0},
				{0                        , 0                         , 1          }};

		int[] bounds = bounds(x, z, brush.getBrushWidth(), brush.getBrushHeight(), scale, rotationRad);
		int minX = bounds[0];
		int maxX = bounds[1];
		int minZ = bounds[2];
		int maxZ = bounds[3];

		for (int u = minX; u <= maxX; u++) {
			for (int v = minZ; v <= maxZ; v++) {
				if (!settings.getLayerMasks().maskedAt(u, v)) continue;

				double[] sample = applyTransform(u, v, inverse_transform);
				if (!brush.isLocalContained((int) sample[0], (int) sample[1])) continue;

				if (brush.getLocalLerpHeight(sample[0], sample[1]) >= brushThreshold) {
					layer.applyValueAt(u, v, material);
				}
			}
		}

		return true;
	}

	// Returns {minX, maxX, minZ, maxZ}
	private int[] bounds(double x, double z, double w, double l, double scale, double rotate) {
		// Create the transform matrix for mapping points in the feature heightmap to points in the scatter heightmap
		double[][] transform = {
				{scale*Math.cos(rotate), -scale*Math.sin(rotate), (-w*scale*Math.cos(rotate)+l*scale*Math.sin(rotate))/2+x},
				{scale*Math.sin(rotate),  scale*Math.cos(rotate), (-w*scale*Math.sin(rotate)-l*scale*Math.cos(rotate))/2+z},
				{0                     , 0                      , 1                                                       }};

		double[] c0 = applyTransform(0, 0, transform);
		double[] c1 = applyTransform(w, 0, transform);
		double[] c2 = applyTransform(w, l, transform);
		double[] c3 = applyTransform(0, l, transform);

		int minX = (int) Math.floor(min(c0[0], c1[0], c2[0], c3[0]));
		int maxX = (int) Math.floor(max(c0[0], c1[0], c2[0], c3[0]));
		int minZ = (int) Math.floor(min(c0[1], c1[1], c2[1], c3[1]));
		int maxZ = (int) Math.floor(max(c0[1], c1[1], c2[1], c3[1]));

		return new int[] {minX, maxX, minZ, maxZ};
	}

	private double[] applyTransform(double x, double z, double[][] transform) {
		return new double[] {
				x*transform[0][0] + z*transform[0][1] + transform[0][2],
				x*transform[1][0] + z*transform[1][1] + transform[1][2]
		};
	}

	private double min(double...values) {
		double min = values[0];

		for (double val: values) {
			min = Math.min(min, val);
		}

		return min;
	}

	private double max(double...values) {
		double max = values[0];

		for (double val: values) {
			max = Math.max(max, val);
		}

		return max;
	}

	@Override
	public void renderTool(double x, double z, LayerWrapperMaterialmap layer, GraphicsContext g, NavigationalCanvas navCanvas) {
		if (primaryActive || secondaryActive || settings.getBrush() == null) return;
		Pixel screenPixelCoordinate = new Pixel(new Coordinate(x, z), navCanvas);
		double brushRotationDeg = settings.getAngle();
		if (settings.getRandomizeRotation()) {
			brushRotationDeg = rotationHashPermutation.gUnitHash(0, (int) x, (int) z)*360.0;
		}
		drawBrush(screenPixelCoordinate.x, screenPixelCoordinate.y, settings.getBrushSize(), brushRotationDeg, g, navCanvas);
	}

	private void drawBrush(double x, double z, double size, double angle, GraphicsContext g, NavigationalCanvas navCanvas) {
		Affine a = g.getTransform();
		g.rotate(angle);

		Vector2d v = new Vector2d(x, z);
		v = Vector2d.rotateDeg(v, -angle);

		Heightbrush brush = settings.getBrush();
		Image brushImage = settings.getBrushPreview();
		g.setStroke(Color.WHEAT);
		g.setLineWidth(1.0);
		g.drawImage(
				brushImage,
				v.getX() - brush.getBrushWidth()*navCanvas.getZoom()*size/2.0,
				v.getY() - brush.getBrushHeight()*navCanvas.getZoom()*size/2.0,
				brush.getBrushWidth()*navCanvas.getZoom()*size,
				brush.getBrushHeight()*navCanvas.getZoom()*size);
		g.strokeRect(
				v.getX() - brush.getBrushWidth()*navCanvas.getZoom()*size/2.0,
				v.getY() - brush.getBrushHeight()*navCanvas.getZoom()*size/2.0,
				brush.getBrushWidth()*navCanvas.getZoom()*size,
				brush.getBrushHeight()*navCanvas.getZoom()*size);

		g.setTransform(a);
	}
}
