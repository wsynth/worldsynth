/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.composer.tool.featuremap;

import java.util.List;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.StrokeLineCap;
import net.worldsynth.composer.layer.featuremap.LayerWrapperFeaturemap;
import net.worldsynth.composer.ui.navcanvas.Coordinate;
import net.worldsynth.composer.ui.navcanvas.NavigationalCanvas;
import net.worldsynth.composer.ui.navcanvas.Pixel;
import net.worldsynth.extent.Extent;
import net.worldsynth.featurepoint.Featurepoint2D;
import net.worldsynth.util.math.MathHelperScalar;

public class ToolFeaturemapMovePoint extends ToolFeaturemap {
	
	private Featurepoint2D movingPoint;
	
	@Override
	public String getName() {
		return "Move point";
	}
	
	@Override
	protected Image toolIconImage() {
		return new Image((getClass().getClassLoader().getResourceAsStream("icons/tools/Point move tool.png")));
	}

	@Override
	public Pane getToolSettingsPane() {
		return new Pane();
	}

	@Override
	public boolean onPrimaryPressed(double x, double z, double pressure, LayerWrapperFeaturemap layer) {
		movingPoint = getClosest(x, z, 5, layer);
		if (movingPoint == null) return false;
		
		layer.removeFeature(movingPoint);
		return true;
	}

	@Override
	public boolean onSecondaryPressed(double x, double z, double pressure, LayerWrapperFeaturemap layer) {
		return false;
	}

	@Override
	public boolean onPrimaryDown(double x, double z, double pressure, LayerWrapperFeaturemap layer) {
		return false;
	}

	@Override
	public boolean onSecondaryDown(double x, double z, double pressure, LayerWrapperFeaturemap layer) {
		return false;
	}

	@Override
	public boolean onPrimaryDragged(double x, double z, double pressure, LayerWrapperFeaturemap layer) {
		return movingPoint != null;
	}

	@Override
	public boolean onSecondaryDragged(double x, double z, double pressure, LayerWrapperFeaturemap layer) {
		return false;
	}

	@Override
	public boolean onPrimaryReleased(double x, double z, double pressure, LayerWrapperFeaturemap layer) {
		if (movingPoint == null) return false;
		
		apply(x, z, layer);
		movingPoint = null;
		return true;
	}

	@Override
	public boolean onSecondaryReleased(double x, double z, double pressure, LayerWrapperFeaturemap layer) {
		return false;
	}
	
	@Override
	public boolean onScroll(double deltaX, double deltaY, LayerWrapperFeaturemap layer) {
		return false;
	}
	
	private void apply(double x, double z, LayerWrapperFeaturemap layer) {
		layer.addFeature(new Featurepoint2D(x, z, movingPoint.getSeed()));
	}
	
	private Featurepoint2D getClosest(double x, double z, double maxDist, LayerWrapperFeaturemap layer) {
		Extent extent = new Extent(x-maxDist, 0, z-maxDist, maxDist*2, 0, maxDist*2);
		
		Featurepoint2D closest = null;
		double dist = 0;
		
		List<Featurepoint2D> fs = layer.getFeatures(extent);
		for (Featurepoint2D f: fs) {
			if ((closest == null && dist(x, z, f) <= maxDist) || dist(x, z, f) < dist) {
				closest = f;
				dist = dist(x, z, f);
			}
		}
		
		return closest;
	}
	
	private double dist(double x, double z, Featurepoint2D f) {
		return Math.sqrt((x - f.getX()) * (x - f.getX()) + (z - f.getZ()) * (z - f.getZ()));
	}

	@Override
	public void renderTool(double x, double z, LayerWrapperFeaturemap layer, GraphicsContext g, NavigationalCanvas navCanvas) {
		if (movingPoint == null) {
			Featurepoint2D f = getClosest(x, z, 5, layer);
			if (f == null) return;
			renderPoint(f.getX(), f.getZ(), g, navCanvas);
		}
		else {
			renderPoint(x, z, g, navCanvas);
		}
	}
	
	private void renderPoint(double x, double z, GraphicsContext g, NavigationalCanvas navCanvas) {
		g.setStroke(Color.WHITE);
		g.setLineWidth(MathHelperScalar.clamp(1.0 * navCanvas.getZoom(), 2.0, 4.0));
		g.setLineCap(StrokeLineCap.ROUND);
		
		Pixel pc  = new Pixel(new Coordinate(x, z), navCanvas);
		g.strokeOval(pc.x-5*navCanvas.getZoom(), pc.y-5*navCanvas.getZoom(), 10*navCanvas.getZoom(), 10*navCanvas.getZoom());
		
		g.strokeLine(pc.x, pc.y-10*navCanvas.getZoom(), pc.x, pc.y+10*navCanvas.getZoom());
		g.strokeLine(pc.x, pc.y+10*navCanvas.getZoom(), pc.x-2*navCanvas.getZoom(), pc.y+8*navCanvas.getZoom());
		g.strokeLine(pc.x, pc.y+10*navCanvas.getZoom(), pc.x+2*navCanvas.getZoom(), pc.y+8*navCanvas.getZoom());
		g.strokeLine(pc.x, pc.y-10*navCanvas.getZoom(), pc.x-2*navCanvas.getZoom(), pc.y-8*navCanvas.getZoom());
		g.strokeLine(pc.x, pc.y-10*navCanvas.getZoom(), pc.x+2*navCanvas.getZoom(), pc.y-8*navCanvas.getZoom());
		
		g.strokeLine(pc.x-10*navCanvas.getZoom(), pc.y, pc.x+10*navCanvas.getZoom(), pc.y);
		g.strokeLine(pc.x+10*navCanvas.getZoom(), pc.y, pc.x+8*navCanvas.getZoom(), pc.y-2*navCanvas.getZoom());
		g.strokeLine(pc.x+10*navCanvas.getZoom(), pc.y, pc.x+8*navCanvas.getZoom(), pc.y+2*navCanvas.getZoom());
		g.strokeLine(pc.x-10*navCanvas.getZoom(), pc.y, pc.x-8*navCanvas.getZoom(), pc.y-2*navCanvas.getZoom());
		g.strokeLine(pc.x-10*navCanvas.getZoom(), pc.y, pc.x-8*navCanvas.getZoom(), pc.y+2*navCanvas.getZoom());
	}
}
