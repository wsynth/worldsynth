/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.composer.tool.masking;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import net.worldsynth.composer.layer.LayerWrapper;

public abstract class LayerMask<T extends LayerWrapper<?>> extends BorderPane {

	protected final T layer;

	private final CheckBox maskEnableCheckBox;
	private final Button xButton;

	public LayerMask(T layer) {
		getStyleClass().add("layer-mask-panel");
		this.layer = layer;

		maskEnableCheckBox = new CheckBox();
		maskEnableCheckBox.setSelected(true);

		ImageView layerThumbnail = new ImageView();
		Label layerLabel = new Label(layer.getLayerName(), layerThumbnail);
		layerLabel.textProperty().bind(layer.layerNameProperty());
		layerThumbnail.imageProperty().bind(layer.layerTumbnailProperty());

		Region spacerRegion = new Region();
		HBox.setHgrow(spacerRegion, Priority.ALWAYS);

		xButton = new Button("X");
		xButton.getStyleClass().add("layer-mask-remove-button");

		HBox topBox = new HBox(maskEnableCheckBox, layerLabel, spacerRegion, xButton);
		topBox.setAlignment(Pos.CENTER);
		topBox.setSpacing(5.0);
		setTop(topBox);
	}

	public final T getLayer() {
		return layer;
	}

	public final Pane getSettingsPane() {
		return this;
	}

	public final boolean isMaskEnabled() {
		return maskEnableCheckBox.isSelected();
	}

	public final void setOnRemove(EventHandler<ActionEvent> onRemove) {
		xButton.setOnAction(onRemove);
	}

	public abstract boolean maskAt(int x, int z);
}
