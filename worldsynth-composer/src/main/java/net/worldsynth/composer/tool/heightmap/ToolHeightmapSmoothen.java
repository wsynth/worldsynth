/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.composer.tool.heightmap;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.transform.Affine;
import net.worldsynth.composer.brush.heightmap.Heightbrush;
import net.worldsynth.composer.layer.heightmap.LayerWrapperHeightmap;
import net.worldsynth.composer.ui.navcanvas.Coordinate;
import net.worldsynth.composer.ui.navcanvas.NavigationalCanvas;
import net.worldsynth.composer.ui.navcanvas.Pixel;
import net.worldsynth.util.gen.Permutation;
import net.worldsynth.util.math.MathHelperScalar;
import net.worldsynth.util.math.Vector2d;

public class ToolHeightmapSmoothen extends ToolHeightmap {
	
	private final ToolHeightmapSmoothenSettings settings = new ToolHeightmapSmoothenSettings();
	
	private final Permutation rotationHashPermuation = new Permutation(256, 1);
	
	private boolean active = false;
	
	@Override
	public String getName() {
		return "Smoothen";
	}
	
	@Override
	protected Image toolIconImage() {
		return new Image((getClass().getClassLoader().getResourceAsStream("icons/tools/smoothen.png")));
	}
	
	@Override
	public Pane getToolSettingsPane() {
		return settings;
	}
	
	@Override
	public boolean onPrimaryPressed(double x, double z, double pressure, LayerWrapperHeightmap layer) {
		active = true;
		return smoothen(x, z, pressure, layer);
	}
	
	@Override
	public boolean onSecondaryPressed(double x, double z, double pressure, LayerWrapperHeightmap layer) {
		return false;
	}
	
	@Override
	public boolean onPrimaryDown(double x, double z, double pressure, LayerWrapperHeightmap layer) {
		return smoothen(x, z, pressure, layer);
	}
	
	@Override
	public boolean onSecondaryDown(double x, double z, double pressure, LayerWrapperHeightmap layer) {
		return false;
	}

	@Override
	public boolean onPrimaryDragged(double x, double z, double pressure, LayerWrapperHeightmap layer) {
		return smoothen(x, z, pressure, layer);
	}
	
	@Override
	public boolean onSecondaryDragged(double x, double z, double pressure, LayerWrapperHeightmap layer) {
		return false;
	}

	@Override
	public boolean onPrimaryReleased(double x, double z, double pressure, LayerWrapperHeightmap layer) {
		active = false;
		return false;
	}
	
	@Override
	public boolean onSecondaryReleased(double x, double z, double pressure, LayerWrapperHeightmap layer) {
		return false;
	}
	
	@Override
	public boolean onScroll(double deltaX, double deltaY, LayerWrapperHeightmap layer) {
		if (settings.getBrush() != null) {
			settings.setSize(MathHelperScalar.clamp(settings.getSize() - deltaY / 20.0, 0.0, 2.0));
			settings.setAngle(settings.getAngle() + deltaX * 5.0);
		}
		return true;
	}

	private boolean smoothen(double x, double z, double pressure, LayerWrapperHeightmap layer) {
		Heightbrush brush = settings.getBrush();
		if (brush == null) {
			return false;
		}
		
		double scale = settings.getSize();
		double rotationDeg = settings.getAngle();
		if (settings.getRandomizeRotatation()) {
			rotationDeg = rotationHashPermuation.gUnitHash(0, (int) x, (int) z)*360.0;
		}
		float strength = (float) (settings.getStrength() * pressure);
		
		// Convert rotation angle from deg to rad
		double rotationRad = Math.toRadians(rotationDeg);
		// Create the transform matrix for mapping points in the scatter heightmap to points in the feature heightmap
		double[][] inverse_transform = {
				{Math.cos(-rotationRad)/scale, -Math.sin(-rotationRad)/scale, (-x*Math.cos(-rotationRad)+z*Math.sin(-rotationRad))/scale + (double)brush.getBrushWidth()/2.0},
				{Math.sin(-rotationRad)/scale,  Math.cos(-rotationRad)/scale, (-x*Math.sin(-rotationRad)-z*Math.cos(-rotationRad))/scale + (double)brush.getBrushHeight()/2.0},
				{0                        , 0                         , 1          }};
		
		int[] bounds = bounds(x, z, brush.getBrushWidth(), brush.getBrushHeight(), scale, rotationRad);
		
		int minX = bounds[0];
		int maxX = bounds[1];
		int boundWidth = maxX-minX;
		int minZ = bounds[2];
		int maxZ = bounds[3];
		int boundHeight = maxZ-minZ;
		
		float[][] currentHeights = new float[boundWidth+2][boundHeight+2];
		for (int u = 0; u < boundWidth+2; u++) {
			for (int v = 0; v < boundHeight+2; v++) {
				currentHeights[u][v] = layer.getValueAt(minX + u - 1, minZ + v - 1);
			}
		}
		
		for (int u = 0; u < boundWidth; u++) {
			for (int v = 0; v < boundHeight; v++) {
				double[] sample = applyTransform(minX + u, minZ + v, inverse_transform);
				if (!brush.isLocalContained((int) sample[0], (int) sample[1])) continue;
				float brushValue = brush.getLocalLerpHeight(sample[0], sample[1]);
				
				float avg = (currentHeights[u+1][v+1] + currentHeights[u][v+1] + currentHeights[u+2][v+1] + currentHeights[u+1][v] + currentHeights[u+1][v+2]) / 5;
				float lerpFactor = brushValue * strength;
				float newHeight = avg * lerpFactor + currentHeights[u+1][v+1] * (1.0f - lerpFactor);
				layer.applyValueAt(minX + u, minZ + v, newHeight);
			}
		}
		
		return true;
	}
	
	// Returns {minX, maxX, minZ, maxZ}
	private int[] bounds(double x, double z, double w, double l, double scale, double rotate) {
		// Create the transform matrix for mapping points in the feature heightmap to points in the scatter heightmap
		double[][] transform = {
				{scale*Math.cos(rotate), -scale*Math.sin(rotate), (-w*scale*Math.cos(rotate)+l*scale*Math.sin(rotate))/2+x},
				{scale*Math.sin(rotate),  scale*Math.cos(rotate), (-w*scale*Math.sin(rotate)-l*scale*Math.cos(rotate))/2+z},
				{0                     , 0                      , 1                                                       }};
		
		double[] c0 = applyTransform(0, 0, transform);
		double[] c1 = applyTransform(w, 0, transform);
		double[] c2 = applyTransform(w, l, transform);
		double[] c3 = applyTransform(0, l, transform);
		
		int minX = (int) Math.floor(min(c0[0], c1[0], c2[0], c3[0]));
		int maxX = (int) Math.floor(max(c0[0], c1[0], c2[0], c3[0]));
		int minZ = (int) Math.floor(min(c0[1], c1[1], c2[1], c3[1]));
		int maxZ = (int) Math.floor(max(c0[1], c1[1], c2[1], c3[1]));
		
		return new int[] {minX, maxX, minZ, maxZ};
	}
	
	private double[] applyTransform(double x, double z, double[][] transform) {
		return new double[] {
			x*transform[0][0] + z*transform[0][1] + transform[0][2],
			x*transform[1][0] + z*transform[1][1] + transform[1][2]
		};
	}
	
	private double min(double...values) {
		double min = values[0];
		
		for (double val: values) {
			min = Math.min(min, val);
		}
		
		return min;
	}
	
	private double max(double...values) {
		double max = values[0];
		
		for (double val: values) {
			max = Math.max(max, val);
		}
		
		return max;
	}
	
	@Override
	public void renderTool(double x, double z, LayerWrapperHeightmap layer, GraphicsContext g, NavigationalCanvas navCanvas) {
		if (active || settings.getBrush() == null) return;
		Pixel screenPixelCoordinate = new Pixel(new Coordinate(x, z), navCanvas);
		double brushRotationDeg = settings.getAngle();
		if (settings.getRandomizeRotatation()) {
			brushRotationDeg = rotationHashPermuation.gUnitHash(0, (int) x, (int) z)*360.0;
		}
		drawRotatedBrush(screenPixelCoordinate.x, screenPixelCoordinate.y, settings.getSize(), brushRotationDeg, g, navCanvas);
	}
	
	private void drawRotatedBrush(double x, double z, double size, double angle, GraphicsContext g, NavigationalCanvas navCanvas) {
		Affine a = g.getTransform();
		g.rotate(angle);
		
		Vector2d v = new Vector2d(x, z);
		v = Vector2d.rotateDeg(v, -angle);
		
		Heightbrush brush = settings.getBrush();
		g.setStroke(Color.WHEAT);
		g.setLineWidth(1.0);
		g.drawImage(
				brush.getBrushImage(),
				v.getX() - brush.getBrushWidth()*navCanvas.getZoom()*size/2.0,
				v.getY() - brush.getBrushHeight()*navCanvas.getZoom()*size/2.0,
				brush.getBrushWidth()*navCanvas.getZoom()*size,
				brush.getBrushHeight()*navCanvas.getZoom()*size);
		g.strokeRect(
				v.getX() - brush.getBrushWidth()*navCanvas.getZoom()*size/2.0,
				v.getY() - brush.getBrushHeight()*navCanvas.getZoom()*size/2.0,
				brush.getBrushWidth()*navCanvas.getZoom()*size,
				brush.getBrushHeight()*navCanvas.getZoom()*size);
		
		g.setTransform(a);
	}
}
